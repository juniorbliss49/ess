import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { TenantProvider } from "./context/TenantContext";
import { AuthProvider } from "./context/AuthContext";
import { ProfileProvider } from "./context/ProfileContext";
import { PayslipProvider } from "./context/PayslipContext";
import { UtilityProvider } from "./context/UtilityContext";
import { PaymentRequestProvider } from "./context/PaymentRequestContext";
import { LoanApplicationProvider } from "./context/LoanApplicationContext";
import { LeaveApplicationProvider } from "./context/LeaveApplicationContext";
import { HomeProvider } from "./context/HomeContext";

ReactDOM.render(
  <TenantProvider>
    <AuthProvider>
      <LeaveApplicationProvider>
        <LoanApplicationProvider>
          <PaymentRequestProvider>
            <UtilityProvider>
              <ProfileProvider>
                <PayslipProvider>
                  <HomeProvider>
                    <App />
                  </HomeProvider>
                </PayslipProvider>
              </ProfileProvider>
            </UtilityProvider>
          </PaymentRequestProvider>
        </LoanApplicationProvider>
      </LeaveApplicationProvider>
    </AuthProvider>
  </TenantProvider>,
  document.getElementById("root")
);
