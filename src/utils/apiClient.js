import request from "superagent";
// import { API_ROOT } from "../CONSTANTS";

const API_ROOT = process.env.REACT_APP_API_ROOT;

const responseBody = (res) => res.body;

const requests = {
  del: (url) =>
    request
      .del(`${API_ROOT}${url}`)
      .set("Access-Control-Allow-Origin", "*")
      // .use(tokenPlugin)
      // .end(handleErrors)
      .then(responseBody),

  get: (url) =>
    request
      .get(`${API_ROOT}${url}`)
      .timeout({
        response: 60000, // Wait 10 seconds for the server to start sending,
        deadline: 60000, // but allow 30 secs for the response to complete.
      })
      .set("Accept", "application/json")
      .set("Access-Control-Allow-Origin", "*")
      // .use(tokenPlugin)
      // .end(handleErrors)
      .then(responseBody),

  put: (url, body) =>
    request
      .put(`${API_ROOT}${url}`, body)
      .timeout({
        response: 60000, // Wait 10 seconds for the server to start sending,
        deadline: 60000, // but allow 30 secs for the response to complete.
      })
      .set("Access-Control-Allow-Origin", "*")
      // .use(tokenPlugin)
      // .end(handleErrors)
      .then(responseBody),

  post: (url, body) =>
    request
      .post(`${API_ROOT}${url}`, body)
      // .timeout({
      //   response: 60000, // Wait 10 seconds for the server to start sending,
      //   deadline: 60000, // but allow 30 secs for the response to complete.
      // })
      .set("Accept", "application/json")
      .set("Access-Control-Allow-Origin", "*")
      // .use(tokenPlugin)
      // .end(handleErrors)
      .then(responseBody),
};

const login = (credentials) => requests.post("/auth/login", credentials);

// const logout = () => requests.get("/auth/logout");

const logout = (token) =>
  request
    .get(API_ROOT + "/auth/logout")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const register = (data) => requests.post("/human-resource/auth/register", data);

// const register = data =>
//   request
//     .post(API_ROOT + "/human-resource/auth/register", data)
//     .then(responseBody);

const resetPassword = (credentials) =>
  requests.post("/auth/reset", credentials);

// const resetPassword = credentials =>
//   request.post(API_ROOT + "/auth/reset", credentials).then(responseBody);

const getUser = (token) =>
  request
    .get(API_ROOT + "/auth/me")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getPayslip = (period, token) =>
  request
    .get(API_ROOT + "/human-resource/payslip/" + period)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    // .use(authPlugin)
    .then(responseBody);

const getPayslipAsPdf = (period, code, token) =>
  request
    .get(API_ROOT + `/human-resource/payslip-pdf/${period}/${code}`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getProfile = (token) =>
  request
    .get(API_ROOT + "/human-resource/employee")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getProfileRefresh = (token) =>
  request
    .get(API_ROOT + "/human-resource/employee/refresh")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getTenant = (code) =>
  requests.post(`/human-resource/company`, { companyCode: code });

const getAllPaymentRequest = (token) =>
  request
    .get(API_ROOT + "/admin-tasks/payment-request")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getCreatePaymentRequest = (token) =>
  request
    .get(API_ROOT + "/admin-tasks/payment-request/new")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

//Creation of Payment Request
const createPaymentRequest = (data, token) =>
  request
    .post(API_ROOT + "/admin-tasks/payment-request/new", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const initPaymentApplicationFieldsForUpdate = (id, token) =>
  request
    .get(API_ROOT + "/admin-tasks/payment-request/edit/" + id)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const refreshPaymentRequest = (docNo, token) =>
  request
    .get(API_ROOT + "/admin-tasks/payment-request/sync/" + docNo)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const refreshAllPaymentRequests = (token) =>
  request
    .get(API_ROOT + "/admin-tasks/payment-request/sync")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const requestApprovalForPaymentRequest = (reqId, token) =>
  request
    .get(API_ROOT + "/admin-tasks/payment-request/request-approval/" + reqId)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const cancelApprovalRequestForPaymentRequest = (reqId, token) =>
  request
    .get(
      API_ROOT + "/admin-tasks/payment-request/cancel-approval-request/" + reqId
    )
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);
// loan Application
const createLoanApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/loan-application/new", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const updateLoanApplication = (id, data, token) =>
  request
    .patch(API_ROOT + "/human-resource/loan-application/" + id, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const getAllLoanApplications = (token) =>
  request
    .get(API_ROOT + "/human-resource/loan-application")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const initLoanApplicationFields = (token) =>
  request
    .get(API_ROOT + "/human-resource/loan-application/new")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const initLoanApplicationFieldsForUpdate = (id, token) =>
  request
    .get(API_ROOT + "/human-resource/loan-application/edit/" + id)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const syncLoanApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/loan-application/sync-card", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const syncLoanRequests = (token) =>
  request
    .get(API_ROOT + "/human-resource/loan-application/sync")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const requestApprovalForLoanApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/loan-application/request-approval", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const cancelApprovalRequestForLoanApplication = (data, token) =>
  request
    .post(
      API_ROOT + "/human-resource/loan-application/cancel-approval-request",
      data
    )
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

//leave Application
const createLeaveApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/leave-application/new", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const updateLeaveApplication = (id, data, token) =>
  request
    .patch(API_ROOT + `/human-resource/leave-application/${id}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const getCreateLeaveApplication = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application/new")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getSettingsForNewAnnualLeaveApplication = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application/new?is_annual=true")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getSettingsForNewNonAnnualLeaveApplication = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application/new")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const initLeaveApplicationFieldsForUpdate = (id, token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application/edit/" + id)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const initAnnualLeaveApplicationFieldsForUpdate = (id, token) =>
  request
    .get(
      API_ROOT +
        "/human-resource/leave-application/edit/" +
        id +
        "?is_annual=true"
    )
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getleaveApplications = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getArchivedLeaveApplications = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application/archived")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getAnnualLeaveApplications = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application?is_annual=true")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getNonAnnualLeaveApplications = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getLeaveReliefRequests = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application-relief")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const postLeaveReliefResponse = (id, data, token) =>
  request
    .patch(API_ROOT + `/human-resource/leave-application-relief/${id}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const requestApprovalForLeaveApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/leave-application/request-approval", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const cancelApprovalRequestForLeaveApplication = (data, token) =>
  request
    .post(
      API_ROOT + "/human-resource/leave-application/cancel-approval-request",
      data
    )
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const syncLeaveApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/leave-application/sync-card", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const syncLeaveRequests = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-application/sync")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getLeaveSchedule = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-schedule")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getEditLeaveSchedule = (id, token) =>
  request
    .get(API_ROOT + `/human-resource/leave-schedule/edit/${id}`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const createLeaveSchudule = (id, data, token) =>
  request
    .patch(API_ROOT + `/human-resource/leave-schedule/${id}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const requestApprovalForLeaveSchedule = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/leave-schedule/request-approval", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const cancelApprovalRequestForLeaveSchedule = (data, token) =>
  request
    .post(
      API_ROOT + "/human-resource/leave-schedule/cancel-approval-request",
      data
    )
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const editPaymentRequest = (id, data, token) =>
  request
    .patch(API_ROOT + `/admin-tasks/payment-request/${id}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);
/* 
  Employee Queries
  ================
*/

// Issued queries
const fetchIssuedQueries = (token) =>
  request
    .get(API_ROOT + "/human-resource/issued-queries")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const fetchNewIssuedQueryFieldSettings = (token) =>
  request
    .get(API_ROOT + "/human-resource/issued-queries/create")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const fetchExistingIssuedQueryFieldSettings = (queryId, token) =>
  request
    .get(API_ROOT + `/human-resource/issued-queries/${queryId}/edit`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const createIssuedQuery = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/issued-queries", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const updateIssuedQuery = (queryId, data, token) =>
  request
    .patch(API_ROOT + `/human-resource/issued-queries/${queryId}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const refreshIssuedQuery = (queryId, token) =>
  request
    .get(API_ROOT + `/human-resource/issued-queries/${queryId}/refresh`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getIssuedQueriesFromErp = (token) =>
  request
    .get(API_ROOT + `/human-resource/issued-queries/sync-all`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const sendIssuedQuery = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/issued-queries/send-query", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);
// ~~ Issued Queries

// Query Responses
const fetchQueryResponses = (token) =>
  request
    .get(API_ROOT + "/human-resource/query-responses")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const fetchExistingQueryResponsesFieldSettings = (queryId, token) =>
  request
    .get(API_ROOT + `/human-resource/query-responses/${queryId}/edit`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const updateQueryResponse = (queryId, data, token) =>
  request
    .patch(API_ROOT + `/human-resource/query-responses/${queryId}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const refreshQueryResponse = (queryId, token) =>
  request
    .get(API_ROOT + `/human-resource/query-responses/${queryId}/refresh`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getQueryResponsesFromErp = (token) =>
  request
    .get(API_ROOT + `/human-resource/query-responses/sync-all`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const submitQueryResponse = (data, token) =>
  request
    .post(
      API_ROOT + "/human-resource/query-responses/submit-query-response",
      data
    )
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);
// ~~ Query Responses

// Processed Queries
const fetchProcessedQueries = (token) =>
  request
    .get(API_ROOT + "/human-resource/processed-queries")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const fetchExistingProcessedQueryFieldSettings = (queryId, token) =>
  request
    .get(API_ROOT + `/human-resource/processed-queries/${queryId}/edit`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const updateProcessedQuery = (queryId, data, token) =>
  request
    .patch(API_ROOT + `/human-resource/processed-queries/${queryId}`, data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const refreshProcessedQuery = (queryId, token) =>
  request
    .get(API_ROOT + `/human-resource/processed-queries/${queryId}/refresh`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getProcessedQueryFromErp = (token) =>
  request
    .get(API_ROOT + `/human-resource/processed-queries/sync-all`)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const forwardQueryToHr = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/processed-queries/forward", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);
// ~~ Processed Queries

// get profile datas
const getProfileData = (token) =>
  request
    .get(API_ROOT + "/human-resource/employee/edit")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const updatePassword = (data, token) =>
  request
    .post(API_ROOT + "/auth/secure", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

//update employee
const updateEmployeeApplication = (data, token) =>
  request
    .patch(API_ROOT + "/human-resource/employee", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);
// Annual Leave Applications

const createAnnualLeaveApplication = (data, token) =>
  request
    .post(API_ROOT + "/human-resource/annual-leave-applications", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);

const getLeaveBalance = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-balances")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const leaveScheduleSync = (token) =>
  request
    .get(API_ROOT + "/human-resource/leave-schedule/sync")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const recallAnnualLeaveApplication = (data, token) =>
  request
    .del(API_ROOT + "/human-resource/annual-leave-applications", data)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .set("Accept", "application/json")
    .set("Access-Control-Allow-Origin", "*")
    .then(responseBody);
// End Annual Leave Appllications

const leaveBalanceSync = (year, token) =>
  request
    .get(API_ROOT + "/human-resource/leave-balances/refresh?year=" + year)
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getDashboardPaymentRequest = (token) =>
  request
    .get(API_ROOT + "/dashboard/payment-request")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getDashboardLoan = (token) =>
  request
    .get(API_ROOT + "/dashboard/loan")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getDashboardAnnualLeave = (token) =>
  request
    .get(API_ROOT + "/dashboard/annual-leave")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getDashboardEmployeeQuery = (token) =>
  request
    .get(API_ROOT + "/dashboard/employee-query")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const getProfilePix = (token) =>
  request
    .get(API_ROOT + "/human-resource/employee/picture")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

const refreshProfilePix = (token) =>
  request
    .get(API_ROOT + "/human-resource/employee/refresh-picture")
    .timeout({
      response: 60000, // Wait 10 seconds for the server to start sending,
      deadline: 60000, // but allow 30 secs for the response to complete.
    })
    .set("Authorization", `Bearer ${token}`)
    .then(responseBody);

export {
  login,
  logout,
  getUser,
  register,
  resetPassword,
  getPayslip,
  getPayslipAsPdf,
  getProfile,
  getTenant,
  getAllPaymentRequest,
  getCreatePaymentRequest,
  createPaymentRequest,
  getAllLoanApplications,
  createLoanApplication,
  initLoanApplicationFields,
  getleaveApplications,
  createLeaveApplication,
  getCreateLeaveApplication,
  updateLeaveApplication,
  getSettingsForNewAnnualLeaveApplication,
  getAnnualLeaveApplications,
  getArchivedLeaveApplications,
  getNonAnnualLeaveApplications,
  getSettingsForNewNonAnnualLeaveApplication,
  getLeaveReliefRequests,
  postLeaveReliefResponse,
  requestApprovalForLeaveApplication,
  syncLeaveApplication,
  initLoanApplicationFieldsForUpdate,
  updateLoanApplication,
  syncLoanApplication,
  requestApprovalForLoanApplication,
  cancelApprovalRequestForLoanApplication,
  initLeaveApplicationFieldsForUpdate,
  initAnnualLeaveApplicationFieldsForUpdate,
  cancelApprovalRequestForLeaveApplication,
  getLeaveSchedule,
  getEditLeaveSchedule,
  createLeaveSchudule,
  initPaymentApplicationFieldsForUpdate,
  editPaymentRequest,
  fetchIssuedQueries,
  fetchQueryResponses,
  fetchProcessedQueries,
  fetchNewIssuedQueryFieldSettings,
  createIssuedQuery,
  fetchExistingIssuedQueryFieldSettings,
  updateIssuedQuery,
  fetchExistingQueryResponsesFieldSettings,
  updateQueryResponse,
  refreshIssuedQuery,
  getIssuedQueriesFromErp,
  refreshQueryResponse,
  getQueryResponsesFromErp,
  fetchExistingProcessedQueryFieldSettings,
  updateProcessedQuery,
  refreshProcessedQuery,
  getProcessedQueryFromErp,
  syncLeaveRequests,
  syncLoanRequests,
  refreshPaymentRequest,
  refreshAllPaymentRequests,
  requestApprovalForPaymentRequest,
  cancelApprovalRequestForPaymentRequest,
  sendIssuedQuery,
  submitQueryResponse,
  forwardQueryToHr,
  getProfileData,
  getProfileRefresh,
  updateEmployeeApplication,
  getLeaveBalance,
  leaveScheduleSync,
  createAnnualLeaveApplication,
  recallAnnualLeaveApplication,
  leaveBalanceSync,
  getDashboardPaymentRequest,
  getDashboardLoan,
  getDashboardAnnualLeave,
  getDashboardEmployeeQuery,
  getProfilePix,
  refreshProfilePix,
  requestApprovalForLeaveSchedule,
  cancelApprovalRequestForLeaveSchedule,
  updatePassword,
};
