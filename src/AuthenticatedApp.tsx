import React, { useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";
import { IonPage, IonRouterOutlet, IonApp, IonSplitPane } from "@ionic/react";

import Home from "./pages/Home";
import Profile from "./pages/Profile/Profile";
import Payslip from "./pages/Payslip/Payslip";
import LeaveSchedule from "./pages/Leave/LeaveSchedule";
import LeaveScheduleForm from "./pages/Leave/LeaveScheduleForm";
import Menu from "./components/Menu";
import { AppPage } from "./declarations";
import { useTenant } from "./context/TenantContext";
import { useAuth } from "./context/AuthContext";
import ComingSoon from "./pages/Layout/ComingSoon";
import LeaveApplicationHome from "./pages/Leave/LeaveApplicationHome";
import AnnualLeaveApplicationForm from "./pages/Leave/AnnualLeaveApplicationForm";
import LeaveApplicationHistory from "./pages/Leave/LeaveApplicationHistory";
import PaymentRequestHistory from "./pages/PaymentRequest/PaymentRequestHistory";
import PaymentRequest from "./pages/PaymentRequest/PaymentRequest";
import LeaveBalance from "./pages/Leave/LeaveBalance";
import LoanRequests from "./pages/Loan/LoanRequests";
import LoanRequestForm from "./pages/Loan/LoanRequestForm";
import LeaveRelief from "./pages/Leave/LeaveRelief";
import AuthenticatedWrapper from "./AuthenticatedWrapper";
import LeaveRequestForm from "./pages/Leave/LeaveRequestForm";
import Appraisals from "./pages/Appraisal/Appraisals";
import AppraisalView from "./pages/Appraisal/AppraisalView";
import Approvals from "./pages/Approval/Approvals";
import ApprovalEntry from "./pages/Approval/ApprovalEntry";
import AnnualLeaveApplications from "./pages/Leave/AnnualLeaveApplications";
import LeaveRequests from "./pages/Leave/LeaveRequests";
import LeaveReliefRequests from "./pages/Leave/LeaveReliefRequests";
import LeaveRequestEditForm from "./pages/Leave/LeaveRequestEditForm";
import AnnualLeaveApplicationEditForm from "./pages/Leave/AnnualLeaveApplicationEditForm";
import LeaveReliefHistory from "./pages/Leave/LeaveReliefHistory";
import LoanRequestEditForm from "./pages/Loan/LoanRequestEditForm";
import PaymentRequestEdit from "./pages/PaymentRequest/PaymentRequestEdit";
import QueryHome from "./pages/Query/QueryHome";
import IssuedQueries from "./pages/Query/IssuedQueries";
import QueryResponses from "./pages/Query/QueryResponses";
import ProcessedQueries from "./pages/Query/ProcessedQueries";
import QueryIssueForm from "./pages/Query/QueryIssueForm";
import QueryIssueEditForm from "./pages/Query/QueryIssueEditForm";
import QueryResponseForm from "./pages/Query/QueryResponseForm";
import ProcessedQueryForm from "./pages/Query/ProcessedQueryForm";
import ProfileEdit from "./pages/Profile/ProfileEdit";
import LeaveScheduleView from "./pages/Leave/LeaveScheduleView";

const appPages: AppPage[] = [
  {
    title: "Home",
    url: "/",
    icon: "assets/images/home.svg"
  },
  {
    title: "Profile",
    url: "/profile",
    icon: "assets/images/profile.svg"
  },
  {
    title: "Payslip",
    url: "/payslip",
    icon: "assets/images/surface1.svg"
  },
  {
    title: "Leave Management",
    url: "/leave-management",
    icon: "assets/images/calendar_10.svg"
  },
  {
    title: "Loan Request",
    url: "/loan-requests",
    icon: "assets/images/loan.svg"
  },
  {
    title: "Payment Request",
    url: "/payment-request-history",
    icon: "assets/images/receipt_2.svg"
  },
  // {
  //   title: "Self Appraisal",
  //   url: "/appraisals",
  //   icon: "assets/images/appraisal.svg"
  // },
  {
    title: "Query Management",
    url: "/query-management",
    icon: "assets/images/query_1.svg"
  }
  // {
  //   title: "Approval",
  //   url: "/approvals",
  //   icon: "assets/images/approved.png"
  // }
];

function ProtectedRoute(props) {
  const { hasTenant } = useTenant();
  const { user } = useAuth();

  if (!hasTenant) {
    return <Redirect to="/" />;
  }

  if (!user) {
    return <Redirect to="/" />;
  }

  return <Route {...props} />;
}

function AuthenticatedApp() {
  return (
    // <AuthenticatedWrapper>
    <Router>
      <div className="App">
        <IonApp>
          <IonSplitPane contentId="main">
            <Menu appPages={appPages} />
            <IonPage id="main">
              <Switch>
                <ProtectedRoute exact path="/" component={Home} />
                <ProtectedRoute exact path="/profile" component={Profile} />
                <ProtectedRoute
                  exact
                  path="/profile-edit"
                  component={ProfileEdit}
                />
                <ProtectedRoute exact path="/payslip" component={Payslip} />
                <ProtectedRoute
                  exact
                  path="/coming-soon"
                  component={ComingSoon}
                />
                <ProtectedRoute
                  exact
                  path="/leave-schedule"
                  component={LeaveSchedule}
                />
                <ProtectedRoute
                  exact
                  path="/leave-schedule-form/:id"
                  component={LeaveScheduleForm}
                />
                <ProtectedRoute
                  exact
                  path="/leave-schedule-edit/:id"
                  component={LeaveScheduleForm}
                />
                <ProtectedRoute
                  exact
                  path="/leave-schedule-view/:id"
                  component={LeaveScheduleView}
                />
                {/* <ProtectedRoute
                  exact
                  path="/leave-application"
                  component={LeaveApplication}
                /> */}

                <ProtectedRoute
                  exact
                  path="/payment-request"
                  component={PaymentRequest}
                />
                <ProtectedRoute
                  exact
                  path="/payment-request/:reqId"
                  component={PaymentRequestEdit}
                />
                <ProtectedRoute
                  exact
                  path="/payment-request-history"
                  component={PaymentRequestHistory}
                />
                <ProtectedRoute
                  exact
                  path="/leave-management"
                  component={LeaveApplicationHome}
                />
                <ProtectedRoute
                  exact
                  path="/coming-soon"
                  component={ComingSoon}
                />
                <ProtectedRoute
                  exact
                  path="/annual-leave-application"
                  component={AnnualLeaveApplicationForm}
                />
                <ProtectedRoute
                  exact
                  path="/leave-application-history"
                  component={LeaveApplicationHistory}
                />
                <ProtectedRoute
                  exact
                  path="/leave-balance"
                  component={LeaveBalance}
                />
                <ProtectedRoute
                  exact
                  path="/loan-request"
                  component={LoanRequestForm}
                />

                <ProtectedRoute
                  exact
                  path="/loan-request/:reqId"
                  component={LoanRequestEditForm}
                />
                <ProtectedRoute
                  exact
                  path="/loan-requests"
                  component={LoanRequests}
                />

                <ProtectedRoute
                  exact
                  path="/leave-request"
                  component={LeaveRequestForm}
                />

                <ProtectedRoute
                  exact
                  path="/leave-requests"
                  component={LeaveRequests}
                />

                <ProtectedRoute
                  exact
                  path="/leave-requests/:reqId"
                  component={LeaveRequestEditForm}
                />

                <ProtectedRoute
                  exact
                  path="/annual-leave-application/:reqId"
                  component={AnnualLeaveApplicationEditForm}
                />

                <ProtectedRoute
                  exact
                  path="/appraisals"
                  component={Appraisals}
                />

                <ProtectedRoute
                  exact
                  path="/appraisal-view"
                  component={AppraisalView}
                />
                <ProtectedRoute exact path="/approvals" component={Approvals} />
                <ProtectedRoute
                  exact
                  path="/approval-entry"
                  component={ApprovalEntry}
                />

                <ProtectedRoute
                  exact
                  path="/query-management"
                  component={QueryHome}
                />

                <ProtectedRoute
                  exact
                  path="/issued-queries"
                  component={IssuedQueries}
                />

                <ProtectedRoute
                  exact
                  path="/issued-queries/create"
                  component={QueryIssueForm}
                />

                <ProtectedRoute
                  exact
                  path="/issued-queries/:queryId/edit"
                  component={QueryIssueEditForm}
                />

                <ProtectedRoute
                  exact
                  path="/query-responses"
                  component={QueryResponses}
                />

                <ProtectedRoute
                  exact
                  path="/query-responses/:queryId/edit"
                  component={QueryResponseForm}
                />

                <ProtectedRoute
                  exact
                  path="/processed-queries"
                  component={ProcessedQueries}
                />

                <ProtectedRoute
                  exact
                  path="/processed-queries/:queryId/edit"
                  component={ProcessedQueryForm}
                />

                <ProtectedRoute
                  exact
                  path="/annual-leave-applications"
                  component={AnnualLeaveApplications}
                />

                <ProtectedRoute
                  exact
                  path="/leave-relief-request"
                  component={LeaveRelief}
                />

                <ProtectedRoute
                  exact
                  path="/leave-relief-requests"
                  component={LeaveReliefRequests}
                />

                <Route render={() => <Redirect to="/" />} />
                {/* </IonRouterOutlet> */}
              </Switch>
            </IonPage>
          </IonSplitPane>
        </IonApp>
      </div>
    </Router>
    // </AuthenticatedWrapper>
  );
}

export default AuthenticatedApp;
