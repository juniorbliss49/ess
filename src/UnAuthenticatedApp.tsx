import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { IonPage, IonRouterOutlet } from "@ionic/react";

import AuthLoading from "./pages/AppCompany/AuthLoading";
import Login from "./pages/Auth/Login";
import Register from "./pages/Auth/Register";
import PasswordReset from "./pages/Auth/PasswordReset";

function UnAutheniticatedApp() {
  return (
    <Router>
      <div className="App">
        <IonPage>
          <Switch>
            {/* <IonRouterOutlet> */}
            <Route exact path="/" component={AuthLoading} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/reset-password" component={PasswordReset} />
            <Route render={() => <Redirect to="/" />} />
            {/* </IonRouterOutlet> */}
          </Switch>
        </IonPage>
      </div>
    </Router>
  );
}

export default UnAutheniticatedApp;
