import React, { useState } from "react";
import {
  IonHeader,
  IonToolbar,
  IonButtons,
  IonButton,
  IonMenuButton,
  IonTitle,
  IonIcon,
  IonGrid,
  IonImg,
  IonAvatar,
  IonRow,
  IonCol,
  IonBadge,
  IonText,
  IonModal,
} from "@ionic/react";
import { appPages, backBtnName } from "../declarations";
import "./MenuHeader.css";
import { useAuth } from "../context/AuthContext";
import { useUtility } from "../context/UtilityContext";
import { RouteComponentProps, withRouter } from "react-router-dom";
import SidebarMenu from "../pages/Layout/SidebarMenu";
import LogoutModal from "./LogoutModal";

type Props = RouteComponentProps<{}> & {
  appPage: appPages;
  backBtn: backBtnName;
};

const MenuHeader: React.SFC<Props> = ({ history, appPage, backBtn }) => {
  const {
    logOutUser,
    changeMenu,
    toggle,
    changeSubMenu,
    changeSideSubMenu,
    user,
  } = useAuth();
  const { modal, changeModal } = useUtility();
  return (
    <>
      <IonHeader>
        <IonModal isOpen={modal} onDidDismiss={() => changeModal(false)}>
          <LogoutModal />
        </IonModal>
        <div className="ion-hide-lg-up">
          <IonToolbar class="custom-menu ion-hide-lg-up" color="">
            <IonButtons slot="start">
              <IonMenuButton />
            </IonButtons>
            <IonTitle>{appPage.name}</IonTitle>
            <IonButtons slot="end">
              <IonIcon
                title="Log Out"
                style={{ cursor: "pointer" }}
                name="ios-log-out"
                class="icon-style ion-padding"
                onClick={() => changeModal(true)}
              />
            </IonButtons>
          </IonToolbar>
        </div>
        <IonToolbar class="custom-menuheader ion-hide-md-down">
          <IonGrid className="desktopmenu">
            <IonRow>
              <IonCol size="2" sizeLg="2" sizeXl="2" className="no-padding">
                <IonButton
                  color="light"
                  className="icon-btn icon-home-hover"
                  onClick={changeMenu}
                >
                  <IonIcon
                    slot="icon-only"
                    title="Menu"
                    style={{ cursor: "pointer" }}
                    name="apps"
                    class="icon-style icon-size"
                  />
                </IonButton>
              </IonCol>
              <IonCol size="7" sizeLg="7" sizeXl="7" className="no-padding">
                <IonTitle>Employee Self Service</IonTitle>
              </IonCol>
              <IonCol
                size="2"
                sizeLg="3"
                sizeXl="3"
                className="no-padding padding-right"
              >
                <IonButton
                  color="light"
                  className="icon-btn icon-home-hover icon-float"
                  onClick={() => changeModal(true)}
                >
                  <IonIcon
                    title="Log Out"
                    style={{ cursor: "pointer" }}
                    name="ios-log-out"
                    class="icon-style  icon-size"
                  />
                </IonButton>
                <IonButton
                  color="light"
                  className="icon-btn icon-home-hover icon-float"
                  onClick={() => changeSubMenu("Notification")}
                >
                  <IonIcon
                    title="Notifications"
                    style={{ cursor: "pointer" }}
                    name="Notifications-outline"
                    class="icon-style icon-size"
                  />
                </IonButton>
                <IonButton
                  color="light"
                  className="icon-btn icon-home-hover icon-float"
                  onClick={() => changeSubMenu("Setting")}
                >
                  <IonIcon
                    title="Help"
                    style={{ cursor: "pointer" }}
                    ios="cog"
                    name="cog"
                    class="icon-style  icon-size"
                  />
                </IonButton>
              </IonCol>
            </IonRow>
          </IonGrid>
          <IonGrid className="no-padding desktopmenu-2">
            <IonRow className="no-padding">
              <IonCol className="no-padding" size="2">
                <IonButton
                  color="light"
                  className="icon-btn sub-icon-btn "
                  onClick={changeSideSubMenu}
                >
                  <IonIcon
                    slot="icon-only"
                    title="Menu"
                    style={{ cursor: "pointer" }}
                    name="menu"
                    class="icon-style sub-icon-style icon-size"
                  />
                </IonButton>
              </IonCol>
              <IonCol size="8">
                <h4 className="page-title">{appPage.name}</h4>
              </IonCol>
              <IonCol size="2">
                {backBtn.name ? (
                  <h6
                    className="page-title content-center"
                    style={{ cursor: "pointer" }}
                    onClick={() => history.goBack()}
                  >
                    <IonIcon
                      title="Back"
                      ios="ios-undo"
                      name="ios-undo"
                      className="menu-header-icon"
                    />
                    Go Back
                  </h6>
                ) : null}
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonToolbar>
      </IonHeader>
    </>
  );
};

export default withRouter(MenuHeader);
