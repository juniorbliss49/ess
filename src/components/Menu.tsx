import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToolbar,
  IonGrid,
  IonRow,
  IonCol,
  IonImg,
  IonAvatar,
  IonSplitPane
} from "@ionic/react";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { AppPage } from "../declarations";
import "./Menu.scss";
import { useAuth } from "../context/AuthContext";
import { useProfile } from "../context/ProfileContext";
import SidebarMenu from "../pages/Layout/SidebarMenu";

type Props = RouteComponentProps<{}> & {
  appPages: AppPage[];
};

const Menu: React.SFC<Props> = ({ history, appPages }) => {
  const { user, toggle } = useAuth();
  const { picture } = useProfile();

  const avatar = () => {};

  return (
    // <IonSplitPane>
    <IonMenu contentId="main" class="custom-menu">
      {/* <div className=""> */}
      <IonHeader class="custom-menu-header ion-hide-lg-up">
        <IonToolbar>
          <IonTitle>
            <IonGrid>
              <IonRow>
                <IonCol size="10" offset="1" class="ion-padding">
                  <IonRow>
                    <IonCol size="5" class="ion-float-left">
                      <IonAvatar>
                        <IonImg
                          src={picture ? picture : "assets/images/man.png"}
                          class="custom-menu-image ion-text-start"
                        />
                      </IonAvatar>
                    </IonCol>
                    <IonCol size="7" class="custom-menu-margin">
                      <span className="custom-menu-head">
                        {user.first_name + " " + user.last_name}
                      </span>
                      <br />
                      {/* <span className="custom-menu-dept">
                        Research & Development
                      </span> */}
                    </IonCol>
                  </IonRow>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent class="outer-content ion-hide-lg-up">
        <IonList class="custom-menu-list">
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index}>
                <IonItem
                  routerDirection="root"
                  onClick={() => history.push(appPage.url)}
                  class="custom-menu-item"
                >
                  {/* <IonIcon
                    slot="start"
                    name={appPage.icon}
                    class="custom-menu-icon"
                  /> */}
                  <IonImg src={appPage.icon} className="custom-menu-icon" />
                  <IonLabel className="custom-sidebar-title">{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
      {/* </div> */}
      {/* <div className="">     */}
      <IonHeader class="custom-menu-header custom-menu-header-margin ion-hide-md-down">
        <IonToolbar>
          {/* <IonTitle>
            <IonGrid>
              <IonRow class="ion-hide-sm-down custom-menu-border">
                <IonCol>
                  <IonImg
                    src="assets/images/logo.png"
                    class="logo custom-logo custom-menu-logo"
                  />
                  <p className=" custom-menu-text">
                    <span>My ESS</span>
                    <br /> Employess Self Service
                  </p>
                </IonCol>
              </IonRow>
            </IonGrid>
          </IonTitle> */}
      {/* {toggle ? <SidebarMenu /> : ''} */}

        </IonToolbar>
      </IonHeader>
      <IonContent class="outer-content ion-hide-md-down">
        <IonGrid>
          <IonRow class=" custom-menu-top-header">
            <IonCol size="10" sizeXl="10" offset="1" class="ion-padding">
              <IonRow>
                <IonCol
                  sizeLg="6"
                  offsetLg="3"
                  sizeXl="6"
                  offsetXl="3"
                  class="ion-float-left"
                >
                  <IonAvatar>
                    <IonImg
                      src={picture ? picture : "assets/images/man.png"}
                      class="custom-menu-image ion-text-start"
                    />
                  </IonAvatar>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol sizeLg="12" sizeXl="12" class="custom-menu-margin">
                  <span className="custom-menu-head">
                    {user.first_name + " " + user.last_name}
                  </span>
                  <br />
                  {/* <span className="custom-menu-dept">
                    Research & Development
                  </span> */}
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        </IonGrid>
        <IonList class="custom-menu-list">
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index}>
                <IonItem
                  routerDirection="root"
                  onClick={() => history.push(appPage.url)}
                  class="custom-menu-item"
                >
                  <IonIcon
                    slot="start"
                    name={appPage.icon}
                    class="custom-menu-icon"
                  />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
      </IonContent>
      {/* </div> */}
    </IonMenu>
    // </IonSplitPane>
  );
};

export default withRouter(Menu);
