import React, { useState, useEffect } from "react";
import {
  IonContent,
  IonRow,
  IonCol,
  IonIcon,
  IonButton,
  IonSpinner,
} from "@ionic/react";
import { useUtility } from "../context/UtilityContext";
import { useAuth } from "../context/AuthContext";

function LogoutModal() {
  const { modal, changeModal } = useUtility();
  const { token, logOutUser } = useAuth();
  const [loading, setLoading] = useState(false);

  useEffect(() => {});

  const logout = () => {
    setLoading(true);
    logOutUser();
    if (!token) {
      setLoading(false);
    }
  };

  return (
    <>
      <IonContent>
        <IonRow>
          <IonCol size="12" className="ion-text-center">
            <IonIcon name="alert" style={{ fontSize: "100px" }} />
            <h4>Are you sure you want to logout?</h4>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol
            size="9"
            style={{ margin: "auto" }}
            className="ion-text-center"
          >
            <IonButton color="medium" onClick={() => changeModal(false)}>
              Cancel
            </IonButton>
            <IonButton color="danger" onClick={logout}>
              Logout
            </IonButton>
            {loading ? (
              <>
                <IonRow>
                  <IonCol size="4" offset="4" className="ion-text-center">
                    <IonSpinner color="primary" />
                  </IonCol>
                </IonRow>
              </>
            ) : null}
          </IonCol>
        </IonRow>
      </IonContent>
    </>
  );
}

export default LogoutModal;
