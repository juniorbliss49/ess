import React from "react";
import { 
            IonHeader, 
            IonToolbar, 
            IonButtons, 
            IonMenuButton, 
            IonTitle, 
            IonIcon, 
            IonGrid,
            IonRow,
            IonCol,
            IonButton,

        } from "@ionic/react";
import { appPages, backBtnName } from "../declarations";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { useAuth, } from "../context/AuthContext";

type Props = RouteComponentProps<{}> & {
    appPage: appPages;
    backBtn: backBtnName
  };

function MenuTopHeader() {
    const { logOutUser, changeMenu, toggle, changeSubMenu, changeSideSubMenu, user } = useAuth();


    return(
        <>
            <IonHeader>
                {/* <div className="ion-hide-lg-up">
                    <IonToolbar class="custom-menu ion-hide-lg-up" color="favorite ">
                        <IonButtons slot="start">
                        <IonMenuButton />
                        </IonButtons>
                        <IonTitle>{appPage.name}</IonTitle>
                        <IonButtons slot="end">
                        <IonIcon
                            title="Log Out"
                            style={{ cursor: "pointer" }}
                            name="ios-log-out"
                            class="icon-style ion-padding"
                            onClick={logOutUser}
                        />
                        </IonButtons>
                    </IonToolbar>
                </div> */}
                <IonToolbar class="custom-menuheader ion-hide-md-down">
                    <IonGrid className="desktopmenu">
                        <IonRow>
                            <IonCol size="2" sizeLg="2" sizeXl="2" className="no-padding">
                                <IonButton color="light" className="icon-btn icon-home-hover" onClick={changeMenu}>
                                    <IonIcon
                                        slot="icon-only"
                                        title="Menu"
                                        style={{ cursor: "pointer" }}
                                        name="apps"
                                        class="icon-style icon-size"
                                        
                                    />
                                </IonButton>
                            </IonCol>
                            <IonCol size="8" sizeLg="7" sizeXl="7" className="no-padding">
                                <IonTitle>ESS</IonTitle>
                            </IonCol>
                            <IonCol size="2" sizeLg="3" sizeXl="3" className="no-padding padding-right">
                                <IonButton color="light" className="icon-btn icon-home-hover icon-float" onClick={logOutUser}>
                                    <IonIcon
                                    title="Log Out"
                                    style={{ cursor: "pointer" }}
                                    name="ios-log-out"
                                    class="icon-style  icon-size"
                                    
                                    />
                                </IonButton>
                                <IonButton color="light" className="icon-btn icon-home-hover icon-float" onClick={()=>changeSubMenu('Notification')}>
                                    <IonIcon
                                    title="Notifications"
                                    style={{ cursor: "pointer" }}
                                    name="Notifications-outline"
                                    class="icon-style icon-size"
                                    
                                    />
                                </IonButton>
                                <IonButton color="light" className="icon-btn icon-home-hover icon-float" onClick={()=>changeSubMenu('Setting')}>
                                    <IonIcon
                                    title="Help"
                                    style={{ cursor: "pointer" }}
                                    ios="cog"
                                    name="cog"
                                    class="icon-style  icon-size"
                                    
                                    />
                                </IonButton>
                            </IonCol>
                        </IonRow>
                        
                    </IonGrid>
                </IonToolbar>
            </IonHeader>
        </>
    )
}

export default MenuTopHeader;