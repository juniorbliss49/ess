import React from "react";
import { IonRow, IonCol } from "@ionic/react";
import "../styles/progress-bar.css";

function ProgressBar({ progress }) {
  return (
    <>
      <IonRow>
        <IonCol size="12">
          <div className="progress-bar">
            <div className="progress" style={{ width: `${progress}%` }}></div>
          </div>
        </IonCol>
      </IonRow>
    </>
  );
}

export default ProgressBar;
