export interface AppPage {
  url: string,
  icon: string,
  title: string
}

export interface ListItem {
  title: string;
  note: string;
  icon: string;
}

export interface appPages{
  name: string
}

export interface backBtnName{
  name: string
}