import React from "react";

/* Core CSS required for Ionic components to work properly */
import "@ionic/core/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/core/css/normalize.css";
import "@ionic/core/css/structure.css";
import "@ionic/core/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/core/css/padding.css";
import "@ionic/core/css/float-elements.css";
import "@ionic/core/css/text-alignment.css";
import "@ionic/core/css/text-transformation.css";
import "@ionic/core/css/flex-utils.css";
import "@ionic/core/css/display.css";

import { useAuth } from "./context/AuthContext";
import { IonSpinner, setupConfig } from "@ionic/react";
import AuthPageWrapper from "./pages/Layout/AuthPageWrapper";
import AuthenticatedWrapper from "./AuthenticatedWrapper";
import { Capacitor, Plugins } from "@capacitor/core";

const loadAuthenticatedApp = () => import("./AuthenticatedApp");
const AuthenticatedApp = React.lazy(loadAuthenticatedApp);
const UnAuthenticatedApp = React.lazy(() => import("./UnAuthenticatedApp"));

setupConfig({
  hardwareBackButton: true,
});

function App() {
  const { user } = useAuth();

  React.useEffect(() => {
    loadAuthenticatedApp();

    if (Capacitor.isNative) {
      Plugins.App.addListener("backButton", () => {
        if (window.location.pathname == "/") {
          Plugins.App.exitApp();
        } else {
          history.back();
        }
      });
    }
  }, []);

  return (
    <React.Suspense fallback={<IonSpinner name="crescent" color="primary" />}>
      {/* <AuthenticatedWrapper> */}
      {/* <AuthPageWrapper name="Home" bg_name="bg-img" backBtnName=""> */}
      {user ? <AuthenticatedApp /> : <UnAuthenticatedApp />}
      {/* </AuthPageWrapper> */}
      {/* </AuthenticatedWrapper> */}
    </React.Suspense>
  );
}

export default App;
