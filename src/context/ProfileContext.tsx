import React, { useContext, useState } from "react";
import {
  getProfile,
  getProfileData,
  getProfileRefresh,
  getProfilePix,
  refreshProfilePix,
} from "../utils/apiClient";
import { useAuth } from "../context/AuthContext";

const ProfileContext = React.createContext(undefined);

interface ProfileContainer {
  profile: any;
  picture: string;
  fetchProfile: (token) => any;
  fetchAllProfileData: (token) => any;
  getProfilePixQuery: (token) => any;
  setLoading: (data) => any;
  fetchProfilePix: (token) => any;
  loading: any;
  errors: any;
  setErrors: any;
  syncAll: (token) => any;
  profileEditErrors: any;
  setProfileEditErrors: any;
}

function ProfileProvider(props: any) {
  const { logOutUser } = useAuth();

  const [profile, setProfile] = useState(null);
  const [picture, setPicture] = useState(null);
  const [errors, setErrors] = useState(null);
  const [profileEditErrors, setProfileEditErrors] = useState(null);
  const [loading, setLoading] = useState(null);

  const getProfileQuery = async (token) => {
    return await getProfile(token);
  };

  const fetchProfilePix = async (token) => {
    try {
      let data = await refreshProfilePix(token);
      setPicture(data.employeePicture);
      return data;
    } catch (error) {
      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }
      setErrors([error.response.body.message]);
    }
  };

  const getProfilePixQuery = (token) => {
    getProfilePix(token)
      .then((data) => {
        setPicture(data.employeePicture);
      })
      .catch((err) => {
        setPicture(null);

        if (!err.response) {
          setErrors(err.response.body.message);
        }

        setErrors(err.response.body.message);
      });
  };

  const fetchProfile = (token) => {
    getProfile(token)
      .then(({ data }) => {
        const { picture, ...employee } = data;
        setProfile(employee);
      })
      .catch((error) => {
        if (!error.response) {
          setErrors(["Network Error, Check Your Internet Connection"]);
          return;
        }

        if (!error.response.body) {
          setErrors(["Something went wrong"]);
          return;
        }
        setErrors([error.response.body.message]);
      });
  };

  const fetchAllProfileData = async (token) => {
    try {
      setLoading(true);
      let data = await getProfileData(token);
      setLoading(false);
      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setProfileEditErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setProfileEditErrors(["Something went wrong"]);
        return;
      }
      setProfileEditErrors([error.response.body.message]);
    }
  };

  const syncAll = async (token) =>
    getProfileRefresh(token).then(({ data }) => {
      fetchProfile(token);
      // const { picture, ...employee } = data;
      // setProfile(employee);
      // setPicture(picture);
    });

  return (
    <ProfileContext.Provider
      value={{
        profile,
        picture,
        fetchProfile,
        fetchAllProfileData,
        loading,
        setLoading,
        errors,
        setErrors,
        syncAll,
        getProfilePixQuery,
        fetchProfilePix,
        profileEditErrors,
        setProfileEditErrors,
      }}
      {...props}
    />
  );
}

function useProfile(): ProfileContainer {
  const context = useContext(ProfileContext);

  if (!context) {
    throw new Error(`useProfile must be used within a ProfileProvider`);
  }

  return context;
}

export { ProfileProvider, useProfile };
