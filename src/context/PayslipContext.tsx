import React, { useContext, useState } from "react";
import { getPayslip, getPayslipAsPdf } from "../utils/apiClient";

const PayslipContext = React.createContext(undefined);

function groupBy(arr: any[], prop: any) {
  return arr.reduce(function (groups, item) {
    const val = item[prop];
    groups[val] = groups[val] || [];
    groups[val].push(item);
    return groups;
  }, {});
}

function PayslipProvider(props: any) {
  const [payslip, setPayslip] = useState(null);
  const [netPay, setNetPay] = useState(null);
  const [totalEarnings, setTotalEarnings] = useState(null);
  const [totalDeductions, setTotalDeductions] = useState(null);
  const [period, setPeriod] = useState(() => {
    const today = new Date();

    let year = today.getFullYear();
    let month = today.getMonth(); //+ 1;

    if (month == 0) {
      month = 12;
      year = year - 1;
    }

    let formattedMonth = month < 10 ? `0${month}` : `${month}`;

    return `${year}-${formattedMonth}`;
  });

  const fetchPayslip = async (period: string, token: string) => {
    setNetPay(null);
    setPayslip(null);
    let { data } = await getPayslip(period, token);

    let netPay = data.filter((entry) => entry.type == "Net-Pay")[0];
    let payslip = groupBy(
      data.filter((entry) => entry.type != "Net-Pay"),
      "type"
    );

    let totalEarnings = payslip.Earning
      ? payslip.Earning.filter(
          (entry) => entry.payslip_text == "Total Earning"
        )[0]
      : null;

    let totalDeductions = payslip.Deduction
      ? payslip.Deduction.filter(
          (entry) => entry.payslip_text == "Total Deduction"
        )[0]
      : null;

    setTotalEarnings(totalEarnings);
    setTotalDeductions(totalDeductions);
    setNetPay(netPay);
    setPayslip(payslip);
  };

  const fetchPayslipPdf = async (period, code, token) => {
    return await getPayslipAsPdf(period, code, token);
  };

  return (
    <PayslipContext.Provider
      value={{
        payslip,
        period,
        setPeriod,
        netPay,
        fetchPayslip,
        fetchPayslipPdf,
        totalDeductions,
        totalEarnings,
      }}
      {...props}
    />
  );
}

function usePayslip() {
  const context = useContext(PayslipContext);

  if (!context) {
    throw new Error(`usePayslip must be used within a PayslipProvider`);
  }

  return context;
}

export { PayslipProvider, usePayslip };
