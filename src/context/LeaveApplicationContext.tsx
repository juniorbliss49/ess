import React, { useState, useContext } from "react";
import {
  getleaveApplications,
  createLeaveApplication,
  getCreateLeaveApplication,
  getSettingsForNewAnnualLeaveApplication,
  getAnnualLeaveApplications,
  getArchivedLeaveApplications,
  getNonAnnualLeaveApplications,
  getSettingsForNewNonAnnualLeaveApplication,
  updateLeaveApplication,
  getLeaveReliefRequests,
  postLeaveReliefResponse,
  requestApprovalForLeaveApplication,
  initLeaveApplicationFieldsForUpdate,
  initAnnualLeaveApplicationFieldsForUpdate,
  getLeaveSchedule,
  getEditLeaveSchedule,
  createLeaveSchudule,
  getLeaveBalance,
  leaveScheduleSync,
  leaveBalanceSync,
} from "../utils/apiClient";
import { useAuth } from "./AuthContext";

const LeaveApplicationContext = React.createContext(null);

function LeaveApplicationProvider(props: any) {
  const { logOutUser } = useAuth();

  const [leaveApplications, setleaveApplications] = useState(null);
  const [
    currentlyEditedLeaveRequest,
    setCurrentlyEditedLeaveRequest,
  ] = useState(null);
  const [
    currentlyEditedAnnualLeaveRequest,
    setCurrentlyEditedAnnualLeaveRequest,
  ] = useState(null);
  // Annual Leave Applications
  const [annualLeaveApplications, setAnnualLeaveApplications] = useState(null);

  // Archived Leave Applications
  const [archivedLeaveApplications, setArchivedLeaveApplications] = useState(
    null
  );

  const [casualLeaveApplication, setCasualLeaveApplication] = useState(null);
  const [reliefNo, setReliefNo] = useState(null);
  const [causeofAbsenceCode, setCauseofAbsenceCode] = useState(null);
  const [leaveApplicationFields, setleaveApplicationFields] = useState(null);
  const [
    annualLeaveApplicationFields,
    setAnnualLeaveApplicationFields,
  ] = useState(null);

  const [leaveReliefRequests, setLeaveReliefRequests] = useState(null);
  const [leaveSchedules, setLeaveSchedule] = useState(null);
  const [
    currentlyViewedReliefRequest,
    setCurrentlyViewedReliefRequest,
  ] = useState(null);

  const [
    leaveReliefResponseIsMandated,
    setLeaveReliefResponseIsMandated,
  ] = useState(false);

  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState([]);

  const [reliefNoSelectOptions, setReliefNoSelectOptions] = useState(null);
  const [
    causeofAbsenceCodeSelectOptions,
    setCauseofAbsenceCodeSelectOptions,
  ] = useState(null);

  const setSelectOptions = (type, lookup) => {
    let requestData = [];

    switch (type) {
      case "relief_no":
        let reliefNo = [];
        lookup.map((data, id) => {
          return reliefNo.push({
            value: lookup[id].no,
            label: lookup[id].first_name + " " + lookup[id].last_name,
          });
        });
        setReliefNoSelectOptions(reliefNo);
        requestData = [...reliefNo];
        break;
      case "cause_of_absence_code":
        let causeofAbsenceCode = [];
        Object.keys(lookup).map((data, id) => {
          return causeofAbsenceCode.push({
            value: lookup[data].code,
            label: lookup[data].description,
          });
        });
        setCauseofAbsenceCodeSelectOptions(causeofAbsenceCode);
        requestData = [...causeofAbsenceCode];
        break;

      default:
        requestData = [];
        break;
    }
    return requestData;
  };

  const fetchLeaveApplications = async (token: any) => {
    setleaveApplications(null);
    let { data } = await getleaveApplications(token);
    setleaveApplications(data);

    let annual = data.filter(function (itm: { cause_of_absence_code: string }) {
      return itm.cause_of_absence_code == "ANNUAL";
    });

    let casual = data.filter(function (itm: { cause_of_absence_code: string }) {
      return itm.cause_of_absence_code == "DAYOFF";
    });

    setAnnualLeaveApplications(annual);
    setCasualLeaveApplication(casual);
  };

  const fetchArchivedLeaveApplications = async (token: any) => {
    setArchivedLeaveApplications(null);
    let { data } = await getArchivedLeaveApplications(token);
    setArchivedLeaveApplications(data);
  };

  const fetchAnnualLeaveApplications = async (token: any) => {
    setAnnualLeaveApplications(null);
    let { data } = await getAnnualLeaveApplications(token);
    setAnnualLeaveApplications(data);
    return data;
  };

  const fetchNonAnnualLeaveApplications = async (token: any) => {
    setleaveApplications(null);
    let { data } = await getNonAnnualLeaveApplications(token);
    setleaveApplications(data);
    return data;
  };

  const PostLeaveApplications = async (data: any, token: any) => {
    try {
      return await createLeaveApplication(data, token);
    } catch (error) {
      throw error;
    }
  };

  const updateCurrentLeaveApplication = async (
    id: any,
    data: any,
    token: any
  ) => {
    try {
      return await updateLeaveApplication(id, data, token);
    } catch (error) {
      throw error;
    }
  };

  const getCreateRequest = async (token: any) => {
    console.log("yes enter");
    let { data } = await getCreateLeaveApplication(token);
    let leave_application_fileds = data;
    console.log(data);
    setleaveApplicationFields(leave_application_fileds);
    let relief_no = data.fields.card.relief_no.lookup;
    let cause_of_absence_code = data.fields.card.cause_of_absence_code.lookup;
    setReliefNo(relief_no);
    setCauseofAbsenceCode(cause_of_absence_code);
  };

  const prepFieldsForAnnualNewLeaveApplication = async (token: any) => {
    let { data } = await getSettingsForNewAnnualLeaveApplication(token);
    setAnnualLeaveApplicationFields(data);
    setReliefNo(data.fields.card.relief_no.lookup);
    setCauseofAbsenceCode(data.fields.card.cause_of_absence_code.lookup);
  };

  const prepFieldsForNonAnnualNewLeaveApplication = async (token: any) => {
    let { data } = await getSettingsForNewNonAnnualLeaveApplication(token);
    setleaveApplicationFields(data);
    let reliefNo = setSelectOptions(
      "relief_no",
      data.fields.card.relief_no.lookup
    );
    let causeofAbsenceCode = setSelectOptions(
      "cause_of_absence_code",
      data.fields.card.cause_of_absence_code.lookup
    );

    setReliefNo(data.fields.card.relief_no.lookup);
    setCauseofAbsenceCode(data.fields.card.cause_of_absence_code.lookup);
  };

  const prepFieldsForAnnualNewLeaveApplicationForUpdate = async (
    leaveReqId,
    token
  ) => {
    try {
      setLoading(true);
      let { data } = await initAnnualLeaveApplicationFieldsForUpdate(
        leaveReqId,
        token
      );
      setAnnualLeaveApplicationFields(data);
      setReliefNo(data.fields.card.relief_no.lookup);
      setCauseofAbsenceCode(data.fields.card.cause_of_absence_code.lookup);
      setLoading(false);

      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
      }

      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }

      setErrors([error.response.body.message]);
    }
  };

  const prepFieldsForNonAnnualNewLeaveApplicationForUpdate = async (
    leaveReqId,
    token
  ) => {
    try {
      setLoading(true);
      let { data } = await initLeaveApplicationFieldsForUpdate(
        leaveReqId,
        token
      );
      setleaveApplicationFields(data);

      let reliefNo = setSelectOptions(
        "relief_no",
        data.fields.card.relief_no.lookup
      );
      let causeofAbsenceCode = setSelectOptions(
        "cause_of_absence_code",
        data.fields.card.cause_of_absence_code.lookup
      );

      setReliefNo(data.fields.card.relief_no.lookup);
      setCauseofAbsenceCode(data.fields.card.cause_of_absence_code.lookup);
      setLoading(false);

      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
      }

      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }

      setErrors([error.response.body.message]);
    }
  };

  const fetchReliefRequests = async (token: any) => {
    let { data } = await getLeaveReliefRequests(token);
    setLeaveReliefRequests(data);
    return data;
  };

  const sendLeaveReliefResponse = async (id: any, data: any, token: any) => {
    try {
      return await postLeaveReliefResponse(id, data, token);
    } catch (error) {
      throw error;
    }
  };

  const sendLeaveRequestForApproval = async (
    data: { leave_request_id: any },
    token: any
  ) => {
    return await requestApprovalForLeaveApplication(data, token);
  };

  const fetchLeaveSchedule = async (token: any) => {
    setLeaveSchedule(null);
    let { data } = await getLeaveSchedule(token);
    setLeaveSchedule(data);
  };

  const getEditLeaveScheduleRequest = async (id: any, token: any) => {
    let { data } = await getEditLeaveSchedule(id, token);
    return data;
  };

  const postLeaveSchedule = async (id, data, token) => {
    try {
      return await createLeaveSchudule(id, data, token);
    } catch (error) {
      throw error;
    }
  };

  const fetchLeaveBalance = async (token) => {
    try {
      return await getLeaveBalance(token);
    } catch (error) {
      throw error;
    }
  };

  const syncLeave = async (token: any) => {
    let { message } = await leaveScheduleSync(token);
    let { data } = await getLeaveSchedule(token);
    setLeaveSchedule(data);
    return message;
  };

  const syncLeaveBalance = async (year: any, token: any) => {
    let { message } = await leaveBalanceSync(year, token);
    return message;
  };

  return (
    <LeaveApplicationContext.Provider
      value={{
        leaveApplications,
        PostLeaveApplications,
        getCreateRequest,
        fetchLeaveApplications,
        annualLeaveApplications,
        casualLeaveApplication,
        reliefNo,
        leaveApplicationFields,
        causeofAbsenceCode,
        prepFieldsForAnnualNewLeaveApplication,
        prepFieldsForNonAnnualNewLeaveApplication,
        annualLeaveApplicationFields,
        fetchAnnualLeaveApplications,
        fetchArchivedLeaveApplications,
        archivedLeaveApplications,
        fetchNonAnnualLeaveApplications,
        currentlyEditedLeaveRequest,
        setCurrentlyEditedLeaveRequest,
        updateCurrentLeaveApplication,
        currentlyEditedAnnualLeaveRequest,
        setCurrentlyEditedAnnualLeaveRequest,
        fetchReliefRequests,
        leaveReliefRequests,
        currentlyViewedReliefRequest,
        setCurrentlyViewedReliefRequest,
        sendLeaveReliefResponse,
        leaveReliefResponseIsMandated,
        setLeaveReliefResponseIsMandated,
        sendLeaveRequestForApproval,
        prepFieldsForNonAnnualNewLeaveApplicationForUpdate,
        prepFieldsForAnnualNewLeaveApplicationForUpdate,
        loading,
        errors,
        fetchLeaveSchedule,
        leaveSchedules,
        getEditLeaveScheduleRequest,
        postLeaveSchedule,
        fetchLeaveBalance,
        setLoading,
        syncLeave,
        setLeaveSchedule,
        causeofAbsenceCodeSelectOptions,
        reliefNoSelectOptions,
        syncLeaveBalance,
      }}
      {...props}
    />
  );
}

// interface LeaveApplicationContainer {
//   leaveApplications: any,
//   PostLeaveApplications: (data: any, token: any) => Promise<any>,
//   getCreateRequest: (token: any) => Promise<void>,
//   fetchLeaveApplications: any,
//   annualLeaveApplications: any,
//   casualLeaveApplication: any,
//   reliefNo: any,
//   leaveApplicationFileds: any,
//   causeofAbsenceCode: any,
//   prepFieldsForNewLeaveApplication: any,
//   annualLeaveApplicationFields: any,
//   fetchAnnualLeaveApplications: any,
//   fetchArchivedLeaveApplications: any,
//   archivedLeaveApplications: any
// }

function useLeaveApplication() {
  const context = useContext(LeaveApplicationContext);

  if (!context) {
    throw new Error(
      `useLeaveApplication must be used within a LeaveApplicationProvider`
    );
  }

  return context;
}

export { LeaveApplicationProvider, useLeaveApplication };
