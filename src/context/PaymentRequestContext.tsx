import React, { useContext, useState } from "react";
import {
  getAllPaymentRequest,
  createPaymentRequest,
  getCreatePaymentRequest,
  initPaymentApplicationFieldsForUpdate,
  editPaymentRequest,
  refreshAllPaymentRequests,
} from "../utils/apiClient";
import { useAuth } from "./AuthContext";

const PaymentRequestContext = React.createContext(undefined);

function PaymentRequestProvider(props: any) {
  const { logOutUser } = useAuth();

  const [paymentRequests, setpaymentRequests] = useState(null);
  const [requestCode, setRequestCode] = useState(null);
  const [preferredPmtMethod, setpreferredPmtMethod] = useState(null);
  const [requestType, setrequestType] = useState(null);
  const [department, setDepartment] = useState(null);
  const [project, setProject] = useState(null);
  const [lineDepartment, setLineDepartment] = useState(null);
  const [lineProject, setLineProject] = useState(null);
  const [customerGroup, setcustomerGroup] = useState(null);
  const [area, setArea] = useState(null);
  const [businessGroup, setbusinessGroup] = useState(null);
  const [salesCampaign, setsalesCampaign] = useState(null);
  const [currencyCode, setcurrencyCode] = useState(null);
  const [filteredPaymentRequests, setFilteredPaymentRequests] = useState(null);
  const [paymentRequestFields, setpaymentRequestFields] = useState(null);
  const [formLine, setFormLine] = useState(null);
  const [paymentLines, setPaymentLines] = useState(null);
  const [newLines, setnewLines] = useState(null);
  const [newPaymentLines, setNewPaymentLines] = useState(null);
  const [errors, setErrors] = useState(null);
  const [loading, setLoading] = useState(null);
  const [requestlistSelectOptions, setrequestlistSelectOptions] = useState(
    null
  );
  const [currencylistSelectOptions, setcurrencylistSelectOptions] = useState(
    null
  );
  const [paymentlistSelectOptions, setpaymentlistSelectOptions] = useState(
    null
  );
  const [
    deparmentslistSelectOptions,
    setdeparmentslistSelectOptions,
  ] = useState(null);
  const [projectlistSelectOptions, setprojectlistSelectOptions] = useState(
    null
  );
  const [
    requestCodelistSelectOptions,
    setrequestCodelistSelectOptions,
  ] = useState(null);
  const [
    deparmentlinelistSelectOptions,
    setdeparmentlinelistSelectOptions,
  ] = useState(null);
  const [
    projectlinelistSelectOptions,
    setprojectlinelistSelectOptions,
  ] = useState(null);
  const [
    customergrouplistSelectOptions,
    setcustomergrouplinelistSelectOptions,
  ] = useState(null);
  const [arealistSelectOptions, setarealinelistSelectOptions] = useState(null);
  const [
    businesslistSelectOptions,
    setbusinesslinelistSelectOptions,
  ] = useState(null);
  const [
    campaignlistSelectOptions,
    setcampaignlinelistSelectOptions,
  ] = useState(null);
  const [
    currencylineSelectOptions,
    setcurrencylinelistSelectOptions,
  ] = useState(null);
  const [requestOption, setRequestOption] = useState(null);
  const [currencyOption, setCurrencyOption] = useState(null);
  const [preferredmethodOption, setPreferredmethodOption] = useState(null);
  const [deparmentOption, setDeparmentOption] = useState(null);
  const [projectOption, setProjectOption] = useState(null);
  const [respCenterOption, setRespCenterOption] = useState(null);
  const [requestCodeSelectValue, setRequestCodeSelectValue] = useState(null);

  const [respCenter, setRespCenter] = useState("");
  const [responsibilityCenters, setResponsibilityCenters] = useState([]);

  const fetchPaymentRequest = async (token) => {
    let { data } = await getAllPaymentRequest(token);
    let paymentData = data.reverse();
    setpaymentRequests(() => {
      return paymentData;
    });
    setFilteredPaymentRequests(() => {
      return paymentData;
    });
    return data;
  };

  const createRequest = async (data, token) => {
    try {
      return await createPaymentRequest(data, token);
    } catch (error) {
      throw error;
    }
  };

  const setSelectOptions = (type, lookup) => {
    let requestData = [];

    switch (type) {
      case "request_type":
        let requests = [];
        Object.keys(lookup).map((data, id) => {
          return requests.push({
            value: lookup[data],
            label: data,
          });
        });
        setrequestlistSelectOptions(requests);
        requestData = [...requests];
        break;
      case "currency_code":
        let currencycodes = [];
        // let currencies = currency_code;
        lookup.map((data, id) => {
          return currencycodes.push({
            value: lookup[id].code,
            label: lookup[id].code,
          });
        });
        setcurrencylistSelectOptions(currencycodes);
        requestData = [...currencycodes];
        break;
      case "preferred_pmt_method":
        let preferredmethod = [];
        Object.keys(lookup).map((data, id) => {
          return preferredmethod.push({
            value: lookup[data],
            label: data,
          });
        });
        setpaymentlistSelectOptions(preferredmethod);
        requestData = [...preferredmethod];
        break;
      case "deparments":
        let deparments = [];
        lookup.map((data, id) => {
          return deparments.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setdeparmentslistSelectOptions(deparments);
        requestData = [...deparments];
        break;
      case "request_code":
        let request = [];
        lookup.map((data, id) => {
          return request.push({
            value: lookup[id].code,
            label: lookup[id].description,
          });
        });
        setrequestCodelistSelectOptions(request);
        requestData = [...request];
        break;
      case "deparmentsline":
        let deparmentsline = [];
        lookup.map((data, id) => {
          return deparmentsline.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setdeparmentlinelistSelectOptions(deparmentsline);
        requestData = [...deparmentsline];
        break;
      case "projectsline":
        let projectsline = [];
        lookup.map((data, id) => {
          return projectsline.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setprojectlinelistSelectOptions(projectsline);
        requestData = [...projectsline];
        break;
      case "projects":
        let projects = [];
        lookup.map((data, id) => {
          return projects.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setprojectlistSelectOptions(projects);
        requestData = [...projects];
        break;
      case "customergroup":
        let customergroup = [];
        lookup.map((data, id) => {
          return customergroup.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setcustomergrouplinelistSelectOptions(customergroup);
        requestData = [...customergroup];
        break;
      case "areas":
        let areas = [];
        lookup.map((data, id) => {
          return areas.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setarealinelistSelectOptions(areas);
        requestData = [...areas];
        break;
      case "business":
        let business = [];
        lookup.map((data, id) => {
          return business.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setbusinesslinelistSelectOptions(business);
        requestData = [...business];
        break;
      case "campaign":
        let campaign = [];
        lookup.map((data, id) => {
          return campaign.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setcampaignlinelistSelectOptions(campaign);
        requestData = [...campaign];
        break;
      case "currency":
        let currency = [];
        lookup.map((data, id) => {
          return currency.push({
            value: lookup[id].code,
            label: lookup[id].code,
          });
        });
        setcurrencylinelistSelectOptions(currency);
        requestData = [...currency];
        break;
      case "responsibility_centers":
        let responsibilityCenters = lookup.map((data) => {
          return {
            value: data.code,
            label: data.name,
          };
        });
        setResponsibilityCenters(responsibilityCenters);
        requestData = [...responsibilityCenters];

        return responsibilityCenters;
        break;

      default:
        requestData = [];
        break;
    }
    return requestData;
  };

  const getCreateRequest = async (token) => {
    let { data } = await getCreatePaymentRequest(token);

    let paymentMethod = data.fields.header.preferred_pmt_method;
    let request_type = data.fields.header.request_type;
    let request_code = data.fields.lines.request_code;
    let department_code = data.fields.header.shortcut_dimension_1_code;
    let project_code = data.fields.lines.shortcut_dimension_2_code;
    let Linedepartment_code = data.fields.lines.shortcut_dimension_1_code;
    let Lineproject_code = data.fields.header.shortcut_dimension_2_code;
    let customer_group = data.fields.lines.shortcut_dim_code_x_005_b_3_x_005_d_;
    let area = data.fields.lines.shortcut_dim_code_x_005_b_4_x_005_d_;
    let business_group = data.fields.lines.shortcut_dim_code_x_005_b_5_x_005_d_;
    let sales_campaign = data.fields.lines.shortcut_dim_code_x_005_b_6_x_005_d_;
    let currency_code = data.fields.header.currency_code;

    let paymentData = data;
    let pushedLines = [];
    Object.keys(paymentData.fields.lines).map((datas, id) => {
      paymentData.fields.lines[datas].enabled
        ? paymentData.fields.lines[datas].enabled.pages.create
          ? (pushedLines[datas] = "")
          : null
        : null;
    });

    let ChangedLine = { ...pushedLines };
    let MobilehangedLine = { ...pushedLines };
    var formLines = [{ ...ChangedLine }];
    var MobileformLines = [{ ...MobilehangedLine }];

    //format drop down
    let requests = setSelectOptions("request_type", request_type.lookup);

    let currencycodes = setSelectOptions("currency_code", currency_code.lookup);

    let preferredmethod = setSelectOptions(
      "preferred_pmt_method",
      paymentMethod.lookup
    );

    let deparments = setSelectOptions("deparments", department_code.lookup);

    let projects = setSelectOptions("projects", project_code.lookup);

    let request = setSelectOptions("request_code", request_code.lookup);

    let deparmentsline = setSelectOptions(
      "deparmentsline",
      department_code.lookup
    );

    let projectsline = setSelectOptions("projectsline", project_code.lookup);

    let customergroup = setSelectOptions(
      "customergroup",
      customer_group.lookup
    );

    let areas = setSelectOptions("areas", area.lookup);

    let business = setSelectOptions("business", business_group.lookup);

    let campaign = setSelectOptions("campaign", sales_campaign.lookup);

    let currency = setSelectOptions("currency", currency_code.lookup);

    setSelectOptions(
      "responsibility_centers",
      data.fields.header.responsibility_center.lookup
    );

    setpreferredPmtMethod(paymentMethod);
    setrequestType(request_type);
    setRequestCode(request_code);
    setDepartment(department_code);
    setProject(project_code);
    setLineDepartment(Linedepartment_code);
    setLineProject(Lineproject_code);
    setcustomerGroup(customer_group);
    setArea(area);
    setbusinessGroup(business_group);
    setsalesCampaign(sales_campaign);
    setcurrencyCode(currency_code);
    setpaymentRequestFields(paymentData);
    setFormLine(formLines);
    setPaymentLines(formLines);
    setnewLines(ChangedLine);
    setNewPaymentLines(MobileformLines);
    return data;
  };

  const postPaymentRequest = async (id, data, token) => {
    try {
      return await editPaymentRequest(id, data, token);
    } catch (error) {
      throw error;
    }
  };

  const prepPaymentApplicationFieldsForUpdate = async (paymentReqId, token) => {
    try {
      setLoading(true);

      let { data } = await initPaymentApplicationFieldsForUpdate(
        paymentReqId,
        token
      );

      const {
        fields: { header, lines },
        payment_request,
      } = data;

      let paymentMethod = header.preferred_pmt_method;
      let request_type = header.request_type;
      let request_code = lines.request_code;
      let department_code = header.shortcut_dimension_1_code;
      let project_code = lines.shortcut_dimension_2_code;
      let Linedepartment_code = lines.shortcut_dimension_1_code;
      let Lineproject_code = header.shortcut_dimension_2_code;
      let customer_group = lines.shortcut_dim_code_x_005_b_3_x_005_d_;
      let area = lines.shortcut_dim_code_x_005_b_4_x_005_d_;
      let business_group = lines.shortcut_dim_code_x_005_b_5_x_005_d_;
      let sales_campaign = lines.shortcut_dim_code_x_005_b_6_x_005_d_;
      let currency_code = header.currency_code;

      let pushedLines = [];
      Object.keys(lines).map((datas, id) => {
        lines[datas].enabled
          ? lines[datas].enabled.pages.create
            ? (pushedLines[datas] = "")
            : null
          : null;
      });

      let ChangedLine = { ...pushedLines };
      let MobilehangedLine = { ...pushedLines };
      var formLines = [ChangedLine];
      var MobileformLines = [MobilehangedLine];

      let newLines = { ...ChangedLine };
      let lineKeys = [];
      let newKeys = [];

      Object.keys(pushedLines).map((data, id) => {
        lineKeys.push(data);
      });

      if (payment_request.lines.length) {
        payment_request.lines.map((data, line) => {
          Object.keys(data).map((field, id) => {
            if (field === "id") {
              newLines["id"] = payment_request.lines[line][field];
            }
            if (lineKeys.includes(field)) {
              newLines[field] = payment_request.lines[line][field];
            }
          });

          newKeys.push({ ...newLines });
        });
        setPaymentLines(newKeys);
        newLines = null;
      } else {
        setPaymentLines(formLines);
      }

      let requests = setSelectOptions("request_type", request_type.lookup);

      let request_option = [];
      if (requests) {
        request_option = requests.filter(function (item) {
          return item.value == data.payment_request.request_type;
        });
      }
      setRequestOption(request_option);

      let currencycodes = setSelectOptions(
        "currency_code",
        currency_code.lookup
      );
      let currency_option = [];
      if (currencycodes) {
        currency_option = currencycodes.filter(function (item) {
          return item.value == data.payment_request.currency_code;
        });
      }
      setCurrencyOption(currency_option);

      let preferredmethod = setSelectOptions(
        "preferred_pmt_method",
        paymentMethod.lookup
      );
      let preferred_method = [];
      if (preferredmethod) {
        preferred_method = preferredmethod.filter(function (item) {
          return item.value == data.payment_request.preferred_pmt_method;
        });
      }
      setPreferredmethodOption(preferred_method);

      let deparments = setSelectOptions("deparments", department_code.lookup);
      let deparment = [];
      if (deparments) {
        deparment = deparments.filter(function (item) {
          return item.value == data.payment_request.shortcut_dimension_1_code;
        });
      }
      setDeparmentOption(deparment);

      let projects = setSelectOptions("projects", project_code.lookup);
      let project = [];
      if (projects) {
        project = deparments.filter(function (item) {
          return item.value == data.payment_request.shortcut_dimension_2_code;
        });
      }
      setProjectOption(project);

      let request = setSelectOptions("request_code", request_code.lookup);
      let deparmentsline = setSelectOptions(
        "deparmentsline",
        department_code.lookup
      );
      let projectsline = setSelectOptions("projectsline", project_code.lookup);
      let customergroup = setSelectOptions(
        "customergroup",
        customer_group.lookup
      );
      let areas = setSelectOptions("areas", area.lookup);
      let business = setSelectOptions("business", business_group.lookup);
      let currency = setSelectOptions("currency", currency_code.lookup);
      let campaign = setSelectOptions("campaign", sales_campaign.lookup);

      let centers = setSelectOptions(
        "responsibility_centers",
        header.responsibility_center.lookup
      );

      setRespCenterOption(
        centers.filter((c) => {
          return c.value == data.payment_request.responsibility_center;
        })
      );

      setpreferredPmtMethod(paymentMethod);
      setrequestType(request_type);
      setRequestCode(request_code);
      setDepartment(department_code);
      setProject(project_code);
      setLineDepartment(Linedepartment_code);
      setLineProject(Lineproject_code);
      setcustomerGroup(customer_group);
      setArea(area);
      setbusinessGroup(business_group);
      setsalesCampaign(sales_campaign);
      setcurrencyCode(currency_code);
      setpaymentRequestFields(data);
      setFormLine(formLines);
      setnewLines(ChangedLine);
      setNewPaymentLines(MobileformLines);
      setRespCenter(header.responsibility_center);

      setLoading(false);
      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }

      setErrors([error.response.body.message]);
    }
  };

  const syncAll = async (token) => {
    let { message } = await refreshAllPaymentRequests(token);
    let data = await fetchPaymentRequest(token);
    return message;
  };

  return (
    <PaymentRequestContext.Provider
      value={{
        paymentRequests,
        fetchPaymentRequest,
        createRequest,
        getCreateRequest,
        requestType,
        preferredPmtMethod,
        requestCode,
        department,
        project,
        lineDepartment,
        lineProject,
        customerGroup,
        area,
        businessGroup,
        salesCampaign,
        currencyCode,
        formLine,
        paymentRequestFields,
        paymentLines,
        setPaymentLines,
        newLines,
        filteredPaymentRequests,
        setFilteredPaymentRequests,
        errors,
        loading,
        setErrors,
        prepPaymentApplicationFieldsForUpdate,
        setLoading,
        postPaymentRequest,
        newPaymentLines,
        setNewPaymentLines,
        requestlistSelectOptions,
        currencylistSelectOptions,
        paymentlistSelectOptions,
        deparmentslistSelectOptions,
        projectlistSelectOptions,
        requestCodelistSelectOptions,
        deparmentlinelistSelectOptions,
        projectlinelistSelectOptions,
        setprojectlinelistSelectOptions,
        customergrouplistSelectOptions,
        arealistSelectOptions,
        businesslistSelectOptions,
        campaignlistSelectOptions,
        currencylineSelectOptions,
        syncAll,
        requestOption,
        setRequestOption,
        setCurrencyOption,
        currencyOption,
        setPreferredmethodOption,
        preferredmethodOption,
        setDeparmentOption,
        deparmentOption,
        setProjectOption,
        projectOption,
        setRequestCodeSelectValue,
        requestCodeSelectValue,
        responsibilityCenters,
        respCenterOption,
        setRespCenterOption,
        respCenter,
      }}
      {...props}
    />
  );
}

function usePaymentRequest() {
  const context = useContext(PaymentRequestContext);

  if (!context) {
    throw new Error(
      `usePaymentRequest must be used within a PaymentRequestProvider`
    );
  }

  return context;
}

export { PaymentRequestProvider, usePaymentRequest };
