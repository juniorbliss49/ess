import React, { useEffect } from "react";
import { getTenant } from "../utils/apiClient";
import { Plugins } from "@capacitor/core";

const { Storage } = Plugins;

const TenantContext = React.createContext(undefined);

function TenantProvider(props: any) {
  const [tenant, setTenant] = React.useState(null);
  const [hasTenant, setHasTenant] = React.useState(null);

  async function getTenantFromStorage() {
    const storedTenant = await Storage.get({ key: "__tenant__" });
    setTenant(JSON.parse(storedTenant.value));
    setHasTenant(!!storedTenant.value);
  }

  useEffect(() => {
    if (!tenant) {
      getTenantFromStorage();
    }
  }, [tenant]);

  const saveTenant = (code: string) =>
    getTenant(code).then(async ({ data }: any) => {
      try {
        setTenant(data);
        await Storage.set({
          key: "__tenant__",
          value: JSON.stringify(data),
        });
      } catch (error) {
        // console.log(error);
      }
    });

  const clearTenant = async () => {
    try {
      setTenant(null);
      await Storage.remove({ key: "__tenant__" });
      setHasTenant(false);
    } catch (error) {
      setTenant(null);
      setHasTenant(false);
      // console.log(error);
    }
  };

  return (
    <TenantContext.Provider
      value={{
        tenant,
        saveTenant,
        clearTenant,
        setTenant,
        hasTenant,
        setHasTenant,
        getTenantFromStorage,
      }}
      {...props}
    />
  );
}

function useTenant() {
  const context = React.useContext(TenantContext);

  if (!context) {
    throw new Error(`useTenant must be used within a TenantProvider`);
  }

  return context;
}

export { TenantProvider, useTenant };
