import React, { useEffect, useState, useContext } from "react";
import {
  getAllLoanApplications,
  createLoanApplication,
  initLoanApplicationFields,
  initLoanApplicationFieldsForUpdate,
  syncLoanRequests,
} from "../utils/apiClient";
import { useAuth } from "./AuthContext";

const LoanApplicationContext = React.createContext(null);

function LoanApplicationProvider(props: any) {
  const { logOutUser } = useAuth();

  // Loan Applications
  const [loanApplications, setLoanApplications] = useState(null);
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState([]);
  const [currentlyEditedLoanRequest, setCurrentlyEditedLoanRequest] = useState(
    {}
  );

  const [payrollEdList, setpayrollEdList] = useState(null);
  const [department, setDepartment] = useState(null);
  const [project, setProject] = useState(null);
  const [customerGroup, setcustomerGroup] = useState(null);
  const [area, setArea] = useState(null);
  const [businessGroup, setbusinessGroup] = useState(null);
  const [salesCampaign, setsalesCampaign] = useState(null);
  const [preferredPmtMethod, setpreferredPmtMethod] = useState(null);
  const [loanEdlist, setloanEd] = useState(null);
  const [globaldimension1code, setGlobaldimension1code] = useState(null);
  const [globaldimension2code, setGlobaldimension2code] = useState(null);
  const [loanApplicationFields, setLoanApplicationFields] = useState(null);
  const [loanEdlistSelectOptions, setloanEdlistSelectOptions] = useState(null);
  const [
    preferredPmtMethodSelectOptions,
    setpreferredPmtMethodSelectOptions,
  ] = useState(null);
  const [
    globalDimensionCodeSelectOptions,
    setglobalDimensionCodeSelectOptions,
  ] = useState(null);
  const [
    globalDimension2CodeSelectOptions,
    setglobalDimension2CodeSelectOptions,
  ] = useState(null);
  const [preferredmethodOption, setPreferredmethodOption] = useState(null);
  const [loanEdOption, setLoanEdOption] = useState(null);
  const [globaldimension1codeOption, setGlobaldimension1codeOption] = useState(
    null
  );
  const [globaldimension2codeOption, setGlobaldimension2codeOption] = useState(
    null
  );

  const fetchLoanApplications = async (token) => {
    try {
      // setLoanApplications(null);
      setLoading(true);
      let { data } = await getAllLoanApplications(token);
      setLoanApplications(data.reverse());
      setLoading(false);
      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }

      setErrors([error.response.body.message]);
    }
  };

  const setSelectOptions = (type, lookup) => {
    let requestData = [];

    switch (type) {
      case "loan_e_d":
        let loanEd = [];
        lookup.map((data, id) => {
          return loanEd.push({
            value: lookup[id].ed_code,
            label: lookup[id].description,
          });
        });
        setloanEdlistSelectOptions(loanEd);
        requestData = [...loanEd];
        break;
      case "preferred_pmt_method":
        let preferredPmtMethod = [];
        Object.keys(lookup).map((data, id) => {
          return preferredPmtMethod.push({
            value: lookup[data],
            label: data,
          });
        });
        setpreferredPmtMethodSelectOptions(preferredPmtMethod);
        requestData = [...preferredPmtMethod];
        break;
      case "global_dimension_1_code":
        let globalDimension1Code = [];
        lookup.map((data, id) => {
          return globalDimension1Code.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setglobalDimensionCodeSelectOptions(globalDimension1Code);
        requestData = [...globalDimension1Code];
        break;
      case "global_dimension_2_code":
        let globalDimension2Code = [];
        lookup.map((data, id) => {
          return globalDimension2Code.push({
            value: lookup[id].code,
            label: lookup[id].name,
          });
        });
        setglobalDimension2CodeSelectOptions(globalDimension2Code);
        requestData = [...globalDimension2Code];
        break;

      default:
        requestData = [];
        break;
    }
    return requestData;
  };

  const prepLoanApplicationFields = async (token) => {
    try {
      setLoanApplicationFields(null);

      setLoading(true);
      let { data } = await initLoanApplicationFields(token);

      const {
        fields: { card },
      } = data;

      let loanEd = setSelectOptions("loan_e_d", card.loan_e_d.lookup);
      let preferred_pmt_method = setSelectOptions(
        "preferred_pmt_method",
        card.preferred_pmt_method.lookup
      );
      let globaldimension1code = setSelectOptions(
        "global_dimension_1_code",
        card.global_dimension_1_code.lookup
      );
      let globaldimension2code = setSelectOptions(
        "global_dimension_2_code",
        card.global_dimension_2_code.lookup
      );

      setLoanApplicationFields(data);
      setpreferredPmtMethod(card.preferred_pmt_method);
      setloanEd(card.loan_e_d);
      setGlobaldimension1code(card.global_dimension_1_code);
      setGlobaldimension2code(card.global_dimension_2_code);

      setLoading(false);

      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }

      setErrors([error.response.body.message]);
    }
  };

  const prepLoanApplicationFieldsForUpdate = async (loanReqId, token) => {
    try {
      setLoanApplicationFields(null);

      setLoading(true);

      let { data } = await initLoanApplicationFieldsForUpdate(loanReqId, token);

      const {
        fields: { card },
        loan_application,
      } = data;

      let loanEd = setSelectOptions("loan_e_d", card.loan_e_d.lookup);
      let loan_ed = [];
      if (loanEd) {
        loan_ed = loanEd.filter(function (item) {
          return item.value == loan_application.loan_e_d;
        });
      }
      setLoanEdOption(loan_ed);

      let preferred_pmt_method = setSelectOptions(
        "preferred_pmt_method",
        card.preferred_pmt_method.lookup
      );
      let preferred_method = [];
      if (preferred_pmt_method) {
        preferred_method = preferred_pmt_method.filter(function (item) {
          return item.value == loan_application.preferred_pmt_method;
        });
      }
      setPreferredmethodOption(preferred_method);

      let globaldimension1code = setSelectOptions(
        "global_dimension_1_code",
        card.global_dimension_1_code.lookup
      );
      let globaldimension_1_code = [];
      if (globaldimension1code) {
        globaldimension_1_code = globaldimension1code.filter(function (item) {
          return item.value == loan_application.global_dimension_1_code;
        });
      }
      setGlobaldimension1codeOption(globaldimension_1_code);

      let globaldimension2code = setSelectOptions(
        "global_dimension_2_code",
        card.global_dimension_2_code.lookup
      );
      let globaldimension_2_code = [];
      if (globaldimension2code) {
        globaldimension_2_code = globaldimension2code.filter(function (item) {
          return item.value == loan_application.global_dimension_2_code;
        });
      }
      setGlobaldimension2codeOption(globaldimension_2_code);

      setLoanApplicationFields(data);
      setpreferredPmtMethod(card.preferred_pmt_method);
      setloanEd(card.loan_e_d);
      setGlobaldimension1code(card.global_dimension_1_code);
      setGlobaldimension2code(card.global_dimension_2_code);

      setCurrentlyEditedLoanRequest(loan_application);

      setLoading(false);

      return data;
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setErrors(["Something went wrong"]);
        return;
      }

      setErrors([error.response.body.message]);
    }
  };

  const syncLoan = async (token) => {
    let { message } = await syncLoanRequests(token);
    let { data } = await getAllLoanApplications(token);
    setLoanApplications(data);
    return data;
  };

  return (
    <LoanApplicationContext.Provider
      value={{
        loading,
        errors,
        fetchLoanApplications,
        loanApplications,
        prepLoanApplicationFields,
        payrollEdList,
        department,
        project,
        customerGroup,
        area,
        businessGroup,
        salesCampaign,
        preferredPmtMethod,
        loanApplicationFields,
        loanEdlist,
        globaldimension1code,
        globaldimension2code,
        currentlyEditedLoanRequest,
        setCurrentlyEditedLoanRequest,
        prepLoanApplicationFieldsForUpdate,
        loanEdlistSelectOptions,
        syncLoan,
        globalDimensionCodeSelectOptions,
        preferredPmtMethodSelectOptions,
        globalDimension2CodeSelectOptions,
        preferredmethodOption,
        loanEdOption,
        globaldimension1codeOption,
        globaldimension2codeOption,
      }}
      {...props}
    />
  );
}

function useLoanApplication() {
  const context = useContext(LoanApplicationContext);

  if (!context) {
    throw new Error(
      `useLoanApplication must be used within a LoanApplicationProvider`
    );
  }

  return context;
}

export { LoanApplicationProvider, useLoanApplication };
