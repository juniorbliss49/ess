import React, { useContext, useState, useEffect } from "react";
import {
  getDashboardPaymentRequest,
  getDashboardLoan,
  getDashboardAnnualLeave,
  getDashboardEmployeeQuery
} from "../utils/apiClient";

const HomeContext = React.createContext(undefined);

function HomeProvider(props: any) {
  const [paymentRequest, setPaymentRequest] = useState(null);
  const [loan, setLoan] = useState(null);
  const [annualLeave, setAnnualLeave] = useState(null);
  const [employeeQuery, setEmployeeQuery] = useState(null);

  const getPaymentRequest = async (token: any) => {
    return await getDashboardPaymentRequest(token);
  };

  const getLoanRequest = async (token: any) => {
    return await getDashboardLoan(token);
  };

  const getAnnualLeave = async (token: any) => {
    return await getDashboardAnnualLeave(token);
  };

  const getEmployeeQuery = async (token: any) => {
    return await getDashboardEmployeeQuery(token);
  };

  const getDashboard = async (token: any) => {
    return Promise.all([
      getPaymentRequest(token),
      getLoanRequest(token),
      getAnnualLeave(token),
      getEmployeeQuery(token)
    ]);
    // .then((values)=>{
    //     console.log(values)
    // })
  };

  return (
    <HomeContext.Provider
      value={{
        getDashboard
      }}
      {...props}
    />
  );
}

function useHomeApplication() {
  const context = useContext(HomeContext);

  if (!context) {
    throw new Error(
      `useHomeApplication must be used within a HomeApplicationProvider`
    );
  }

  return context;
}

export { HomeProvider, useHomeApplication };
