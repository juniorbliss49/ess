import React, { useState, useContext } from "react";

const UtilityContext = React.createContext(undefined);

function UtilityProvider(props: any) {
  const [modal, setModal] = useState(false);
  const [updatemodal, setUpdateModal] = useState(false);
  const [syncmodal, setSyncModal] = useState(false);
  const [printmodal, setPrintModal] = useState(false);

  const changeModal = data => {
    setModal(data);
  };
  const changeUpdateModal = data => {
    setUpdateModal(data);
  };

  const changeSyncModal = data => {
    setSyncModal(data);
  };

  const changePrintModal = data => {
    setPrintModal(data);
  };

  return (
    <UtilityContext.Provider
      value={{
        modal,
        updatemodal,
        changeUpdateModal,
        changeModal,
        syncmodal,
        changeSyncModal,
        changePrintModal,
        printmodal
      }}
      {...props}
    />
  );
}

function useUtility() {
  const context = useContext(UtilityContext);

  if (!context) {
    throw new Error(`useUtility must be used within a UtilityProvider`);
  }

  return context;
}

export { UtilityProvider, useUtility };
