import React, { useState, useContext, useEffect } from "react";
import { login, logout, getUser } from "../utils/apiClient";
import { Plugins } from "@capacitor/core";

const { Storage } = Plugins;

const AuthContext = React.createContext(undefined);

function AuthProvider(props: any) {
  const [token, setToken] = useState(null);
  const [user, setUser] = useState(null);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [subMenuToggle, setSubToggle] = useState(false);
  const [subMenuContent, setMenuContent] = useState(null);
  const [sideSubMenuToggle, setSideSubMenuContent] = useState(true);

  useEffect(() => {
    if (!user) {
      loadUser();
    }
  }, [user]);

  const changeMenu = () => {
    setToggle(!toggle);
  };

  const changeSubMenu = (data) => {
    if (subMenuToggle) {
      if (data == subMenuContent) {
        setSubToggle(false);
        return;
      }
      setMenuContent(data);
    } else {
      setMenuContent(data);
      setSubToggle(true);
    }
  };

  const changeToggleSubMenu = () => {
    setSubToggle(!subMenuToggle);
  };

  const changeSideSubMenu = () => {
    setSideSubMenuContent(!sideSubMenuToggle);
  };

  const changeMenuContent = (data) => {
    setMenuContent(data);
  };

  async function loadUser() {
    try {
      const storedToken = await Storage.get({ key: "__token__" });
      if (!storedToken) throw new Error();
      const { access_token } = JSON.parse(storedToken.value);
      const { user } = await getUser(access_token);
      setToken(access_token);
      setUser(user);
      setIsAuthenticated(true);
    } catch (error) {
      setToken(null);
      setUser(null);
      setIsAuthenticated(false);
    }
  }

  const loginUser = async (cred) => {
    const tokenInfo = await login(cred);
    await Storage.set({ key: "__token__", value: JSON.stringify(tokenInfo) });
    const { access_token } = tokenInfo;
    setToken(access_token);
    const { user } = await getUser(access_token);
    setUser(user);
    setIsAuthenticated(true);
  };

  const logOutUser = async () => {
    try {
      const storedToken = await Storage.get({ key: "__token__" });
      if (!storedToken) throw new Error();
      const { access_token } = JSON.parse(storedToken.value);
      await Storage.remove({ key: "__token__" });
      await logout(access_token);
      setToken(null);
      setUser(null);
      setIsAuthenticated(false);
      window.location.reload();
    } catch (error) {
      setToken(null);
      setUser(null);
      setIsAuthenticated(false);
      window.location.reload();
    }
  };

  return (
    <AuthContext.Provider
      value={{
        user,
        token,
        loginUser,
        logOutUser,
        loadUser,
        isAuthenticated,
        changeMenu,
        toggle,
        changeSubMenu,
        subMenuToggle,
        subMenuContent,
        changeMenuContent,
        changeSideSubMenu,
        sideSubMenuToggle,
        changeToggleSubMenu,
      }}
      {...props}
    />
  );
}

function useAuth() {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error(`useAuth must be used within a AuthProvider`);
  }

  return context;
}

export { AuthProvider, useAuth };
