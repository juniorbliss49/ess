import React from "react";
import { IonContent, IonGrid, IonRow } from "@ionic/react";
import MenuTopHeader from "./components/MenuTopHeader";
import { useAuth } from "./context/AuthContext";
import SidebarMenu from "./pages/Layout/SidebarMenu";
import Notification from "./pages/Layout/Notification";

function AuthenticatedWrapper(props) {
  const { toggle, subMenuToggle, sideSubMenuToggle } = useAuth();
  return (
    <>
      <MenuTopHeader />
      <IonContent>
        {toggle ? <SidebarMenu /> : ""}
        {subMenuToggle ? <Notification /> : ""}
        <IonGrid>
          <IonRow>{props.childern}</IonRow>
        </IonGrid>
      </IonContent>
    </>
  );
}

export default AuthenticatedWrapper;
