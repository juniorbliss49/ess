import {
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonContent,
  IonDatetime,
  IonFab,
  IonFabButton,
  IonGrid,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonRow,
  IonListHeader,
  IonSpinner,
  IonToast,
  IonLoading,
} from "@ionic/react";
import React, { useState, useEffect } from "react";
import { Plugins } from "@capacitor/core";
import { isPlatform } from "@ionic/core";
import { appPages } from "../../declarations";
import "../../styles/payslip.scss";
import { useAuth } from "../../context/AuthContext";
import { usePayslip } from "../../context/PayslipContext";
import { useTenant } from "../../context/TenantContext";
import AuthWrapper from "../Layout/AuthWrapper";

const appPage: appPages = {
  name: "Payslip",
};

const { Browser } = Plugins;

function Payslip() {
  const [isFetching, setIsFetching] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errMsg, setErrMsg] = useState("");
  const [downloadingPdf, setDownloadingPdf] = useState(false);

  const { tenant } = useTenant();

  const { token, logOutUser } = useAuth();

  const {
    payslip,
    period,
    setPeriod,
    netPay,
    fetchPayslip,
    fetchPayslipPdf,
    totalEarnings,
    totalDeductions,
  } = usePayslip();

  const _handleToastDismissed = () => {
    setShowError(false);
    setErrMsg("");
  };

  // useEffect(() => {
  //   if (!payslip) {
  //     setIsFetching(true);
  //     fetchPayslip(period, token)
  //       .then((_) => {
  //         setIsFetching(false);
  //       })
  //       .catch((error) => {
  //         setIsFetching(false);
  //         // setShowError(true);

  //         if (error && error.status == 401) {
  //           logOutUser();
  //           return;
  //         }

  //         if (!error.response) {
  //           setErrMsg("Network Error, Check Your Internet Connection");
  //           return;
  //         }

  //         if (!error.response.body) {
  //           setErrMsg("Something went wrong !!!");
  //           return;
  //         }

  //         if (error.response.body.message) {
  //           setErrMsg(error.response.body.message);
  //           return;
  //         }
  //       });
  //   }
  // }, [period]);

  const _updatePeriod = (period) => {
    // will parse date by using substring,
    // it would be better if a date library (moment) was used
    // however the overhead of using a library for this particular
    // use case has been considered, so lets do this for now
    const payslipPeriod = period.substring(0, 7);
    setPeriod(payslipPeriod);
  };

  const _handlePayslipFetch = async () => {
    try {
      setIsFetching(true);
      await fetchPayslip(period, token);
      setIsFetching(false);
    } catch (error) {
      setIsFetching(false);
      // setShowError(true);
      // console.log(error);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setErrMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setErrMsg("Something went wrong !!!");
        return;
      }

      setErrMsg(error.response.body.message);
    }
  };

  const _handlePayslipPdfDownload = async () => {
    // console.log("fetching pdf payslip");

    let companyCode = tenant.company.company_code;

    try {
      setDownloadingPdf(true);
      const { fileUrl } = await fetchPayslipPdf(period, companyCode, token);
      await Browser.open({ url: fileUrl });
      setDownloadingPdf(false);
    } catch (error) {
      console.log(error);
      setDownloadingPdf(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setErrMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setErrMsg("Something went wrong !!!");
        return;
      }

      setErrMsg(error.response.body.message);
    }
  };

  return (
    <>
      {isPlatform(window, "mobile") ? (
        <IonFab
          color="favorite"
          vertical="bottom"
          horizontal="end"
          className="custom-gradient ion-hide-lg-up"
          slot="fixed"
          style={{ zIndex: "9999" }}
        >
          <IonFabButton color="favorite" onClick={_handlePayslipPdfDownload}>
            <IonIcon name="download" />
          </IonFabButton>
        </IonFab>
      ) : null}

      <AuthWrapper
        name={appPage.name}
        bg_name="payslip-bg"
        backBtnName="Payslip"
      >
        <IonRow>
          <IonCol
            sizeMd="8"
            sizeSm="11"
            sizeXs="11"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <IonToast
              isOpen={!!errMsg}
              message={errMsg}
              duration={3000}
              onDidDismiss={_handleToastDismissed}
            />

            <IonLoading
              isOpen={downloadingPdf || isFetching}
              onDidDismiss={() => null}
              message="Please wait..."
            />

            <IonGrid class="bg-border">
              <IonRow
                class="ion-hide-lg-up"
                style={{ display: "flex", alignItems: "center" }}
              >
                <IonCol size="10">
                  <IonList>
                    <IonItem>
                      <IonLabel color="favorite" class="bold">
                        Select Period
                      </IonLabel>
                      <IonDatetime
                        min={`${tenant.payrollSettings.start_year}-${tenant.payrollSettings.start_month}`}
                        max={`${tenant.payrollSettings.end_year}-${tenant.payrollSettings.end_month}`}
                        displayFormat="MMMM YYYY"
                        placeholder="Select Period"
                        value={period}
                        onIonChange={(e) => _updatePeriod(e.detail.value)}
                      />
                    </IonItem>
                  </IonList>
                </IonCol>
                <IonCol size="1">
                  <IonButton
                    color="favorite"
                    disabled={isFetching}
                    onClick={_handlePayslipFetch}
                  >
                    <IonIcon slot="icon-only" name="arrow-forward"></IonIcon>
                  </IonButton>
                </IonCol>
              </IonRow>

              {payslip ? (
                <IonRow>
                  <IonCol>
                    <IonCard class="payslip-bg1 payslip">
                      <IonCardHeader>
                        <IonCardSubtitle>Net Pay </IonCardSubtitle>
                        <IonCardTitle class="bold">
                          {netPay ? "₦" + netPay.amount : null}
                        </IonCardTitle>
                      </IonCardHeader>
                    </IonCard>
                  </IonCol>
                  <IonCol>
                    <IonCard class="payslip-bg2 payslip">
                      <IonCardHeader>
                        <IonCardSubtitle>Total Earnings</IonCardSubtitle>
                        <IonCardTitle class="bold">
                          {totalEarnings ? "₦" + totalEarnings.amount : null}
                        </IonCardTitle>
                      </IonCardHeader>
                    </IonCard>
                  </IonCol>
                  <IonCol>
                    <IonCard class="payslip-bg3 payslip">
                      <IonCardHeader>
                        <IonCardSubtitle>Total Deductions</IonCardSubtitle>
                        <IonCardTitle class="bold">
                          {totalDeductions
                            ? "₦" + totalDeductions.amount
                            : null}
                        </IonCardTitle>
                      </IonCardHeader>
                    </IonCard>
                  </IonCol>
                </IonRow>
              ) : null}

              <br />
              <br />
              <IonRow>
                <IonCol class="ion-hide-md-down" sizeLg="5">
                  <IonList class="custom-payslip">
                    <IonItem>
                      <IonLabel color="favorite" class="bold">
                        Select Period
                      </IonLabel>
                      <IonDatetime
                        min={`${tenant.payrollSettings.start_year}-${tenant.payrollSettings.start_month}`}
                        max={`${tenant.payrollSettings.end_year}-${tenant.payrollSettings.end_month}`}
                        displayFormat="MMMM YYYY"
                        placeholder="Select Period"
                        value={period}
                        // onIonChange={e => setPeriod(e.detail.value)}
                        onIonChange={(e) => _updatePeriod(e.detail.value)}
                      />
                    </IonItem>

                    <IonRow>
                      <IonCol size="12">
                        <IonRow>
                          <IonCol size="6">
                            <IonButton
                              disabled={isFetching}
                              shape="round"
                              expand="full"
                              onClick={_handlePayslipFetch}
                              class="custom-gradient custom-payslip-btn"
                            >
                              {isFetching ? (
                                <IonSpinner
                                  name="crescent"
                                  color="light"
                                  slot="end"
                                />
                              ) : (
                                <IonIcon name="eye" class="custom-login-icon" />
                              )}
                              View
                            </IonButton>
                          </IonCol>
                          <IonCol size="6">
                            <IonButton
                              disabled={downloadingPdf}
                              shape="round"
                              expand="full"
                              onClick={_handlePayslipPdfDownload}
                              class="custom-gradient custom-payslip-btn"
                            >
                              {downloadingPdf ? (
                                <IonSpinner
                                  name="crescent"
                                  color="light"
                                  slot="end"
                                />
                              ) : (
                                <IonIcon
                                  name="download"
                                  class="custom-login-icon"
                                />
                              )}
                              Download
                            </IonButton>
                          </IonCol>
                        </IonRow>
                      </IonCol>
                    </IonRow>
                    {/* <br />
                <br /> */}
                  </IonList>
                </IonCol>
                {payslip ? (
                  <IonCol sizeLg="7" sizeSm="12">
                    <IonList>
                      {payslip.Earning ? (
                        <>
                          <IonListHeader>
                            <IonLabel>Earnings</IonLabel>
                          </IonListHeader>
                          {payslip.Earning.map((entry, index) => (
                            <IonItem key={index}>
                              <span className="faint-color">
                                <b>{entry.payslip_text}</b>
                              </span>

                              <IonLabel slot="end" color="favorite">
                                <div className="ion-text-end">
                                  <b>₦ {entry.amount}</b>
                                </div>
                              </IonLabel>
                            </IonItem>
                          ))}
                        </>
                      ) : null}

                      {payslip.Deduction ? (
                        <>
                          <IonListHeader>
                            <IonLabel>Deductions</IonLabel>
                          </IonListHeader>
                          {payslip.Deduction.map((entry, index) => (
                            <IonItem key={index}>
                              <span className="faint-color">
                                <b>{entry.payslip_text}</b>
                              </span>

                              <IonLabel slot="end" color="favorite">
                                <div className="ion-text-end">
                                  <b>₦ {entry.amount}</b>
                                </div>
                              </IonLabel>
                            </IonItem>
                          ))}
                        </>
                      ) : null}

                      {payslip.Other ? (
                        <>
                          <IonListHeader>
                            <IonLabel>Other</IonLabel>
                          </IonListHeader>
                          {payslip.Other.map((entry, index) => (
                            <IonItem key={index}>
                              <span className="faint-color">
                                <b>{entry.payslip_text}</b>
                              </span>

                              <IonLabel slot="end" color="favorite">
                                <div className="ion-text-end">
                                  <b>₦ {entry.amount}</b>
                                </div>
                              </IonLabel>
                            </IonItem>
                          ))}
                        </>
                      ) : null}
                    </IonList>
                  </IonCol>
                ) : null}
              </IonRow>
            </IonGrid>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default Payslip;
