import React from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import { IonRow, IonCol, IonButton, IonImg } from "@ionic/react";
import "../../styles/approval-entry.css";

function ApprovalEntry() {
    return(
        <>
            <AuthWrapper name="Approval Entry" bg_name="form-bg-img-2" backBtnName="Leave Application">
                <IonRow className="ion-hide-md-down">
                    <IonCol size="10" offset="1">
                        <IonRow className="hide-wrapper">
                            <IonCol>
                                <div className="ion-float-right">
                                    <IonButton className="custom-btn-2" size="default" style={{marginRight:"20px" }}>
                                        <span className="custom-img-btn">
                                            <IonImg src="/assets/images/create_24px_outlined.svg" />
                                        </span>
                                        Edit 
                                    </IonButton>
                                    <IonButton className="custom-btn-2" size="default" style={{marginRight:"20px" }}>
                                        <span className="custom-img-btn">
                                            <IonImg src="/assets/images/thumb_up_24px_outlined.svg" />
                                        </span>
                                        Approve
                                    </IonButton>
                                    <IonButton className="custom-btn-2" size="default" style={{marginRight:"20px" }}>
                                        <span className="custom-img-btn">
                                            <IonImg src="/assets/images/sync_24px_outlined.svg" />
                                        </span>
                                        Quick Sync 
                                    </IonButton>
                                    <IonButton className="custom-btn-2" size="default" style={{marginRight:"-1px" }}>
                                        <span className="custom-img-btn">
                                           <IonImg src="/assets/images/filter_tilt_shift_24px_outlined.svg" />
                                           
                                        </span>
                                        Sync All 
                                    </IonButton>
                                </div>
                            </IonCol>
                        </IonRow>
                        <br/>
                        <IonRow className="approval-entry-border">
                            <IonCol size="12" className="no-padding">
                                <div className="approval-entry-header no-padding">
                                    <p style={{color: "#fff", fontWeight: "bold"}}>Approval Entry Details</p>
                                </div>
                            </IonCol>
                            <IonCol size="6" className="custom-padding-approval">
                                <table className="approvals-entry">
                                   <tbody>
                                       <tr>
                                           <td>
                                               <b>User ID:</b> Ajanlekoko Brown
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Tenant ID:</b> Testportal-limited-DTYXQ-6kDhn
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Table ID:</b> 60200
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Limit Type:</b> Limit
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Approval Type:</b> Approval Type
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Document Type:</b> Document Type
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Document No:</b> Document No
                                           </td>
                                       </tr>
                                   </tbody>
                               </table>
                            </IonCol>
                            <IonCol size="6" className="custom-padding-approval">
                                <table className="approvals-entry">
                                   
                                   <tbody>
                                       <tr>
                                           <td>
                                               <b>User ID:</b> Ajanlekoko Brown
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Tenant ID:</b> Testportal-limited-DTYXQ-6kDhn
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Table ID:</b> 60200
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Limit Type:</b> Limit
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Approval Type:</b> Approval Type
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Document Type:</b> Document Type
                                           </td>
                                       </tr>
                                       <tr>
                                           <td>
                                               <b>Document No:</b> Document No
                                           </td>
                                       </tr>
                                   </tbody>
                               </table>
                            </IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
                <IonRow className="ion-hide-lg-up">
                    <IonCol>
                        <div className="mobile-approval-entry">
                            <table >
                                <thead>
                                    <tr>
                                        <th>Approval Entry Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <b>User ID:</b> Ajanlekoko Brown
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Tenant ID:</b> Testportal-limited-DTYXQ-6kDhn
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Table ID:</b> 60200
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Limit Type:</b> Limit
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Approval Type:</b> Approval Type
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Document Type:</b> Document Type
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Document No:</b> Document No
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </IonCol>
                </IonRow>
            </AuthWrapper>
        </>
    )
}

export default ApprovalEntry;