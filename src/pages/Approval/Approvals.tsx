import React from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import { IonRow, IonCol, IonButton, IonIcon, IonImg } from "@ionic/react";
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemPanel,
    AccordionItemButton
} from 'react-accessible-accordion';    
import "../../styles/approvals.css";
import 'react-accessible-accordion/dist/fancy-example.css';

function Approvals({history}) {
    return(
        <>
            <AuthWrapper name="Approvals" bg_name="form-bg-img-2" backBtnName="Leave Application">
                <IonRow className="ion-hide-md-down">
                    <IonCol size="10" offset="1">
                        <IonRow className="hide-wrapper">
                            <IonCol>
                                <div className="ion-float-right">
                                    <IonButton className="custom-btn-2" size="default" style={{marginRight:"20px" }}>
                                        <span className="custom-img-btn">
                                            <IonImg src="/assets/images/sync_24px_outlined.svg" />
                                        </span>
                                        Quick Sync 
                                    </IonButton>
                                    <IonButton className="custom-btn-2" size="default" style={{marginRight:"-1px" }}>
                                        <span className="custom-img-btn">
                                           <IonImg src="/assets/images/filter_tilt_shift_24px_outlined.svg" />
                                           
                                        </span>
                                        Sync All 
                                    </IonButton>
                                </div>
                            </IonCol>
                        </IonRow>
                        <br/>
                        <IonRow >
                            <IonCol>
                                <table className="approvals-table">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Limit Type</th>
                                            <th>Sender</th>
                                            <th>Approval Type</th>
                                            <th>Doc Type</th>
                                            <th>Doc No</th>
                                            <th>Amount LCY</th>
                                            <th>Sent At</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Opeyemi Temitope</td>
                                            <td>Limit Type</td>
                                            <td>GEMS\O.MATTI</td>
                                            <td>Approval Type</td>
                                            <td>A document</td>
                                            <td>EMP-10225</td>
                                            <td>Amount LCY</td>
                                            <td>Mon, Jul 22, 2019</td>
                                            <td>Approved</td>
                                            <td>
                                                <IonButton className="custom-view-btn" size="small" onClick={()=>history.push('/approval-entry')} >
                                                    <span>
                                                    <IonIcon name="eye" />
                                                    </span>
                                                </IonButton>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <br/><br/>
                                </table>
                            </IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
                <IonRow className="ion-hide-lg-up">
                    <IonCol size="12">
                        <IonRow>
                            <IonCol>
                                <Accordion>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Doc No: EMP-10225
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>User ID:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Sender:</b> GEMS\O.MATTI</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Status:</b> Approved</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Doc No: EMP-10229
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>User ID:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Sender:</b> GEMS\O.MATTI</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Status:</b> Approved</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Doc No: EMP-10230
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>User ID:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Sender:</b> GEMS\O.MATTI</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Status:</b> Approved</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                                Doc No: EMP-10231
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>User ID:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Sender:</b> GEMS\O.MATTI</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Status:</b> Approved</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                </Accordion>
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="4" offset="4" className="ion-text-center">
                                <IonButton className="history-btn">
                                    Load More...
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
            </AuthWrapper>
        </>
    )
}

export default Approvals;