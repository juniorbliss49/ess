import React, { useState, useEffect } from "react";
import { IonSpinner } from "@ionic/react";
import { useTenant } from "../../context/TenantContext";
import AuthAction from "./AuthAction";

function TenantDesktop(props) {
  const {
    clearTenant,
    saveTenant,
    tenant,
    setHasTenant,
    hasTenant,
  } = useTenant();
  const [fetchingTenant, setFetchingTenant] = useState(false);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    // if (!tenant) {
    let companyCode = location.hostname.split(".")[0];
    handleTenancy(companyCode);
    // }
  }, []);

  async function handleTenancy(code) {
    try {
      setFetchingTenant(true);
      await clearTenant();
      await saveTenant(code);
      setHasTenant(true);
      setFetchingTenant(false);
    } catch (error) {
      setFetchingTenant(false);
      setHasError(true);
    }
  }

  if (hasError) {
    return <div>An Error Occured</div>;
  }

  if (fetchingTenant) {
    return <IonSpinner />;
  }

  return <AuthAction {...props} />;
}

export default TenantDesktop;
