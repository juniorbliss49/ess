import React, { useState } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonButton,
  IonImg,
  IonIcon,
  IonText,
} from "@ionic/react";
import "../../styles/tenant-confirmation.css";

import { useTenant } from "../../context/TenantContext";
import AuthAction from "./AuthAction";

function TenantConfirmation(props) {
  const [tenantConfirmed, setTenantConfirmed] = useState(false);
  const { tenant, clearTenant, setHasTenant } = useTenant();

  const _cancel = () => {
    clearTenant();
    props.history.replace("/");
  };

  const _handleTenantConfirmation = () => {
    setHasTenant(true);
  };

  // if (tenantConfirmed) {
  //   return <AuthAction {...props} />;
  // }

  const companyName = tenant ? tenant.company.company_name : null;

  return (
    <>
      <IonContent class="bg-img">
        <IonGrid>
          <br />
          <br />
          {/* <IonRow>
            <IonCol class="ion-text-left">
              <IonButton
                color="custom-confirmation"
                class="custom-confirmation"
                shape="round"
              >
                Sign In
              </IonButton>
            </IonCol>
            <IonCol class="ion-text-right">
              <IonButton
                color="custom-confirmation"
                class="custom-confirmation"
                shape="round"
              >
                Sign Up
              </IonButton>
            </IonCol>
          </IonRow> */}
          <IonRow>
            <IonCol
              size="4"
              sizeSm="4"
              offsetSm="4"
              offset="4"
              class="center ion-text-center"
            >
              <IonImg src="assets/images/logo.png" class="logo center" />
            </IonCol>
          </IonRow>
          <br />
          <br />
          <br />
          <br />
          <br />
          <br />
          <IonRow>
            <IonCol size="8" offset="2" class="center">
              <h4 className="briefcase">Confirm Organisation</h4>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol size="10" offset="1" class="center">
              <IonIcon name="ios-business" class="custom-confirmation-icon" />
              <div className="custom-confirmation-text">
                <h1>
                  <IonText>
                    <b>{companyName}</b>
                  </IonText>
                </h1>
              </div>
            </IonCol>
          </IonRow>
          <br />
          <IonRow>
            <IonCol size="8" offset="2">
              <IonRow>
                <IonCol class="ion-text-center">
                  <IonButton
                    color="custom-confirmation-btn-2"
                    class="custom-confirmation-btn-2"
                    shape="round"
                    onClick={_cancel}
                  >
                    <IonIcon name="close" />
                  </IonButton>
                </IonCol>
                <IonCol class="ion-text-center">
                  <IonButton
                    color="custom-confirmation-btn"
                    shape="round"
                    class="custom-confirmation-btn"
                    onClick={_handleTenantConfirmation}
                  >
                    <IonIcon name="checkmark" />
                  </IonButton>
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </>
  );
}
export default TenantConfirmation;
