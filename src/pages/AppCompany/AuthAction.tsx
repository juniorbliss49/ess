import React, { useState } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonImg,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonText,
} from "@ionic/react";
import "../../styles/auth-action.css";
import { useTenant } from "../../context/TenantContext";

import CompanyCodeForm from "./CompanyCodeForm";

function AuthAction(props) {
  const { clearTenant } = useTenant();
  const [tenantIsCleared, setTenantIsCleared] = useState(false);

  const changeTenant = () => {
    clearTenant();
    setTenantIsCleared(true);
  };

  if (tenantIsCleared) return <CompanyCodeForm {...props} />;

  return (
    <>
      <IonContent class="options-bg">
        <IonGrid>
          <br />
          <br />
          <br />
          {/* <br />
          <br /> */}
          <IonRow className="ion-justify-content-center">
            <IonCol size="6" class="ion-text-center">
              <IonImg
                src="assets/images/logo.png"
                class="logo center custom-logo-btn custom-option-logo ion-text-center"
              />
            </IonCol>
          </IonRow>
          <br />
          <br />
          <br />
          {/* <br />
          <br /> */}
          <IonRow class="custom-option-page">
            <IonCol
              size="12"
              sizeLg="7"
              class="ion-text-center"
              color="favorite"
            >
              <h2>
                <IonText color="favorite" class="custom-options-text">
                  Get Started
                </IonText>
              </h2>
            </IonCol>
            <IonCol size="12" sizeLg="5">
              <IonRow>
                <IonCol size="6">
                  <IonCard
                    button
                    onClick={() => props.history.push("login")}
                    class="ion-padding custom-option-color"
                  >
                    <IonCardHeader
                      class="custom-color action-card"
                      style={{ padding: "15px" }}
                    >
                      <IonImg
                        src="assets/images/web-log-in.png"
                        class="home-img"
                      />
                      <IonCardTitle class="bold">Sign In</IonCardTitle>
                    </IonCardHeader>
                  </IonCard>
                </IonCol>
                <IonCol size="6">
                  <IonCard
                    button
                    onClick={() => props.history.push("register")}
                    class="ion-padding custom-option-color"
                  >
                    <IonCardHeader
                      class="custom-color action-card"
                      style={{ padding: "15px" }}
                    >
                      <IonImg
                        src="assets/images/external-link-symbol.png"
                        class="home-img"
                      />
                      <IonCardTitle class="bold">Sign Up</IonCardTitle>
                    </IonCardHeader>
                  </IonCard>
                </IonCol>
                <IonCol size="6">
                  <IonCard
                    button
                    onClick={changeTenant}
                    class="ion-padding custom-option-color"
                  >
                    <IonCardHeader
                      class="custom-color action-card"
                      style={{ padding: "15px" }}
                    >
                      <IonImg
                        src="assets/images/couple-of-arrows-changing-places.png"
                        class="home-img"
                      />
                      <IonCardTitle class="bold">Change Company</IonCardTitle>
                    </IonCardHeader>
                  </IonCard>
                </IonCol>
                <IonCol size="6">
                  <IonCard
                    button
                    onClick={() => props.history.push("reset-password")}
                    class="ion-padding custom-option-color"
                  >
                    <IonCardHeader
                      class="custom-color action-card"
                      style={{ padding: "15px" }}
                    >
                      <IonImg
                        src="assets/images/settings.png"
                        class="home-img"
                      />
                      <IonCardTitle class="bold">Reset Password</IonCardTitle>
                    </IonCardHeader>
                  </IonCard>
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </>
  );
}

export default AuthAction;
