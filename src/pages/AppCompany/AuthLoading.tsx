import React, { useState } from "react";

import { isPlatform } from "@ionic/core";

import { useTenant } from "../../context/TenantContext";
import CompanyCodeForm from "./CompanyCodeForm";
import AuthAction from "./AuthAction";
import TenantDesktop from "./TenantDesktop";

function AuthLoading(props) {
  const [isDesktop, setIsDesktop] = useState(() => {
    return isPlatform(window, "desktop");
  });

  const { hasTenant } = useTenant();

  if (isDesktop) {
    return <TenantDesktop {...props} />;
  }

  if (hasTenant) {
    return <AuthAction {...props} />;
  } else {
    return <CompanyCodeForm {...props} />;
  }
}

export default AuthLoading;
