import React, { useState } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonInput,
  IonIcon,
  IonButton,
  IonImg,
  IonSpinner,
  IonToast,
} from "@ionic/react";
import "../../styles/company-code-form.scss";
import { useTenant } from "../../context/TenantContext";
import TenantConfirmation from "./TenantConfirmation";

function CompanyCodeForm(props) {
  const [loading, setLoading] = useState(false);
  const [companyCode, setCompanyCode] = useState("");
  const [showError, setShowError] = useState(false);
  const [errMsg, setErrMsg] = useState("");

  const { saveTenant, clearTenant, tenant } = useTenant();

  const _handleInput = (e) => setCompanyCode(e.target.value);

  const _handleToastDismissed = () => {
    setShowError(false);
    setErrMsg("");
  };

  const _handleSubmit = () => {
    setLoading(true);
    saveTenant(companyCode)
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        if (!err.response) {
          setErrMsg("Something went wrong");
        }

        if (err.response.body.message) {
          setErrMsg(err.response.body.message);
        }

        setShowError(true);
        setLoading(false);
        clearTenant();
      });
  };

  if (tenant) {
    return <TenantConfirmation {...props} />;
  }

  return (
    <>
      <IonContent class="bg-img">
        <IonToast
          isOpen={showError}
          message={errMsg}
          duration={3000}
          onDidDismiss={_handleToastDismissed}
        />

        <IonGrid>
          <br />
          <br />
          <br />
          <br />
          <br />
          <IonRow class="ion-align-items-center ion-justify-content-center">
            <IonCol size="6" style={{ margin: "auto" }}>
              <IonImg src="assets/images/logo.png" class="logo" />
            </IonCol>
          </IonRow>
          <IonRow className="ion-justify-content-center">
            <IonCol size="6" class="center">
              <h2 style={{ whiteSpace: "nowrap" }} className="briefcase">
                Welcome
              </h2>
            </IonCol>
          </IonRow>
          <br />
          <IonRow className="ion-justify-content-center">
            <IonCol size="10" sizeMd="8" sizeXl="6" class="input-form">
              <IonInput
                placeholder="Enter your Company Code"
                clear-input
                onInput={_handleInput}
                value={companyCode}
              >
                <IonIcon name="briefcase" class="briefcase" />
              </IonInput>
            </IonCol>
          </IonRow>
          <br />
          <IonRow className="ion-justify-content-center">
            <IonCol size="4" class="center">
              <IonButton color="btnColor" shape="round" onClick={_handleSubmit}>
                {loading ? (
                  <IonSpinner name="crescent" color="primary" />
                ) : (
                  <IonIcon name="arrow-round-forward" />
                )}
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </>
  );
}

export default CompanyCodeForm;
