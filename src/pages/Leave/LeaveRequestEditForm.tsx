import React, { useState, useEffect } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonItem,
  IonLabel,
  IonDatetime,
  IonInput,
  IonIcon,
  IonToggle,
  IonSelect,
  IonSelectOption,
  IonSpinner,
  IonToast,
  IonLoading,
} from "@ionic/react";

import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";
import { Redirect } from "react-router";
import Select from "react-select";

function LeaveRequestEditForm({ history, match }) {
  const {
    updateCurrentLeaveApplication,
    prepFieldsForNonAnnualNewLeaveApplicationForUpdate,
    leaveApplicationFields,
    causeofAbsenceCode,
    reliefNo,
    loading,
    errors,
    causeofAbsenceCodeSelectOptions,
    reliefNoSelectOptions,
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  const [leaveRequest, setLeaveRequest] = useState(null);
  const [submitting, setSubmitting] = useState(false);
  const [requestWasUpdated, setRequestWasUpdated] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");

  useEffect(() => {
    prepFieldsForNonAnnualNewLeaveApplicationForUpdate(
      match.params.reqId,
      token
    )
      .then(({ leave_application }) => {
        setLeaveRequest(leave_application);
      })
      .catch((error) => {
        if (error && error.status == 401) {
          logOutUser();
          return;
        }
      });
  }, []);

  const handleFormUpdate = (field, value) => {
    setLeaveRequest({ ...leaveRequest, [field]: value });
  };

  const submitForm = async (e) => {
    const { id, ...formData } = leaveRequest;

    setSubmitting(true);

    try {
      const { message, error } = await updateCurrentLeaveApplication(
        id,
        formData,
        token
      );

      if (error) {
        throw Error(message);
      }

      // setTimeout(() => {
      //   setSubmitting(false);
      //   setRequestWasUpdated(true);
      // }, 2000);

      history.replace("/leave-requests", { message });
      // setToastMsg("Leave Application Created");
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      const {
        response: { body },
      } = error;

      setToastMsg(body.message);

      body.message && setFormErrors([body.message]);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  return (
    <>
      <AuthWrapper
        name="Edit Leave Request"
        bg_name="form-bg-img"
        backBtnName="Leave Application"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={loading || submitting}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {formErrors.length ? (
          <IonRow>
            <ul>
              {formErrors.map((error, i) => (
                <li key={i}>{error}</li>
              ))}
            </ul>
          </IonRow>
        ) : null}

        {loading ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : (
          <IonRow className="ion-justify-content-center">
            {leaveApplicationFields ? (
              <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
                <IonRow className="ion-padding form-bg form-border">
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.from_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {leaveApplicationFields.fields.card.from_date.mapping}
                          :
                        </IonLabel>
                        <IonDatetime
                          displayFormat="DD/MM/YYYY"
                          onIonChange={(e) =>
                            handleFormUpdate("from_date", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.from_date : null}
                        ></IonDatetime>
                      </IonItem>
                    </IonCol>
                  ) : null}

                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.to_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {leaveApplicationFields.fields.card.to_date.mapping}:
                        </IonLabel>
                        <IonDatetime
                          displayFormat="DD/MM/YYYY"
                          onIonChange={(e) =>
                            handleFormUpdate("to_date", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.to_date : null}
                        ></IonDatetime>
                      </IonItem>
                    </IonCol>
                  ) : null}

                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.quantity.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {leaveApplicationFields.fields.card.quantity.mapping}:
                        </IonLabel>
                        <IonInput
                          type="number"
                          onIonChange={(e) =>
                            handleFormUpdate("quantity", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.quantity : null}
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}

                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.quantity_base.enabled.pages.update ? (
                    <>
                      <IonCol sizeXs="12" sizeMd="4">
                        <div>
                          <p className="form-p">
                            {
                              leaveApplicationFields.fields.card.quantity_base
                                .mapping
                            }
                          </p>
                          <IonItem className="form-border-bottom">
                            <IonInput
                              type="number"
                              onIonChange={(e) =>
                                handleFormUpdate(
                                  "quantity_base",
                                  e.detail.value
                                )
                              }
                              value={
                                leaveRequest ? leaveRequest.quantity_base : null
                              }
                            ></IonInput>
                          </IonItem>
                        </div>
                      </IonCol>
                    </>
                  ) : null}
                </IonRow>
                <br />
                <br />
                <br />
                <IonRow className="ion-padding form-bg form-img">
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.employee_no.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card.employee_no
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            type="text"
                            onIonChange={(e) =>
                              handleFormUpdate("employee_no", e.detail.value)
                            }
                            value={
                              leaveRequest ? leaveRequest.employee_no : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.employee_name.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card.employee_name
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            onIonChange={(e) =>
                              handleFormUpdate("employee_name", e.detail.value)
                            }
                            value={
                              leaveRequest ? leaveRequest.employee_name : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}

                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.resumption_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card.resumption_date
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            type="text"
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "resumption_date",
                                e.detail.value
                              )
                            }
                            value={
                              leaveRequest ? leaveRequest.resumption_date : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.cause_of_absence_code.enabled.pages
                      .update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card
                              .cause_of_absence_code.mapping
                          }
                        </p>
                        {/* <IonItem className="form-border-bottom"> */}
                        {leaveRequest ? (
                          <Select
                            onChange={(e) =>
                              handleFormUpdate("cause_of_absence_code", e.value)
                            }
                            options={causeofAbsenceCodeSelectOptions}
                            value={causeofAbsenceCodeSelectOptions.filter(
                              (item) =>
                                item.value == leaveRequest.cause_of_absence_code
                            )}
                          />
                        ) : null}
                        {/* <IonSelect
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={e =>
                            handleFormUpdate(
                              "cause_of_absence_code",
                              e.detail.value
                            )
                          }
                          value={
                            leaveRequest
                              ? leaveRequest.cause_of_absence_code
                              : null
                          }
                          className="request_select_placeholder"
                          placeholder="Select Cause of Absence"
                        >
                          {causeofAbsenceCode
                            ? Object.keys(causeofAbsenceCode).map(
                                (causeofAbsence_code, id) => {
                                  return (
                                    <IonSelectOption
                                      key={id}
                                      value={
                                        causeofAbsenceCode[causeofAbsence_code]
                                          .code
                                      }
                                    >
                                      {
                                        causeofAbsenceCode[causeofAbsence_code]
                                          .description
                                      }
                                    </IonSelectOption>
                                  );
                                }
                              )
                            : null}
                        </IonSelect> */}
                        {/* </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.description.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card.description
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("description", e.detail.value)
                            }
                            value={
                              leaveRequest ? leaveRequest.description : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.relief_no.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {leaveApplicationFields.fields.card.relief_no.mapping}
                        </p>
                        {leaveRequest ? (
                          <Select
                            onChange={(e) =>
                              handleFormUpdate("relief_no", e.value)
                            }
                            options={reliefNoSelectOptions}
                            value={reliefNoSelectOptions.filter(
                              (item) => item.value == leaveRequest.relief_no
                            )}
                          />
                        ) : null}
                        {/* <IonItem className="form-border-bottom"> */}
                        {/* <IonSelect
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={e =>
                            handleFormUpdate("relief_no", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.relief_no : null}
                          className="request_select_placeholder"
                          placeholder="Select Reliever"
                        >
                          {reliefNo
                            ? Object.keys(reliefNo).map((relief_no, id) => {
                                return (
                                  <IonSelectOption
                                    key={id}
                                    value={reliefNo[relief_no].no}
                                  >
                                    {reliefNo[relief_no].first_name +
                                      " " +
                                      reliefNo[relief_no].last_name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect> */}
                        {/* </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.unit_of_measure_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card
                              .unit_of_measure_code.mapping
                          }
                          :
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "unit_of_measure_code",
                                e.detail.value
                              )
                            }
                            value={
                              leaveRequest
                                ? leaveRequest.unit_of_measure_code
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!leaveApplicationFields ? null : leaveApplicationFields
                      .fields.card.process_allowance_payment.enabled.pages
                      .update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            leaveApplicationFields.fields.card
                              .process_allowance_payment.mapping
                          }
                        </p>
                        <IonToggle
                          onIonChange={(e) =>
                            handleFormUpdate(
                              "process_allowance_payment",
                              e.detail.value
                            )
                          }
                          value={
                            leaveRequest
                              ? leaveRequest.process_allowance_payment
                              : null
                          }
                          color="customColor"
                        />
                      </div>
                    </IonCol>
                  ) : null}
                  <br />
                  <br />

                  <IonCol sizeXs="12" sizeMd="12">
                    <div className="ion-float-right">
                      <IonButton
                        className="custom-btn"
                        size="large"
                        onClick={submitForm}
                        disabled={submitting}
                      >
                        Update
                        <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                          {submitting ? (
                            <IonSpinner
                              name="crescent"
                              color="light"
                              slot="end"
                            />
                          ) : (
                            <IonIcon name="arrow-dropright" />
                          )}
                        </span>
                      </IonButton>
                      <IonButton
                        className="custom-cancel-btn"
                        size="large"
                        disabled={submitting}
                        onClick={() => history.goBack()}
                      >
                        Cancel
                        <span
                          style={{ marginTop: "-3px", marginLeft: "3px" }}
                        ></span>
                      </IonButton>
                    </div>
                  </IonCol>
                </IonRow>
              </IonCol>
            ) : errors ? (
              <IonCol size="4" offset="4" className="ion-text-center">
                {errors.map((error, i) => (
                  <div key={i}>{error}</div>
                ))}
              </IonCol>
            ) : null}
          </IonRow>
        )}
      </AuthWrapper>
    </>
  );
}

export default LeaveRequestEditForm;
