import React, { useState, useEffect } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonSpinner,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonIcon,
  IonText,
} from "@ionic/react";
import "../../styles/leave-application-history.css";

import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";

function LeaveApplicationHistory() {
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);

  const {
    archivedLeaveApplications,
    fetchArchivedLeaveApplications,
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  useEffect(() => {
    // if (!leaveApplications) {
    setLoading(true);
    try {
      fetchArchivedLeaveApplications(token);
      setLoading(false);
    } catch (error) {
      if (error && error.status == 401) {
        logOutUser();
        return;
      }
      console.log(error);
      setLoading(false);
      setError(error);
    }
    // }
  }, []);

  return (
    <>
      <AuthWrapper
        name="Leave History"
        bg_name="form-bg-img-2"
        backBtnName="Leave Application"
      >
        <IonRow>
          <IonCol
            sizeLg="10"
            offsetLg="1"
            sizeMd="10"
            offsetMd="1"
            sizeSm="10"
            offsetSm="1"
            sizeXs="12"
          >
            <IonRow className="ion-padding custom-margin-top">
              <IonCol size="12">
                {/* <div className="ion-float-right content-center">
                  <IonButton className="custom-history-btn custom-history-padding border-right-shape">
                    Open
                  </IonButton>
                  <IonButton className="custom-history-btn custom-history-padding-2">
                    Approved
                  </IonButton>
                  <IonButton className="custom-history-btn border-right-shape-3">
                    Declined
                  </IonButton>
                </div> */}
              </IonCol>
            </IonRow>
            <IonRow className="ion-padding card-history-main">
              {loading ? (
                <>
                  <IonCol size="4" offset="4" className="ion-text-center">
                    <IonSpinner color="primary" />
                  </IonCol>
                </>
              ) : null}
              {archivedLeaveApplications
                ? archivedLeaveApplications.map((data, i) => {
                    return (
                      <IonCol
                        key={i}
                        sizeLg="4"
                        sizeMd="4"
                        sizeSm="4"
                        sizeXs="12"
                      >
                        <IonCard className="card-history">
                          <IonCardHeader className="card-history-header">
                            <IonCardTitle>
                              {data.cause_of_absence_code}
                            </IonCardTitle>
                          </IonCardHeader>
                          <IonCardContent className="ion-text-center">
                            <h5>{data.status}</h5>
                            <h3>{data.document_no}</h3>
                            <div>
                              <span>
                                <IonIcon
                                  title="Days"
                                  style={{ cursor: "pointer" }}
                                  ios="briefcase"
                                  name="briefcase"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>{data.quantity} Days</IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Time"
                                  style={{ cursor: "pointer" }}
                                  ios="ios-time"
                                  name="ios-time"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>From: {data.from_date}</IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Time"
                                  style={{ cursor: "pointer" }}
                                  ios="ios-time"
                                  name="ios-time"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>To: {data.to_date}</IonText>
                              </span>
                            </div>
                          </IonCardContent>
                        </IonCard>
                      </IonCol>
                    );
                  })
                : null}
              {archivedLeaveApplications === null ||
              archivedLeaveApplications.length == 0 ? (
                <h3>No leave application history yet</h3>
              ) : null}
            </IonRow>
            {/* <IonRow>
              <IonCol size="4" offset="4" className="ion-text-center">
                <IonButton className="history-btn">Load More...</IonButton>
              </IonCol>
            </IonRow> */}
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveApplicationHistory;
