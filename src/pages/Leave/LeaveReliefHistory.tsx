import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonIcon,
  IonText,
  IonSegment,
  IonSegmentButton,
  IonLabel
} from "@ionic/react";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";

function LeaveReliefHistory({ history }) {
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);
  const [leaveRequestForActions, setLeaveRequestForActions] = useState(null);
  const [filteredLeaveApplications, setFilteredLeaveApplications] = useState(
    []
  );

  const [filterState, setFilterState] = useState("All");

  const {
    annualLeaveApplications,
    setCurrentlyEditedAnnualLeaveRequest,
    fetchReliefRequests,
    leaveReliefRequests
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  useEffect(() => {
    // if (!annualLeaveApplications) {
    try {
      fetchReliefRequests(token).then(data => {
        setFilteredLeaveApplications(data);
      });
    } catch (error) {
      if (error && error.status == 401) {
        logOutUser();
        return;
      }
    }
    // }
  }, []);

  const prepForActions = index => {
    if (
      leaveRequestForActions != null &&
      leaveRequestForActions.index == index
    ) {
      setLeaveRequestForActions(null);
      return;
    }

    let leaveRequest = filteredLeaveApplications[index];
    leaveRequest["index"] = index;
    if (
      leaveRequest.status == "" ||
      leaveRequest.status == null ||
      leaveRequest.status == "Open"
    ) {
      setLeaveRequestForActions(leaveRequest);
      return;
    }
  };

  const editLeaveApplication = index => {
    let leaveRequest = filteredLeaveApplications[index];

    if (
      leaveRequest.status == "" ||
      leaveRequest.status == null ||
      leaveRequest.status == "Open"
    ) {
      setCurrentlyEditedAnnualLeaveRequest(leaveRequest);
      history.push("/edit-annual-leave-application");
      return;
    }

    return;
  };

  const applyFilterToLeaveApplications = param => {
    setLeaveRequestForActions(null);

    setFilterState(param);

    setFilteredLeaveApplications(
      annualLeaveApplications &&
        annualLeaveApplications.filter(leaveApp => {
          if (param == "All") {
            return leaveApp;
          }

          if (param == "Open") {
            return (
              leaveApp.status == param ||
              leaveApp.status == "" ||
              leaveApp.status == null
            );
          }

          return leaveApp.status == param;
        })
    );
  };

  return (
    <>
      <AuthWrapper
        name="Annual Leave Applications"
        backBtnName="Home"
        bg_name="form-bg-img"
      >
        <IonRow>
          <IonCol size="10" offset="1">
            <IonRow className="ion-padding">
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <IonButton
                  className="create-btn"
                  onClick={() => history.push("/leave-application-form")}
                >
                  <IonIcon slot="start" name="add" />
                  New Application
                </IonButton>
                {leaveRequestForActions ? (
                  <>
                    <IonButton
                      color="medium"
                      className="create-btn"
                      onClick={() =>
                        editLeaveApplication(leaveRequestForActions.index)
                      }
                    >
                      <IonIcon slot="start" name="create" />
                      Edit
                    </IonButton>
                    <IonButton color="secondary" className="create-btn">
                      <IonIcon slot="start" name="send" />
                      Send for Approval
                    </IonButton>
                  </>
                ) : null}
              </IonCol>
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <div className="ion-float-right content-center">
                  {/* <IonButton className="custom-history-btn custom-history-padding border-right-shape">
                    Open
                  </IonButton>
                  <IonButton className="custom-history-btn custom-history-padding-2">
                    Pending
                  </IonButton>
                  <IonButton className="custom-history-btn border-right-shape-3">
                    Approved
                  </IonButton> */}
                  <IonSegment
                    onIonChange={e =>
                      applyFilterToLeaveApplications(e.detail.value)
                    }
                    value={filterState}
                  >
                    <IonSegmentButton
                      style={{ marginRight: "3px" }}
                      value="All"
                      className="custom-history-btn custom-history-padding border-right-shape"
                    >
                      <IonLabel style={{ color: "#fff" }}>All</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Open"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Open</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Pending"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Pending</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Approved"
                      className="custom-history-btn border-right-shape-3"
                    >
                      <IonLabel style={{ color: "#fff" }}>Approved</IonLabel>
                    </IonSegmentButton>
                  </IonSegment>
                </div>
              </IonCol>
            </IonRow>
            <br />
            <br />
            <IonRow className="ion-padding card-history-main">
              {leaveReliefRequests
                ? leaveReliefRequests.map((reliefRequest, i) => {
                    return (
                      <IonCol
                        key={i}
                        sizeLg="4"
                        sizeMd="4"
                        sizeSm="4"
                        sizeXs="12"
                      >
                        <IonCard
                          className="card-history"
                          style={
                            leaveRequestForActions &&
                            leaveRequestForActions.index == i
                              ? { border: "4px solid blue", cursor: "pointer" }
                              : { cursor: "pointer" }
                          }
                          onClick={() => prepForActions(i)}
                        >
                          <IonCardHeader className="card-history-header">
                            <IonCardTitle>
                              {reliefRequest.cause_of_absence_code}
                            </IonCardTitle>
                          </IonCardHeader>
                          <IonCardContent className="ion-text-center">
                            <h5>{reliefRequest.status}</h5>
                            <h3>{reliefRequest.portal_id}</h3>
                            <div>
                              <span>
                                <IonIcon
                                  title="Days"
                                  style={{ cursor: "pointer" }}
                                  ios="briefcase"
                                  name="briefcase"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>{reliefRequest.quantity} Days</IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Time"
                                  style={{ cursor: "pointer" }}
                                  ios="ios-time"
                                  name="ios-time"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>
                                  From: {reliefRequest.from_date}
                                </IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Time"
                                  style={{ cursor: "pointer" }}
                                  ios="ios-time"
                                  name="ios-time"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>To: {reliefRequest.to_date}</IonText>
                              </span>
                            </div>
                          </IonCardContent>
                        </IonCard>
                      </IonCol>
                    );
                  })
                : null}
            </IonRow>

            {/* <IonRow>
              <IonCol size="4" offset="4" className="ion-text-center">
                <IonButton className="history-btn">Load More...</IonButton>
              </IonCol>
            </IonRow> */}
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveReliefHistory;
