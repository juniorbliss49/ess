import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonIcon,
  IonText,
  IonSegment,
  IonSegmentButton,
  IonLabel,
  IonToast,
  IonLoading,
  IonActionSheet,
} from "@ionic/react";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";
import {
  syncLeaveRequests,
  syncLeaveApplication,
  requestApprovalForLeaveApplication,
  cancelApprovalRequestForLeaveApplication,
} from "../../utils/apiClient";
import { getPlatforms } from "@ionic/core";

function AnnualLeaveApplications({ history, location }) {
  const [leaveRequestForActions, setLeaveRequestForActions] = useState(null);
  const [filteredLeaveApplications, setFilteredLeaveApplications] = useState(
    []
  );

  const [filterState, setFilterState] = useState("All");
  const [processing, setProcessing] = useState(false);
  const [errors, setErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");
  const [mobile, setMobile] = useState(false);
  const [showActionSheet, setShowActionSheet] = useState(false);
  const [actionButtons, setActionButtons] = useState(null);

  const {
    fetchAnnualLeaveApplications,
    annualLeaveApplications,
    leaveReliefResponseIsMandated,
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  useEffect(() => {
    setProcessing(true);
    fetchAnnualLeaveApplications(token)
      .then((data) => {
        setFilteredLeaveApplications(data);
        setProcessing(false);
      })
      .catch((error) => {
        setProcessing(false);

        if (error && error.status == 401) {
          logOutUser();
          return;
        }

        if (!error.response) {
          setErrors(["Network Error, Check Your Internet Connection"]);
          return;
        }

        if (!error.response.body) {
          setErrors(["Something went wrong"]);
          return;
        }

        setErrors([error.response.body.message]);
      });
    let platformType = platform();
    if (
      platformType.includes("mobile") &&
      !platformType.includes("ipad") &&
      !platformType.includes("tablet")
    ) {
      setMobile(true);
    }
  }, []);

  const prepForActions = (index) => {
    setShowActionSheet(true);
    if (
      leaveRequestForActions != null &&
      leaveRequestForActions.index == index
    ) {
      setLeaveRequestForActions(null);
      return;
    }

    let leaveRequest = filteredLeaveApplications[index];
    leaveRequest["index"] = index;

    setLeaveRequestForActions(leaveRequest);
    buttonProperty(leaveRequest);
  };

  function platform() {
    return getPlatforms(window);
  }

  function buttonProperty(leaveRequest) {
    let button = [];

    if (leaveRequest.status == "Open") {
      button = [
        {
          text: "Edit",
          icon: "create",
          handler: () => {
            editLeaveApplication(leaveRequest.index);
          },
        },
        // {
        //   text: 'View More',
        //   icon: 'eye',
        //   handler: () => {
        //     setShowPaymentRequestModal(true)
        //   }
        // },
        {
          text: "Refresh Application",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            refreshLeaveRequest(leaveRequest.index);
          },
        },
        {
          text: "Request Approval",
          icon: "send",
          handler: () => {
            requestApproval(leaveRequest.index);
          },
        },
      ];
    } else if (leaveRequest.status == "Pending_Approval") {
      button = [
        // {
        //   text: 'View More',
        //   icon: 'eye',
        //   handler: () => {
        //     setShowPaymentRequestModal(true)
        //   }
        // },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            refreshLeaveRequest(leaveRequest.index);
          },
        },
        {
          text: "Cancel Approval Request",
          icon: "close",
          cssClass: "icon-danger",
          handler: () => {
            cancelApprovalRequest(leaveRequest.index);
          },
        },
      ];
    } else {
      button = [
        // {
        //   text: 'View More',
        //   icon: 'eye',
        //   handler: () => {
        //     setShowPaymentRequestModal(true)
        //   }
        // },
        {
          text: "Refresh",
          icon: "refresh",
          handler: () => {
            refreshLeaveRequest(leaveRequest.index);
          },
        },
      ];
    }
    setActionButtons(button);
  }

  const editLeaveApplication = (index) => {
    let leaveRequest = filteredLeaveApplications[index];

    if (leaveRequest.status != "Open") {
      return;
    }

    history.push(`/annual-leave-application/${leaveRequest.id}`);
  };

  const applyFilterToLeaveApplications = (param) => {
    setLeaveRequestForActions(null);

    setFilterState(param);

    setFilteredLeaveApplications(
      annualLeaveApplications &&
        annualLeaveApplications.filter((leaveApp) => {
          if (param == "All") {
            return leaveApp;
          }

          if (param == "Open") {
            return (
              leaveApp.status == param ||
              leaveApp.status == "" ||
              leaveApp.status == null
            );
          }

          return leaveApp.status == param;
        })
    );
  };

  async function requestApproval(index) {
    try {
      setProcessing(true);
      let leaveRequest = filteredLeaveApplications[index];

      let { message } = await requestApprovalForLeaveApplication(
        { leave_request_id: leaveRequest.id },
        token
      );

      let data = await fetchAnnualLeaveApplications(token);
      setFilteredLeaveApplications(data);
      setProcessing(false);

      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  }

  async function cancelApprovalRequest(index) {
    try {
      setProcessing(true);
      let leaveRequest = filteredLeaveApplications[index];

      let { message } = await cancelApprovalRequestForLeaveApplication(
        { leave_request_id: leaveRequest.id },
        token
      );

      let data = await fetchAnnualLeaveApplications(token);
      setFilteredLeaveApplications(data);
      setProcessing(false);

      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  }

  async function refreshLeaveRequest(index) {
    try {
      setProcessing(true);
      let leaveRequest = filteredLeaveApplications[index];

      let { message } = await syncLeaveApplication(
        { leave_request_id: leaveRequest.id },
        token
      );

      let data = await fetchAnnualLeaveApplications(token);
      setFilteredLeaveApplications(data);
      setProcessing(false);

      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  }

  async function _recallLeaveApplication(inde) {}

  async function _syncAll() {
    try {
      setProcessing(true);

      let { message } = await syncLeaveRequests(token);

      let data = await fetchAnnualLeaveApplications(token);
      setFilteredLeaveApplications(data);

      setProcessing(false);

      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  }

  const _clearLocationStateAndToast = () => {
    if (location.state && location.state.message) {
      location.state.message = "";
    }

    setToastMsg("");
  };

  return (
    <>
      <AuthWrapper
        name="Annual Leave Applications"
        backBtnName="Home"
        bg_name="form-bg-img"
      >
        {toastMsg || (location.state && location.state.message) ? (
          <IonToast
            isOpen={true}
            message={toastMsg || location.state.message}
            duration={3000}
            onDidDismiss={_clearLocationStateAndToast}
          />
        ) : null}

        {/* <IonLoading
          isOpen={processing}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        <IonRow>
          <IonCol
            sizeLg="10"
            offsetLg="1"
            sizeMd="10"
            offsetMd="1"
            sizeSm="10"
            offsetSm="1"
            sizeXs="12"
          >
            <IonRow className="ion-padding custom-sticky">
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                {/* <IonButton
                  title="New Application"
                  className="create-btn"
                  onClick={() => history.push("/annual-leave-application")}
                >
                  <IonIcon slot="start" name="add" />
                  New Application
                </IonButton> */}

                <IonButton
                  title="Sync All"
                  className="ribbon-color-btn"
                  onClick={_syncAll}
                >
                  <IonIcon slot="icon-only" name="sync" />
                </IonButton>

                {leaveRequestForActions ? (
                  <>
                    {mobile ? (
                      <>
                        <IonActionSheet
                          isOpen={showActionSheet}
                          onDidDismiss={() => {
                            setShowActionSheet(false);
                            setLeaveRequestForActions(null);
                          }}
                          buttons={[...actionButtons]}
                        ></IonActionSheet>
                      </>
                    ) : (
                      <>
                        {leaveRequestForActions.status == "Open" ? (
                          <IonButton
                            title="Edit"
                            className="ribbon-color-btn"
                            onClick={() =>
                              editLeaveApplication(leaveRequestForActions.index)
                            }
                          >
                            <IonIcon slot="icon-only" name="create" />
                          </IonButton>
                        ) : null}

                        {leaveRequestForActions.markings == "Posted" ? (
                          <IonButton
                            title="Refresh Application"
                            className="ribbon-color-btn"
                            onClick={() =>
                              refreshLeaveRequest(leaveRequestForActions.index)
                            }
                          >
                            <IonIcon slot="icon-only" name="refresh" />
                          </IonButton>
                        ) : null}

                        {leaveRequestForActions.status == "Open" &&
                        ((leaveRequestForActions.relief_response != null &&
                          leaveRequestForActions.relief_response != "" &&
                          leaveReliefResponseIsMandated) ||
                          !leaveReliefResponseIsMandated) ? (
                          <IonButton
                            title="Send for Approval"
                            className="ribbon-color-btn"
                            onClick={() =>
                              requestApproval(leaveRequestForActions.index)
                            }
                          >
                            <IonIcon slot="icon-only" name="send" />
                          </IonButton>
                        ) : null}

                        {leaveRequestForActions.status == "Pending_Approval" ? (
                          <IonButton
                            title="Cancel Approval Request"
                            color="danger"
                            className="create-btn"
                            onClick={() =>
                              cancelApprovalRequest(
                                leaveRequestForActions.index
                              )
                            }
                          >
                            <IonIcon slot="icon-only" name="close" />
                          </IonButton>
                        ) : null}
                      </>
                    )}
                  </>
                ) : null}
              </IonCol>
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <div
                  style={{ maxWidth: "100%" }}
                  className="ion-float-right content-center"
                >
                  <IonSegment
                    onIonChange={(e) =>
                      applyFilterToLeaveApplications(e.detail.value)
                    }
                    value={filterState}
                  >
                    <IonSegmentButton
                      style={{ marginRight: "3px" }}
                      value="All"
                      className="custom-history-btn custom-history-padding border-right-shape"
                    >
                      <IonLabel style={{ color: "#fff" }}>All</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Open"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Open</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Pending_Approval"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Pending</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Approved"
                      className="custom-history-btn border-right-shape-3"
                    >
                      <IonLabel style={{ color: "#fff" }}>Approved</IonLabel>
                    </IonSegmentButton>
                  </IonSegment>
                </div>
              </IonCol>
            </IonRow>

            {processing ? (
              <IonRow className="custom-sticky">
                <IonCol
                  sizeMd="8"
                  sizeSm="12"
                  sizeXs="12"
                  style={{ margin: "auto", textAlign: "center" }}
                >
                  <h6 color="blue">Please Wait...</h6>
                </IonCol>
              </IonRow>
            ) : null}

            <IonRow className="ion-padding card-history-main">
              {filteredLeaveApplications ? (
                filteredLeaveApplications.map((data, i) => (
                  <IonCol key={i} sizeLg="4" sizeMd="4" sizeSm="4" sizeXs="12">
                    <IonCard
                      className="card-history"
                      style={
                        leaveRequestForActions &&
                        leaveRequestForActions.index == i
                          ? { border: "4px solid #2c4d85", cursor: "pointer" }
                          : { cursor: "pointer" }
                      }
                      onClick={() => prepForActions(i)}
                    >
                      <IonCardHeader className="card-history-header">
                        <IonCardTitle>
                          {data.cause_of_absence_code}-
                        </IonCardTitle>
                      </IonCardHeader>
                      <IonCardContent className="ion-text-center">
                        <h5>{data.status}</h5>
                        <h3>{data.document_no}</h3>
                        <div>
                          <span>
                            <IonIcon
                              title="Days"
                              style={{ cursor: "pointer" }}
                              ios="briefcase"
                              name="briefcase"
                              // class="icon-style  icon-size"
                            />
                            <IonText>{data.quantity} Days</IonText>
                          </span>
                          <span>
                            <IonIcon
                              title="Time"
                              style={{ cursor: "pointer" }}
                              ios="ios-time"
                              name="ios-time"
                              // class="icon-style  icon-size"
                            />
                            <IonText>From: {data.from_date}</IonText>
                          </span>
                          <span>
                            <IonIcon
                              title="Time"
                              style={{ cursor: "pointer" }}
                              ios="ios-time"
                              name="ios-time"
                              // class="icon-style  icon-size"
                            />
                            <IonText>To: {data.to_date}</IonText>
                          </span>
                        </div>
                      </IonCardContent>
                    </IonCard>
                  </IonCol>
                ))
              ) : errors ? (
                <IonCol size="4" sizeXs="12" offsetXs="4" offset="4">
                  {errors.map((error, i) => (
                    <div key={i}>{error}</div>
                  ))}
                </IonCol>
              ) : null}
            </IonRow>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default AnnualLeaveApplications;
