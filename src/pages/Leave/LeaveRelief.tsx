import React, { useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonDatetime,
  IonButton,
  IonIcon,
  IonSpinner
} from "@ionic/react";

import { useAuth } from "../../context/AuthContext";

import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { Redirect } from "react-router";

function LeaveRelief({ history }) {
  const [loading, setLoading] = useState(null);
  const [errors, setErrors] = useState(null);
  const [submitting, setSubmitting] = useState(false);

  const { token, logOutUser } = useAuth();
  const [responseWasSent, setResponseWasSent] = useState(false);
  const [toastMsg, setToastMsg] = useState("");

  const {
    currentlyViewedReliefRequest,
    sendLeaveReliefResponse
  } = useLeaveApplication();

  const updateErrors = errors => {
    setErrors(Object.values(errors).flat());
  };

  const sendResponse = async response => {
    setSubmitting(true);

    try {
      const leaveApplication = await sendLeaveReliefResponse(
        currentlyViewedReliefRequest.id,
        { relief_response: response },
        token
      );

      if (leaveApplication.error) {
        throw Error(leaveApplication.message);
      }

      // setTimeout(() => {
      setSubmitting(false);
      setResponseWasSent(true);
      // }, 2000);
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      const {
        response: { body }
      } = error;

      setToastMsg(body.message);

      updateErrors(body.errors);
    }
  };

  if (responseWasSent) {
    return <Redirect to="/leave-relief-request" />;
  }

  return (
    <>
      <AuthWrapper
        name="Leave Relief Request"
        bg_name="form-bg-img-2"
        backBtnName="Leave Application"
      >
        <IonRow>
          <IonCol size="10" offset="1">
            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">Start Date</IonLabel>
                  <IonDatetime
                    displayFormat="DD/MM/YYYY"
                    disabled
                    value={currentlyViewedReliefRequest.from_date}
                  ></IonDatetime>
                </IonItem>
              </IonCol>
              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">End Date</IonLabel>
                  <IonDatetime
                    displayFormat="DD/MM/YYYY"
                    disabled
                    value={currentlyViewedReliefRequest.to_date}
                  ></IonDatetime>
                </IonItem>
              </IonCol>
              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">Quantity</IonLabel>
                  <IonInput
                    disabled
                    value={currentlyViewedReliefRequest.quantity}
                  ></IonInput>
                </IonItem>
              </IonCol>
            </IonRow>
            <br />
            <br />
            <br />
            <IonRow className="ion-padding form-bg form-img">
              <IonCol sizeXs="12" sizeMd="5">
                <p className="form-p">Cause of Absence</p>
                <IonItem className="form-border-bottom">
                  <IonInput
                    disabled
                    value={currentlyViewedReliefRequest.cause_of_absence_code}
                  ></IonInput>
                </IonItem>
                <br />
                <p className="form-p">Description</p>
                <IonItem className="form-border-bottom">
                  <IonInput
                    disabled
                    value={currentlyViewedReliefRequest.description}
                  ></IonInput>
                </IonItem>
              </IonCol>
              <IonCol sizeMd="2" />
              <IonCol sizeXs="12" sizeMd="5">
                <p className="form-p">Unit of Measure</p>
                <IonItem className="form-border-bottom">
                  <IonInput
                    disabled
                    value={currentlyViewedReliefRequest.unit_of_measure_code}
                  ></IonInput>
                </IonItem>
                <br />
                <p className="form-p">Requestor</p>
                <IonItem className="form-border-bottom">
                  <IonInput
                    disabled
                    value={currentlyViewedReliefRequest.employee_name}
                  ></IonInput>
                </IonItem>
                <br />
                <br />
                <div className="ion-float-right">
                  <span style={{ marginLeft: "20px" }}>
                    {submitting ? (
                      <IonSpinner name="crescent" color="primary" />
                    ) : null}
                  </span>
                  <IonButton
                    disabled={submitting}
                    className="custom-btn"
                    size="large"
                    onClick={() => sendResponse("ACCEPT")}
                  >
                    Accept
                  </IonButton>
                  <IonButton
                    disabled={submitting}
                    className="custom-cancel-btn"
                    size="large"
                    onClick={() => sendResponse("REJECT")}
                  >
                    Reject
                  </IonButton>
                </div>
              </IonCol>
            </IonRow>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveRelief;
