import React, { useEffect, useState } from "react";

import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonToggle,
  IonButton,
  IonIcon,
  IonDatetime,
  IonSelect,
  IonSelectOption,
  IonToast,
  IonSpinner,
  IonLoading,
} from "@ionic/react";
import "../../styles/leave-application-form.css";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";
import { Redirect } from "react-router";

function AnnualLeaveApplicationForm({ history }) {
  const [submitting, setSubmitting] = useState(false);

  const [quantity, setQuantity] = useState(null);
  const [startDate, setStartDate] = useState(() => {
    let today = new Date();

    let dd = today.getDate() as any;
    let mm = (today.getMonth() + 1) as any;
    let yyyy = today.getFullYear() as any;

    if (dd < 10) {
      dd = "0" + dd;
    }

    if (mm < 10) {
      mm = "0" + mm;
    }

    return `${yyyy}-${mm}-${dd}`;
  });
  const [causeofAbsence, setcauseofAbsence] = useState(null);
  const [description, setDescription] = useState(null);
  const [reliever, setReliever] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [employeeNo, setEmployeeNo] = useState(null);
  const [employeeName, setEmployeeName] = useState(null);
  const [quantityBase, setQuantityBase] = useState(null);
  const [resumptionDate, setResumptionDate] = useState(null);
  const [unitofMeasureCode, setUnitofMeasureCode] = useState(null);
  const [processAllowance, setProcessAllowance] = useState(false);

  const [requestWasCreated, setRequestWasCreated] = useState(false);

  const [errors, setErrors] = useState([]);
  const [formErrors, setFormErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");
  const [loading, setLoading] = useState(false);

  const {
    PostLeaveApplications,
    prepFieldsForAnnualNewLeaveApplication,
    annualLeaveApplicationFields,
    causeofAbsenceCode,
    reliefNo,
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  useEffect(() => {
    setLoading(true);
    prepFieldsForAnnualNewLeaveApplication(token)
      .then((_) => {
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);

        if (error && error.status == 401) {
          logOutUser();
          return;
        }

        if (!error.response) {
          setErrors(["Network Error, Check Your Internet Connection"]);
          return;
        }

        if (!error.response.body) {
          setErrors(["Something went wrong"]);
          return;
        }

        setErrors([errors]);
      });
  }, []);

  const handlecauseofAbsence = (e) => {
    setcauseofAbsence(e.target.value);
  };

  const handleDescription = (e) => {
    setDescription(e.target.value);
  };

  const handleReliever = (e) => {
    setReliever(e.target.value);
  };

  const handleProcessAllowance = (e) => {
    setProcessAllowance(e.target.checked);
  };

  const handleEmployeeNo = (e) => {
    setEmployeeNo(e.target.value);
  };

  const handleEmployeeName = (e) => {
    setEmployeeName(e.target.value);
  };

  const handleQuantityBase = (e) => {
    setQuantityBase(e.target.value);
  };

  const handleResumptionDate = (e) => {
    setResumptionDate(e.target.checked);
  };

  const handleUnitofMeasureCode = (e) => {
    setUnitofMeasureCode(e.target.value);
  };

  const submitForm = async (e) => {
    let formData = {
      cause_of_absence_code: causeofAbsence,
      from_date: startDate,
      description: description,
      quantity: quantity,
      relief_no: reliever,
      process_allowance_payment: processAllowance,
    };

    setSubmitting(true);

    try {
      const leaveApplication = await PostLeaveApplications(formData, token);

      if (leaveApplication.error) {
        throw Error(leaveApplication.message);
      }

      setTimeout(() => {
        setSubmitting(false);
        setRequestWasCreated(true);
      }, 2000);
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      const {
        response: { body },
      } = error;

      setToastMsg(body.message);

      body.message && setFormErrors([body.message]);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  const closeError = () => {
    setErrors([]);
  };

  if (requestWasCreated) {
    return <Redirect to="/annual-leave-application-history" />;
  }

  return (
    <>
      <AuthWrapper
        name="Annual Leave Application"
        bg_name="form-bg-img"
        backBtnName="Leave Application"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        <IonLoading
          isOpen={loading || submitting}
          onDidDismiss={() => null}
          message="Please wait..."
        />

        {formErrors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {errors.map((error, i) => (
                    <li key={i}>{error}</li>
                  ))}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

<IonRow className="ion-justify-content-center">
          {annualLeaveApplicationFields ? (
            <IonCol
              sizeMd="10"
              sizeXs="11"
              sizeSm="10"
            >
              <IonRow className="ion-padding form-bg form-border">
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.from_date.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          annualLeaveApplicationFields.fields.card.from_date
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonDatetime
                        displayFormat="DD/MM/YYYY"
                        onIonChange={(e) => setStartDate(e.detail.value)}
                        value={startDate}
                      ></IonDatetime>
                    </IonItem>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.to_date.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          annualLeaveApplicationFields.fields.card.to_date
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonDatetime
                        displayFormat="DD/MM/YYYY"
                        onIonChange={(e) => setEndDate(e.detail.value)}
                        value={endDate}
                      ></IonDatetime>
                    </IonItem>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.quantity.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          annualLeaveApplicationFields.fields.card.quantity
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="number"
                        onIonChange={(e) => setQuantity(e.detail.value)}
                        value={quantity}
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.quantity_base.enabled.pages.create ? (
                  <>
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            annualLeaveApplicationFields.fields.card
                              .quantity_base.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleQuantityBase}
                            value={quantityBase}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  </>
                ) : null}
              </IonRow>
              <br />
              <br />
              <br />
              <IonRow className="ion-padding form-bg form-img">
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.employee_no.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.employee_no
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onInput={handleEmployeeNo}
                          value={employeeNo}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.employee_name.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.employee_name
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onInput={handleEmployeeName}
                          value={employeeName}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.resumption_date.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .resumption_date.mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onInput={handleResumptionDate}
                          value={resumptionDate}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.cause_of_absence_code.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .cause_of_absence_code.mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonSelect
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={handlecauseofAbsence}
                          className="request_select_placeholder"
                          placeholder="Select Cause of Absence"
                        >
                          {causeofAbsenceCode
                            ? Object.keys(causeofAbsenceCode).map(
                                (causeofAbsence_code, id) => {
                                  return (
                                    <IonSelectOption
                                      key={id}
                                      value={
                                        causeofAbsenceCode[causeofAbsence_code]
                                          .code
                                      }
                                    >
                                      {
                                        causeofAbsenceCode[causeofAbsence_code]
                                          .description
                                      }
                                    </IonSelectOption>
                                  );
                                }
                              )
                            : null}
                        </IonSelect>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.description.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.description
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onInput={handleDescription}
                          value={description}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.relief_no.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.relief_no
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonSelect
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={handleReliever}
                          className="request_select_placeholder"
                          placeholder="Select Reliever"
                        >
                          {reliefNo
                            ? Object.keys(reliefNo).map((relief_no, id) => {
                                return (
                                  <IonSelectOption
                                    key={id}
                                    value={reliefNo[relief_no].no}
                                  >
                                    {reliefNo[relief_no].first_name +
                                      " " +
                                      reliefNo[relief_no].last_name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.unit_of_measure_code.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .unit_of_measure_code.mapping
                        }
                        :
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onInput={handleUnitofMeasureCode}
                          value={unitofMeasureCode}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.process_allowance_payment.enabled.pages
                    .create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .process_allowance_payment.mapping
                        }
                      </p>
                      <IonToggle
                        onIonChange={handleProcessAllowance}
                        checked
                        value="processAllowance"
                        color="customColor"
                      />
                    </div>
                  </IonCol>
                ) : null}
                <br />
                <br />

                <IonCol sizeXs="12" sizeMd="12">
                  <div className="ion-float-right">
                    <IonButton
                      className="custom-btn"
                      size="large"
                      onClick={submitForm}
                      disabled={submitting}
                    >
                      Submit
                      <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                        {submitting ? (
                          <IonSpinner
                            name="crescent"
                            color="light"
                            slot="end"
                          />
                        ) : (
                          <IonIcon name="arrow-dropright" />
                        )}
                      </span>
                    </IonButton>
                    <IonButton
                      className="custom-cancel-btn"
                      size="large"
                      disabled={submitting}
                      onClick={() =>
                        history.replace("/annual-leave-applications")
                      }
                    >
                      Cancel
                      <span
                        style={{ marginTop: "-3px", marginLeft: "3px" }}
                      ></span>
                    </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonCol>
          ) : errors ? (
            <IonCol size="4" offset="4" className="ion-text-center">
              {errors.map((error, i) => (
                <div key={i}>{error}</div>
              ))}
            </IonCol>
          ) : null}
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default AnnualLeaveApplicationForm;
