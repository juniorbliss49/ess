import React from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonCard,
  IonCardHeader,
  IonImg,
  IonCardTitle,
} from "@ionic/react";
import MenuHeader from "../../components/MenuHeader";
import SidebarMenu from "../Layout/SidebarMenu";
import AuthWrapper from "../Layout/AuthWrapper";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";

function LeaveApplicationHome({ history }) {
  const { leaveReliefResponseIsMandated } = useLeaveApplication();

  return (
    <>
      <AuthWrapper
        name="Leave Application"
        bg_name="form-bg-img-2"
        backBtnName="Home"
      >
        <IonRow className="custom-row">
          <IonCol size="3">
            <IonCard onClick={() => history.push("/annual-leave-applications")}>
              <div className="home-img-2 custom-color-1">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/logout.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/exit.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Annual Leave Application
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>
          <IonCol size="3">
            <IonCard onClick={() => history.push("/leave-balance")}>
              <div className="home-img-2 custom-color-2">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/date_1.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/date.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Leave Balances
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>
          <IonCol size="3">
            <IonCard onClick={() => history.push("/leave-application-history")}>
              <div className="home-img-2 custom-color-3">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/calendar_1.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/calendar.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Leave History
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>
          <IonCol size="3">
            <IonCard onClick={() => history.push("/leave-requests")}>
              <div className="home-img-2 custom-color-4">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/request.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/web.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Leave Request
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>
        </IonRow>
        <br />
        <IonRow className="custom-row">
          <IonCol size="3">
            <IonCard onClick={() => history.push("/leave-schedule")}>
              <div className="home-img-2 custom-color-5">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/calendar_3.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/calendar_2.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Leave Schedule
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>
          {/* {leaveReliefResponseIsMandated ? (
            <IonCol size="3">
              <IonCard onClick={() => history.push("/leave-relief-requests")}>
                <div className="home-img-2 custom-color-6">
                  <IonCardHeader class="custom-color">
                    <IonImg
                      src="assets/images/appointment_1.svg"
                      class="ion-hide-md-down"
                    />
                    <IonImg
                      src="assets/images/appointment.svg"
                      className="home-mobile-img ion-hide-lg-up"
                    />
                    <IonCardTitle class="bold color-2">
                      Leave Relief Requests
                    </IonCardTitle>
                  </IonCardHeader>
                </div>
              </IonCard>
            </IonCol>
          ) : null} */}

          {/* <IonCol size="3">
            <IonCard onClick={() => history.push("/leave-relief-history")}>
              <div className="home-img-2 custom-color-6">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/appointment_1.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/appointment.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Leave Relief History
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol> */}
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveApplicationHome;
