import React, { useEffect, useState } from "react";
import {
  IonContent,
  IonGrid,
  IonCol,
  IonRow,
  IonBadge,
  IonText,
  IonIcon,
  IonFab,
  IonFabButton,
  IonModal,
  IonButton,
  IonImg,
  IonItem,
  IonLabel,
  IonInput,
  IonSpinner,
  IonDatetime,
  IonToast,
  IonLoading,
} from "@ionic/react";
import "../../styles/leave-schedule.css";

import AuthWrapper from "../Layout/AuthWrapper";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";
import { useTenant } from "../../context/TenantContext";
import { Redirect } from "react-router";
import { getPlatforms } from "@ionic/core";
import {
  createAnnualLeaveApplication,
  recallAnnualLeaveApplication,
} from "../../utils/apiClient";

function LeaveScheduleForm({ match, history }) {
  const [loading, setLoading] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [leaveScheduleFields, setleaveScheduleFields] = useState(null);
  const [lines, setLines] = useState(null);
  const [Headerlines, setHeaderLines] = useState(null);
  const [editable, setEditable] = useState(false);
  const [editableValue, setEditableValue] = useState(null);
  const { tenant } = useTenant();
  const [showModal, setShowModal] = useState(false);
  const [mobile, setMobile] = useState(false);
  const [newLeaveLines, setNewLeaveLines] = useState(null);
  const [newModalLeaveLines, setNewModalLeaveLines] = useState(null);

  const { token, logOutUser } = useAuth();
  const {
    getEditLeaveScheduleRequest,
    postLeaveSchedule,
  } = useLeaveApplication();
  const [errors, setErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");
  const [requestWasCreated, setRequestWasCreated] = useState(false);
  const [processing, setProcessing] = useState(false);

  useEffect(() => {
    setProcessing(true);
    // try {
    let isSubscribed = true;

    getEditLeaveScheduleRequest(match.params.id, token)
      .then((data) => {
        if (isSubscribed) {
          setleaveScheduleFields(data);
          setProcessing(false);

          let leaveData = data;
          let pushedLines = [];
          Object.keys(leaveData.fields.lines).map((datas, id) => {
            leaveData.fields.lines[datas].enabled
              ? leaveData.fields.lines[datas].enabled.pages.update
                ? (pushedLines[datas] = "")
                : null
              : null;
          });

          let changetoArrofObj = [];
          let newLines = { ...pushedLines };
          let lineKeys = [];
          let newKeys = [];

          changetoArrofObj.push({ ...pushedLines });

          setHeaderLines(pushedLines);
          let newModalLines = { ...pushedLines };
          let setNewModalLines = { ...pushedLines };
          setNewModalLeaveLines([setNewModalLines]);
          setNewLeaveLines([newModalLines]);

          Object.keys(pushedLines).map((data, id) => {
            lineKeys.push(data);
          });

          if (leaveData.leave_schedule_data.lines.length) {
            leaveData.leave_schedule_data.lines.map((data, line) => {
              Object.keys(data).map((field, id) => {
                if (field === "id") {
                  return (newLines["id"] =
                    leaveData.leave_schedule_data.lines[line][field]);
                }
                if (field === "header_id") {
                  return (newLines["header_id"] =
                    leaveData.leave_schedule_data.lines[line][field]);
                }
                if (lineKeys.includes(field)) {
                  return (newLines[field] =
                    leaveData.leave_schedule_data.lines[line][field]);
                }
              });
              newKeys.push({ ...newLines });
            });
            setLines(newKeys);
            setEditableValue(true);
          } else {
            setLines(changetoArrofObj);
            setEditableValue(false);
          }
        }
      })
      .catch((error) => {
        if (isSubscribed) {
          setProcessing(false);

          if (error && error.status == 401) {
            logOutUser();
            return;
          }

          if (!error.response) {
            setErrors(["Network Error, Check Your Internet Connection"]);
            return;
          }

          if (!error.response.body) {
            setErrors(["Something went wrong"]);
            return;
          }

          setErrors([error.response.body.message]);
        }
      });

    let platformType = platform();
    if (
      platformType.includes("mobile") &&
      !platformType.includes("ipad") &&
      !platformType.includes("tablet")
    ) {
      setMobile(true);
    }

    return () => (isSubscribed = false);
  }, []);

  const handleOnInputChangeEndDate = (e, field, index) => {
    let getOldLinesInput = [...lines];
    getOldLinesInput[index][field] = e.target.value;
    if (
      getOldLinesInput[index]["start_date"] &&
      getOldLinesInput[index]["no_of_days_scheduled"]
    ) {
      let noDays = parseInt(getOldLinesInput[index]["no_of_days_scheduled"]);
      if (noDays < 1) {
        return;
      }
      let startDate = getOldLinesInput[index]["start_date"];
      startDate = new Date(startDate);
      let count = 0;
      let newDate: any = "";
      while (count < noDays) {
        newDate = new Date(startDate.setDate(startDate.getDate() + 1));
        if (newDate.getDay() != 0 && newDate.getDay() != 6) {
          count++;
        }
      }
      // let newDate = new Date(startDate)

      let dd: any = newDate.getDate();
      var mm: any = newDate.getMonth() + 1; //January is 0!

      var yyyy = newDate.getFullYear();
      if (dd < 10) {
        dd = "0" + dd;
      }
      if (mm < 10) {
        mm = "0" + mm;
      }

      let newAddedDate = yyyy + "-" + mm + "-" + dd;
      getOldLinesInput[index]["end_date"] = newAddedDate;
    } else {
      if (field != "end_date") {
        getOldLinesInput[index]["end_date"] = "";
      }
    }
    setLines(getOldLinesInput);
  };
  function platform() {
    return getPlatforms(window);
  }

  function numProps(obj) {
    var c = 0;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) ++c;
    }
    return c;
  }

  const handleMobileEndDate = (e, field, index) => {
    let getOldLinesInput = [...newLeaveLines];
    getOldLinesInput[index][field] = e.target.value;
    if (
      getOldLinesInput[index]["start_date"] &&
      getOldLinesInput[index]["no_of_days_taken"]
    ) {
      let noDays = parseInt(getOldLinesInput[index]["no_of_days_taken"]);
      if (noDays < 1) {
        return;
      }
      let startDate = getOldLinesInput[index]["start_date"];
      startDate = new Date(startDate);
      let count = 0;
      let newDate: any = "";
      while (count < noDays) {
        newDate = new Date(startDate.setDate(startDate.getDate() + 1));
        if (newDate.getDay() != 0 && newDate.getDay() != 6) {
          //Date.getDay() gives weekday starting from 0(Sunday) to 6(Saturday)
          count++;
        }
      }
      // let newDate = new Date(startDate)

      let dd: any = newDate.getDate();
      var mm: any = newDate.getMonth() + 1; //January is 0!

      var yyyy = newDate.getFullYear();
      if (dd < 10) {
        dd = "0" + dd;
      }
      if (mm < 10) {
        mm = "0" + mm;
      }

      let newAddedDate = yyyy + "-" + mm + "-" + dd;
      getOldLinesInput[index]["end_date"] = newAddedDate;
    } else {
      if (field != "end_date") {
        getOldLinesInput[index]["end_date"] = "";
      }
    }
    setNewLeaveLines(getOldLinesInput);
  };
  const addLine = () => {
    let oldLine = [...lines];
    let newLines = { ...Headerlines };
    if (editableValue) {
      let numerofarray = numProps(lines);

      newLines["id"] = numerofarray + 1;
      newLines["header_id"] = leaveScheduleFields.leave_schedule_data.id;
      oldLine.push({ ...newLines });
      setLines(oldLine);
    } else {
      oldLine.push({ ...Headerlines });
      setLines(oldLine);
    }
  };

  const updateErrors = (errors) => {
    setErrors(Object.values(errors).flat());
  };

  const handleDeleteLine = (index) => {
    if (lines.length === 1) {
      return;
    }
    let getOldLinesInput = [...lines];
    getOldLinesInput.splice(index, 1);
    setLines(getOldLinesInput);
  };

  const handleOnInputChange = (e, field, index) => {
    let getOldLinesInput = [...lines];
    getOldLinesInput[index][field] = e.target.value;
    setLines(getOldLinesInput);
  };

  const handleMobileOnInputChange = (e, field, index) => {
    let getOldLinesInput = [...newLeaveLines];
    getOldLinesInput[index][field] = e.target.value;
    setNewLeaveLines(getOldLinesInput);
  };

  const updateModal = () => {
    let previousLines = [...lines];
    let getOldLinesInput = [...newLeaveLines];
    let numerofarray = numProps(lines);
    getOldLinesInput[0]["id"] = numerofarray + 1;
    getOldLinesInput[0]["header_id"] =
      leaveScheduleFields.leave_schedule_data.id;

    previousLines.push(getOldLinesInput[0]);
    setLines(previousLines);
    setShowModal(false);
    setNewLeaveLines(() => {
      return newModalLeaveLines;
    });
  };

  const closeError = () => {
    setErrors([]);
  };

  const submitForm = async (e) => {
    setLoading(true);

    let newLines = [];
    if (editableValue) {
      newLines = [...lines];
    } else {
      newLines = lines.map((line, id) => {
        line["id"] = id;
        line["header_id"] = leaveScheduleFields.leave_schedule_data.id;
        return line;
      });
    }

    let formData = {
      leave_schedule_data: {
        id: leaveScheduleFields.leave_schedule_data.id,
        portal_id: leaveScheduleFields.leave_schedule_data.portal_id,
        portal_user_id: leaveScheduleFields.leave_schedule_data.portal_user_id,
        tenantID: tenant.tenantID,
        year_no: leaveScheduleFields.leave_schedule_data.year_no,
        employee_no: leaveScheduleFields.leave_schedule_data.employee_no,
        employee_name: leaveScheduleFields.leave_schedule_data.employee_name,
        absence_code: leaveScheduleFields.leave_schedule_data.absence_code,
        manager_no: leaveScheduleFields.leave_schedule_data.manager_no,
        global_dimension_1_code:
          leaveScheduleFields.leave_schedule_data.global_dimension_1_code,
        global_dimension_2_code:
          leaveScheduleFields.leave_schedule_data.global_dimension_2_code,
        no_of_days_entitled:
          leaveScheduleFields.leave_schedule_data.no_of_days_entitled,
        no_of_days_b_f: leaveScheduleFields.leave_schedule_data.no_of_days_b_f,
        status: leaveScheduleFields.leave_schedule_data.status,
        closed: leaveScheduleFields.leave_schedule_data.closed,
        leave_allowance:
          leaveScheduleFields.leave_schedule_data.leave_allowance,
        employment_date:
          leaveScheduleFields.leave_schedule_data.employment_date,
        no_of_days_b_f_expended:
          leaveScheduleFields.leave_schedule_data.no_of_days_b_f_expended,
        x_003_c_no_of_days_subtracted_x_003_e_:
          leaveScheduleFields.leave_schedule_data
            .x_003_c_no_of_days_subtracted_x_003_e_,
        no_of_days_added:
          leaveScheduleFields.leave_schedule_data.no_of_days_added,
        no_of_days_utilised:
          leaveScheduleFields.leave_schedule_data.no_of_days_utilised,
        markings: leaveScheduleFields.leave_schedule_data.markings,
        created_at: leaveScheduleFields.leave_schedule_data.markings,
        updated_at: leaveScheduleFields.leave_schedule_data.markings,
        lines: newLines,
      },
    };
    try {
      const { error, message } = await postLeaveSchedule(
        match.params.id,
        formData,
        token
      );

      if (error) throw Error(message);

      setProcessing(false);
      setLoading(false);

      history.replace("/leave-schedule", { message });
    } catch (error) {
      setLoading(false);

      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      const {
        response: { body },
      } = error;

      setToastMsg(body.message);

      body.errors && updateErrors(body.errors);
    }
  };

  if (requestWasCreated) {
    return <Redirect to="/leave-schedule" />;
  }

  return (
    <>
      <AuthWrapper
        name="Edit Leave Schedule"
        backBtnName="Home"
        bg_name="form-bg-img"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={processing}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {errors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {errors.map((error, i) => (
                    <li key={i}>{error}</li>
                  ))}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        {processing ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : leaveScheduleFields ? (
          <IonRow className="ion-justify-content-center">
            <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
              <br />
              <IonRow className="ion-padding form-bg form-border">
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .Status.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {leaveScheduleFields.fields.header.Status.mapping}:
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={leaveScheduleFields.leave_schedule_data.status}
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .year_no.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {leaveScheduleFields.fields.header.year_no.mapping}:
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={leaveScheduleFields.leave_schedule_data.year_no}
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .manager_no.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {leaveScheduleFields.fields.header.manager_no.mapping}:
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data.manager_no
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .employee_no.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {leaveScheduleFields.fields.header.employee_no.mapping}:
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data.employee_no
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .employee_name.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header.employee_name
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data.employee_name
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .no_of_days_b_f.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header.no_of_days_b_f
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data.no_of_days_b_f
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .no_of_days_added.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header.no_of_days_added
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data
                            .no_of_days_added
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .no_of_days_entitled.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header.no_of_days_entitled
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data
                            .no_of_days_entitled
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .no_of_days_utilised.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header.no_of_days_utilised
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data
                            .no_of_days_utilised
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .global_dimension_1_code.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header
                            .global_dimension_1_code.mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data
                            .global_dimension_1_code
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .global_dimension_2_code.enabled.pages.show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header
                            .global_dimension_2_code.mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data
                            .global_dimension_2_code
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
                {!leaveScheduleFields ? null : leaveScheduleFields.fields.header
                    .x_003_c_no_of_days_subtracted_x_003_e_.enabled.pages
                    .show ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          leaveScheduleFields.fields.header
                            .x_003_c_no_of_days_subtracted_x_003_e_.mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        type="text"
                        // onIonChange={e => setQuantity(e.detail.value)}
                        value={
                          leaveScheduleFields.leave_schedule_data
                            .x_003_c_no_of_days_subtracted_x_003_e_
                        }
                        disabled
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}
              </IonRow>
              <br />
              <br />
              <br />
              <IonRow className="ion-padding form-bg form-img">
                <IonCol size="12">
                  <IonRow>
                    <IonCol className="">
                      <div className="ion-float-right">
                        {mobile ? (
                          <IonButton
                            onClick={() => setShowModal(true)}
                            color="dark"
                          >
                            Add Line
                          </IonButton>
                        ) : (
                          <IonButton onClick={addLine} color="dark">
                            Add Line
                          </IonButton>
                        )}
                      </div>
                    </IonCol>
                  </IonRow>
                  <IonModal
                    isOpen={showModal}
                    className="ModalScroll"
                    onDidDismiss={() => setShowModal(false)}
                  >
                    {newLeaveLines && newLeaveLines
                      ? newLeaveLines.map((datas, index) => {
                          return (
                            <IonRow key={index}>
                              {Object.keys(datas).map((field, id) => {
                                switch (field) {
                                  case "start_date":
                                    return (
                                      <IonCol
                                        key={id}
                                        size="12"
                                        className="ion-padding"
                                      >
                                        {
                                          leaveScheduleFields.fields.lines[
                                            field
                                          ].mapping
                                        }
                                        <IonDatetime
                                          className="form-border-all"
                                          displayFormat="DD/MM/YYYY"
                                          onIonChange={(e) =>
                                            handleMobileEndDate(e, field, index)
                                          }
                                          // value={lines[index][field]}
                                        ></IonDatetime>
                                      </IonCol>
                                    );
                                  case "no_of_days_scheduled":
                                    return (
                                      <IonCol
                                        key={id}
                                        size="12"
                                        className="ion-padding"
                                      >
                                        {
                                          leaveScheduleFields.fields.lines[
                                            field
                                          ].mapping
                                        }
                                        <IonItem className="form-border-bottom form-border-all">
                                          <IonInput
                                            onInput={(e) =>
                                              handleMobileEndDate(
                                                e,
                                                field,
                                                index
                                              )
                                            }
                                            required
                                          ></IonInput>
                                        </IonItem>
                                      </IonCol>
                                    );
                                  case "end_date":
                                    return (
                                      <IonCol
                                        key={id}
                                        size="12"
                                        className="ion-padding"
                                      >
                                        {
                                          leaveScheduleFields.fields.lines[
                                            field
                                          ].mapping
                                        }
                                        <IonDatetime
                                          className="form-border-all"
                                          displayFormat="DD/MM/YYYY"
                                          onIonChange={(e) =>
                                            handleMobileOnInputChange(
                                              e,
                                              field,
                                              index
                                            )
                                          }
                                          value={newLeaveLines[index][field]}
                                          disabled
                                        ></IonDatetime>
                                      </IonCol>
                                    );
                                  case "id":
                                    return null;
                                  case "header_id":
                                    return null;
                                  default:
                                    return (
                                      <IonCol
                                        key={id}
                                        size="12"
                                        className="ion-padding"
                                      >
                                        {
                                          leaveScheduleFields.fields.lines[
                                            field
                                          ].mapping
                                        }
                                        <IonItem className="form-border-bottom form-border-all">
                                          <IonInput
                                            onInput={(e) =>
                                              handleMobileOnInputChange(
                                                e,
                                                field,
                                                index
                                              )
                                            }
                                            required
                                          ></IonInput>
                                        </IonItem>
                                      </IonCol>
                                    );
                                }
                              })}
                              <div className="ion-text-center fix-button">
                                <IonButton
                                  onClick={updateModal}
                                  className="custom-btn"
                                >
                                  ADD
                                </IonButton>
                                <IonButton
                                  onClick={() => setShowModal(false)}
                                  color="danger"
                                >
                                  Close
                                </IonButton>
                              </div>
                            </IonRow>
                          );
                        })
                      : null}
                  </IonModal>

                  <IonRow>
                    <IonCol>
                      <div className="request_line_width">
                        <table className="request_table">
                          <thead>
                            <tr>
                              <th style={{ width: "50px !important" }}></th>
                              {Headerlines
                                ? Object.keys(Headerlines).map((data, id) => {
                                    return (
                                      <th key={id}>
                                        {
                                          leaveScheduleFields.fields.lines[data]
                                            .mapping
                                        }
                                      </th>
                                    );
                                  })
                                : null}
                            </tr>
                          </thead>
                          <tbody>
                            {lines
                              ? lines.map((data, index) => {
                                  return (
                                    <tr key={index}>
                                      <td style={{ width: "50px !important" }}>
                                        <IonButton
                                          color="danger"
                                          title="Delete Line"
                                          className="remove_btn"
                                          onClick={() =>
                                            handleDeleteLine(index)
                                          }
                                        >
                                          <IonIcon name="close" />
                                        </IonButton>
                                      </td>
                                      {Object.keys(data).map((field, id) => {
                                        switch (field) {
                                          case "start_date":
                                            return (
                                              <td key={id}>
                                                <IonDatetime
                                                  displayFormat="DD/MM/YYYY"
                                                  onIonChange={(e) =>
                                                    handleOnInputChangeEndDate(
                                                      e,
                                                      field,
                                                      index
                                                    )
                                                  }
                                                  value={lines[index][field]}
                                                ></IonDatetime>
                                              </td>
                                            );
                                          case "end_date":
                                            return (
                                              <td key={id}>
                                                <IonDatetime
                                                  displayFormat="DD/MM/YYYY"
                                                  onIonChange={(e) =>
                                                    handleOnInputChange(
                                                      e,
                                                      field,
                                                      index
                                                    )
                                                  }
                                                  value={lines[index][field]}
                                                  disabled
                                                ></IonDatetime>
                                              </td>
                                            );
                                          case "no_of_days_scheduled":
                                            return (
                                              <IonCol
                                                key={id}
                                                size="12"
                                                className="ion-padding"
                                              >
                                                <IonItem className="form-border-bottom form-border-all">
                                                  <IonInput
                                                    // disabled
                                                    onInput={(e) =>
                                                      handleOnInputChangeEndDate(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    value={lines[index][field]}
                                                    required
                                                  ></IonInput>
                                                </IonItem>
                                              </IonCol>
                                            );
                                          case "id":
                                            return null;
                                          case "header_id":
                                            return null;
                                          case "no_of_days_taken":
                                            return (
                                              <td key={id}>
                                                <IonItem className="form-border-bottom form-border-all">
                                                  <IonInput
                                                    disabled
                                                    value={lines[index][field]}
                                                  ></IonInput>
                                                </IonItem>
                                              </td>
                                            );
                                          default:
                                            return (
                                              <td key={id}>
                                                <IonItem className="form-border-bottom">
                                                  <IonInput
                                                    onInput={(e) =>
                                                      handleOnInputChange(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    value={lines[index][field]}
                                                    required
                                                  ></IonInput>
                                                </IonItem>
                                              </td>
                                            );
                                        }
                                      })}
                                    </tr>
                                  );
                                })
                              : null}
                          </tbody>
                        </table>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCol>
              </IonRow>
              <div className="ion-float-right">
                <IonButton
                  className="custom-btn"
                  size="large"
                  onClick={submitForm}
                  disabled={loading}
                >
                  Submit
                  {loading ? (
                    <IonSpinner name="crescent" color="light" slot="end" />
                  ) : (
                    <IonIcon name="arrow-dropright" />
                  )}
                  <span style={{ marginTop: "-3px", marginLeft: "3px" }}></span>
                </IonButton>
                <IonButton
                  className="custom-cancel-btn"
                  size="large"
                  disabled={loading}
                  onClick={() => history.goBack()}
                >
                  Cancel
                  <span style={{ marginTop: "-3px", marginLeft: "3px" }}></span>
                </IonButton>
              </div>
            </IonCol>
          </IonRow>
        ) : null}
      </AuthWrapper>
    </>
  );
}

export default LeaveScheduleForm;
