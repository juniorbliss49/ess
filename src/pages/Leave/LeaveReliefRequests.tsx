import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonIcon,
  IonText,
  IonSegment,
  IonSegmentButton,
  IonLabel
} from "@ionic/react";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";

function LeaveReliefRequests({ history }) {
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);
  const [reliefRequestForActions, setReliefRequestForActions] = useState(null);
  const [filteredReliefRequests, setFilteredReliefRequests] = useState([]);

  const [filterState, setFilterState] = useState("All");

  const {
    setCurrentlyViewedReliefRequest,
    fetchReliefRequests,
    leaveReliefRequests
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  useEffect(() => {
    // if (!annualLeaveApplications) {
    fetchReliefRequests(token)
      .then(data => {
        setFilteredReliefRequests(data);
      })
      .catch(error => {
        if (error && error.status == 401) {
          logOutUser();
          return;
        }
      });

    // }
  }, []);

  const prepForActions = index => {
    if (
      reliefRequestForActions != null &&
      reliefRequestForActions.index == index
    ) {
      setReliefRequestForActions(null);
      return;
    }

    let leaveRequest = filteredReliefRequests[index];
    leaveRequest["index"] = index;
    if (
      leaveRequest.status == "" ||
      leaveRequest.status == null ||
      leaveRequest.status == "Open"
    ) {
      setReliefRequestForActions(leaveRequest);
      return;
    }
  };

  const viewReliefRequest = index => {
    let leaveRequest = filteredReliefRequests[index];

    if (
      leaveRequest.status == "" ||
      leaveRequest.status == null ||
      leaveRequest.status == "Open"
    ) {
      setCurrentlyViewedReliefRequest(leaveRequest);
      history.push("/leave-relief-request");
      return;
    }

    return;
  };

  const applyFilterToLeaveApplications = param => {
    setReliefRequestForActions(null);

    setFilterState(param);

    setFilteredReliefRequests(
      leaveReliefRequests &&
        leaveReliefRequests.filter(reliefRequest => {
          if (param == "No Response") {
            return (
              reliefRequest.relief_response != "ACCEPT" &&
              reliefRequest.relief_response != "REJECT"
            );
          }

          if (param == "Accepted") {
            return reliefRequest.relief_response == "ACCEPT";
          }

          if (param == "Rejected") {
            return reliefRequest.relief_response == "REJECT";
          }

          return reliefRequest;
        })
    );
  };

  return (
    <>
      <AuthWrapper
        name="Leave Relief Requests"
        backBtnName="Home"
        bg_name="form-bg-img"
      >
        <IonRow>
          <IonCol
            sizeLg="10"
            offsetLg="1"
            sizeMd="10"
            offsetMd="1"
            sizeSm="10"
            offsetSm="1"
            sizeXs="12"
          >
            <IonRow className="ion-padding">
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                {/* <IonButton
                  className="create-btn"
                  onClick={() => history.push("/leave-application-form")}
                >
                  <IonIcon slot="start" name="add" />
                  New Application
                </IonButton> */}
                {reliefRequestForActions ? (
                  <>
                    <IonButton
                      className="create-btn"
                      onClick={() =>
                        viewReliefRequest(reliefRequestForActions.index)
                      }
                    >
                      <IonIcon slot="start" name="refresh" />
                      Respond to request
                    </IonButton>
                    {/* <IonButton color="secondary" className="create-btn">
                      <IonIcon slot="start" name="send" />
                      Send for Approval
                    </IonButton> */}
                  </>
                ) : null}
              </IonCol>
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <div className="ion-float-right content-center minus-margin">
                  <IonSegment
                    onIonChange={e =>
                      applyFilterToLeaveApplications(e.detail.value)
                    }
                    value={filterState}
                  >
                    <IonSegmentButton
                      style={{ marginRight: "3px" }}
                      value="All"
                      className="custom-history-btn custom-history-padding border-right-shape"
                    >
                      <IonLabel style={{ color: "#fff" }}>All</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="No Response"
                      className="custom-history-btn custom-history-padding-2 custom-minus-margin"
                    >
                      <IonLabel style={{ color: "#fff" }}>No Response</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Accepted"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Accepted</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Rejected"
                      className="custom-history-btn border-right-shape-3"
                    >
                      <IonLabel style={{ color: "#fff" }}>Rejected</IonLabel>
                    </IonSegmentButton>
                  </IonSegment>
                </div>
              </IonCol>
            </IonRow>
            <br />
            <br />
            <IonRow className="ion-padding card-history-main">
              {filteredReliefRequests
                ? filteredReliefRequests.map((reliefRequest, i) => {
                    return (
                      <IonCol
                        key={i}
                        sizeLg="4"
                        sizeMd="4"
                        sizeSm="4"
                        sizeXs="12"
                      >
                        <IonCard
                          className="card-history"
                          style={
                            reliefRequestForActions &&
                            reliefRequestForActions.index == i
                              ? { border: "4px solid blue", cursor: "pointer" }
                              : { cursor: "pointer" }
                          }
                          onClick={() => prepForActions(i)}
                        >
                          <IonCardHeader className="card-history-header">
                            <IonCardTitle>
                              {reliefRequest.cause_of_absence_code}
                            </IonCardTitle>
                          </IonCardHeader>
                          <IonCardContent className="ion-text-center">
                            <h5>{reliefRequest.status}</h5>
                            <h3>{reliefRequest.portal_id}</h3>
                            <div>
                              <span>
                                <IonIcon
                                  title="Days"
                                  style={{ cursor: "pointer" }}
                                  ios="person"
                                  name="person"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>{reliefRequest.employee_name}</IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Days"
                                  style={{ cursor: "pointer" }}
                                  ios="briefcase"
                                  name="briefcase"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>{reliefRequest.quantity} Days</IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Time"
                                  style={{ cursor: "pointer" }}
                                  ios="ios-time"
                                  name="ios-time"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>
                                  From: {reliefRequest.from_date}
                                </IonText>
                              </span>
                              <span>
                                <IonIcon
                                  title="Time"
                                  style={{ cursor: "pointer" }}
                                  ios="ios-time"
                                  name="ios-time"
                                  // class="icon-style  icon-size"
                                />
                                <IonText>To: {reliefRequest.to_date}</IonText>
                              </span>
                            </div>
                          </IonCardContent>
                        </IonCard>
                      </IonCol>
                    );
                  })
                : null}
            </IonRow>

            {/* <IonRow>
              <IonCol size="4" offset="4" className="ion-text-center">
                <IonButton className="history-btn">Load More...</IonButton>
              </IonCol>
            </IonRow> */}
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveReliefRequests;
