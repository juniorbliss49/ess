import React, { useEffect, useState } from "react";

import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonToggle,
  IonButton,
  IonIcon,
  IonDatetime,
  IonSelect,
  IonSelectOption,
  IonToast,
  IonSpinner,
  IonLoading,
} from "@ionic/react";
import "../../styles/leave-application-form.css";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";
import { Redirect } from "react-router";

function AnnualLeaveApplicationEditForm({ history, match }) {
  const {
    updateCurrentLeaveApplication,
    prepFieldsForAnnualNewLeaveApplicationForUpdate,
    annualLeaveApplicationFields,
    causeofAbsenceCode,
    reliefNo,
    currentlyEditedAnnualLeaveRequest,
    loading,
    errors,
  } = useLeaveApplication();

  const { token, logOutUser } = useAuth();

  const [leaveRequest, setLeaveRequest] = useState(null);
  const [submitting, setSubmitting] = useState(false);
  const [requestWasUpdated, setRequestWasUpdated] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");

  useEffect(() => {
    prepFieldsForAnnualNewLeaveApplicationForUpdate(
      match.params.reqId,
      token
    ).then(({ leave_application }) => {
      setLeaveRequest(leave_application);
    });
  }, []);

  const handleFormUpdate = (field, value) => {
    setLeaveRequest({ ...leaveRequest, [field]: value });
  };

  const submitForm = async (e) => {
    const { id, ...formData } = leaveRequest;

    // console.log(currentlyEditedAnnualLeaveRequest);
    setSubmitting(true);

    try {
      const { message } = await updateCurrentLeaveApplication(
        leaveRequest.id,
        formData,
        token
      );

      setSubmitting(false);

      history.replace("/annual-leave-applications", { message });
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      const {
        response: { body },
      } = error;

      body.message && setFormErrors([body.message]);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  return (
    <>
      <AuthWrapper
        name="Annual Leave Request"
        bg_name="form-bg-img"
        backBtnName="Leave Application"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={submitting}
          // isOpen={loading || submitting}
          onDidDismiss={() => {}}
          message="Please wait..."
        /> */}

        {formErrors.length ? (
          <IonRow>
            <ul>
              {formErrors.map((error, i) => (
                <li key={i}>{error}</li>
              ))}
            </ul>
          </IonRow>
        ) : null}

        <IonRow className="ion-justify-content-center">
          {annualLeaveApplicationFields ? (
            <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
              <IonRow className="ion-padding form-bg form-border">
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.from_date.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          annualLeaveApplicationFields.fields.card.from_date
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonDatetime
                        disabled
                        displayFormat="DD/MM/YYYY"
                        onIonChange={(e) =>
                          handleFormUpdate("from_date", e.detail.value)
                        }
                        value={leaveRequest ? leaveRequest.from_date : null}
                      ></IonDatetime>
                    </IonItem>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.to_date.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          annualLeaveApplicationFields.fields.card.to_date
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonDatetime
                        disabled
                        displayFormat="DD/MM/YYYY"
                        onIonChange={(e) =>
                          handleFormUpdate("to_date", e.detail.value)
                        }
                        value={leaveRequest ? leaveRequest.to_date : null}
                      ></IonDatetime>
                    </IonItem>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.quantity.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonItem>
                      <IonLabel className="form-input">
                        {
                          annualLeaveApplicationFields.fields.card.quantity
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonInput
                        disabled
                        type="number"
                        onIonChange={(e) =>
                          handleFormUpdate("quantity", e.detail.value)
                        }
                        value={leaveRequest ? leaveRequest.quantity : null}
                      ></IonInput>
                    </IonItem>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.quantity_base.enabled.pages.update ? (
                  <>
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            annualLeaveApplicationFields.fields.card
                              .quantity_base.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            type="number"
                            onIonChange={(e) =>
                              handleFormUpdate("quantity_base", e.detail.value)
                            }
                            value={
                              leaveRequest ? leaveRequest.quantity_base : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  </>
                ) : null}
              </IonRow>
              <br />
              <br />
              <br />
              <IonRow className="ion-padding form-bg form-img">
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.employee_no.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.employee_no
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          disabled
                          type="text"
                          onIonChange={(e) =>
                            handleFormUpdate("employee_no", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.employee_no : null}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.employee_name.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.employee_name
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          disabled
                          onIonChange={(e) =>
                            handleFormUpdate("employee_name", e.detail.value)
                          }
                          value={
                            leaveRequest ? leaveRequest.employee_name : null
                          }
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}

                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.resumption_date.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .resumption_date.mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          type="text"
                          onIonChange={(e) =>
                            handleFormUpdate("resumption_date", e.detail.value)
                          }
                          value={
                            leaveRequest ? leaveRequest.resumption_date : null
                          }
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.cause_of_absence_code.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .cause_of_absence_code.mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonSelect
                          disabled
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={(e) =>
                            handleFormUpdate(
                              "cause_of_absence_code",
                              e.detail.value
                            )
                          }
                          value={
                            leaveRequest
                              ? leaveRequest.cause_of_absence_code
                              : null
                          }
                          className="request_select_placeholder"
                          placeholder="Select Cause of Absence"
                        >
                          {causeofAbsenceCode
                            ? Object.keys(causeofAbsenceCode).map(
                                (causeofAbsence_code, id) => {
                                  return (
                                    <IonSelectOption
                                      key={id}
                                      value={
                                        causeofAbsenceCode[causeofAbsence_code]
                                          .code
                                      }
                                    >
                                      {
                                        causeofAbsenceCode[causeofAbsence_code]
                                          .description
                                      }
                                    </IonSelectOption>
                                  );
                                }
                              )
                            : null}
                        </IonSelect>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.description.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.description
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onIonChange={(e) =>
                            handleFormUpdate("description", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.description : null}
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.relief_no.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card.relief_no
                            .mapping
                        }
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonSelect
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={(e) =>
                            handleFormUpdate("relief_no", e.detail.value)
                          }
                          value={leaveRequest ? leaveRequest.relief_no : null}
                          className="request_select_placeholder"
                          placeholder="Select Reliever"
                        >
                          {reliefNo
                            ? Object.keys(reliefNo).map((relief_no, id) => {
                                return (
                                  <IonSelectOption
                                    key={id}
                                    value={reliefNo[relief_no].no}
                                  >
                                    {reliefNo[relief_no].first_name +
                                      " " +
                                      reliefNo[relief_no].last_name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.unit_of_measure_code.enabled.pages.update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .unit_of_measure_code.mapping
                        }
                        :
                      </p>
                      <IonItem className="form-border-bottom">
                        <IonInput
                          onIonChange={(e) =>
                            handleFormUpdate(
                              "unit_of_measure_code",
                              e.detail.value
                            )
                          }
                          value={
                            leaveRequest
                              ? leaveRequest.unit_of_measure_code
                              : null
                          }
                        ></IonInput>
                      </IonItem>
                    </div>
                  </IonCol>
                ) : null}
                {!annualLeaveApplicationFields ? null : annualLeaveApplicationFields
                    .fields.card.process_allowance_payment.enabled.pages
                    .update ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <div>
                      <p className="form-p">
                        {
                          annualLeaveApplicationFields.fields.card
                            .process_allowance_payment.mapping
                        }
                      </p>
                      <IonToggle
                        onIonChange={(e) =>
                          handleFormUpdate(
                            "process_allowance_payment",
                            e.detail.value
                          )
                        }
                        value={
                          leaveRequest
                            ? leaveRequest.process_allowance_payment
                            : null
                        }
                        color="customColor"
                      />
                    </div>
                  </IonCol>
                ) : null}
                <br />
                <br />

                <IonCol sizeXs="12" sizeMd="12">
                  <div className="ion-float-right">
                    <IonButton
                      className="custom-btn"
                      size="large"
                      onClick={submitForm}
                      disabled={submitting}
                    >
                      Update
                      <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                        {submitting ? (
                          <IonSpinner
                            name="crescent"
                            color="light"
                            slot="end"
                          />
                        ) : (
                          <IonIcon name="arrow-dropright" />
                        )}
                      </span>
                    </IonButton>
                    <IonButton
                      className="custom-cancel-btn"
                      size="large"
                      disabled={submitting}
                      onClick={() =>
                        history.replace("/annual-leave-applications")
                      }
                    >
                      Cancel
                      <span
                        style={{ marginTop: "-3px", marginLeft: "3px" }}
                      ></span>
                    </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonCol>
          ) : errors ? (
            <IonCol size="4" offset="4" className="ion-text-center">
              {errors.map((error, i) => (
                <div key={i}>{error}</div>
              ))}
            </IonCol>
          ) : null}
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default AnnualLeaveApplicationEditForm;
