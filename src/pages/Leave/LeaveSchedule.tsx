import React, { Component, useState, useEffect } from "react";
import {
  IonContent,
  IonGrid,
  IonCol,
  IonRow,
  IonBadge,
  IonText,
  IonIcon,
  IonFab,
  IonFabButton,
  IonModal,
  IonButton,
  IonImg,
  IonSpinner,
  IonToast,
  IonLoading,
  IonActionSheet,
} from "@ionic/react";
import { isPlatform } from "@ionic/core";
import "../../styles/leave-schedule.css";

import {
  requestApprovalForLeaveSchedule,
  cancelApprovalRequestForLeaveSchedule,
} from "../../utils/apiClient";

import MenuHeader from "../../components/MenuHeader";

import { appPages, backBtnName } from "../../declarations";
import AuthWrapper from "../Layout/AuthWrapper";
import { useAuth } from "../../context/AuthContext";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";

function LeaveSchedule({ history }) {
  const [loading, setLoading] = useState(null);
  const [error, setError] = useState(null);
  const [toastMsg, setToastMsg] = useState("");
  const [selectedSchedule, setSelectedSchedule] = useState(null);

  const { token, logOutUser } = useAuth();

  const {
    leaveSchedules,
    fetchLeaveSchedule,
    syncLeave,
    setLeaveSchedule,
  } = useLeaveApplication();

  useEffect(() => {
    let isSubscribed = true;
    setLoading(true);

    isSubscribed &&
      fetchLeaveSchedule(token)
        .then((_) => {
          isSubscribed && setLoading(false);
        })
        .catch((error) => {
          if (isSubscribed) {
            if (error && error.status == 401) {
              logOutUser();
              return;
            }
            setLoading(false);
          }
        });

    return () => (isSubscribed = false);
  }, []);

  const syncLeaveSchedule = async () => {
    try {
      setLoading(true);
      let msg = await syncLeave(token);
      setTimeout(() => {
        setToastMsg(msg);
        setLoading(false);
      }, 2000);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }
      const {
        response: { body },
      } = error;
      setToastMsg(body.message);
    }
  };

  const _requestApproval = async (scheduleId) => {
    try {
      setLoading(true);

      let { message } = await requestApprovalForLeaveSchedule(
        {
          leave_schedule_id: scheduleId,
        },
        token
      );

      await fetchLeaveSchedule(token);

      setToastMsg(message);
      setLoading(false);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }
      const {
        response: { body },
      } = error;
      setToastMsg(body.message);
    }
  };

  const _cancelApprovalRequest = async (scheduleId) => {
    try {
      setLoading(true);

      let { message } = await cancelApprovalRequestForLeaveSchedule(
        {
          leave_schedule_id: scheduleId,
        },
        token
      );

      await fetchLeaveSchedule(token);

      setToastMsg(message);
      setLoading(false);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }
      const {
        response: { body },
      } = error;
      setToastMsg(body.message);
    }
  };

  return (
    <>
      <AuthWrapper
        name="Leave Schedule"
        backBtnName="Home"
        bg_name="form-bg-img"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={loading}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        <IonActionSheet
          isOpen={!!selectedSchedule}
          onDidDismiss={() => setSelectedSchedule(null)}
          buttons={[
            !!selectedSchedule && selectedSchedule.status == "Open"
              ? {
                  text: "Edit Schedule",
                  icon: "create",
                  handler: () => {
                    history.push("/leave-schedule-edit/" + selectedSchedule.id);
                  },
                }
              : null,
            !!selectedSchedule && selectedSchedule.status == "Approved"
              ? {
                  text: "View Schedule",
                  icon: "eye",
                  handler: () => {
                    history.push("/leave-schedule-view/" + selectedSchedule.id);
                  },
                }
              : null,
            !!selectedSchedule && selectedSchedule.status == "Open"
              ? {
                  text: "Request Approval",
                  icon: "send",
                  handler: () => {
                    _requestApproval(selectedSchedule.id);
                  },
                }
              : null,
            !!selectedSchedule && selectedSchedule.status == "Pending_Approval"
              ? {
                  text: "Cancel Approval Request",
                  icon: "close",
                  cssClass: "danger",
                  handler: () => {
                    _cancelApprovalRequest(selectedSchedule.id);
                  },
                }
              : null,
          ].filter((action) => action != null)}
        />

        <IonRow className="ion-justify-content-center">
          <IonCol sizeLg="10" sizeMd="10" sizeSm="10" sizeXs="12">
            <IonRow className="hide-wrapper">
              <IonCol>
                <div className="ion-float-right">
                  <IonButton
                    className="custom-btn-2 ribbon-color-btn"
                    size="default"
                    style={{ marginRight: "-1px" }}
                    onClick={syncLeaveSchedule}
                    title="Sync All"
                  >
                    <IonIcon slot="icon-only" name="sync" />
                  </IonButton>
                </div>
              </IonCol>
            </IonRow>
            <br />
            <IonRow>
              <IonCol>
                {loading ? (
                  <IonCol
                    sizeMd="8"
                    sizeSm="12"
                    sizeXs="12"
                    style={{ margin: "auto", textAlign: "center" }}
                  >
                    <h4>Please Wait...</h4>
                  </IonCol>
                ) : leaveSchedules != null && leaveSchedules.length ? (
                  <table className="approvals-table">
                    <thead>
                      <tr>
                        <th>Year</th>
                        {/* <th>Employee No</th> */}
                        <th>Days Entitled</th>
                        {/* <th>Days Utilised</th> */}
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {leaveSchedules.map((data, id) => {
                        return (
                          <tr key={id}>
                            <td>{data.year_no}</td>
                            {/* <td>{data.employee_no}</td> */}
                            <td>{data.no_of_days_entitled}</td>
                            {/* <td>{data.no_of_days_utilised}</td> */}
                            <td>{data.status}</td>
                            <td>
                              {isPlatform(window, "mobile") ||
                              isPlatform(window, "tablet") ||
                              isPlatform(window, "ipad") ? (
                                <IonButton
                                  className="custom-view-btn"
                                  size="small"
                                  onClick={() => setSelectedSchedule(data)}
                                >
                                  <span>
                                    <IonIcon mode="ios" name="arrow-forward" />
                                  </span>
                                </IonButton>
                              ) : (
                                <>
                                  {data.status == "Open" ? (
                                    <>
                                      <IonButton
                                        className="custom-view-btn"
                                        size="small"
                                        title="Edit"
                                        onClick={() =>
                                          history.push(
                                            "/leave-schedule-edit/" + data.id
                                          )
                                        }
                                      >
                                        <span>
                                          <IonIcon name="create" />
                                        </span>
                                      </IonButton>

                                      <IonButton
                                        className="ribbon-color-btn"
                                        color="success"
                                        title="Send for Approval"
                                        onClick={() =>
                                          _requestApproval(data.id)
                                        }
                                      >
                                        <span>
                                          <IonIcon name="send" />
                                        </span>
                                      </IonButton>
                                    </>
                                  ) : null}

                                  {data.status == "Pending_Approval" ? (
                                    <IonButton
                                      title="Cancel Approval Request"
                                      color="danger"
                                      className="create-btn"
                                      onClick={() =>
                                        _cancelApprovalRequest(data.id)
                                      }
                                    >
                                      <span>
                                        <IonIcon
                                          slot="icon-only"
                                          name="close"
                                        />
                                      </span>
                                    </IonButton>
                                  ) : null}

                                  {data.status == "Approved" ||
                                  data.status == "Closed" ? (
                                    <IonButton
                                      className="ribbon-color-btn"
                                      title="View"
                                      onClick={() =>
                                        history.push(
                                          "/leave-schedule-view/" + data.id
                                        )
                                      }
                                    >
                                      <span>
                                        <IonIcon name="eye" />
                                      </span>
                                    </IonButton>
                                  ) : null}
                                </>
                              )}
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                ) : !!leaveSchedules && !loading && !leaveSchedules.length ? (
                  <IonCol
                    sizeMd="8"
                    sizeSm="12"
                    sizeXs="12"
                    style={{ margin: "auto", textAlign: "center" }}
                  >
                    <h4>Schedules not available, kindly refresh</h4>
                  </IonCol>
                ) : null}
              </IonCol>
            </IonRow>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveSchedule;
