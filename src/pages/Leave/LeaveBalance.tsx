import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonIcon,
  IonLoading,
  IonToast,
} from "@ionic/react";
import "../../styles/leave-balance.css";
import { useLeaveApplication } from "../../context/LeaveApplicationContext";
import { useAuth } from "../../context/AuthContext";

function LeaveBalance({ history }) {
  const {
    fetchLeaveBalance,
    setLoading,
    loading,
    syncLeaveBalance,
  } = useLeaveApplication();
  const { token, logOutUser } = useAuth();

  const [leaveBalance, setLeaveBalance] = useState(null);
  const [toastMsg, setToastMsg] = useState("");

  useEffect(() => {
    getAllLeaveBalance();
  }, []);

  const getAllLeaveBalance = () => {
    setLoading(true);

    fetchLeaveBalance(token)
      .then((data) => {
        let balances = data.balances[0].balances;
        const leaveBalance = JSON.parse(balances);
        setLeaveBalance(leaveBalance);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);

        if (error && error.status == 401) {
          logOutUser();
          return;
        }
      });
  };

  async function _syncAll() {
    try {
      setLoading(true);

      let date = new Date();
      const year = date.getFullYear();
      let { message } = await syncLeaveBalance(year, token);
      getAllLeaveBalance();
      setLoading(false);
      setToastMsg(message);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  }

  return (
    <>
      <AuthWrapper
        name="Leave Balances"
        bg_name="form-bg-img-2"
        backBtnName="Leave Application"
      >
        {/* <IonLoading
          isOpen={loading}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}
        <IonRow>
          <IonCol
            sizeLg="10"
            offsetLg="1"
            sizeMd="10"
            offsetMd="1"
            sizeSm="10"
            offsetSm="1"
            sizeXs="12"
          >
            <IonRow className="ion-padding">
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <IonButton
                  title="Refresh"
                  className="ribbon-color-btn"
                  onClick={_syncAll}
                >
                  <IonIcon slot="icon-only" name="sync" />
                </IonButton>
              </IonCol>
            </IonRow>
            <IonRow className="ion-padding">
              <IonCol>
                {leaveBalance ? (
                  <table className="appraisals-table table-space">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Period</th>
                        {/* <th>Type</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {leaveBalance
                        ? Object.keys(leaveBalance).map((data, i) => {
                            return (
                              <tr key={i}>
                                <td>{data}</td>
                                <td>{leaveBalance[data]}</td>
                              </tr>
                            );
                          })
                        : null}
                    </tbody>
                  </table>
                ) : loading ? (
                  <div style={{ textAlign: "center" }}>
                    <h4>Please wait...</h4>
                  </div>
                ) : (
                  <div style={{ textAlign: "center" }}>
                    <h4>Your leave balances are not available</h4>
                  </div>
                )}
              </IonCol>
            </IonRow>
            {/* <IonRow>
              <IonCol size="4" offset="4" className="ion-text-center">
                <IonButton className="history-btn">Load More...</IonButton>
              </IonCol>
            </IonRow> */}
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LeaveBalance;
