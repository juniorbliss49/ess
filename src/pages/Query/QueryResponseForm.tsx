import React, { useState, useEffect } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonLoading,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonButton,
  IonIcon,
  IonInput,
  IonTextarea,
  IonSpinner,
} from "@ionic/react";

import {
  fetchExistingQueryResponsesFieldSettings,
  updateQueryResponse,
} from "../../utils/apiClient";
import { useAuth } from "../../context/AuthContext";

function QueryResponseForm({ history, match }) {
  const { token, logOutUser } = useAuth();

  const [loading, setLoading] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState("");
  const [formErrors, setFormErrors] = useState([]);
  const [employees, setEmployees] = useState([]);
  const [fields, setFields] = useState(null);
  const [causesOfQuery, setCausesOfQuery] = useState([]);

  const [query, setQuery] = useState(null);
  const [queryToEdit, setQueryToEdit] = useState(null);

  useEffect(() => {
    _initFieldsAndQuery();
  }, []);

  const _handleFormUpdate = (field, value) => {
    setQuery({ ...query, [field]: value });
  };

  const _initFieldsAndQuery = async () => {
    try {
      setLoading(true);
      let {
        causesOfQuery,
        queryResponse,
        fields,
      } = await fetchExistingQueryResponsesFieldSettings(
        match.params.queryId,
        token
      );
      setQuery(queryResponse);
      setQueryToEdit(queryResponse);

      setCausesOfQuery(causesOfQuery);
      setFields(fields);

      setLoading(false);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setError("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setError("Something went wrong");
        return;
      }

      setError(error.response.body.message);
    }
  };

  const _updateQuery = async () => {
    try {
      setSubmitting(true);

      let { message } = await updateQueryResponse(queryToEdit.id, query, token);

      setSubmitting(false);

      history.replace("/query-responses", { message });
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setFormErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setFormErrors(["Something went wrong"]);
        return;
      }

      setFormErrors([error.response.body.message].flat());
    }
  };

  return (
    <AuthWrapper
      name="Query Response"
      bg_name="form-bg-img-2"
      backBtnName="Home"
    >
      {formErrors.length ? (
        <IonRow>
          <IonCol
            sizeMd="10"
            offsetMd="1"
            sizeSm="12"
            style={{ backgroundColor: "#d05b5b" }}
          >
            <IonRow>
              <IonCol size="12">
                <IonIcon
                  name="close"
                  className="ion-float-right closeBtn"
                  onClick={() => setFormErrors([])}
                />
              </IonCol>
            </IonRow>
            <IonRow>
              <ul style={{ listStyleType: "none", color: "#fff" }}>
                {formErrors.map((error, i) => (
                  <li key={i}>{error}</li>
                ))}
              </ul>
            </IonRow>
          </IonCol>
        </IonRow>
      ) : null}

      {loading ? (
        <IonCol
          sizeMd="8"
          sizeSm="12"
          sizeXs="12"
          style={{ margin: "auto", textAlign: "center" }}
        >
          <h4>Please Wait...</h4>
        </IonCol>
      ) : query ? (
        <IonRow>
          <IonCol size="10" offset="1">
            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Queried By</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={query && queryToEdit ? query.queried_by_name : null}
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Cause of Query</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={
                      query && queryToEdit ? query.cause_of_query_code : null
                    }
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Date of Query</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={query && queryToEdit ? query.date_of_query : null}
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Response Deadline</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={
                      query && queryToEdit ? query.response_dead_line : null
                    }
                  ></IonInput>
                </IonItem>
              </IonCol>
            </IonRow>

            <br />
            <br />

            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="12">
                <IonItem>
                  <IonLabel className="form-input">Explanation</IonLabel>
                  <IonTextarea
                    placeholder="Enter your explanation here..."
                    value={query && queryToEdit ? query.explanation : null}
                    onIonChange={(e) =>
                      _handleFormUpdate("explanation", e.detail.value)
                    }
                  ></IonTextarea>
                </IonItem>
              </IonCol>
            </IonRow>

            <IonCol sizeXs="12" sizeMd="12">
              <br />
              <br />
              <div className="ion-float-right">
                <IonButton
                  className="custom-btn"
                  size="large"
                  onClick={_updateQuery}
                >
                  Update
                  <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                    {submitting ? (
                      <IonSpinner name="crescent" color="light" slot="end" />
                    ) : (
                      <IonIcon name="arrow-dropright" />
                    )}
                  </span>
                </IonButton>
              </div>
            </IonCol>
          </IonCol>
        </IonRow>
      ) : error ? (
        <IonRow>
          <IonCol size="6" offset="2">
            {error}
          </IonCol>
        </IonRow>
      ) : null}
    </AuthWrapper>
  );
}

export default QueryResponseForm;
