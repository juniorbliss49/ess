import React, { useState, useEffect } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonToast,
  IonLoading,
  IonRow,
  IonCol,
  IonSegment,
  IonButton,
  IonIcon,
  IonSegmentButton,
  IonLabel,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonText,
  IonActionSheet,
} from "@ionic/react";
import { useAuth } from "../../context/AuthContext";
import {
  fetchProcessedQueries,
  refreshProcessedQuery,
  getProcessedQueryFromErp,
  forwardQueryToHr,
} from "../../utils/apiClient";
import { getPlatforms } from "@ionic/core";

function ProcessedQueries({ history, location }) {
  const { token, logOutUser } = useAuth();

  const [queries, setQueries] = useState([]);
  const [queryForActions, setQueryForAction] = useState(null);
  const [loading, setLoading] = useState(false);
  const [mobile, setMobile] = useState(false);
  const [showActionSheet, setShowActionSheet] = useState(false);
  const [actionButtons, setActionButtons] = useState(null);

  const [toastMsg, setToastMsg] = useState("");

  useEffect(() => {
    _fetchQueries();
    let platformType = platform();
    if (platformType.includes("mobile")) {
      setMobile(true);
    }
  }, []);

  const _fetchQueries = async () => {
    try {
      setLoading(true);

      let { processedQueries } = await fetchProcessedQueries(token);

      setQueries(processedQueries);

      setLoading(false);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body);
    }
  };

  function platform() {
    return getPlatforms(window);
  }

  const _clearLocationStateAndToast = () => {
    if (location.state && location.state.message) {
      location.state.message = "";
    }

    setToastMsg("");
  };

  const _prepForActions = (i) => {
    setShowActionSheet(true);
    if (queryForActions != null && queryForActions.index == i) {
      setQueryForAction(null);
      return;
    }

    let query = queries[i];
    query["index"] = i;

    setQueryForAction(query);
    buttonProperty(query);
  };

  function buttonProperty(query) {
    let button = [];
    button = [
      {
        text: "Process Query",
        role: "query",
        icon: "create",
        handler: () => {
          _processQuery(query);
        },
      },
      {
        text: "Forward to HR",
        icon: "send",
        handler: () => {
          _forwardToHr(query);
        },
      },
      {
        text: "Refresh",
        role: "refresh",
        icon: "refresh",
        handler: () => {
          _refresh(query);
        },
      },
    ];

    setActionButtons(button);
  }

  const _processQuery = (query) => {
    if (query.status != "Answered") {
      return;
    }

    history.push(`/processed-queries/${query.id}/edit`);
  };

  const _refresh = async (query) => {
    try {
      setLoading(true);

      let { message } = await refreshProcessedQuery(query.id, token);

      let { processedQueries } = await fetchProcessedQueries(token);

      setQueries(processedQueries);

      setLoading(false);

      setToastMsg(message);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _syncAll = async () => {
    try {
      setLoading(true);

      let { message } = await getProcessedQueryFromErp(token);

      let { processedQueries } = await fetchProcessedQueries(token);

      setQueries(processedQueries);

      setLoading(false);

      setToastMsg(message);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _forwardToHr = async (query) => {
    try {
      setLoading(true);

      let { message } = await forwardQueryToHr({ query_id: query.id }, token);

      let { processedQueries } = await fetchProcessedQueries(token);

      setQueries(processedQueries);

      setQueryForAction(null);

      setLoading(false);

      setToastMsg(message);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  return (
    <AuthWrapper
      name="Processed Queries"
      bg_name="form-bg-img-2"
      backBtnName="Home"
    >
      {toastMsg || (location.state && location.state.message) ? (
        <IonToast
          isOpen={true}
          message={toastMsg || location.state.message}
          duration={3000}
          onDidDismiss={_clearLocationStateAndToast}
        />
      ) : null}

      {/* <IonLoading
        isOpen={loading}
        onDidDismiss={() => null}
        message="Please wait..."
      /> */}

      <IonRow>
        <IonCol
          sizeLg="10"
          offsetLg="1"
          sizeMd="10"
          offsetMd="1"
          sizeSm="10"
          offsetSm="1"
          sizeXs="12"
        >
          <IonRow className="ion-padding custom-sticky">
            <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
              <IonButton
                title="Sync All"
                className="ribbon-color-btn"
                onClick={_syncAll}
              >
                Sync All
                <IonIcon slot="end" name="sync" />
              </IonButton>
              {queryForActions ? (
                <>
                  {mobile ? (
                    <>
                      <IonActionSheet
                        isOpen={showActionSheet}
                        onDidDismiss={() => {
                          setShowActionSheet(false);
                          setQueryForAction(null);
                        }}
                        buttons={[...actionButtons]}
                      ></IonActionSheet>
                    </>
                  ) : (
                    <>
                      <IonButton
                        title="Respond to query"
                        className="ribbon-color-btn"
                        onClick={() => _processQuery(queryForActions)}
                      >
                        <IonIcon slot="icon-only" name="create" />
                      </IonButton>

                      <IonButton
                        title="Forward to HR"
                        className="ribbon-color-btn"
                        onClick={() => _forwardToHr(queryForActions)}
                      >
                        <IonIcon slot="icon-only" name="send" />
                      </IonButton>

                      <IonButton
                        title="Refresh"
                        className="ribbon-color-btn"
                        onClick={() => _refresh(queryForActions)}
                      >
                        <IonIcon slot="icon-only" name="refresh" />
                      </IonButton>
                    </>
                  )}
                </>
              ) : null}
            </IonCol>
          </IonRow>

          {loading ? (
            <IonRow className="custom-sticky">
              <IonCol
                sizeMd="8"
                sizeSm="12"
                sizeXs="12"
                style={{ margin: "auto", textAlign: "center" }}
              >
                <h6 color="blue">Please Wait...</h6>
              </IonCol>
            </IonRow>
          ) : null}

          <IonRow className="ion-padding card-history-main">
            {queries &&
              queries.map((query, i) => (
                <IonCol key={i} sizeLg="4" sizeMd="4" sizeSm="4" sizeXs="12">
                  <IonCard
                    style={
                      queryForActions && queryForActions.index == i
                        ? { border: "4px solid #2c4d85", cursor: "pointer" }
                        : { cursor: "pointer" }
                    }
                    className="card-history"
                    onClick={() => _prepForActions(i)}
                  >
                    <IonCardHeader className="card-history-header">
                      <IonCardTitle>{query.cause_of_query_code}</IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent className="ion-text-center">
                      <h5>{query.status}</h5>
                      <h3>{query.portal_id}</h3>
                      <div>
                        <span>
                          <IonText>
                            <IonIcon title="Offence" name="cash" />
                            <IonText>{query.offence}</IonText>
                          </IonText>
                        </span>
                      </div>
                    </IonCardContent>
                  </IonCard>
                </IonCol>
              ))}
          </IonRow>
        </IonCol>
      </IonRow>
    </AuthWrapper>
  );
}

export default ProcessedQueries;
