import React, { useState, useEffect } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonLoading,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonButton,
  IonIcon,
  IonInput,
  IonSpinner,
} from "@ionic/react";

import {
  fetchExistingIssuedQueryFieldSettings,
  updateIssuedQuery,
} from "../../utils/apiClient";
import { useAuth } from "../../context/AuthContext";
import Select from "react-select";

function QueryIssueEditForm({ history, match }) {
  const { token, logOutUser } = useAuth();

  const [loading, setLoading] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState("");
  const [formErrors, setFormErrors] = useState([]);
  const [employees, setEmployees] = useState([]);
  const [fields, setFields] = useState(null);
  const [causesOfQuery, setCausesOfQuery] = useState([]);
  const [causesOfQueryOptions, setCausesOfQueryOptions] = useState([]);
  const [employeesOptions, setEmployeesOptions] = useState([]);

  const [query, setQuery] = useState(null);
  const [queryToEdit, setQueryToEdit] = useState(null);
  const [deadlineMeasure, setDeadlineMeasure] = useState("D");
  const [responseDeadline, setResponseDeadline] = useState();

  const deadlineOptions = [
    { value: "D", label: "Days" },
    { value: "WD", label: "Weekday" },
    { value: "W", label: "Week" },
    { value: "M", label: "Month" },
    { value: "Q", label: "Quarter" },
    { value: "Y", label: "Year" },
  ];

  useEffect(() => {
    _initFieldsAndQuery();
  }, []);

  const _handleFormUpdate = (field, value) => {
    setQuery({ ...query, [field]: value });
  };

  const _handleResponseDeadlineUpdate = (e) => {
    setResponseDeadline(e.detail.value);

    _handleFormUpdate(
      "response_dead_line",
      `${e.detail.value}${deadlineMeasure}`
    );
  };

  const _handleDeadlineMeasureUpdate = (e) => {
    setDeadlineMeasure(e.value);

    if (!responseDeadline) return;

    _handleFormUpdate("response_dead_line", `${responseDeadline}${e.value}`);
  };

  const setSelectOptions = (type, lookup) => {
    let requestData = [];

    switch (type) {
      case "causes_of_query":
        let causesOfQuery = [];
        lookup.map((data, id) => {
          return causesOfQuery.push({
            value: lookup[id].code,
            label: lookup[id].code,
          });
        });
        setCausesOfQueryOptions(causesOfQuery);
        requestData = [...causesOfQuery];
        break;
      case "employees":
        let employees = [];
        Object.keys(lookup).map((data, id) => {
          return employees.push({
            value: lookup[data].no,
            label: lookup[id].first_name + " " + lookup[id].last_name,
          });
        });
        setEmployeesOptions(employees);
        requestData = [...employees];
        break;

      default:
        requestData = [];
        break;
    }
    return requestData;
  };

  const _initFieldsAndQuery = async () => {
    try {
      setLoading(true);
      let {
        causesOfQuery,
        issuedQuery,
        employees,
        fields,
      } = await fetchExistingIssuedQueryFieldSettings(
        match.params.queryId,
        token
      );
      setQuery(issuedQuery);
      setQueryToEdit(issuedQuery);

      setResponseDeadline(() => {
        if (!issuedQuery.response_dead_line) {
          return null;
        }

        let pattern = /[0-9]+/g;

        let matches = issuedQuery.response_dead_line.match(pattern);

        return matches[0];
      });

      setDeadlineMeasure(() => {
        if (!issuedQuery.response_dead_line) {
          return "D";
        }

        let pattern = /[A-Z]+/g;

        let matches = issuedQuery.response_dead_line.match(pattern);

        return matches[0];
      });

      let CausesOfQuery = setSelectOptions("causes_of_query", causesOfQuery);
      let Employees = setSelectOptions("employees", employees);

      setCausesOfQuery(causesOfQuery);
      setEmployees(employees);
      setFields(fields);

      setLoading(false);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setError("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setError("Something went wrong");
        return;
      }

      setError(error.response.body.message);
    }
  };

  const _updateQuery = async () => {
    try {
      setSubmitting(true);

      let { message } = await updateIssuedQuery(queryToEdit.id, query, token);

      setSubmitting(false);

      history.replace("/issued-queries", { message });
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setFormErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setFormErrors(["Something went wrong"]);
        return;
      }

      setFormErrors([error.response.body.message].flat());
    }
  };

  return (
    <AuthWrapper name="Issued Query" bg_name="form-bg-img-2" backBtnName="Home">
      {/* <IonLoading
        isOpen={loading}
        onDidDismiss={() => null}
        message="Please wait..."
      /> */}

      {formErrors.length ? (
        <IonRow>
          <IonCol
            sizeMd="10"
            offsetMd="1"
            sizeSm="12"
            style={{ backgroundColor: "#d05b5b" }}
          >
            <IonRow>
              <IonCol size="12">
                <IonIcon
                  name="close"
                  className="ion-float-right closeBtn"
                  onClick={() => setFormErrors([])}
                />
              </IonCol>
            </IonRow>
            <IonRow>
              <ul style={{ listStyleType: "none", color: "#fff" }}>
                {formErrors.map((error, i) => (
                  <li key={i}>{error}</li>
                ))}
              </ul>
            </IonRow>
          </IonCol>
        </IonRow>
      ) : null}

      {loading ? (
        <IonCol
          sizeMd="8"
          sizeSm="12"
          sizeXs="12"
          style={{ margin: "auto", textAlign: "center" }}
        >
          <h4>Please Wait...</h4>
        </IonCol>
      ) : query ? (
        <IonRow className="ion-justify-content-center">
          <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="6">
                {/* <IonItem> */}
                <IonLabel className="form-input">Employee</IonLabel>
                <Select
                  onChange={(e) => _handleFormUpdate("employee_no", e.value)}
                  options={employeesOptions}
                  value={employeesOptions.filter(
                    (item) => item.value == query.employee_no
                  )}
                />
                {/* <IonSelect
                    className="request_select_placeholder"
                    placeholder="Select Employee"
                    okText="Okay"
                    cancelText="Dismiss"
                    value={query && queryToEdit ? query.employee_no : null}
                    onIonChange={e =>
                      _handleFormUpdate("employee_no", e.detail.value)
                    }
                  >
                    {employees.map((emp, i) => (
                      <IonSelectOption key={i} value={emp.no}>
                        {emp.first_name + " " + emp.last_name}
                      </IonSelectOption>
                    ))}
                  </IonSelect> */}
                {/* </IonItem> */}
              </IonCol>

              <IonCol sizeXs="12" sizeMd="6">
                {/* <IonItem> */}
                <IonLabel className="form-input">Cause of Query</IonLabel>
                {/* <IonSelect
                    className="request_select_placeholder"
                    placeholder="Cause of Query"
                    okText="Okay"
                    cancelText="Dismiss"
                    value={
                      query && queryToEdit ? query.cause_of_query_code : null
                    }
                    onIonChange={e =>
                      _handleFormUpdate("cause_of_query_code", e.detail.value)
                    }
                  >
                    {causesOfQuery.map((cause, i) => (
                      <IonSelectOption key={i} value={cause.code}>
                        {cause.code}
                      </IonSelectOption>
                    ))}
                  </IonSelect> */}
                {/* </IonItem> */}
                <Select
                  onChange={(e) =>
                    _handleFormUpdate("cause_of_query_code", e.value)
                  }
                  options={causesOfQueryOptions}
                  value={causesOfQueryOptions.filter(
                    (item) => item.value == query.cause_of_query_code
                  )}
                />
              </IonCol>
            </IonRow>

            <br />
            <br />

            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="6">
                <IonItem>
                  <IonLabel className="form-input">Response Deadline</IonLabel>
                  <IonInput
                    type="number"
                    value={responseDeadline}
                    onIonChange={_handleResponseDeadlineUpdate}
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="6">
                {/* <IonItem> */}

                <IonLabel className="form-input">Deadline Measure</IonLabel>
                <Select
                  onChange={_handleDeadlineMeasureUpdate}
                  options={deadlineOptions}
                  placeholder="Deadline Measure"
                  value={deadlineOptions.filter(
                    (item) => item.value == deadlineMeasure
                  )}
                />
                {/* <IonSelect
                    className="request_select_placeholder"
                    placeholder="Deadline Measure"
                    okText="Okay"
                    cancelText="Dismiss"
                    value={deadlineMeasure}
                    onIonChange={_handleDeadlineMeasureUpdate}
                  >
                    <IonSelectOption value="D">Days</IonSelectOption>
                    <IonSelectOption value="WD">Weekday</IonSelectOption>
                    <IonSelectOption value="W">Week</IonSelectOption>
                    <IonSelectOption value="M">Month</IonSelectOption>
                    <IonSelectOption value="Q">Quarter</IonSelectOption>
                    <IonSelectOption value="Y">Year</IonSelectOption>
                  </IonSelect> */}
                {/* </IonItem> */}
              </IonCol>
            </IonRow>

            <IonCol sizeXs="12" sizeMd="12">
              <br />
              <br />
              <div className="ion-float-right">
                <IonButton
                  className="custom-btn"
                  size="large"
                  onClick={_updateQuery}
                  disabled={submitting}
                >
                  Update
                  <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                    {submitting ? (
                      <IonSpinner name="crescent" color="light" slot="end" />
                    ) : (
                      <IonIcon name="arrow-dropright" />
                    )}
                  </span>
                </IonButton>
              </div>
            </IonCol>
          </IonCol>
        </IonRow>
      ) : error ? (
        <IonRow>
          <IonCol size="6" offset="2">
            {error}
          </IonCol>
        </IonRow>
      ) : null}
    </AuthWrapper>
  );
}

export default QueryIssueEditForm;
