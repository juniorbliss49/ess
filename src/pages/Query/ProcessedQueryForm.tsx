import React, { useState, useEffect } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonLoading,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonSelect,
  IonSelectOption,
  IonButton,
  IonIcon,
  IonInput,
  IonTextarea,
  IonDatetime,
  IonSpinner,
} from "@ionic/react";

import {
  fetchExistingProcessedQueryFieldSettings,
  updateProcessedQuery,
} from "../../utils/apiClient";
import { useAuth } from "../../context/AuthContext";

function ProcessedQueryForm({ history, match }) {
  const { token, logOutUser } = useAuth();

  const [loading, setLoading] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [error, setError] = useState("");
  const [formErrors, setFormErrors] = useState([]);
  const [fields, setFields] = useState(null);
  const [causesOfInactivity, setCausesOfInactivity] = useState([]);
  const [groundsForTermination, setGroundsForTermination] = useState([]);

  const [query, setQuery] = useState(null);
  const [queryToEdit, setQueryToEdit] = useState(null);

  useEffect(() => {
    _initFieldsAndQuery();
  }, []);

  const _handleFormUpdate = (field, value) => {
    setQuery({ ...query, [field]: value });
  };

  const _initFieldsAndQuery = async () => {
    try {
      setLoading(true);
      let {
        processedQuery,
        causesOfInactivity,
        groundsForTermination,
        fields,
      } = await fetchExistingProcessedQueryFieldSettings(
        match.params.queryId,
        token
      );

      setQuery(processedQuery);
      setQueryToEdit(processedQuery);

      setCausesOfInactivity(causesOfInactivity);
      setGroundsForTermination(groundsForTermination);
      setFields(fields);

      setLoading(false);
    } catch (error) {
      setLoading(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setError("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setError("Something went wrong");
        return;
      }

      setError(error.response.body.message);
    }
  };

  const _updateQuery = async () => {
    try {
      setSubmitting(true);

      let { message } = await updateProcessedQuery(
        queryToEdit.id,
        query,
        token
      );

      setSubmitting(false);

      history.replace("/processed-queries", { message });
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setFormErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      if (!error.response.body) {
        setFormErrors(["Something went wrong"]);
        return;
      }

      setFormErrors([error.response.body.message].flat());
    }
  };

  return (
    <AuthWrapper
      name="Processed Query"
      bg_name="form-bg-img-2"
      backBtnName="Home"
    >
      {/* <IonLoading
        isOpen={loading}
        onDidDismiss={() => null}
        message="Please wait..."
      /> */}

      {formErrors.length ? (
        <IonRow>
          <IonCol
            sizeMd="10"
            offsetMd="1"
            sizeSm="12"
            style={{ backgroundColor: "#d05b5b" }}
          >
            <IonRow>
              <IonCol size="12">
                <IonIcon
                  name="close"
                  className="ion-float-right closeBtn"
                  onClick={() => setFormErrors([])}
                />
              </IonCol>
            </IonRow>
            <IonRow>
              <ul style={{ listStyleType: "none", color: "#fff" }}>
                {formErrors.map((error, i) => (
                  <li key={i}>{error}</li>
                ))}
              </ul>
            </IonRow>
          </IonCol>
        </IonRow>
      ) : null}

      {loading ? (
        <IonCol
          sizeMd="8"
          sizeSm="12"
          sizeXs="12"
          style={{ margin: "auto", textAlign: "center" }}
        >
          <h4>Please Wait...</h4>
        </IonCol>
      ) : query ? (
        <IonRow className="ion-justify-content-center">
          <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Queried Employee</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={query && queryToEdit ? query.employee_no : null}
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Cause of Query</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={
                      query && queryToEdit ? query.cause_of_query_code : null
                    }
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Date of Query</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={query && queryToEdit ? query.date_of_query : null}
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="3">
                <IonItem>
                  <IonLabel className="form-input">Response Deadline</IonLabel>
                  <IonInput
                    type="text"
                    disabled
                    value={
                      query && queryToEdit ? query.response_dead_line : null
                    }
                  ></IonInput>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="12">
                <IonItem>
                  <IonLabel className="form-input">Explanation</IonLabel>
                  <IonTextarea
                    disabled
                    placeholder="Enter your explanation here..."
                    value={query && queryToEdit ? query.explanation : null}
                  ></IonTextarea>
                </IonItem>
              </IonCol>
            </IonRow>

            <br />
            <br />

            <IonRow className="ion-padding form-bg form-border">
              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">Action</IonLabel>
                  <IonSelect
                    value={query && queryToEdit ? query.action : null}
                    onIonChange={(e) =>
                      _handleFormUpdate("action", e.detail.value)
                    }
                  >
                    <IonSelectOption value="No_Action">
                      No Action
                    </IonSelectOption>
                    <IonSelectOption value="Advice">Advice</IonSelectOption>
                    <IonSelectOption value="Warning">Warning</IonSelectOption>
                    <IonSelectOption value="Suspension_with_pay">
                      Suspension with pay
                    </IonSelectOption>
                    <IonSelectOption value="Suspension_without_pay">
                      Suspension without pay
                    </IonSelectOption>
                    <IonSelectOption value="Dismissal">
                      Dismissal
                    </IonSelectOption>
                  </IonSelect>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">
                    Effective Date of Action
                  </IonLabel>
                  <IonDatetime
                    placeholder="Effective Date of Action"
                    displayFormat="YYYY-MM-DD"
                    onIonChange={(e) =>
                      _handleFormUpdate(
                        "effective_date_of_action",
                        e.detail.value
                      )
                    }
                    value={
                      query && queryToEdit
                        ? query.effective_date_of_action
                        : null
                    }
                  ></IonDatetime>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">
                    Cause of Inactivity
                  </IonLabel>
                  <IonSelect
                    className="request_select_placeholder"
                    placeholder="Cause of Inactivity"
                    okText="Okay"
                    cancelText="Dismiss"
                    value={
                      query && queryToEdit
                        ? query.cause_of_inactivity_code
                        : null
                    }
                    onIonChange={(e) =>
                      _handleFormUpdate(
                        "cause_of_inactivity_code",
                        e.detail.value
                      )
                    }
                  >
                    {causesOfInactivity.map((cause, i) => (
                      <IonSelectOption key={i} value={cause.code}>
                        {cause.code}
                      </IonSelectOption>
                    ))}
                  </IonSelect>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">
                    Grounds for Terminination
                  </IonLabel>
                  <IonSelect
                    className="request_select_placeholder"
                    placeholder="Grounds for Terminination"
                    okText="Okay"
                    cancelText="Dismiss"
                    value={
                      query && queryToEdit ? query.grounds_for_term_code : null
                    }
                    onIonChange={(e) =>
                      _handleFormUpdate("grounds_for_term_code", e.detail.value)
                    }
                  >
                    {groundsForTermination.map((grounds, i) => (
                      <IonSelectOption key={i} value={grounds.code}>
                        {grounds.code}
                      </IonSelectOption>
                    ))}
                  </IonSelect>
                </IonItem>
              </IonCol>

              <IonCol sizeXs="12" sizeMd="4">
                <IonItem>
                  <IonLabel className="form-input">
                    Suspension Duration
                  </IonLabel>
                  <IonInput
                    type="text"
                    onIonChange={(e) =>
                      _handleFormUpdate("suspemsion_duration", e.detail.value)
                    }
                    value={
                      query && queryToEdit ? query.suspemsion_duration : null
                    }
                  ></IonInput>
                </IonItem>
              </IonCol>
            </IonRow>

            <IonCol sizeXs="12" sizeMd="12">
              <br />
              <br />
              <div className="ion-float-right">
                <IonButton
                  className="custom-btn"
                  size="large"
                  onClick={_updateQuery}
                  disabled={submitting}
                >
                  Update
                  <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                    {submitting ? (
                      <IonSpinner name="crescent" color="light" slot="end" />
                    ) : (
                      <IonIcon name="arrow-dropright" />
                    )}
                  </span>
                </IonButton>
              </div>
            </IonCol>
          </IonCol>
        </IonRow>
      ) : error ? (
        <IonRow>
          <IonCol size="6" offset="2">
            {error}
          </IonCol>
        </IonRow>
      ) : null}
    </AuthWrapper>
  );
}

export default ProcessedQueryForm;
