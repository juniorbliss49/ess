import React from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonCard,
  IonCardHeader,
  IonImg,
  IonCardTitle
} from "@ionic/react";

function QueryHome({ history }) {
  return (
    <>
      <AuthWrapper
        name="Query Management"
        bg_name="form-bg-img-2"
        backBtnName="Home"
      >
        <IonRow className="custom-row">
          <IonCol size="3">
            <IonCard onClick={() => history.push("/issued-queries")}>
              <div className="home-img-2 custom-color-1">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/exclamation.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/alert.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Issued Queries
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>

          <IonCol size="3">
            <IonCard onClick={() => history.push("/query-responses")}>
              <div className="home-img-2 custom-color-2">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/help_1.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/conversation.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Query Responses
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>

          <IonCol size="3">
            <IonCard onClick={() => history.push("/processed-queries")}>
              <div className="home-img-2 custom-color-2">
                <IonCardHeader class="custom-color">
                  <IonImg
                    src="assets/images/question.svg"
                    class="ion-hide-md-down"
                  />
                  <IonImg
                    src="assets/images/profiles.svg"
                    className="home-mobile-img ion-hide-lg-up"
                  />
                  <IonCardTitle class="bold color-2">
                    Processed Queries
                  </IonCardTitle>
                </IonCardHeader>
              </div>
            </IonCard>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default QueryHome;
