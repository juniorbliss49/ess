import React, { useState, useEffect } from "react";
import {
  IonGrid,
  IonContent,
  IonRow,
  IonCol,
  IonSegment,
  IonSegmentButton,
  IonLabel,
  IonImg,
  IonIcon,
  IonAvatar,
  IonList,
  IonItem,
  IonText,
  IonFab,
  IonFabButton,
  IonModal,
  IonSkeletonText,
  IonSpinner,
  IonLoading,
  IonButton,
  IonFabList,
  IonInput,
  IonToast,
} from "@ionic/react";

import "../../styles/profile.scss";

import { appPages } from "../../declarations";

import MenuHeader from "../../components/MenuHeader";
import ProfileModal from "./ProfileModal";
import { useProfile } from "../../context/ProfileContext";
import { useAuth } from "../../context/AuthContext";
import AuthWrapper from "../Layout/AuthWrapper";
import { getPlatforms, isPlatform } from "@ionic/core";
import { updatePassword } from "../../utils/apiClient";

const appPage: appPages = {
  name: "Profile",
};

function Profile({ history }) {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [segment, setSegment] = useState("general");
  const [loading, setLoading] = useState(false);
  const [hasError, setHasError] = useState(false);
  const [mobile, setMobile] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [showChangePassword, setShowChangePassword] = useState(false);
  const [passwordData, setPasswordData] = useState({
    password: "",
    password_confirmation: "",
  });
  const [updatingPassword, setUpdatingPassword] = useState(false);
  const [toastMsg, setToastMsg] = useState("");

  const { token, logOutUser } = useAuth();
  const {
    profile,
    picture,
    fetchProfile,
    syncAll,
    getProfilePixQuery,
    errors,
    setErrors,
    fetchProfilePix,
  } = useProfile();

  useEffect(() => {
    if (!profile) {
      setHasError(false);
      setLoading(true);
      try {
        setLoading(true);
        fetchProfile(token);
        getProfilePixQuery(token);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setHasError(true);

        if (error && error.status == 401) {
          logOutUser();
          return;
        }
        // setLoading(false);
      }
    }

    let platformType = platform();
    if (platformType.includes("mobile")) {
      setMobile(true);
    }
  }, []);

  function platform() {
    return getPlatforms(window);
  }

  const handleSegmentChange = (e: any) => {
    setSegment(e.detail.value);
  };

  const refreshProfilePix = () => {
    setLoading(true);
    try {
      setLoading(true);
      fetchProfilePix(token);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  async function _syncAll() {
    setHasError(false);
    setLoading(true);
    syncAll(token)
      .then(() => {
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        if (error && error.status == 401) {
          logOutUser();
          return;
        }

        setHasError(true);
      });
  }

  const closeError = () => {
    setErrors(null);
  };

  const _changePassword = async () => {
    setUpdatingPassword(true);

    try {
      let { message } = await updatePassword(passwordData, token);

      setToastMsg(message);

      setUpdatingPassword(false);

      setShowChangePassword(false);
    } catch (error) {
      setUpdatingPassword(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong !!!");
        return;
      }

      if (error.response.body.errors.password) {
        let errorMessages = error.response.body.errors.password as string[];
        setToastMsg(errorMessages.join(", "));
        return;
      }

      if (error.response.body.message) {
        setToastMsg(error.response.body.message);
        return;
      }
    }
  };

  return (
    <>
      <IonModal isOpen={modalIsOpen} onDidDismiss={() => setModalIsOpen(false)}>
        <ProfileModal dismissModal={() => setModalIsOpen(false)} />
      </IonModal>

      <IonLoading
        isOpen={loading}
        onDidDismiss={() => null}
        message="Please wait..."
      />

      {!!toastMsg ? (
        <IonToast
          isOpen={true}
          message={toastMsg}
          onDidDismiss={() => setToastMsg("")}
          duration={3000}
        />
      ) : null}

      <IonModal
        isOpen={showChangePassword}
        onDidDismiss={() => {
          setShowChangePassword(false);
          setPasswordData({ password: "", password_confirmation: "" });
        }}
        className="Modal_Resize"
        id="password-modal"
      >
        {/* <div className="ion-padding"> */}
        <IonRow className="ion-justify-content-center">
          <IonCol size="9" style={{ textAlign: "center" }}>
            <h3>Change Password</h3>
          </IonCol>
        </IonRow>
        <IonRow className="ion-justify-content-center">
          <IonCol size="10" sizeMd="8" sizeSm="10" sizeLg="8">
            <IonItem class="custom-item-login">
              <IonLabel
                position="stacked"
                class="custom-label-login ion-text-uppercase"
              >
                New Password
              </IonLabel>
              <IonInput
                name="password"
                type="password"
                class="custom-input-login"
                value={passwordData.password}
                onIonChange={(e) =>
                  setPasswordData({ ...passwordData, password: e.detail.value })
                }
              >
                <IonIcon name="lock" class="custom-icon" />
              </IonInput>
            </IonItem>
          </IonCol>
        </IonRow>
        <IonRow className="ion-justify-content-center">
          <IonCol size="10" sizeMd="8" sizeSm="10" sizeLg="8">
            <IonItem class="custom-item-login">
              <IonLabel
                position="stacked"
                class="custom-label-login ion-text-uppercase"
              >
                Confirm New Password
              </IonLabel>
              <IonInput
                name="password_confirmation"
                type="password"
                class="custom-input-login"
                value={passwordData.password_confirmation}
                onIonChange={(e) =>
                  setPasswordData({
                    ...passwordData,
                    password_confirmation: e.detail.value,
                  })
                }
              >
                <IonIcon name="lock" class="custom-icon" />
              </IonInput>
            </IonItem>
          </IonCol>
        </IonRow>
        <IonRow className="ion-justify-content-center">
          <IonCol size="6">
            <IonButton
              disabled={updatingPassword}
              size="default"
              expand="full"
              shape="round"
              class="custom-gradient"
              type="submit"
              onClick={_changePassword}
            >
              {updatingPassword ? (
                <IonSpinner name="crescent" color="light" slot="end" />
              ) : (
                <IonIcon
                  slot="end"
                  name="arrow-forward"
                  class="custom-login-icon"
                />
              )}
              Submit
            </IonButton>
          </IonCol>
        </IonRow>
        {/* </div> */}
      </IonModal>

      {mobile ? (
        <IonFab
          vertical="bottom"
          horizontal="end"
          slot="fixed"
          color="favorite custom-gradient"
          className="custom-gradient"
        >
          <IonFabButton
            color="favorite custom-gradient"
            className="custom-gradient"
          >
            <IonIcon name="arrow-up" />
          </IonFabButton>
          <IonFabList side="top">
            <IonFabButton className="ribbon-color-btn" onClick={_syncAll}>
              <IonIcon name="sync" />
            </IonFabButton>

            <IonFabButton
              onClick={() => history.push("/profile-edit")}
              color="favorite custom-gradient"
              className="custom-gradient"
            >
              <IonIcon name="create" />
            </IonFabButton>

            <IonFabButton
              onClick={() => setShowChangePassword(true)}
              color="secondary"
              className="custom-gradient"
            >
              <IonIcon name="lock" />
            </IonFabButton>
          </IonFabList>
        </IonFab>
      ) : null}

      <AuthWrapper name={appPage.name} bg_name="" backBtnName="Home">
        {/* <IonContent> */}
        {/* <IonGrid> */}

        {errors ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {/* {errors.map((error, i) => ( */}
                  <li>{errors}</li>
                  {/* ))} */}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        <div className="custom-profile-margin">
          <IonRow></IonRow>
        </div>

        {/* {!mobile ? (
          <IonRow>
            <IonCol>
              <div className="custom-profile-margin ">
                <IonRow>
                  <IonCol size="4">
                    <IonAvatar style={{ width: "100px", height: "100px" }}>
                      <IonImg src="assets/images/man.png" />
                    </IonAvatar>

                    <br />
                    <br />
                  </IonCol>
                  <IonCol size="8" className="ion-text-end">
                    <div
                      style={{ position: "absolute", bottom: "40", right: "0" }}
                    >
                      <IonButton
                        onClick={() => history.push("/profile-edit")}
                        color="favorite"
                      >
                        Edit Profile
                      </IonButton>
                      <IonButton
                        title="Refresh All"
                        className="ribbon-color-btn"
                        // color="tertiary"
                        onClick={_syncAll}
                      >
                        <IonIcon slot="icon-only" name="sync" />
                      </IonButton>
                    </div>
                  </IonCol>
                </IonRow>
              </div>
            </IonCol>
          </IonRow>
        ): null}   */}

        {!mobile ? (
          <IonRow>
            <IonCol>
              <div className="custom-profile-margin ">
                <IonRow>
                  <IonCol size="4" className="ion-text-center">
                    <IonAvatar style={{ width: "100px", height: "100px" }}>
                      <IonImg
                        src={picture ? picture : "assets/images/man.png"}
                      />
                      {/* {!picture ? ( */}
                      <IonButton
                        title="Refresh All"
                        className="ribbon-color-btn"
                        // color="tertiary"
                        onClick={refreshProfilePix}
                        shape="round"
                      >
                        <IonIcon slot="icon-only" name="sync" />
                      </IonButton>
                      {/* ) : null} */}
                    </IonAvatar>

                    <br />
                    <br />
                  </IonCol>
                  <IonCol size="8" className="ion-text-end">
                    <div
                      style={{ position: "absolute", bottom: "40", right: "0" }}
                    >
                      <IonButton
                        color="light"
                        onClick={() => setShowChangePassword(true)}
                      >
                        Reset Password
                      </IonButton>
                      <IonButton
                        onClick={() => history.push("/profile-edit")}
                        color="favorite"
                      >
                        Edit Profile
                      </IonButton>
                      <IonButton
                        title="Refresh All"
                        className="ribbon-color-btn"
                        // color="tertiary"
                        onClick={_syncAll}
                      >
                        <IonIcon slot="icon-only" name="sync" />
                      </IonButton>
                    </div>
                  </IonCol>
                </IonRow>
              </div>
            </IonCol>
          </IonRow>
        ) : null}

        <IonRow class="custom-profile ion-hide-md-up">
          <IonCol size="3" size-sm="2" className="custom-profile-div">
            <IonRow class="custom-profile-2">
              <IonCol size="5">
                <IonIcon name="ios-arrow-down" class="icon" />
              </IonCol>
              <IonCol size="5">
                <IonAvatar class="border">
                  <IonImg src={picture ? picture : "assets/images/man.png"} />
                </IonAvatar>
              </IonCol>
            </IonRow>
          </IonCol>
          <IonCol size="9" size-sm="10" class=" favorite">
            <IonRow class="custom-padding">
              <IonCol>
                {loading ? (
                  <>
                    <IonSkeletonText
                      animated
                      style={{ width: "80%", height: "20px" }}
                    />
                    <IonSkeletonText animated width="70%" />
                  </>
                ) : profile ? (
                  <>
                    <span className="head">
                      {`${profile.general["First Name"]} ${profile.general["Last Name"]}`.toUpperCase()}
                    </span>
                    <br />
                    <span className="dept">
                      {profile.employment["Job Title Code"]}
                    </span>
                  </>
                ) : null}
              </IonCol>
            </IonRow>
          </IonCol>
        </IonRow>
        {/* </IonGrid> */}
        <br className="ion-hide-lg-up" />
        <br className="ion-hide-lg-up" />
        <br className="ion-hide-lg-up" />
        <br className="ion-hide-lg-up" />
        <br className="ion-hide-lg-up" />

        <br />
        {loading ? (
          <>
            <IonRow>
              <IonCol size="4" offset="4" className="ion-text-center">
                <IonSpinner color="primary" />
              </IonCol>
            </IonRow>
          </>
        ) : profile ? (
          <div className="custom-profile-margin">
            <IonSegment
              value={segment}
              scrollable
              onIonChange={handleSegmentChange}
              color="favorite"
              className="custom-profile-tab custom-profile-padding"
            >
              <IonSegmentButton value="general">
                <IonLabel>General</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="address">
                <IonLabel>Address</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="contact">
                <IonLabel>Contact</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="employment">
                <IonLabel>Employment</IonLabel>
              </IonSegmentButton>
            </IonSegment>
            <br />
            <br />
            {/* <IonGrid> */}
            <IonRow className="custom-profile-padding custom-profile-row">
              <IonCol>
                {segment == "general" ? (
                  <IonList className="custom-profile-list">
                    {Object.keys(profile.general).map((entry, index) => (
                      <IonItem class="custom-color" key={index}>
                        <IonLabel>
                          <b>{entry}:</b>
                        </IonLabel>
                        <IonText>{profile.general[entry]}</IonText>
                      </IonItem>
                    ))}
                  </IonList>
                ) : segment == "address" ? (
                  <IonList className="custom-profile-list">
                    {Object.keys(profile.address).map((entry, index) => (
                      <IonItem class="custom-color" key={index}>
                        <IonLabel>
                          <b>{entry}:</b>
                        </IonLabel>
                        <IonText>{profile.address[entry]}</IonText>
                      </IonItem>
                    ))}
                  </IonList>
                ) : segment == "contact" ? (
                  <IonList className="custom-profile-list">
                    {Object.keys(profile.contact).map((entry, index) => (
                      <IonItem class="custom-color" key={index}>
                        <IonLabel>
                          <b>{entry}:</b>
                        </IonLabel>
                        <IonText>{profile.contact[entry]}</IonText>
                      </IonItem>
                    ))}
                  </IonList>
                ) : segment == "employment" ? (
                  <IonList className="custom-profile-list">
                    {Object.keys(profile.employment).map((entry, index) => (
                      <IonItem class="custom-color" key={index}>
                        <IonLabel>
                          <b>{entry}:</b>
                        </IonLabel>
                        <IonText>{profile.employment[entry]}</IonText>
                      </IonItem>
                    ))}
                  </IonList>
                ) : (
                  <IonList />
                )}
              </IonCol>
            </IonRow>
            {/* </IonGrid> */}
          </div>
        ) : null}
        {/* </IonContent> */}
      </AuthWrapper>
    </>
  );
}
export default Profile;
