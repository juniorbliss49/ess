import React, { Component } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonBadge,
  IonIcon,
  IonList,
  IonItem,
  IonButton,
  IonInput
} from "@ionic/react";

import "../../styles/profile-modal.css";

type Props = {
  dismissModal: () => void;
};

type State = {
  showModal: boolean;
};

class ProfileModal extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <>
        <IonContent>
          <IonGrid>
            <IonRow>
              <IonCol class="ion-text-right">
                <IonBadge
                  color="favorite"
                  class="custom-modal-round"
                  onClick={this.props.dismissModal}
                >
                  <IonIcon name="close" />
                </IonBadge>
              </IonCol>
            </IonRow>
            <IonList>
              <IonItem class="custom-item-login no-padding">
                <IonInput
                  class="custom-input-login"
                  placeholder="Phone Number"
                />
              </IonItem>
              <br />
              <IonItem class="custom-item-login no-padding">
                <IonInput
                  class="custom-input-login"
                  placeholder="Employee ID"
                />
              </IonItem>
              <br />
              <IonItem class="custom-item-login no-padding">
                <IonInput class="custom-input-login" placeholder="Email" />
              </IonItem>
              <br />
              <IonItem class="custom-item-login no-padding">
                <IonInput class="custom-input-login" placeholder="Department" />
              </IonItem>
            </IonList>
            <IonCol class="ion-text-center">
              <div className="ion-text-center">
                <IonButton onClick={this.props.dismissModal} color="favorite">
                  Cancel
                </IonButton>
                <IonButton onClick={this.props.dismissModal} color="light">
                  Send
                </IonButton>
              </div>
            </IonCol>
          </IonGrid>
        </IonContent>
      </>
    );
  }
}

export default ProfileModal;
