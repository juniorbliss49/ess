import React, { useEffect, useState } from "react";
import {
  IonRow,
  IonCol,
  IonButton,
  IonItem,
  IonLabel,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonDatetime,
  IonLoading,
  IonToast,
  IonAvatar,
  IonImg,
  IonIcon,
  IonModal,
  IonSpinner,
} from "@ionic/react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton,
} from "react-accessible-accordion";
import { getPlatforms, isPlatform } from "@ionic/core";
import { appPages } from "../../declarations";
import { useAuth } from "../../context/AuthContext";
import { useProfile } from "../../context/ProfileContext";
import { updateEmployeeApplication } from "../../utils/apiClient";

const appPage: appPages = {
  name: "Edit Profile",
};

function ProfileEdit() {
  const [mobile, setMobile] = useState(false);
  const {
    fetchAllProfileData,
    setLoading,
    loading,
    errors,
    setErrors,
    fetchProfilePix,
    picture,
    profileEditErrors,
    setProfileEditErrors,
  } = useProfile();
  const { token, logOutUser } = useAuth();
  const [profileFieldsSetup, setProfileFieldsSetup] = useState(null);
  const [profileFieldData, setProfileFieldData] = useState(null);
  const [formErrors, setFormErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");
  const [processing, setProcessing] = useState(false);
  const [submitting, setSubmitting] = useState(false);

  useEffect(() => {
    setLoading(true);

    fetchAllProfileData(token)
      .then((data) => {
        setProfileFieldsSetup(data.fieldsSetup);
        setProfileFieldData(data.employee);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);

        if (error && error.status == 401) {
          logOutUser();
          return;
        }

        if (!error.response) {
          setToastMsg("Network Error, Check Your Internet Connection");
          return;
        }

        if (!error.response.body) {
          setToastMsg("Something went wrong");
          return;
        }
        setLoading(false);
      });

    let platformType = platform();

    if (platformType.includes("mobile")) {
      setMobile(true);
    }
  }, []);

  const handleFormUpdate = (field, value) => {
    setProfileFieldData({ ...profileFieldData, [field]: value });
  };

  function platform() {
    return getPlatforms(window);
  }

  const submitForm = async () => {
    try {
      setSubmitting(true);
      let { message } = await updateEmployeeApplication(
        profileFieldData,
        token
      );

      setSubmitting(false);

      setToastMsg(message);
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      const {
        response: { body },
      } = error;

      body.message && setToastMsg(body.message);
      // body.errors && updateErrors(body.errors);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  const closeError = () => {
    setFormErrors(null);
    setProfileEditErrors(null);
  };

  const refreshProfilePix = () => {
    try {
      setProcessing(true);
      fetchProfilePix(token);
      setProcessing(false);
    } catch (error) {
      setProcessing(false);
    }
  };

  const updateErrors = (errors) => {
    setErrors(Object.values(errors).flat());
  };

  return (
    <>
      <AuthWrapper name={appPage.name} bg_name="" backBtnName="Home">
        {profileEditErrors ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  <li>{profileEditErrors}</li>
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        {/* <IonLoading
          isOpen={loading}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* {formErrors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {formErrors.map((error, i) => <li key={i}>{error}</li>) }
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null} */}

        {loading ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : (
          <IonRow>
            <IonCol
              sizeLg="10"
              offsetLg="1"
              sizeMd="10"
              offsetMd="1"
              sizeSm="10"
              offsetSm="1"
              sizeXs="12"
            >
              <IonRow className="ion-padding form-bg form-border">
                <IonCol size="12">
                  <IonRow>
                    <IonCol
                      sizeLg="4"
                      sizeMd="4"
                      sizeXs="12"
                      offsetLg="4"
                      offsetMd="4"
                      className="ion-text-center"
                    >
                      <div className="ion-text-center">
                        <h3 style={{ color: "#5D8CDA" }}>Profile Picture</h3>
                        <IonAvatar
                          style={{
                            width: "100px",
                            height: "100px",
                            margin: "0 auto",
                          }}
                        >
                          <IonImg
                            src={picture ? picture : "assets/images/man.png"}
                          />
                        </IonAvatar>
                        <IonButton
                          title="Refresh"
                          className="ribbon-color-btn"
                          // color="tertiary"
                          onClick={refreshProfilePix}
                          shape="round"
                        >
                          {processing ? (
                            <IonSpinner
                              name="crescent"
                              color="light"
                              slot="icon-only"
                            />
                          ) : (
                            <IonIcon slot="icon-only" name="sync" />
                          )}
                        </IonButton>
                      </div>
                      <br />
                      <br />
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol size="12">
                      <h3 style={{ color: "#5D8CDA" }}>Other Information</h3>
                      <Accordion preExpanded={["General"]}>
                        <AccordionItem uuid="General">
                          <AccordionItemHeading>
                            <AccordionItemButton>
                              General
                              {/* <span style={{float: "right"}}>10-11-2019</span> */}
                            </AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            <IonRow>
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .first_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.first_name.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.first_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "first_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .last_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.last_name.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.last_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "last_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .middle_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.middle_name.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.middle_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "middle_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .maiden_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.maiden_name.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.maiden_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "maiden_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employee_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employee_name
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employee_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employee_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employer_code.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employer_code
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employer_code
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employer_code",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employer_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employer_name
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employer_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employer_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .gender.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.gender.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.gender
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "gender",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .marital_status.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.marital_status
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.marital_status
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "marital_status",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .birth_date.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.birth_date.mapping}
                                      </IonLabel>
                                      <IonDatetime
                                        displayFormat="YYYY/MM/DD"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.birth_date
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "birth_date",
                                            e.detail.value
                                          )
                                        }
                                      ></IonDatetime>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .social_security_no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.social_security_no
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.social_security_no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "social_security_no",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .state_code.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.state_code.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.state_code
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "state_code",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .pension_no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.pension_no.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.pension_no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "pension_no",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .height.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.height.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.height
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "height",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .religion.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.religion.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.religion
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "religion",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                            </IonRow>
                          </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem uuid="Address">
                          <AccordionItemHeading>
                            <AccordionItemButton>
                              Address
                              {/* <span style={{float: "right"}}>10-11-2019</span> */}
                            </AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            <IonRow>
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .address.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.address.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.address
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "address",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .address_2.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.address_2.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.address_2
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "address_2",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .extension.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.extension.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.extension
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "extension",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .post_code.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.post_code.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.post_code
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "post_code",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .city.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.city.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.city
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "city",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                            </IonRow>
                          </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem uuid="Contact">
                          <AccordionItemHeading>
                            <AccordionItemButton>
                              Contact
                              {/* <span style={{float: "right"}}>10-11-2019</span> */}
                            </AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            <IonRow>
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .mobile_phone_no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.mobile_phone_no
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.mobile_phone_no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "mobile_phone_no",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .phone_no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.phone_no.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.phone_no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "phone_no",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}

                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .e_mail.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.e_mail.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.e_mail
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "e_mail",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .company_e_mail.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.company_e_mail
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.company_e_mail
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "company_e_mail",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .fax_no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.fax_no.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.fax_no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "fax_no",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                            </IonRow>
                          </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem uuid="Employment">
                          <AccordionItemHeading>
                            <AccordionItemButton>
                              Employment
                              {/* <span style={{float: "right"}}>10-11-2019</span> */}
                            </AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            <IonRow>
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.no.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate("no", e.detail.value)
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .job_title_code.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.job_title_code
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.job_title_code
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "job_title_code",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .grade_level_code.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.grade_level_code
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.grade_level_code
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "grade_level_code",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employee_category.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employee_category
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employee_category
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employee_category",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .manager_no.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.manager_no.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.manager_no
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "manager_no",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .designation.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {profileFieldsSetup.designation.mapping}
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.designation
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "designation",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .current_appointment_date.enabled.pages
                                  .update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup
                                            .current_appointment_date.mapping
                                        }
                                      </IonLabel>
                                      <IonDatetime
                                        displayFormat="YYYY/MM/DD"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.current_appointment_date
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "current_appointment_date",
                                            e.detail.value
                                          )
                                        }
                                      ></IonDatetime>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employment_type.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employment_type
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employment_type
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employment_type",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employment_date.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employment_date
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonDatetime
                                        displayFormat="YYYY/MM/DD"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employment_date
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employment_date",
                                            e.detail.value
                                          )
                                        }
                                      ></IonDatetime>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .cause_of_inactivity_code.enabled.pages
                                  .update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup
                                            .cause_of_inactivity_code.mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.cause_of_inactivity_code
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "cause_of_inactivity_code",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .employment_status.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.employment_status
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.employment_status
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "employment_status",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .confirmation_date.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.confirmation_date
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonDatetime
                                        displayFormat="YYYY/MM/DD"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.confirmation_date
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "confirmation_date",
                                            e.detail.value
                                          )
                                        }
                                      ></IonDatetime>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .confirmation_due_date.enabled.pages
                                  .update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup
                                            .confirmation_due_date.mapping
                                        }
                                      </IonLabel>
                                      <IonDatetime
                                        displayFormat="YYYY/MM/DD"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.confirmation_due_date
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "confirmation_due_date",
                                            e.detail.value
                                          )
                                        }
                                      ></IonDatetime>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .rent_loan_limit.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.rent_loan_limit
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.rent_loan_limit
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "rent_loan_limit",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {!profileFieldsSetup ? null : profileFieldsSetup
                                  .company_name.enabled.pages.update ? (
                                <>
                                  <IonCol sizeMd="4" sizeXs="12">
                                    <IonItem>
                                      <IonLabel className="form-input">
                                        {
                                          profileFieldsSetup.company_name
                                            .mapping
                                        }
                                      </IonLabel>
                                      <IonInput
                                        type="text"
                                        value={
                                          profileFieldData
                                            ? profileFieldData.company_name
                                            : null
                                        }
                                        onIonChange={(e) =>
                                          handleFormUpdate(
                                            "company_name",
                                            e.detail.value
                                          )
                                        }
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                </>
                              ) : null}
                              {/* {!profileFieldsSetup ? null : profileFieldsSetup
                                                    .last_modified_date_time.enabled.pages.update ? (
                                                        <>
                                                            <IonCol sizeMd="4" sizeXs="12">
                                                                <IonItem>
                                                                    <IonLabel className="form-input">
                                                                    {profileFieldsSetup.last_modified_date_time.mapping}
                                                                    </IonLabel>
                                                                    <IonDatetime
                                                                        displayFormat="YYYY/MM/DD"
                                                                        value={profileFieldData ? profileFieldData.last_modified_date_time : null}
                                                                    ></IonDatetime>
                                                                </IonItem>
                                                            </IonCol>
                                                        </>
                                                    ) : null} */}
                            </IonRow>
                          </AccordionItemPanel>
                        </AccordionItem>
                      </Accordion>
                      <br />
                      <div className="ion-float-right">
                        <IonButton
                          className="custom-btn"
                          size="large"
                          onClick={submitForm}
                          disabled={submitting}
                        >
                          Update
                          <span
                            style={{ marginTop: "-3px", marginLeft: "3px" }}
                          >
                            {submitting ? (
                              <IonSpinner
                                name="crescent"
                                color="light"
                                slot="end"
                              />
                            ) : (
                              <IonIcon name="arrow-dropright" />
                            )}
                          </span>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        )}
      </AuthWrapper>
    </>
  );
}

export default ProfileEdit;
