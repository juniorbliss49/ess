import React from "react";
import { IonRow, IonCol, IonButton, IonIcon, IonCard, IonCardHeader, IonCardTitle, IonCardContent, IonText } from "@ionic/react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemPanel,
    AccordionItemButton
} from 'react-accessible-accordion'; 

function Queries({history}) {
    return(
        <>
            <AuthWrapper name="Queries" bg_name="form-bg-img-2" backBtnName="Home">
                <IonRow className="ion-hide-md-down">
                    <IonCol size="10" offset="1">
                        <IonRow className="ion-padding">
                            <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                                <IonButton className="create-btn" onClick={()=>history.push('/new-query')}>
                                <IonIcon
                                    slot="start"
                                    name="add"
                                />
                                    New Query
                                    </IonButton>
                            </IonCol>
                            <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                                <div className="ion-float-right content-center">
                                    <IonButton className="custom-history-btn custom-history-padding border-right-shape">All</IonButton>
                                    <IonButton className="custom-history-btn custom-history-padding-2">Sent</IonButton>
                                    <IonButton className="custom-history-btn border-right-shape-3">Received</IonButton>
                                </div>
                            </IonCol>
                        </IonRow>
                        <IonRow className="ion-padding">
                            <IonCol>
                            <table className="appraisals-table table-space">
                                <thead>
                                    <tr>
                                        <th>Query Date</th>
                                        <th>Queried By</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>10-11-2019</td>
                                        <td>Samuel Aramide</td>
                                        <td>Warning</td>
                                        <td>
                                            <IonButton className="secondary-color" onClick={()=>history.push('/query-view')}>
                                                View
                                            </IonButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10-11-2019</td>
                                        <td>Samuel Aramide</td>
                                        <td>Warning</td>
                                        <td>
                                            <IonButton className="secondary-color" onClick={()=>history.push('/query-view')}>
                                                View
                                            </IonButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10-11-2019</td>
                                        <td>Samuel Aramide</td>
                                        <td>Warning</td>
                                        <td>
                                            <IonButton className="secondary-color" onClick={()=>history.push('/query-view')}>
                                                View
                                            </IonButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10-11-2019</td>
                                        <td>Samuel Aramide</td>
                                        <td>Warning</td>
                                        <td>
                                            <IonButton className="secondary-color" onClick={()=>history.push('/query-view')}>
                                                View
                                            </IonButton>
                                        </td>
                                    </tr>
                                </tbody>
                                
                            </table>
                            </IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
                <IonRow className="ion-hide-lg-up">
                    <IonCol size="12">
                        <IonRow>
                            <IonCol>
                                <Accordion>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                            By: Samuel Aramide <span style={{float: "right"}}>10-11-2019</span>
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>To:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Subject:</b> This is a test query subject</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Action:</b> Warning</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                            By: Samuel Aramide <span style={{float: "right"}}>10-11-2019</span>
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>To:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Subject:</b> This is a test query subject</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Action:</b> Warning</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                            By: Samuel Aramide <span style={{float: "right"}}>10-11-2019</span>
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>To:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Subject:</b> This is a test query subject</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Action:</b> Warning</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                    <AccordionItem>
                                        <AccordionItemHeading>
                                            <AccordionItemButton>
                                            By: Samuel Aramide <span style={{float: "right"}}>10-11-2019</span>
                                            </AccordionItemButton>
                                        </AccordionItemHeading>
                                        <AccordionItemPanel>
                                            <table className="approval-mobile">
                                                <tbody>
                                                    <tr>
                                                        <td><b>To:</b> Opeyemi Temitope</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Subject:</b> This is a test query subject</td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>Action:</b> Warning</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <IonButton expand="block" className="custom-btn-3">
                                                            <IonIcon name="eye" />
                                                                Show More
                                                            </IonButton>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </AccordionItemPanel>
                                    </AccordionItem>
                                </Accordion>
                            </IonCol>
                        </IonRow>
                        <IonRow>
                            <IonCol size="4" offset="4" className="ion-text-center">
                                <IonButton className="history-btn">
                                    Load More...
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCol>
                </IonRow>
            </AuthWrapper>
        </>
    )
}

export default Queries