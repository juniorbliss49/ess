import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonDatetime,
  IonSelect,
  IonButton,
  IonIcon,
  IonSpinner,
  IonSelectOption,
  IonModal,
  IonToast,
  IonLoading,
} from "@ionic/react";
import { Redirect } from "react-router-dom";
import "../../styles/payment-request.css";
import { usePaymentRequest } from "../../context/PaymentRequestContext";
import { useAuth } from "../../context/AuthContext";
import { getPlatforms } from "@ionic/core";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";

function PaymentRequest({ history }) {
  const {
    requestType,
    preferredPmtMethod,
    getCreateRequest,
    requestCode,
    department,
    project,
    customerGroup,
    area,
    businessGroup,
    salesCampaign,
    currencyCode,
    createRequest,
    paymentRequestFields,
    lineDepartment,
    lineProject,
    formLine,
    paymentLines,
    setPaymentLines,
    newLines,
    newPaymentLines,
    setNewPaymentLines,
    requestlistSelectOptions,
    currencylistSelectOptions,
    paymentlistSelectOptions,
    deparmentslistSelectOptions,
    projectlistSelectOptions,
    requestCodelistSelectOptions,
    deparmentlinelistSelectOptions,
    projectlinelistSelectOptions,
    customergrouplistSelectOptions,
    arealistSelectOptions,
    businesslistSelectOptions,
    campaignlistSelectOptions,
    currencylineSelectOptions,
    responsibilityCenters,
  } = usePaymentRequest();

  const { token, logOutUser } = useAuth();

  useEffect(() => {
    // try {

    setLoading(true);
    getCreateRequest(token)
      .then((data) => {
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        setError(true);

        if (error && error.status == 401) {
          logOutUser();
          return;
        }

        if (!error.response) {
          setErrors(["Network Error, Check Your Internet Connection"]);
          return;
        }

        if (!error.response.body) {
          setErrors(["Something went wrong"]);
          return;
        }

        setErrors([error.response.body.message]);
      });

    let platformType = platform();
    if (
      platformType.includes("mobile") &&
      !platformType.includes("ipad") &&
      !platformType.includes("tablet")
    ) {
      setMobile(true);
    }
  }, []);

  const [lines, setLines] = useState(() => {
    if (formLine) {
      return formLine;
    }

    return formLine;
  });
  const [amount, setAmount] = useState(null);
  const [status, setStatus] = useState(null);
  const [paymentMethod, setPaymentMethod] = useState("");
  const [request_type, setRequestMethod] = useState("");
  const [beneficiary, setBeneficiary] = useState("");
  const [creationDate, setCreationDate] = useState("");
  const [submitting, setSubmitting] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [documentDate, setDocument] = useState("");
  const [payeeBankAccount, setPayeeBankAccount] = useState("");
  const [currency, setCurrency] = useState("");
  const [description, setDescription] = useState("");
  const [projectCode, setProject] = useState("");
  const [productCode, setproductCode] = useState("");
  const [DepartmentalCode, setDepartmentalCode] = useState("");
  const [preferredBankCode, setPreferredBankCode] = useState("");
  const [showMsg, setShow] = useState(false);
  const [Msg, setMsg] = useState("");
  const [errors, setErrors] = useState([]);
  const [toastMsg, setToastMsg] = useState("");
  const [requestWasCreated, setRequestWasCreated] = useState(false);
  const [mobile, setMobile] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [respCenter, setRespCenter] = useState("");

  function platform() {
    return getPlatforms(window);
  }

  const _handleToastDismissed = () => {
    setShow(false);
    setMsg("");
  };

  const handleAmount = (e) => {
    setAmount(e.target.value);
  };
  const handlePaymentMethod = (e) => {
    setPaymentMethod(e.value);
  };

  const handleStatus = (e) => {
    setStatus(e.target.value);
  };

  const handleBeneficiary = (e) => {
    setBeneficiary(e.target.value);
  };

  const handleCreationDate = (e) => {
    setCreationDate(e);
  };

  const handleDocument = (e) => {
    setDocument(e);
  };

  const handleCurrency = (e) => {
    setCurrency(e.value);
  };

  const handleRespCenterSelection = (e) => {
    setRespCenter(e.value);
  };

  const handleDescription = (e) => {
    setDescription(e.target.value);
  };

  const handleProject = (e) => {
    setProject(e.value);
  };

  const handleProductCode = (e) => {
    setproductCode(e.target.value);
  };

  const handleDepartmentalCode = (e) => {
    setDepartmentalCode(e.value);
  };

  const handleRequestMethod = (e) => {
    setRequestMethod(e.value);
  };

  const handlePreferredBankCode = (e) => {
    setPreferredBankCode(e.target.value);
  };

  const handlePayeeBankAccount = (e) => {
    setPayeeBankAccount(e.target.value);
  };

  const updateErrors = (errors) => {
    setErrors(Object.values(errors).flat());
  };

  const submitForm = async () => {
    setSubmitting(true);
    let formData = {
      payment_request_data: {
        amount: amount,
        status: status,
        beneficiary: beneficiary,
        responsibility_center: respCenter,
        // creation_date: creationDate,
        currency_code: currency,
        // document_date: documentDate,
        posting_description: description,
        preferred_bank_code: preferredBankCode,
        payee_bank_account_no: payeeBankAccount,
        preferred_pmt_method: paymentMethod,
        shortcut_dimension_1_Code: DepartmentalCode,
        shortcut_dimension_2_Code: projectCode,
        request_type: request_type,
        lines: paymentLines,
      },
    };
    try {
      let paymentRequest = await createRequest(formData, token);

      if (paymentRequest.error) {
        throw Error(paymentRequest.message);
      }

      setTimeout(() => {
        setToastMsg(paymentRequest.message);
        setSubmitting(false);
        setRequestWasCreated(true);
      }, 2000);
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }
      const {
        response: { body },
      } = error;

      setToastMsg(body.message);

      body.errors && updateErrors(body.errors);
    }
  };

  const handleOnInputChange = (e, field, index) => {
    const changeOldLinesInput = [];
    changeOldLinesInput.push(...paymentLines);
    changeOldLinesInput[index][field] = e.target.value;
    setPaymentLines(changeOldLinesInput);
  };

  const handleOnInputChangeDropDown = (e, field, index) => {
    let changeValue = [];
    changeValue.push(...paymentLines);
    changeValue[index][field] = e.value;
    setPaymentLines(changeValue);
  };

  const handleMobileOnInputChange = (e, field, index) => {
    let getOldLinesInput = [...newPaymentLines];
    getOldLinesInput[index][field] = e.target.value;
    setNewPaymentLines(getOldLinesInput);
  };

  const handleMobileOnInputChangeDropDown = (e, field, index) => {
    let getOldLinesInput = [...newPaymentLines];
    getOldLinesInput[index][field] = e.value;
    setNewPaymentLines(getOldLinesInput);
  };

  const updateModal = () => {
    let previousLines = [...paymentLines];
    let getOldLinesInput = [...newPaymentLines];
    previousLines.push(getOldLinesInput[0]);
    setPaymentLines(previousLines);
    setShowModal(false);
    let formLines = [...formLine];
    setNewPaymentLines(() => {
      return formLines;
    });
  };

  const handleDeleteLine = (index) => {
    if (paymentLines.length === 1) {
      return;
    }
    let getOldLinesInput = [...paymentLines];
    getOldLinesInput.splice(index, 1);
    setPaymentLines(getOldLinesInput);
  };

  const addLine = () => {
    let oldLine = [...paymentLines];
    let newLine = { ...newLines };
    oldLine.push(newLine);
    setPaymentLines(oldLine);
  };

  const closeError = () => {
    setErrors([]);
  };

  if (requestWasCreated) {
    return <Redirect to="/payment-request-history" />;
  }

  return (
    <>
      <AuthWrapper
        name="Payment Request"
        bg_name="form-bg-img"
        backBtnName="Payment Requests"
      >
        <IonToast
          isOpen={showMsg}
          message={Msg}
          duration={3000}
          onDidDismiss={_handleToastDismissed}
        />

        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={loading}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {errors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {errors.map((error, i) => (
                    <li key={i}>{error}</li>
                  ))}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        {loading ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : (
          <IonRow>
            <IonCol
              sizeMd="10"
              sizeSm="10"
              sizeXs="11"
              style={{ margin: "auto" }}
            >
              <IonRow className="ion-padding form-bg form-border">
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.amount.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.amount.mapping}:
                    </IonLabel>
                    <IonInput
                      type="text"
                      onIonChange={(e) => handleAmount(e)}
                      value={amount}
                      className="input-bg"
                    ></IonInput>
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                <br />
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.status.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.status.mapping}:
                    </IonLabel>
                    <IonInput
                      type="text"
                      onIonChange={(e) => handleStatus(e)}
                      value={status}
                      className="input-bg"
                    ></IonInput>
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                <br />
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.beneficiary.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.beneficiary.mapping}:
                    </IonLabel>
                    <IonInput
                      type="text"
                      onIonChange={(e) => handleBeneficiary(e)}
                      value={beneficiary}
                      className="input-bg"
                    ></IonInput>
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                <br />
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.request_type.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.request_type.mapping}:
                    </IonLabel>
                    {requestType.lookup ? (
                      <Select
                        onChange={(e) => handleRequestMethod(e)}
                        options={requestlistSelectOptions}
                      />
                    ) : null}
                    {/* <IonSelect
                      okText="Okay"
                      cancelText="Dismiss"
                      placeholder="Select Request Type"
                      onIonChange={e => handleRequestMethod(e)}
                    >
                      {requestType.lookup
                        ? Object.keys(requestType.lookup).map((data, id) => {
                            return (
                              <IonSelectOption
                                key={id}
                                value={requestType.lookup[data]}
                              >
                                {data}
                              </IonSelectOption>
                            );
                          })
                        : null}
                    </IonSelect> */}
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                <br />
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.creation_date.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonRow> */}
                    {/* <IonCol size="12"> */}
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.creation_date.mapping}
                      :
                    </IonLabel>
                    {/* </IonCol>
                    <IonCol size="12"> */}
                    <DatePicker
                      selected={creationDate}
                      onChange={(e) => handleCreationDate(e)}
                      dateFormat="dd/MM/yyyy"
                    />
                    {/* </IonCol> */}
                    {/* </IonRow> */}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.currency_code.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.currency_code.mapping}
                      :
                    </IonLabel>
                    {currencyCode.lookup ? (
                      <Select
                        onChange={(e) => handleCurrency(e)}
                        options={currencylistSelectOptions}
                      />
                    ) : null}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.responsibility_center.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header.responsibility_center
                          .mapping
                      }
                      :
                    </IonLabel>
                    <Select
                      onChange={(e) => handleRespCenterSelection(e)}
                      options={responsibilityCenters}
                    />
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.document_date.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonRow> */}
                    {/* <IonCol size="6"> */}
                    <IonLabel className="form-input">
                      {paymentRequestFields.fields.header.document_date.mapping}
                      :
                    </IonLabel>
                    {/* </IonCol> */}
                    {/* <IonCol size="6"> */}
                    <DatePicker
                      selected={documentDate}
                      onChange={(e) => handleDocument(e)}
                      dateFormat="dd/MM/yyyy"
                    />
                    {/* </IonCol> */}
                    {/* </IonRow> */}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.posting_description.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header.posting_description
                          .mapping
                      }
                      :
                    </IonLabel>
                    <IonInput
                      type="text"
                      onIonChange={(e) => handleDescription(e)}
                      value={description}
                      className="input-bg"
                    ></IonInput>
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.preferred_bank_code.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header.preferred_bank_code
                          .mapping
                      }
                      :
                    </IonLabel>
                    <IonInput
                      type="text"
                      onIonChange={(e) => handlePreferredBankCode(e)}
                      value={preferredBankCode}
                      className="input-bg"
                    ></IonInput>
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.preferred_pmt_method.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header.preferred_pmt_method
                          .mapping
                      }
                      :
                    </IonLabel>
                    {/* <IonSelect
                      okText="Okay"
                      cancelText="Dismiss"
                      placeholder={
                        paymentRequestFields.fields.header.preferred_pmt_method
                          .mapping
                      }
                      onIonChange={e => handlePaymentMethod(e)}
                    >
                      {preferredPmtMethod.lookup
                        ? Object.keys(preferredPmtMethod.lookup).map(
                            (data, id) => {
                              return (
                                <IonSelectOption
                                  key={id}
                                  value={preferredPmtMethod.lookup[data]}
                                >
                                  {data}
                                </IonSelectOption>
                              );
                            }
                          )
                        : null}
                    </IonSelect>
                  </IonItem> */}
                    {preferredPmtMethod.lookup ? (
                      <Select
                        onChange={(e) => handlePaymentMethod(e)}
                        options={paymentlistSelectOptions}
                      />
                    ) : null}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.payee_bank_account_no.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header.payee_bank_account_no
                          .mapping
                      }
                      :
                    </IonLabel>
                    <IonInput
                      type="text"
                      onIonChange={(e) => handlePayeeBankAccount(e)}
                      value={payeeBankAccount}
                      className="input-bg"
                    ></IonInput>
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.shortcut_dimension_1_code.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header
                          .shortcut_dimension_1_code.mapping
                      }
                      :
                    </IonLabel>
                    {/* <IonSelect
                      okText="Okay"
                      cancelText="Dismiss"
                      placeholder={
                        paymentRequestFields.fields.header
                          .shortcut_dimension_1_code.mapping
                      }
                      onIonChange={e => handleDepartmentalCode(e)}
                    >
                      {department.lookup
                        ? department.lookup.map((data, id) => {
                            return (
                              <IonSelectOption
                                key={id}
                                value={department.lookup[id].code}
                              >
                                {department.lookup[id].name}
                              </IonSelectOption>
                            );
                          })
                        : null}
                    </IonSelect> */}
                    {/* </IonItem> */}
                    {department.lookup ? (
                      <Select
                        onChange={(e) => handleDepartmentalCode(e)}
                        options={deparmentslistSelectOptions}
                      />
                    ) : null}
                  </IonCol>
                ) : null}
                {!paymentRequestFields ? null : paymentRequestFields.fields
                    .header.shortcut_dimension_2_code.enabled.pages.create ? (
                  <IonCol sizeXs="12" sizeMd="4">
                    {/* <IonItem> */}
                    <IonLabel className="form-input">
                      {
                        paymentRequestFields.fields.header
                          .shortcut_dimension_2_code.mapping
                      }
                      :
                    </IonLabel>
                    {/* <IonSelect
                      okText="Okay"
                      cancelText="Dismiss"
                      placeholder={
                        paymentRequestFields.fields.header
                          .shortcut_dimension_2_code.mapping
                      }
                      onIonChange={e => handleProject(e)}
                    >
                      {project.lookup
                        ? project.lookup.map((data, id) => {
                            return (
                              <IonSelectOption
                                key={id}
                                value={project.lookup[id].code}
                              >
                                {project.lookup[id].name}
                              </IonSelectOption>
                            );
                          })
                        : null}
                    </IonSelect> */}
                    {project.lookup ? (
                      <Select
                        onChange={(e) => handleProject(e)}
                        options={projectlistSelectOptions}
                      />
                    ) : null}
                    {/* </IonItem> */}
                  </IonCol>
                ) : null}
              </IonRow>

              <br />
              <br />
              <br />
              {/* Modal for mobile application */}
              <IonModal
                isOpen={showModal}
                onDidDismiss={() => setShowModal(false)}
                className="ModalScroll"
              >
                {/* <> */}
                {newPaymentLines && newPaymentLines
                  ? newPaymentLines.map((datas, index) => {
                      return (
                        <IonRow>
                          {Object.keys(datas).map((field, id) => {
                            switch (field) {
                              case "request_code":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {requestCode.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={requestCodelistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Request Code"
                                      />
                                    ) : null}
                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Request Code"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {requestCode && requestCode.lookup
                                      ? requestCode.lookup.map(
                                          (request, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  requestCode.lookup[id].code
                                                }
                                              >
                                                {
                                                  requestCode.lookup[id]
                                                    .description
                                                }
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "shortcut_dimension_1_code":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {lineDepartment.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={deparmentlinelistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Department"
                                      />
                                    ) : null}

                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Department"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {lineDepartment && lineDepartment.lookup
                                      ? lineDepartment.lookup.map(
                                          (department_code, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  lineDepartment.lookup[id].code
                                                }
                                              >
                                                {lineDepartment.lookup[id].name}
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "shortcut_dimension_2_code":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {lineProject.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={projectlinelistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Project"
                                      />
                                    ) : null}
                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Project"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {lineProject && lineProject.lookup
                                      ? lineProject.lookup.map(
                                          (project_code, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  lineProject.lookup[id].code
                                                }
                                              >
                                                {lineProject.lookup[id].name}
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_3_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {customerGroup.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={customergrouplistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Customer"
                                      />
                                    ) : null}

                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Customer"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {customerGroup && customerGroup.lookup
                                      ? customerGroup.lookup.map(
                                          (customer_group, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  customerGroup.lookup[id].code
                                                }
                                              >
                                                {customerGroup.lookup[id].name}
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_4_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {area.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={arealistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Area"
                                      />
                                    ) : null}
                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Area"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {area && area.lookup
                                      ? area.lookup.map((area_group, id) => {
                                          return (
                                            <IonSelectOption
                                              key={id}
                                              value={area.lookup[id].code}
                                            >
                                              {area.lookup[id].name}
                                            </IonSelectOption>
                                          );
                                        })
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_5_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {businessGroup.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={businesslistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Business"
                                      />
                                    ) : null}
                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Business"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {businessGroup && businessGroup.lookup
                                      ? businessGroup.lookup.map(
                                          (business_group, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  businessGroup.lookup[id].code
                                                }
                                              >
                                                {businessGroup.lookup[id].name}
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_6_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {salesCampaign.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={campaignlistSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Campaign"
                                      />
                                    ) : null}

                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Campaign"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {salesCampaign && salesCampaign.lookup
                                      ? salesCampaign.lookup.map(
                                          (sales_campaign, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  salesCampaign.lookup[id].code
                                                }
                                              >
                                                {salesCampaign.lookup[id].name}
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );
                              case "currency":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {currencyCode.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={currencylineSelectOptions}
                                        value={newPaymentLines[index][field]}
                                        placeholder="Select Currency"
                                      />
                                    ) : null}

                                    {/* <IonSelect
                                    className="request_line"
                                    okText="Okay"
                                    cancelText="Dismiss"
                                    onIonChange={e =>
                                      handleMobileOnInputChange(e, field, index)
                                    }
                                    placeholder="Select Currency"
                                    value={newPaymentLines[index][field]}
                                  >
                                    {currencyCode && currencyCode.lookup
                                      ? currencyCode.lookup.map(
                                          (currency_code, id) => {
                                            return (
                                              <IonSelectOption
                                                key={id}
                                                value={
                                                  currencyCode.lookup[id].code
                                                }
                                              >
                                                {currencyCode.lookup[id].code}
                                              </IonSelectOption>
                                            );
                                          }
                                        )
                                      : null}
                                  </IonSelect> */}
                                  </IonCol>
                                );

                              default:
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {
                                      paymentRequestFields.fields.lines[field]
                                        .mapping
                                    }
                                    <IonItem className="form-border-bottom form-border-all">
                                      <IonInput
                                        onInput={(e) =>
                                          handleMobileOnInputChange(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        value={newPaymentLines[index][field]}
                                        required
                                      ></IonInput>
                                    </IonItem>
                                  </IonCol>
                                );
                            }
                          })}
                          <div className="ion-text-center fix-button">
                            <IonButton
                              onClick={updateModal}
                              className="custom-btn"
                            >
                              ADD
                            </IonButton>
                            <IonButton
                              onClick={() => setShowModal(false)}
                              color="danger"
                            >
                              Close
                            </IonButton>
                          </div>
                        </IonRow>
                      );
                    })
                  : null}
                {/* </IonRow> */}
              </IonModal>

              <IonRow className="ion-padding form-bg form-img">
                <IonCol size="12">
                  <IonRow>
                    <IonCol className="">
                      <div className="ion-float-right">
                        {mobile ? (
                          <IonButton
                            onClick={() => setShowModal(true)}
                            color="dark"
                          >
                            Add Line
                          </IonButton>
                        ) : (
                          <IonButton onClick={addLine} color="dark">
                            Add Line
                          </IonButton>
                        )}
                      </div>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol>
                      <div className="request_line_width">
                        <table className="request_table">
                          <thead>
                            <tr>
                              <th></th>
                              {formLine && formLine
                                ? formLine.map((data, id) => {
                                    return Object.keys(data).map(
                                      (field, keys) => {
                                        return (
                                          <th key={keys}>
                                            {
                                              paymentRequestFields.fields.lines[
                                                field
                                              ].mapping
                                            }
                                          </th>
                                        );
                                      }
                                    );
                                  })
                                : null}
                            </tr>
                          </thead>
                          <tbody>
                            {paymentLines && paymentLines
                              ? paymentLines.map((data, index) => {
                                  return (
                                    <tr>
                                      <td>
                                        <IonButton
                                          color="danger"
                                          className="remove_btn"
                                          onClick={() =>
                                            handleDeleteLine(index)
                                          }
                                        >
                                          <IonIcon name="close" />
                                        </IonButton>
                                      </td>
                                      {Object.keys(data).map((field, id) => {
                                        switch (field) {
                                          case "request_code":
                                            return (
                                              <td key={id}>
                                                {requestCode.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      requestCodelistSelectOptions
                                                    }
                                                    placeholder="Select Request Code"
                                                  />
                                                ) : null}

                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Request Code"
                                              >
                                                {requestCode.lookup
                                                  ? requestCode.lookup.map(
                                                      (request, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              requestCode
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              requestCode
                                                                .lookup[id]
                                                                .description
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "shortcut_dimension_1_code":
                                            return (
                                              <td key={id}>
                                                {lineDepartment.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      deparmentlinelistSelectOptions
                                                    }
                                                    placeholder="Select Department"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Department"
                                              >
                                                {lineDepartment.lookup
                                                  ? lineDepartment.lookup.map(
                                                      (department_code, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              lineDepartment
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              lineDepartment
                                                                .lookup[id].name
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "shortcut_dimension_2_code":
                                            return (
                                              <td key={id}>
                                                {lineProject.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      projectlinelistSelectOptions
                                                    }
                                                    placeholder="Select Project"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Project"
                                              >
                                                {lineProject.lookup
                                                  ? lineProject.lookup.map(
                                                      (project_code, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              lineProject
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              lineProject
                                                                .lookup[id].name
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "shortcut_dim_code_x_005_b_3_x_005_d_":
                                            return (
                                              <td key={id}>
                                                {customerGroup.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      customergrouplistSelectOptions
                                                    }
                                                    placeholder="Select Customer"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Customer"
                                              >
                                                {customerGroup.lookup
                                                  ? customerGroup.lookup.map(
                                                      (customer_group, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              customerGroup
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              customerGroup
                                                                .lookup[id].name
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "shortcut_dim_code_x_005_b_4_x_005_d_":
                                            return (
                                              <td key={id}>
                                                {area.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      arealistSelectOptions
                                                    }
                                                    placeholder="Select Area"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Area"
                                              >
                                                {area.lookup
                                                  ? area.lookup.map(
                                                      (area_group, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              area.lookup[id]
                                                                .code
                                                            }
                                                          >
                                                            {
                                                              area.lookup[id]
                                                                .name
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "shortcut_dim_code_x_005_b_5_x_005_d_":
                                            return (
                                              <td key={id}>
                                                {businessGroup.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      businesslistSelectOptions
                                                    }
                                                    placeholder="Select Business"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Business"
                                              >
                                                {businessGroup.lookup
                                                  ? businessGroup.lookup.map(
                                                      (business_group, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              businessGroup
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              businessGroup
                                                                .lookup[id].name
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "shortcut_dim_code_x_005_b_6_x_005_d_":
                                            return (
                                              <td key={id}>
                                                {salesCampaign.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      campaignlistSelectOptions
                                                    }
                                                    placeholder="Select Campaign"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Campaign"
                                              >
                                                {salesCampaign.lookup
                                                  ? salesCampaign.lookup.map(
                                                      (sales_campaign, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              salesCampaign
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              salesCampaign
                                                                .lookup[id].name
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );
                                          case "currency":
                                            return (
                                              <td key={id}>
                                                {currencyCode.lookup ? (
                                                  <Select
                                                    onChange={(e) =>
                                                      handleOnInputChangeDropDown(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    options={
                                                      currencylineSelectOptions
                                                    }
                                                    placeholder="Select Currency"
                                                  />
                                                ) : null}
                                                {/* <IonSelect
                                                className="request_line"
                                                okText="Okay"
                                                cancelText="Dismiss"
                                                onIonChange={e =>
                                                  handleOnInputChange(
                                                    e,
                                                    field,
                                                    index
                                                  )
                                                }
                                                placeholder="Select Currency"
                                              >
                                                {currencyCode.lookup
                                                  ? currencyCode.lookup.map(
                                                      (currency_code, id) => {
                                                        return (
                                                          <IonSelectOption
                                                            key={id}
                                                            value={
                                                              currencyCode
                                                                .lookup[id].code
                                                            }
                                                          >
                                                            {
                                                              currencyCode
                                                                .lookup[id].code
                                                            }
                                                          </IonSelectOption>
                                                        );
                                                      }
                                                    )
                                                  : null}
                                              </IonSelect> */}
                                              </td>
                                            );

                                          default:
                                            return (
                                              <td key={id}>
                                                <IonItem className="form-border-bottom">
                                                  <IonInput
                                                    onInput={(e) =>
                                                      handleOnInputChange(
                                                        e,
                                                        field,
                                                        index
                                                      )
                                                    }
                                                    value={
                                                      paymentLines[index][field]
                                                    }
                                                    required
                                                  ></IonInput>
                                                </IonItem>
                                              </td>
                                            );
                                        }
                                      })}
                                    </tr>
                                  );
                                })
                              : null}
                          </tbody>
                        </table>
                      </div>
                    </IonCol>
                  </IonRow>
                  <div className="ion-float-right">
                    <IonButton
                      className="custom-btn"
                      size="large"
                      onClick={submitForm}
                      disabled={submitting}
                    >
                      Submit
                      <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                        {submitting ? (
                          <IonSpinner
                            name="crescent"
                            color="light"
                            slot="end"
                          />
                        ) : (
                          <IonIcon name="arrow-dropright" />
                        )}
                      </span>
                    </IonButton>
                    <IonButton
                      className="custom-cancel-btn"
                      size="large"
                      disabled={loading}
                      onClick={() => history.goBack()}
                    >
                      Cancel
                      <span
                        style={{ marginTop: "-3px", marginLeft: "3px" }}
                      ></span>
                    </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        )}
      </AuthWrapper>
    </>
  );
}

export default PaymentRequest;
