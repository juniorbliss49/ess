import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import { useAuth } from "../../context/AuthContext";
import { usePaymentRequest } from "../../context/PaymentRequestContext";
import {
  IonRow,
  IonCol,
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonIcon,
  IonText,
  IonSpinner,
  IonToast,
  IonLoading,
  IonModal,
  IonSegment,
  IonSegmentButton,
  IonLabel,
  IonActionSheet,
  IonList,
  IonItem,
} from "@ionic/react";
import { getPlatforms } from "@ionic/core";
import _ from "lodash";

import {
  refreshPaymentRequest,
  refreshAllPaymentRequests,
  requestApprovalForPaymentRequest,
  cancelApprovalRequestForPaymentRequest,
} from "../../utils/apiClient";

function PaymentRequestHistory({ history }) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errors, setErrors] = useState(null);
  const [paymentRequestForActions, setPaymentRequestForActions] = useState(
    null
  );
  const [showPaymentRequestModal, setShowPaymentRequestModal] = useState(false);
  const [processing, setProcessing] = useState(false);
  const [toastMsg, setToastMsg] = useState("");
  const [filterState, setFilterState] = useState("All");
  const [mobile, setMobile] = useState(false);
  const [showActionSheet, setShowActionSheet] = useState(false);
  const [actionButtons, setActionButtons] = useState(null);

  const showValues = [
    "posting_description",
    "request_type",
    "preferred_pmt_method",
  ];

  const { token, logOutUser } = useAuth();
  const {
    paymentRequests,
    fetchPaymentRequest,
    filteredPaymentRequests,
    setFilteredPaymentRequests,
    syncAll,
  } = usePaymentRequest();

  useEffect(() => {
    setError(false);
    setProcessing(true);

    fetchPaymentRequest(token)
      .then((data) => {
        setProcessing(false);
      })
      .catch((error) => {
        if (error && error.status == 401) {
          logOutUser();
          return;
        }
        setProcessing(false);
      });

    let platformType = platform();
    if (
      platformType.includes("mobile") &&
      !platformType.includes("ipad") &&
      !platformType.includes("tablet")
    ) {
      setMobile(true);
    }
  }, []);

  function applyFilterToPaymentRequests(param) {
    setPaymentRequestForActions(null);

    setFilterState(param);

    setFilteredPaymentRequests(
      paymentRequests
        ? paymentRequests.filter((payReq) => {
            if (param == "All") {
              return payReq;
            }

            return payReq.status == param;
          })
        : []
    );
  }

  function platform() {
    return getPlatforms(window);
  }

  const prepForActions = (index) => {
    setShowActionSheet(true);
    if (
      paymentRequestForActions != null &&
      paymentRequestForActions.index == index
    ) {
      setPaymentRequestForActions(null);
      return;
    }

    let paymentRequest = filteredPaymentRequests[index];
    paymentRequest["index"] = index;

    setPaymentRequestForActions(paymentRequest);
    buttonProperty(paymentRequest);
  };

  function buttonProperty(paymentReq) {
    let button = [];

    if (paymentReq.status == "Open") {
      button = [
        {
          text: "Edit",
          role: "edit",
          icon: "create",
          handler: () => {
            prepForEdit(paymentReq);
          },
        },
        {
          text: "View More",
          icon: "eye",
          handler: () => {
            setShowPaymentRequestModal(true);
          },
        },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            _refresh(paymentReq);
          },
        },
        {
          text: "Request Approval",
          role: "request approval",
          icon: "send",
          handler: () => {
            _requestApproval(paymentReq);
          },
        },
      ];
    } else if (paymentReq.status == "Pending_Approval") {
      button = [
        {
          text: "View More",
          icon: "eye",
          handler: () => {
            setShowPaymentRequestModal(true);
          },
        },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            _refresh(paymentReq);
          },
        },
        {
          text: "Cancel Approval Request",
          icon: "close",
          cssClass: "icon-danger",
          handler: () => {
            _cancelApprovalRequest(paymentReq);
          },
        },
      ];
    } else {
      button = [
        {
          text: "View More",
          icon: "eye",
          handler: () => {
            setShowPaymentRequestModal(true);
          },
        },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            _refresh(paymentReq);
          },
        },
      ];
    }
    setActionButtons(button);
  }

  const prepForEdit = (paymentRequest) => {
    if (paymentRequest.status != "Open") {
      return;
    }

    history.push(`/payment-request/${paymentRequest.id}`);
  };

  const _refresh = async (request) => {
    try {
      setProcessing(true);

      let { message } = await refreshPaymentRequest(
        request.posted_nav_no,
        token
      );

      await fetchPaymentRequest(token);
      setTimeout(() => {
        applyFilterToPaymentRequests(filterState);
        setProcessing(false);
        setToastMsg(message);
      }, 2000);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _syncAll = async () => {
    try {
      setProcessing(true);

      let { message } = await syncAll(token);
      // applyFilterToPaymentRequests(filterState);
      setProcessing(false);
      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _requestApproval = async (request) => {
    try {
      setProcessing(true);

      let { message } = await requestApprovalForPaymentRequest(
        request.id,
        token
      );

      await fetchPaymentRequest(token);
      // applyFilterToPaymentRequests(filterState);
      setProcessing(false);
      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _cancelApprovalRequest = async (request) => {
    try {
      setProcessing(true);

      let { message } = await cancelApprovalRequestForPaymentRequest(
        request.id,
        token
      );

      await fetchPaymentRequest(token);
      // applyFilterToPaymentRequests(filterState);
      setProcessing(false);
      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  return (
    <>
      <AuthWrapper
        name="My Payment Requests"
        backBtnName="Home"
        bg_name="form-bg-img"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={processing}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        <IonModal
          isOpen={showPaymentRequestModal}
          onDidDismiss={() => setShowPaymentRequestModal(false)}
          className="ModalScroll"
        >
          <IonList>
            {paymentRequestForActions ? (
              _.map(paymentRequestForActions, (val, key) => (
                <React.Fragment key={key}>
                  {showValues.includes(key) ? (
                    <IonItem key={key}>
                      <IonLabel>
                        {key == "lines" ? null : (
                          <>
                            {key}: <span style={{ color: "#999" }}> {val}</span>
                          </>
                        )}
                      </IonLabel>
                    </IonItem>
                  ) : null}
                </React.Fragment>
              ))
            ) : (
              <div>No loan card was selected</div>
            )}
          </IonList>
          <div style={{ width: "50%", textAlign: "center" }}>
            <IonButton
              color="danger"
              className="ribbon-color-btn"
              onClick={() => setShowPaymentRequestModal(false)}
            >
              Close
            </IonButton>
          </div>
        </IonModal>

        <IonRow>
          <IonCol
            sizeLg="10"
            offsetLg="1"
            sizeMd="10"
            offsetMd="1"
            sizeSm="10"
            offsetSm="1"
            sizeXs="12"
          >
            <div className="ion-custom-position">
              <IonRow className="ion-padding custom-sticky">
                <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                  <IonButton
                    className="create-btn"
                    onClick={() => history.push("/payment-request")}
                  >
                    <IonIcon slot="start" name="add" />
                    New Payment Request
                  </IonButton>

                  <IonButton
                    title="Sync All"
                    className="create-btn ribbon-color-btn"
                    onClick={_syncAll}
                  >
                    <IonIcon slot="icon-only" name="sync" />
                  </IonButton>
                  {paymentRequestForActions ? (
                    <>
                      {mobile ? (
                        <>
                          <IonActionSheet
                            isOpen={showActionSheet}
                            onDidDismiss={() => setShowActionSheet(false)}
                            buttons={[...actionButtons]}
                          ></IonActionSheet>
                        </>
                      ) : (
                        <>
                          {paymentRequestForActions.status == "Open" ? (
                            <IonButton
                              title="Edit"
                              className="ribbon-color-btn"
                              onClick={() =>
                                prepForEdit(paymentRequestForActions)
                              }
                            >
                              <IonIcon slot="icon-only" name="create" />
                            </IonButton>
                          ) : null}

                          <IonButton
                            title="View More"
                            className="ribbon-color-btn"
                            onClick={() => setShowPaymentRequestModal(true)}
                          >
                            <IonIcon slot="icon-only" name="eye" />
                          </IonButton>

                          <IonButton
                            title="Refresh"
                            className="ribbon-color-btn"
                            onClick={() => _refresh(paymentRequestForActions)}
                          >
                            <IonIcon slot="icon-only" name="refresh" />
                          </IonButton>

                          {paymentRequestForActions.status == "Open" ? (
                            <IonButton
                              title="Request Approval"
                              className="ribbon-color-btn"
                              onClick={() =>
                                _requestApproval(paymentRequestForActions)
                              }
                            >
                              <IonIcon slot="icon-only" name="send" />
                            </IonButton>
                          ) : null}

                          {paymentRequestForActions.status ==
                          "Pending_Approval" ? (
                            <IonButton
                              color="danger"
                              title="Cancel Approval Request"
                              className="ribbon-color-btn"
                              onClick={() =>
                                _cancelApprovalRequest(paymentRequestForActions)
                              }
                            >
                              <IonIcon slot="icon-only" name="close" />
                            </IonButton>
                          ) : null}
                        </>
                      )}
                    </>
                  ) : null}
                </IonCol>
                <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                  <div
                    style={{ maxWidth: "100%" }}
                    className="ion-float-right content-center"
                  >
                    <IonSegment
                      onIonChange={(e) =>
                        applyFilterToPaymentRequests(e.detail.value)
                      }
                      value={filterState}
                    >
                      <IonSegmentButton
                        style={{ marginRight: "3px" }}
                        value="All"
                        className="custom-history-btn custom-history-padding border-right-shape"
                      >
                        <IonLabel style={{ color: "#fff" }}>All</IonLabel>
                      </IonSegmentButton>
                      <IonSegmentButton
                        value="Open"
                        className="custom-history-btn custom-history-padding-2"
                      >
                        <IonLabel style={{ color: "#fff" }}>Open</IonLabel>
                      </IonSegmentButton>
                      <IonSegmentButton
                        value="Pending_Approval"
                        className="custom-history-btn custom-history-padding-2"
                      >
                        <IonLabel style={{ color: "#fff" }}>Pending</IonLabel>
                      </IonSegmentButton>
                      <IonSegmentButton
                        value="Approved"
                        className="custom-history-btn border-right-shape-3"
                      >
                        <IonLabel style={{ color: "#fff" }}>Approved</IonLabel>
                      </IonSegmentButton>
                    </IonSegment>
                  </div>
                </IonCol>
              </IonRow>
            </div>

            {processing ? (
              <IonRow className="custom-sticky">
                <IonCol
                  sizeMd="8"
                  sizeSm="12"
                  sizeXs="12"
                  style={{ margin: "auto", textAlign: "center" }}
                >
                  <h6 color="blue">Please Wait...</h6>
                </IonCol>
              </IonRow>
            ) : null}

            <IonRow className="ion-padding card-history-main">
              {filteredPaymentRequests ? (
                filteredPaymentRequests.map((data, i) => {
                  return (
                    <IonCol
                      key={i}
                      sizeLg="4"
                      sizeMd="4"
                      sizeSm="4"
                      sizeXs="12"
                    >
                      <IonCard
                        className="card-history"
                        style={
                          paymentRequestForActions &&
                          paymentRequestForActions.index == i
                            ? { border: "4px solid #2c4d85", cursor: "pointer" }
                            : { cursor: "pointer" }
                        }
                        onClick={() => prepForActions(i)}
                      >
                        <IonCardHeader className="card-history-header">
                          <IonCardTitle>{data.request_type}</IonCardTitle>
                        </IonCardHeader>
                        <IonCardContent className="ion-text-center">
                          <h5>{data.status}</h5>
                          <h3>{data.posted_nav_no}</h3>
                          <div>
                            <span>
                              <IonText>
                                <IonIcon title="Request Amount" name="cash" />
                                {data.amount}
                              </IonText>
                            </span>
                          </div>
                        </IonCardContent>
                      </IonCard>
                    </IonCol>
                  );
                })
              ) : errors ? (
                <IonCol size="4" offset="4" className="ion-text-center">
                  {errors.map((error, i) => (
                    <div key={i}>{error}</div>
                  ))}
                </IonCol>
              ) : null}
            </IonRow>
            {/* <IonRow>
              <IonCol size="4" offset="4" className="ion-text-center">
                <IonButton className="history-btn">Load More...</IonButton>
              </IonCol>
            </IonRow> */}
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default PaymentRequestHistory;
