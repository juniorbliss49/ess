import React from "react";
import {
  IonContent,
  IonRow,
  IonCol,
  IonIcon,
  IonButton,
  IonSelect,
  IonItem,
  IonInput
} from "@ionic/react";

function PaymentRequestModal(props) {
  return (
    <>
      {console.log(props.data)}
      <IonContent>
        <IonRow>
          <IonCol>
            {props.data && props.data
              ? props.data.map((datas, index) => {
                  return (
                    <tr>
                      {Object.keys(datas).map((field, id) => {
                        switch (field) {
                          default:
                            return (
                              <td key={id}>
                                <IonItem className="form-border-bottom">
                                  {field}
                                  <IonInput required></IonInput>
                                </IonItem>
                              </td>
                            );
                        }
                      })}
                    </tr>
                  );
                })
              : null}
          </IonCol>
        </IonRow>
      </IonContent>
    </>
  );
}

export default PaymentRequestModal;
