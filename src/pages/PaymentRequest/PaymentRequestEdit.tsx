import React, { useEffect, useState } from "react";
import {
  IonLoading,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonDatetime,
  IonIcon,
  IonSpinner,
  IonToast,
  IonModal,
  IonButton,
} from "@ionic/react";
import { usePaymentRequest } from "../../context/PaymentRequestContext";
import { useAuth } from "../../context/AuthContext";
import { Redirect } from "react-router";
import AuthWrapper from "../Layout/AuthWrapper";
import { getPlatforms } from "@ionic/core";
import Select from "react-select";
import DatePicker from "react-datepicker";

function PaymentRequestEdit({ match }) {
  const {
    prepPaymentApplicationFieldsForUpdate,
    paymentRequestFields,
    loading,
    requestType,
    currencyCode,
    preferredPmtMethod,
    department,
    project,
    paymentLines,
    formLine,
    requestCode,
    lineDepartment,
    lineProject,
    customerGroup,
    area,
    businessGroup,
    salesCampaign,
    setPaymentLines,
    newLines,
    setLoading,
    postPaymentRequest,
    newPaymentLines,
    setNewPaymentLines,
    requestlistSelectOptions,
    requestOption,
    setRequestOption,
    currencylistSelectOptions,
    currencyOption,
    setCurrencyOption,
    setPreferredmethodOption,
    preferredmethodOption,
    paymentlistSelectOptions,
    setDeparmentOption,
    deparmentOption,
    deparmentslistSelectOptions,
    setProjectOption,
    projectOption,
    projectlistSelectOptions,
    requestCodelistSelectOptions,
    deparmentlinelistSelectOptions,
    setRequestCodeSelectValue,
    requestCodeSelectValue,
    projectlinelistSelectOptions,
    customergrouplistSelectOptions,
    arealistSelectOptions,
    businesslistSelectOptions,
    campaignlistSelectOptions,
    currencylineSelectOptions,
    responsibilityCenters,
    respCenterOption,
    setRespCenterOption,
    respCenter,
  } = usePaymentRequest();

  const { token, logOutUser } = useAuth();

  const [submitting, setSubmitting] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [requestWasUpdated, setRequestWasUpdated] = useState(false);
  const [paymentRequest, setPaymentRequest] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [requestWasCreated, setRequestWasCreated] = useState(false);
  const [toastMsg, setToastMsg] = useState(null);
  const [errors, setErrors] = useState([]);
  const [mobile, setMobile] = useState(false);
  const [selectedOption, setSelectedOption] = useState({
    value: "vanilla",
    label: "Vanilla",
  });

  useEffect(() => {
    prepPaymentApplicationFieldsForUpdate(match.params.reqId, token).then(
      (data) => {
        setPaymentRequest(data.payment_request);
      }
    );
    let platformType = platform();
    if (
      platformType.includes("mobile") &&
      !platformType.includes("ipad") &&
      !platformType.includes("tablet")
    ) {
      setMobile(true);
    }
  }, []);

  function platform() {
    return getPlatforms(window);
  }

  const handleInputFieldDropDown = (e, field) => {
    let oldPaymentRequest = { ...paymentRequest };
    oldPaymentRequest[field] = e.value;
    switch (field) {
      case "request_type":
        setRequestOption(e);

        break;
      case "currency_code":
        setCurrencyOption(e);
        break;
      case "preferred_pmt_method":
        setPreferredmethodOption(e);
        break;
      case "shortcut_dimension_1_code":
        setDeparmentOption(e);
        break;
      case "shortcut_dimension_2_code":
        setProjectOption(e);
        break;
      case "responsibility_center":
        setRespCenterOption(e);
        break;

      default:
        break;
    }
    setPaymentRequest(oldPaymentRequest);
  };

  const handleInputField = (e, field) => {
    let oldPaymentRequest = { ...paymentRequest };
    oldPaymentRequest[field] = e.target.value;
    setPaymentRequest(oldPaymentRequest);
  };

  const handleCreationDate = (e, field) => {
    let oldPaymentRequest = { ...paymentRequest };
    oldPaymentRequest[field] = e;
    setPaymentRequest(oldPaymentRequest);
  };

  const handleOnInputChangeDropDown = (e, field, index) => {
    let getOldLinesInput = [...paymentLines];
    getOldLinesInput[index][field] = e.value;

    setPaymentLines(getOldLinesInput);
  };

  const handleOnInputChange = (e, field, index) => {
    let getOldLinesInput = [...paymentLines];
    getOldLinesInput[index][field] = e.target.value;
    setPaymentLines(getOldLinesInput);
  };

  const handleMobileOnInputChange = (e, field, index) => {
    let getOldLinesInput = [...newPaymentLines];
    getOldLinesInput[index][field] = e.target.value;
    setNewPaymentLines(getOldLinesInput);
  };

  const handleMobileOnInputChangeDropDown = (e, field, index) => {
    let getOldLinesInput = [...newPaymentLines];
    getOldLinesInput[index][field] = e.value;
    setNewPaymentLines(getOldLinesInput);
  };

  const filterSearchedList = (value, list, field) => {
    switch (field) {
      case "request_code":
        let results = list.filter((item) => {
          return item.value == value;
        });
        setRequestCodeSelectValue(results);
        break;

      default:
        break;
    }
  };

  const updateModal = () => {
    let previousLines = [...paymentLines];
    let getOldLinesInput = [...newPaymentLines];
    previousLines.push(getOldLinesInput[0]);
    setPaymentLines(previousLines);
    setShowModal(false);
    let formLines = [...formLine];
    setNewPaymentLines(() => {
      return formLines;
    });
  };

  function numProps(obj) {
    var c = 0;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) ++c;
    }
    return c;
  }

  const addLine = () => {
    let oldLine = [...paymentLines];
    let newLine = { ...newLines };
    let numerofarray = numProps(paymentLines);
    newLine["id"] = numerofarray + 1;
    oldLine.push(newLine);
    setPaymentLines(oldLine);
  };

  const handleDeleteLine = (index) => {
    if (paymentLines.length === 1) {
      return;
    }
    let getOldLinesInput = [...paymentLines];
    getOldLinesInput.splice(index, 1);
    setPaymentLines(getOldLinesInput);
  };

  const closeError = () => {
    setErrors([]);
  };

  const submitForm = async () => {
    setSubmitting(true);
    let formData = {
      payment_request_data: {
        amount: paymentRequest.amount,
        status: paymentRequest.status,
        beneficiary: paymentRequest.beneficiary,
        request_type: paymentRequest.request_type,
        responsibility_center: paymentRequest.responsibility_center,
        creation_date: paymentRequest.creation_date,
        currency_code: paymentRequest.currency_code,
        document_date: paymentRequest.document_date,
        posting_description: paymentRequest.posting_description,
        preferred_bank_code: paymentRequest.preferred_bank_code,
        preferred_pmt_method: paymentRequest.preferred_pmt_method,
        payee_bank_account_no: paymentRequest.payee_bank_account_no,
        shortcut_dimension_1_Code: paymentRequest.shortcut_dimension_1_code,
        shortcut_dimension_2_Code: paymentRequest.shortcut_dimension_2_code,
        lines: paymentLines,
      },
    };
    try {
      const paymentRequestField = await postPaymentRequest(
        match.params.reqId,
        formData,
        token
      );

      if (paymentRequestField.error) {
        throw Error(paymentRequestField.message);
      }

      setTimeout(() => {
        setToastMsg(paymentRequestField.message);
        setSubmitting(false);
        setRequestWasCreated(true);
      }, 2000);
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      const {
        response: { body },
      } = error;

      setToastMsg(body.message);

      body.message && setFormErrors([body.message]);
      body.errors && updateErrors(body.errors);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  const updateErrors = (errors) => {
    setErrors(Object.values(errors).flat());
  };

  if (requestWasCreated) {
    return <Redirect to="/payment-request-history" />;
  }

  return (
    <>
      <AuthWrapper
        name="Payment Request"
        bg_name="form-bg-img"
        backBtnName="My Payment Requests"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}

        {/* <IonLoading
          isOpen={loading || submitting}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {formErrors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {errors && errors.map((error, i) => <li key={i}>{error}</li>)}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        {loading ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : (
          <IonRow>
            <IonCol
              sizeMd="10"
              sizeSm="10"
              sizeXs="11"
              style={{ margin: "auto" }}
            >
              {paymentRequestFields ? (
                <IonRow className="ion-padding form-bg form-border">
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {paymentRequestFields.fields.header.amount.mapping}:
                        </IonLabel>
                        <IonInput
                          type="text"
                          onIonChange={(e) => handleInputField(e, "amount")}
                          value={paymentRequest ? paymentRequest.amount : null}
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.status.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {paymentRequestFields.fields.header.status.mapping}:
                        </IonLabel>
                        <IonInput
                          type="text"
                          onIonChange={(e) => handleInputField(e, "status")}
                          value={paymentRequest ? paymentRequest.status : null}
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.beneficiary.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {
                            paymentRequestFields.fields.header.beneficiary
                              .mapping
                          }
                          :
                        </IonLabel>
                        <IonInput
                          type="text"
                          onIonChange={(e) =>
                            handleInputField(e, "beneficiary")
                          }
                          value={
                            paymentRequest ? paymentRequest.beneficiary : null
                          }
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.request_type.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header.request_type
                            .mapping
                        }
                        :
                      </IonLabel>
                      {requestType.lookup ? (
                        <Select
                          value={requestOption}
                          onChange={(e) =>
                            handleInputFieldDropDown(e, "request_type")
                          }
                          options={requestlistSelectOptions}
                        />
                      ) : null}
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.creation_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {
                            paymentRequestFields.fields.header.creation_date
                              .mapping
                          }
                          :
                        </IonLabel>
                        {/* <DatePicker
                                        selected={paymentRequest ? paymentRequest.creation_date.substr(0, 10) : null}
                                        onChange={e => handleCreationDate(e, 'creation_date')}
                                        dateFormat="yyyy-MM-dd"
                                    /> */}
                        <IonDatetime
                          displayFormat="DD/MM/YYYY"
                          onIonChange={(e) =>
                            handleInputField(e, "creation_date")
                          }
                          value={
                            paymentRequest ? paymentRequest.creation_date : null
                          }
                        ></IonDatetime>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.currency_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header.currency_code
                            .mapping
                        }
                        :
                      </IonLabel>
                      {currencyCode.lookup ? (
                        <Select
                          value={currencyOption}
                          onChange={(e) =>
                            handleInputFieldDropDown(e, "currency_code")
                          }
                          options={currencylistSelectOptions}
                        />
                      ) : null}
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.responsibility_center.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header
                            .responsibility_center.mapping
                        }
                        :
                      </IonLabel>
                      <Select
                        value={respCenterOption}
                        onChange={(e) =>
                          handleInputFieldDropDown(e, "responsibility_center")
                        }
                        options={responsibilityCenters}
                      />
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.document_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header.document_date
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonItem>
                        <IonDatetime
                          displayFormat="DD/MM/YYYY"
                          onIonChange={(e) =>
                            handleInputField(e, "document_date")
                          }
                          value={
                            paymentRequest ? paymentRequest.document_date : null
                          }
                        ></IonDatetime>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.posting_description.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header.posting_description
                            .mapping
                        }
                        :
                      </IonLabel>
                      <IonItem>
                        <IonInput
                          type="text"
                          onIonChange={(e) =>
                            handleInputField(e, "posting_description")
                          }
                          value={
                            paymentRequest
                              ? paymentRequest.posting_description
                              : null
                          }
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.preferred_bank_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {
                            paymentRequestFields.fields.header
                              .preferred_bank_code.mapping
                          }
                          :
                        </IonLabel>
                        <IonInput
                          type="text"
                          onIonChange={(e) =>
                            handleInputField(e, "preferred_bank_code")
                          }
                          value={
                            paymentRequest
                              ? paymentRequest.preferred_bank_code
                              : null
                          }
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.preferred_pmt_method.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header
                            .preferred_pmt_method.mapping
                        }
                        :
                      </IonLabel>
                      {preferredPmtMethod.lookup ? (
                        <Select
                          value={preferredmethodOption}
                          onChange={(e) =>
                            handleInputFieldDropDown(e, "preferred_pmt_method")
                          }
                          options={paymentlistSelectOptions}
                        />
                      ) : null}
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.payee_bank_account_no.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonItem>
                        <IonLabel className="form-input">
                          {
                            paymentRequestFields.fields.header
                              .payee_bank_account_no.mapping
                          }
                          :
                        </IonLabel>
                        <IonInput
                          type="text"
                          onIonChange={(e) =>
                            handleInputField(e, "payee_bank_account_no")
                          }
                          value={
                            paymentRequest
                              ? paymentRequest.payee_bank_account_no
                              : null
                          }
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.shortcut_dimension_1_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header
                            .shortcut_dimension_1_code.mapping
                        }
                        :
                      </IonLabel>

                      {department.lookup ? (
                        <Select
                          value={deparmentOption}
                          onChange={(e) =>
                            handleInputFieldDropDown(
                              e,
                              "shortcut_dimension_1_code"
                            )
                          }
                          options={deparmentslistSelectOptions}
                        />
                      ) : null}
                    </IonCol>
                  ) : null}
                  {!paymentRequestFields ? null : paymentRequestFields.fields
                      .header.shortcut_dimension_2_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          paymentRequestFields.fields.header
                            .shortcut_dimension_2_code.mapping
                        }
                        :
                      </IonLabel>
                      {preferredPmtMethod.lookup ? (
                        <Select
                          value={projectOption}
                          onChange={(e) =>
                            handleInputFieldDropDown(
                              e,
                              "shortcut_dimension_2_code"
                            )
                          }
                          options={projectlistSelectOptions}
                        />
                      ) : null}
                    </IonCol>
                  ) : null}
                </IonRow>
              ) : null}
              <br />
              <br />
              <br />
              {/* Modal for mobile application */}
              <IonModal
                isOpen={showModal}
                className="ModalScroll"
                onDidDismiss={() => setShowModal(false)}
              >
                {/* <> */}
                {newPaymentLines && newPaymentLines
                  ? newPaymentLines.map((datas, index) => {
                      return (
                        <IonRow key={index}>
                          {Object.keys(datas).map((field, id) => {
                            switch (field) {
                              case "request_code":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {requestCode && requestCode.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={requestCodelistSelectOptions}
                                        value={requestCodelistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Request Code"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "shortcut_dimension_1_code":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {lineDepartment && lineDepartment.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={deparmentlinelistSelectOptions}
                                        value={deparmentlinelistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Department"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "shortcut_dimension_2_code":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {lineProject && lineProject.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={projectlinelistSelectOptions}
                                        value={projectlinelistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Project"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_3_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {customerGroup && customerGroup.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={customergrouplistSelectOptions}
                                        value={customergrouplistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Customer Group"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_4_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {area && area.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={arealistSelectOptions}
                                        value={arealistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Area"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_5_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {businessGroup && businessGroup.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={businesslistSelectOptions}
                                        value={businesslistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Business"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "shortcut_dim_code_x_005_b_6_x_005_d_":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {salesCampaign && salesCampaign.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={campaignlistSelectOptions}
                                        value={campaignlistSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Campaign"
                                      />
                                    ) : null}
                                  </IonCol>
                                );
                              case "currency":
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {currencyCode && currencyCode.lookup ? (
                                      <Select
                                        onChange={(e) =>
                                          handleMobileOnInputChangeDropDown(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        options={currencylineSelectOptions}
                                        value={currencylineSelectOptions.filter(
                                          (item) =>
                                            item.value ==
                                            newPaymentLines[index][field]
                                        )}
                                        placeholder="Select Campaign"
                                      />
                                    ) : null}
                                  </IonCol>
                                );

                              default:
                                return (
                                  <IonCol
                                    key={id}
                                    size="12"
                                    className="ion-padding"
                                  >
                                    {
                                      paymentRequestFields.fields.lines[field]
                                        .mapping
                                    }
                                    <IonItem className="form-border-bottom form-border-all">
                                      <IonInput
                                        onInput={(e) =>
                                          handleMobileOnInputChange(
                                            e,
                                            field,
                                            index
                                          )
                                        }
                                        value={newPaymentLines[index][field]}
                                        required
                                      ></IonInput>
                                    </IonItem>
                                    <br />
                                  </IonCol>
                                );
                            }
                          })}
                          <div className="ion-text-center fix-button">
                            <IonButton
                              onClick={updateModal}
                              className="custom-btn"
                            >
                              ADD
                            </IonButton>
                            <IonButton
                              onClick={() => setShowModal(false)}
                              color="danger"
                            >
                              Close
                            </IonButton>
                          </div>
                        </IonRow>
                      );
                    })
                  : null}
                {/* </IonRow> */}
              </IonModal>
              {paymentRequestFields ? (
                <IonRow className="ion-padding form-bg form-img">
                  <IonCol size="12">
                    <IonRow>
                      <IonCol className="">
                        <div className="ion-float-right">
                          {mobile ? (
                            <IonButton
                              onClick={() => setShowModal(true)}
                              color="dark"
                            >
                              Add Line
                            </IonButton>
                          ) : (
                            <IonButton onClick={addLine} color="dark">
                              Add Line
                            </IonButton>
                          )}
                        </div>
                      </IonCol>
                    </IonRow>
                    <IonRow>
                      <IonCol>
                        <div className="request_line_width">
                          <table className="request_table">
                            <thead>
                              <tr>
                                <th></th>
                                {formLine && formLine
                                  ? formLine.map((data, id) => {
                                      return Object.keys(data).map(
                                        (field, keys) => {
                                          return (
                                            <th key={keys}>
                                              {
                                                paymentRequestFields.fields
                                                  .lines[field].mapping
                                              }
                                            </th>
                                          );
                                        }
                                      );
                                    })
                                  : null}
                              </tr>
                            </thead>
                            <tbody>
                              {paymentLines && paymentLines
                                ? paymentLines.map((data, index) => {
                                    return (
                                      <tr key={index}>
                                        <td>
                                          <IonButton
                                            color="danger"
                                            className="remove_btn"
                                            onClick={() =>
                                              handleDeleteLine(index)
                                            }
                                          >
                                            <IonIcon name="close" />
                                          </IonButton>
                                        </td>
                                        {Object.keys(data).map((field, id) => {
                                          switch (field) {
                                            case "request_code":
                                              return (
                                                <td key={id}>
                                                  {requestCode &&
                                                  requestCode.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        requestCodelistSelectOptions
                                                      }
                                                      value={requestCodelistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Request Code"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "shortcut_dimension_1_code":
                                              // filterSearchedList(paymentLines[index][field], deparmentlinelistSelectOptions, field)
                                              return (
                                                <td key={id}>
                                                  {lineDepartment &&
                                                  lineDepartment.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        deparmentlinelistSelectOptions
                                                      }
                                                      value={deparmentlinelistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Department"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "shortcut_dimension_2_code":
                                              return (
                                                <td key={id}>
                                                  {lineProject &&
                                                  lineProject.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        projectlinelistSelectOptions
                                                      }
                                                      value={projectlinelistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Project"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "shortcut_dim_code_x_005_b_3_x_005_d_":
                                              return (
                                                <td key={id}>
                                                  {customerGroup &&
                                                  customerGroup.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        customergrouplistSelectOptions
                                                      }
                                                      value={customergrouplistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Customer Group"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "shortcut_dim_code_x_005_b_4_x_005_d_":
                                              return (
                                                <td key={id}>
                                                  {area && area.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        arealistSelectOptions
                                                      }
                                                      value={arealistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Area"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "shortcut_dim_code_x_005_b_5_x_005_d_":
                                              return (
                                                <td key={id}>
                                                  {businessGroup &&
                                                  businessGroup.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        businesslistSelectOptions
                                                      }
                                                      value={businesslistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Business"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "shortcut_dim_code_x_005_b_6_x_005_d_":
                                              return (
                                                <td key={id}>
                                                  {salesCampaign &&
                                                  salesCampaign.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        campaignlistSelectOptions
                                                      }
                                                      value={campaignlistSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Business"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "currency":
                                              return (
                                                <td key={id}>
                                                  {currencyCode &&
                                                  currencyCode.lookup ? (
                                                    <Select
                                                      onChange={(e) =>
                                                        handleOnInputChangeDropDown(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      options={
                                                        currencylineSelectOptions
                                                      }
                                                      value={currencylineSelectOptions.filter(
                                                        (item) =>
                                                          item.value ==
                                                          paymentLines[index][
                                                            field
                                                          ]
                                                      )}
                                                      placeholder="Select Currency"
                                                    />
                                                  ) : null}
                                                </td>
                                              );
                                            case "id":
                                              return null;

                                            default:
                                              return (
                                                <td key={id}>
                                                  <IonItem className="form-border-bottom">
                                                    <IonInput
                                                      onInput={(e) =>
                                                        handleOnInputChange(
                                                          e,
                                                          field,
                                                          index
                                                        )
                                                      }
                                                      value={
                                                        paymentLines[index][
                                                          field
                                                        ]
                                                      }
                                                      required
                                                    ></IonInput>
                                                  </IonItem>
                                                </td>
                                              );
                                          }
                                        })}
                                      </tr>
                                    );
                                  })
                                : null}
                            </tbody>
                          </table>
                        </div>
                      </IonCol>
                    </IonRow>
                    <div className="ion-float-right">
                      <IonButton
                        className="custom-btn"
                        size="large"
                        onClick={submitForm}
                        disabled={submitting}
                      >
                        Update
                        <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                          {submitting ? (
                            <IonSpinner
                              name="crescent"
                              color="light"
                              slot="end"
                            />
                          ) : (
                            <IonIcon name="arrow-dropright" />
                          )}
                        </span>
                      </IonButton>
                    </div>
                  </IonCol>
                </IonRow>
              ) : null}
            </IonCol>
          </IonRow>
        )}
      </AuthWrapper>
    </>
  );
}

export default PaymentRequestEdit;
