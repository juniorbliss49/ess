import React, { useState, useEffect, useRef } from "react";
import {
  IonRow,
  IonCol,
  IonImg,
  IonButton,
  IonIcon,
  IonSlides,
  IonSlide,
  IonLoading,
  IonSkeletonText,
} from "@ionic/react";

import "../styles/home.scss";

import MenuHeader from "../components/MenuHeader";
import SidebarMenu from "./Layout/SidebarMenu";
import { useAuth } from "../context/AuthContext";
import Notification from "./Layout/Notification";
import AuthWrapper from "./Layout/AuthWrapper";
import AuthPageWrapper from "./Layout/AuthPageWrapper";
import Doughtnut from "../components/Doughtnut";
import ProgressBar from "../components/ProgressBar";
import { useHomeApplication } from "../context/HomeContext";

function Home({ history }) {
  const { token, logOutUser } = useAuth();

  const [paymentRequestCount, setPaymentRequestCount] = useState(null);
  const [lastPaymentRequestAmount, setLastPaymentRequestAmount] = useState(
    null
  );
  const [lastLoanAmount, setLastLoanAmount] = useState(null);
  const [annualLeavesTaken, setAnnualLeavesTaken] = useState(0);
  const [annualLeavesScheduled, setAnnualLeavesScheduled] = useState(0);
  const [receivedQueries, setReceivedQueries] = useState(null);
  const [sentQueries, setSentQueries] = useState(null);
  const [loading, setLoading] = useState(false);
  const [year, setYear] = useState(null);
  // const [loading, setLoading] = useState(false);
  // const [loading, setLoading] = useState(false);

  const { getDashboard } = useHomeApplication();
  const slider = useRef(null);
  const slider_2 = useRef(null);

  const donut = {
    value: 63,
    valuelabel: "Completed",
    size: 116,
    strokewidth: 26,
  };
  let progressBar = annualLeavesTaken;
  const margin = progressBar === 10 ? 9 : progressBar;
  const slideOpts = {
    initialSlide: 0,
    speed: 400,
  };

  var month = new Array();
  month[0] = "January";
  month[1] = "February";
  month[2] = "March";
  month[3] = "April";
  month[4] = "May";
  month[5] = "June";
  month[6] = "July";
  month[7] = "August";
  month[8] = "September";
  month[9] = "October";
  month[10] = "November";
  month[11] = "December";

  useEffect(() => {
    setLoading(true);
    let present_year = new Date();
    setYear(present_year);
    getDashboard(token)
      .then((data) => {
        setLoading(false);
        setPaymentRequestCount(parseInt(data[0].paymentRequestCount));
        setLastPaymentRequestAmount(parseInt(data[0].lastPaymentRequestAmount));
        setLastLoanAmount(parseInt(data[1].lastLoanAmount));
        setAnnualLeavesTaken(data[2].annualLeavesTaken);
        setAnnualLeavesScheduled(data[2].annualLeavesScheduled);
        setReceivedQueries(data[3].receivedQueries);
        setSentQueries(data[3].sentQueries);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        if (error && error.status == 401) {
          logOutUser();
          return;
        }
      });
  }, []);

  const nextSlide = (e) => {
    // const slides = document.getElementById('ionslide')
    return true;
  };

  const goToNextSlide = () => {
    slider.current.slideNext();
  };

  const goToPreSlide = () => {
    slider.current.slidePrev();
  };
  const goToNextSlide_2 = () => {
    slider_2.current.slideNext();
  };

  const goToPreSlide_2 = () => {
    slider_2.current.slidePrev();
  };

  return (
    <>
      <AuthWrapper name="Home" bg_name="home-bg-img" backBtnName="">
        {/* <IonLoading
          isOpen={loading}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        <IonRow className="custom-column">
          <IonCol sizeMd="12" sizeLg="12" sizeSm="12" sizeXs="12">
            <IonRow>
              <IonCol
                sizeMd="4"
                sizeLg="4"
                sizeSm="4"
                sizeXs="12"
                className="ion-float-left"
                style={{ paddingTop: "0" }}
              >
                <div className="custom-request">
                  <IonRow>
                    <IonCol>
                      <IonImg
                        src="assets/images/receipt_1.svg"
                        className="ion-float-right home-icon"
                      />
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol className="ion-text-center">
                      {loading ? (
                        <>
                          <h1>
                            <IonSkeletonText animated></IonSkeletonText>
                          </h1>
                          <p>
                            <IonSkeletonText animated></IonSkeletonText>
                          </p>
                        </>
                      ) : (
                        <>
                          <h1>
                            {paymentRequestCount && paymentRequestCount
                              ? parseInt(paymentRequestCount)
                              : 0}
                          </h1>
                          <p>Payment Requests Raised</p>
                        </>
                      )}
                    </IonCol>
                  </IonRow>
                </div>
              </IonCol>
              <IonCol
                sizeMd="4"
                sizeLg="4"
                sizeSm="4"
                sizeXs="12"
                style={{ paddingTop: "0" }}
              >
                <div className="custom-request">
                  <IonRow>
                    <IonCol>
                      <IonImg
                        src="assets/images/payment-method_1.svg"
                        className="ion-float-right home-icon"
                      />
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol className="ion-text-center">
                      {loading ? (
                        <>
                          <h1>
                            <IonSkeletonText animated></IonSkeletonText>
                          </h1>
                          <p>
                            <IonSkeletonText animated></IonSkeletonText>
                          </p>
                        </>
                      ) : (
                        <>
                          <h1>
                            {lastPaymentRequestAmount &&
                            lastPaymentRequestAmount
                              ? lastPaymentRequestAmount.toFixed(2)
                              : 0}
                          </h1>
                          <p>Last Payment Request Amount</p>
                        </>
                      )}
                    </IonCol>
                  </IonRow>
                </div>
              </IonCol>
              <IonCol
                sizeMd="4"
                sizeLg="4"
                sizeSm="4"
                sizeXs="12"
                style={{ paddingTop: "0" }}
              >
                <div className="custom-request">
                  <IonRow>
                    <IonCol>
                      <IonImg
                        src="assets/images/funds_1.svg"
                        className="ion-float-right home-icon"
                      />
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol className="ion-text-center">
                      {loading ? (
                        <>
                          <h1>
                            <IonSkeletonText animated></IonSkeletonText>
                          </h1>
                          <p>
                            <IonSkeletonText animated></IonSkeletonText>
                          </p>
                        </>
                      ) : (
                        <>
                          <h1>
                            {lastLoanAmount && lastLoanAmount
                              ? lastLoanAmount.toFixed(2)
                              : 0}
                          </h1>
                          <p>Last Loan Amount</p>
                        </>
                      )}
                    </IonCol>
                  </IonRow>
                </div>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol className="custom-progress-bg">
                {annualLeavesTaken != annualLeavesScheduled ? (
                  <p
                    className="meter meter-2"
                    style={{
                      marginLeft: `${
                        (annualLeavesTaken / annualLeavesScheduled) * 100
                      }%`,
                      // position: "absolute",
                      // marginTop: "-15px"
                    }}
                  >
                    {annualLeavesTaken} days
                  </p>
                ) : null}
                <ProgressBar
                  progress={(annualLeavesTaken / annualLeavesScheduled) * 100}
                />
                <IonRow>
                  <IonCol className="ion-text-left">
                    {loading ? (
                      <IonSkeletonText animated></IonSkeletonText>
                    ) : (
                      <p className="meter">0 Days</p>
                    )}
                  </IonCol>
                  <IonCol className="ion-text-center">
                    <p
                      style={{
                        whiteSpace: "nowrap",
                        overflow: "auto",
                        textOverflow: "ellipsis",
                      }}
                    >
                      Annual Leave Meter
                    </p>
                  </IonCol>
                  <IonCol className="ion-text-right">
                    {loading ? (
                      <IonSkeletonText animated></IonSkeletonText>
                    ) : (
                      <p className="meter">{annualLeavesScheduled} Days</p>
                    )}
                  </IonCol>
                </IonRow>
              </IonCol>
            </IonRow>
          </IonCol>
          {/* <IonCol
            sizeMd="3"
            sizeLg="3"
            sizeSm="3"
            sizeXs="12"
            className="ion-hide-card"
          >
            <div className="donut ion-text-center">
              <div style={{margin: "0 auto", position: "relative", width: "70%", height: "100%"}}>
              <Doughtnut {...donut} />
              <br/><br/><br/>
              <p
                className="custom-p-2"
                style={{
                  position: "absolute",
                  bottom: "6%",
                  left: "30%",
                  fontSize: "16px",
                  color: "#385C8D",
                  textAlign: "center"
                }}
              >
                Last Appraisal Score
              </p>
            </div>
            </div>
          </IonCol> */}
        </IonRow>
        <br />
        <IonRow className="custom-column">
          <IonCol sizeMd="9" sizeLg="9" sizeSm="9" sizeXs="12">
            <IonSlides id="ionslide" options={slideOpts} ref={slider}>
              <IonSlide>
                <IonRow className="custom-home-border-bottom">
                  <IonCol size="9" className="no-padding">
                    <h3 className="custom-home-h3">Received Queries</h3>
                  </IonCol>
                  <IonCol size="3" className="ion-text-right no-padding">
                    <IonButton className="custom-home-btn-icon" size="small">
                      <IonIcon name="ios-arrow-back" />
                    </IonButton>
                    <IonButton
                      className="custom-home-btn-icon"
                      size="small"
                      onClick={goToNextSlide}
                    >
                      <IonIcon name="ios-arrow-forward" />
                    </IonButton>
                  </IonCol>
                </IonRow>
                <br />
                <br />
                <IonRow>
                  <IonCol size="12" className="no-padding">
                    {receivedQueries ? (
                      <table
                        className="leave-balance-table table-space"
                        id="leave-balance-table"
                      >
                        <thead>
                          <tr>
                            <th>Date of Query</th>
                            <th>Queried By</th>
                            <th>Reason</th>
                          </tr>
                        </thead>
                        <tbody>
                          {receivedQueries
                            ? receivedQueries.map((query) => {
                                return (
                                  <tr
                                    style={{ backgroundColor: "#fff" }}
                                    key={query.id}
                                  >
                                    <td>{query.date_of_query}</td>
                                    <td>{query.queried_by_name}</td>
                                    <td>{query.cause_of_query_code}</td>
                                  </tr>
                                );
                              })
                            : null}
                        </tbody>
                      </table>
                    ) : null}
                  </IonCol>
                </IonRow>
              </IonSlide>
              <IonSlide>
                <IonRow className="custom-home-border-bottom">
                  <IonCol size="9" className="no-padding">
                    <h3 className="custom-home-h3">Sent Queries</h3>
                  </IonCol>
                  <IonCol size="3" className="ion-text-right no-padding">
                    <IonButton
                      className="custom-home-btn-icon"
                      size="small"
                      onClick={goToPreSlide}
                    >
                      <IonIcon name="ios-arrow-back" />
                    </IonButton>
                    <IonButton
                      className="custom-home-btn-icon"
                      size="small"
                      onClick={goToNextSlide}
                    >
                      <IonIcon name="ios-arrow-forward" />
                    </IonButton>
                  </IonCol>
                </IonRow>
                <br />
                <br />
                <IonRow>
                  <IonCol size="12" className="no-padding">
                    {sentQueries ? (
                      <table
                        className="leave-balance-table table-space"
                        id="leave-balance-table"
                      >
                        <thead>
                          <tr>
                            <th>Date of Query</th>
                            <th>Employee</th>
                            <th>Reason</th>
                          </tr>
                        </thead>
                        <tbody>
                          {sentQueries
                            ? sentQueries.map((query) => {
                                return (
                                  <tr
                                    style={{ backgroundColor: "#fff" }}
                                    key={query.id}
                                  >
                                    <td>{query.date_of_query}</td>
                                    <td>{query.employee_no}</td>
                                    <td>{query.cause_of_query_code}</td>
                                  </tr>
                                );
                              })
                            : null}
                        </tbody>
                      </table>
                    ) : null}
                  </IonCol>
                </IonRow>
              </IonSlide>
              {/* <IonSlide>
                  <h1>Slide 3</h1>
                </IonSlide> */}
            </IonSlides>
          </IonCol>
          <IonCol
            sizeMd="3"
            sizeLg="3"
            sizeSm="3"
            sizeXs="12"
            className="ion-hide-card"
          >
            <IonSlides id="ionslide" options={slideOpts} ref={slider_2}>
              <IonSlide>
                <IonRow className="custom-home-border-bottom">
                  <IonCol size="8" className="no-padding">
                    <h3 className="custom-home-h3">Sent Queries</h3>
                  </IonCol>
                  <IonCol size="4" className="ion-text-right no-padding">
                    <IonButton
                      className="custom-home-btn-icon-2"
                      size="small"
                      onClick={goToPreSlide_2}
                    >
                      <IonIcon name="ios-arrow-back" />
                    </IonButton>
                    <IonButton
                      className="custom-home-btn-icon-2"
                      size="small"
                      onClick={goToNextSlide_2}
                    >
                      <IonIcon name="ios-arrow-forward" />
                    </IonButton>
                  </IonCol>
                </IonRow>
                <br />
                <br />
                <IonRow>
                  <div className="custom-home-side-bar">
                    <IonCol
                      size="12"
                      style={{ paddingLeft: "0", paddingTop: "0" }}
                    >
                      <IonRow>
                        <IonCol size="6" offset="3" className="ion-text-center">
                          <h1>{sentQueries ? sentQueries.length : 0}</h1>
                        </IonCol>
                      </IonRow>
                      <IonRow className="custom-home-border">
                        <IonCol className="ion-text-center">
                          <p className="custom-home-p-2">
                            From: Jan {new Date().getFullYear()}
                          </p>
                        </IonCol>
                        <IonCol className="ion-text-center">
                          <p className="custom-home-p-2">
                            To: {month[new Date().getMonth()]}{" "}
                            {new Date().getFullYear()}
                          </p>
                        </IonCol>
                      </IonRow>
                      <IonRow className="custom-home-margin">
                        <IonCol
                          size="9"
                          style={{ margin: "auto" }}
                          className="ion-text-center"
                        >
                          <IonButton
                            className="custom-btn custom-btn-home"
                            size="large"
                            onClick={() => history.push("/issued-queries")}
                          >
                            Issue Queries
                            <span
                              style={{ marginTop: "3px", marginLeft: "3px" }}
                            >
                              <IonIcon name="ios-arrow-forward" />
                            </span>
                          </IonButton>
                        </IonCol>
                      </IonRow>
                    </IonCol>
                  </div>
                </IonRow>
              </IonSlide>
              <IonSlide>
                <IonRow className="custom-home-border-bottom">
                  <IonCol size="8" className="no-padding">
                    <h3 className="custom-home-h3">Received Queries</h3>
                  </IonCol>
                  <IonCol size="4" className="ion-text-right no-padding">
                    <IonButton
                      className="custom-home-btn-icon-2"
                      size="small"
                      onClick={goToPreSlide_2}
                    >
                      <IonIcon name="ios-arrow-back" />
                    </IonButton>
                    <IonButton
                      className="custom-home-btn-icon-2"
                      size="small"
                      onClick={goToNextSlide_2}
                    >
                      <IonIcon name="ios-arrow-forward" />
                    </IonButton>
                  </IonCol>
                </IonRow>
                <br />
                <br />
                <IonRow>
                  <div className="custom-home-side-bar">
                    <IonCol
                      size="12"
                      style={{ paddingLeft: "0", paddingTop: "0" }}
                    >
                      <IonRow>
                        <IonCol
                          sizeMd="6"
                          offsetMd="3"
                          sizeLg="6"
                          offsetLg="3"
                          sizeSm="8"
                          offsetSm="2"
                          className="ion-text-center"
                        >
                          <h1>
                            {receivedQueries ? receivedQueries.length : 0}
                          </h1>
                        </IonCol>
                      </IonRow>
                      <IonRow className="custom-home-border">
                        <IonCol className="ion-text-center">
                          <p className="custom-home-p-2">
                            From: Jan {new Date().getFullYear()}
                          </p>
                        </IonCol>
                        <IonCol className="ion-text-center">
                          <p className="custom-home-p-2">
                            To: {month[new Date().getMonth()]}{" "}
                            {new Date().getFullYear()}
                          </p>
                        </IonCol>
                      </IonRow>
                      <IonRow className="custom-home-margin">
                        <IonCol
                          size="6"
                          className="ion-text-center"
                          style={{ margin: "auto" }}
                        >
                          <IonButton
                            className="custom-btn custom-btn-home"
                            size="large"
                            onClick={() => history.push("/query-responses")}
                          >
                            See all
                            <span
                              style={{ marginTop: "3px", marginLeft: "3px" }}
                            >
                              <IonIcon name="ios-arrow-forward" />
                            </span>
                          </IonButton>
                        </IonCol>
                      </IonRow>
                    </IonCol>
                  </div>
                </IonRow>
              </IonSlide>
            </IonSlides>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default Home;
