import React, { useState } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonIcon,
  IonButton,
  IonText,
  IonSpinner,
  IonToast,
  IonImg,
} from "@ionic/react";
import { Formik } from "formik";
import * as Yup from "yup";
import { register } from "../../utils/apiClient";
import { useTenant } from "../../context/TenantContext";

function Register(props) {
  const [showToast, setShowToast] = useState(false);
  const [toastMsg, setToastMsg] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const { tenant } = useTenant();

  const _handleToastDismissed = () => {
    setShowToast(false);
    setToastMsg("");
  };

  const initialFormValues = {
    first_name: "",
    last_name: "",
    email: "",
    employee_no: "",
    mobile_no: "",
    password: "",
    password_confirmation: "",
  };

  const validationSchema = Yup.object().shape({
    first_name: Yup.string().required("First Name is required"),
    last_name: Yup.string().required("Last Name is required"),
    email: Yup.string()
      .email("Email is not valid")
      .required("Email is required"),
    employee_no: Yup.string().required("Employee number is required"),
    mobile_no: Yup.string()
      .matches(/^\d{11}$/, {
        excludeEmptyString: true,
        message: "Mobile number must be 11 digits",
      })
      .required("Mobile No is required"),
    password: Yup.string().min(6).required("Password is required"),
    password_confirmation: Yup.string().oneOf(
      [Yup.ref("password")],
      "Must match given password"
    ),
  });

  const _handleSubmit = async (values) => {
    let credentials = Object.assign({}, values, {
      tenantID: tenant.tenantID,
      email: values.email.trim(),
      first_name: values.first_name.trim(),
      last_name: values.last_name.trim(),
      employee_no: values.employee_no.trim(),
      mobile_no: values.mobile_no.trim(),
    });

    setIsSubmitting(true);

    try {
      const { message } = await register(credentials);
      setIsSubmitting(false);
      setToastMsg(message);
      setShowToast(true);
      setTimeout(() => {
        window.location.replace("/");
      }, 3000);
    } catch (error) {
      console.log(error);

      if (!error.response) {
        setToastMsg("Possible Network Error");
        setShowToast(true);
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        setShowToast(true);
        return;
      }

      const {
        response: { body },
      } = error;
      setIsSubmitting(false);

      body ? setToastMsg(body.message) : setToastMsg("Something went wrong");
      setShowToast(true);
    }
  };

  return (
    <>
      <IonToast
        isOpen={showToast}
        message={toastMsg}
        duration={3000}
        onDidDismiss={_handleToastDismissed}
      />
      <IonContent class="bg-img ion-padding">
        <IonRow class="ion-hide-sm-down">
          <IonCol>
            <IonImg
              src={
                tenant.companyLogo
                  ? tenant.companyLogo
                  : "assets/images/logo.png"
              }
              class="logo custom-logo"
            />
            <p className="custom-login-text">
              <span>My ESS</span>
              <br /> Employees Self Service
            </p>
          </IonCol>
        </IonRow>
        <br />
        <br />
        <Formik
          initialValues={initialFormValues}
          onSubmit={_handleSubmit}
          validationSchema={validationSchema}
        >
          {({
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <IonGrid>
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  handleSubmit(e);
                }}
              >
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        First Name
                      </IonLabel>
                      <IonInput
                        name="first_name"
                        value={values.first_name}
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="person" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.first_name && touched.first_name ? (
                      <div style={{ color: "red" }}>{errors.first_name}</div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Last Name
                      </IonLabel>
                      <IonInput
                        name="last_name"
                        value={values.last_name}
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="person" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.last_name && touched.last_name ? (
                      <div style={{ color: "red" }}>{errors.last_name}</div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Email
                      </IonLabel>
                      <IonInput
                        type="email"
                        name="email"
                        value={values.email}
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="ios-mail" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.email && touched.email ? (
                      <div style={{ color: "red" }}>{errors.email}</div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {" "}
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Employee No.
                      </IonLabel>
                      <IonInput
                        name="employee_no"
                        value={values.employee_no}
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="ios-code" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.employee_no && touched.employee_no ? (
                      <div style={{ color: "red" }}>{errors.employee_no}</div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Mobile
                      </IonLabel>
                      <IonInput
                        name="mobile_no"
                        value={values.mobile_no}
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="ios-call" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.mobile_no && touched.mobile_no ? (
                      <div style={{ color: "red" }}>{errors.mobile_no}</div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Password
                      </IonLabel>
                      <IonInput
                        name="password"
                        value={values.password}
                        type="password"
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="lock" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.password && touched.password ? (
                      <div style={{ color: "red" }}>{errors.password}</div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="stacked"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Confirm Password
                      </IonLabel>
                      <IonInput
                        name="password_confirmation"
                        value={values.password_confirmation}
                        type="password"
                        onIonChange={handleChange}
                        onIonBlur={handleBlur}
                        class="custom-input-login"
                      >
                        <IonIcon name="lock" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    {errors.password_confirmation &&
                    touched.password_confirmation ? (
                      <div style={{ color: "red" }}>
                        {errors.password_confirmation}
                      </div>
                    ) : null}
                  </IonCol>
                </IonRow>
                {/* <br /> */}
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonButton
                      size="large"
                      expand="full"
                      shape="round"
                      class="custom-gradient"
                      disabled={isSubmitting}
                      type="submit"
                    >
                      {isSubmitting ? (
                        <IonSpinner name="crescent" color="light" slot="end" />
                      ) : (
                        <IonIcon
                          slot="end"
                          name="arrow-forward"
                          class="custom-login-icon"
                        />
                      )}
                      SIGN UP
                    </IonButton>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol class="ion-text-center" size="12">
                    <p className="custom-login-p">
                      {/* By creating an account, you agree to our <br /> */}
                      Already have an account ? <br /> <br />
                      <IonText
                        onClick={() => props.history.replace("/login")}
                        color="favorite"
                        style={{ cursor: "pointer" }}
                      >
                        {/* Terms of Service and Privacy Policy */}
                        Sign In
                      </IonText>
                    </p>
                  </IonCol>
                </IonRow>
              </form>
            </IonGrid>
          )}
        </Formik>
      </IonContent>
    </>
  );
}

export default Register;
