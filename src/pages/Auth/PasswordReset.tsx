import React, { useState } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonIcon,
  IonButton,
  IonSegment,
  IonSegmentButton,
  IonImg,
  IonToast,
  IonSpinner
} from "@ionic/react";
import { resetPassword } from "../../utils/apiClient";
import { useTenant } from "../../context/TenantContext";

function PasswordReset(props) {
  const [email, setEmail] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [toastMsg, setToastMsg] = useState("");
  const [showToast, setShowToast] = useState(false);

  const { tenant } = useTenant();

  const _handleToastDismissed = () => {
    setShowToast(false);
    setToastMsg("");
  };

  const _handleSubmit = async () => {
    try {
      setIsSubmitting(true);
      const { message } = await resetPassword({
        email,
        tenantID: tenant.tenantID
      });
      setToastMsg(message);
      setIsSubmitting(false);
      setShowToast(true);
      setTimeout(() => {
        window.location.replace("/");
      }, 3000);
    } catch (error) {
      const {
        response: { body }
      } = error;

      body ? setToastMsg(body.message) : setToastMsg("Something went wrong");
      setIsSubmitting(false);
      console.log(error);
      setShowToast(true);
    }
  };

  return (
    <>
      <IonContent class="bg-img">
        <IonToast
          isOpen={showToast}
          message={toastMsg}
          duration={3000}
          onDidDismiss={_handleToastDismissed}
        />
        <IonGrid>
          <IonRow class="ion-hide-sm-down">
            <IonCol>
              <IonImg
                src={
                  tenant.companyLogo
                    ? tenant.companyLogo
                    : "assets/images/logo.png"
                }
                class="logo custom-logo"
              />
              <p className="custom-login-text">
                <span>My ESS</span>
                <br /> Employees Self Service
              </p>
            </IonCol>
          </IonRow>
          <IonGrid>
            <br />
            <br />
            <br />
            <IonRow>
              <IonCol size="12" sizeMd="4" offsetMd="4" sizeSm="6" offsetSm="3">
                <IonSegment scrollable color="favorite">
                  <IonSegmentButton
                    onClick={() => props.history.replace("/login")}
                    value="general"
                  >
                    <IonLabel>Login</IonLabel>
                  </IonSegmentButton>
                  <IonSegmentButton checked value="address">
                    <IonLabel>Forgot Password</IonLabel>
                  </IonSegmentButton>
                </IonSegment>
              </IonCol>
              <br />
              <br />
              <IonCol size="10" offset="1">
                <p className="ion-text-center">
                  Enter the email address you used to create your account.
                </p>
                <p className="ion-text-center">
                  We will email you a link to reset your password
                </p>
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="6"
                    offsetMd="3"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonItem class="custom-item-login">
                      <IonLabel
                        position="floating"
                        class="custom-label-login ion-text-uppercase"
                      >
                        Email
                      </IonLabel>
                      <IonInput
                        value={email}
                        onIonChange={e => setEmail(e.detail.value)}
                        type="email"
                        class="custom-input-login"
                      >
                        <IonIcon name="ios-mail" class="custom-icon" />
                      </IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <br />
                <IonRow>
                  <IonCol
                    size="12"
                    sizeMd="4"
                    offsetMd="4"
                    sizeSm="6"
                    offsetSm="3"
                    sizeLg="4"
                    offsetLg="4"
                  >
                    <IonButton
                      size="large"
                      expand="full"
                      shape="round"
                      onClick={_handleSubmit}
                      class="custom-gradient custom-login-btn"
                    >
                      {isSubmitting ? (
                        <IonSpinner name="crescent" color="light" slot="end" />
                      ) : (
                        <IonIcon
                          slot="end"
                          name="arrow-forward"
                          class="custom-login-icon"
                        />
                      )}
                      SEND MAIL
                    </IonButton>
                  </IonCol>
                </IonRow>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonGrid>
      </IonContent>
    </>
  );
}

export default PasswordReset;
