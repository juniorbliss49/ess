import React, { useState } from "react";
import {
  IonContent,
  IonGrid,
  IonRow,
  IonCol,
  IonItem,
  IonLabel,
  IonInput,
  IonIcon,
  IonSegment,
  IonSegmentButton,
  IonButton,
  IonText,
  IonImg,
  IonSpinner,
  IonToast,
} from "@ionic/react";

import "../../styles/login.scss";
import { useAuth } from "../../context/AuthContext";
import { useTenant } from "../../context/TenantContext";
import { Redirect } from "react-router";

function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [showError, setShowError] = useState(false);
  const [errMsg, setErrMsg] = useState("");

  const { loginUser, isAuthenticated } = useAuth();
  const { tenant } = useTenant();

  const _handleToastDismissed = () => {
    setShowError(false);
    setErrMsg("");
  };

  const handleLogin = () => {
    setLoading(true);

    loginUser({ email, password, tenantID: tenant.tenantID }).catch((error) => {
      setLoading(false);

      if (!error.response) {
        setErrMsg("Network Error, Check Your Internet Connection");
        setShowError(true);
        return;
      }

      if (!error.response.body) {
        setErrMsg("Network Error, Check Your Internet Connection");
        setShowError(true);
        return;
      }

      const {
        response: { body },
      } = error;
      setErrMsg(body.message);

      setLoading(false);
      setShowError(true);
    });
  };

  const handleEmailUpdate = (e) => {
    setEmail(e.target.value.trim());
  };

  const handlePasswordUpdate = (e) => {
    setPassword(e.target.value);
  };

  return (
    <>
      <IonContent class="bg-img ion-padding">
        <IonToast
          isOpen={showError}
          message={errMsg}
          duration={3000}
          onDidDismiss={_handleToastDismissed}
        />
        <IonGrid>
          <IonRow class="ion-hide-sm-down">
            <IonCol>
              <IonImg
                src={
                  tenant.companyLogo
                    ? tenant.companyLogo
                    : "assets/images/logo.png"
                }
                class="logo custom-logo"
              />
              <p className="custom-login-text">
                <span>My ESS</span>
                <br /> Employees Self Service
              </p>
            </IonCol>
          </IonRow>
          <br />
          <br />
          <IonRow>
            <IonCol size="12" sizeMd="4" offsetMd="4" sizeSm="6" offsetSm="3">
              <IonSegment scrollable color="favorite">
                <IonSegmentButton checked value="general">
                  <IonLabel>Login</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton
                  onClick={() => props.history.replace("/reset-password")}
                  value="address"
                >
                  <IonLabel>Forgot Password</IonLabel>
                </IonSegmentButton>
              </IonSegment>
            </IonCol>
          </IonRow>
          <br />
          <br />
          <IonRow>
            <IonCol
              size="12"
              sizeMd="6"
              offsetMd="3"
              sizeSm="6"
              offsetSm="3"
              sizeLg="4"
              offsetLg="4"
            >
              <IonItem class="custom-item-login">
                <IonLabel
                  position="floating"
                  class="custom-label-login ion-text-uppercase"
                >
                  Email
                </IonLabel>
                <IonInput
                  type="email"
                  value={email}
                  onInput={handleEmailUpdate}
                  class="custom-input-login"
                  required
                >
                  <IonIcon name="person" class="custom-icon" />
                </IonInput>
              </IonItem>
            </IonCol>
          </IonRow>
          <br />
          <IonRow>
            <IonCol
              size="12"
              sizeMd="6"
              offsetMd="3"
              sizeSm="6"
              offsetSm="3"
              sizeLg="4"
              offsetLg="4"
            >
              <IonItem class="custom-item-login">
                <IonLabel
                  position="floating"
                  class="custom-label-login ion-text-uppercase"
                >
                  Password
                </IonLabel>
                <IonInput
                  value={password}
                  onInput={handlePasswordUpdate}
                  type="password"
                  class="custom-input-login"
                  required
                >
                  <IonIcon name="lock" class="custom-icon" />
                </IonInput>
              </IonItem>
            </IonCol>
          </IonRow>
          <br />
          <IonRow>
            <IonCol
              size="12"
              sizeMd="4"
              offsetMd="4"
              sizeSm="6"
              offsetSm="3"
              sizeLg="4"
              offsetLg="4"
            >
              <IonButton
                size="large"
                expand="full"
                shape="round"
                class="custom-gradient custom-login-btn"
                onClick={handleLogin}
              >
                {loading ? (
                  <IonSpinner name="crescent" color="light" slot="end" />
                ) : (
                  <IonIcon
                    slot="end"
                    name="arrow-forward"
                    class="custom-login-icon"
                  />
                )}
                LOG IN
              </IonButton>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol class="ion-text-center" size="12">
              <p className="custom-login-p">
                Don't have an account?
                <br />
                <br />
                <IonText
                  onClick={() => props.history.replace("/register")}
                  color="favorite"
                  style={{ cursor: "pointer" }}
                >
                  Create a new account
                </IonText>
              </p>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol
              size="12"
              sizeSm="6"
              offsetSm="3"
              sizeLg="4"
              offsetLg="4"
              sizeMd="4"
              offsetMd="4"
            >
              <IonRow>
                <IonCol class="custom-line" />
                <IonCol class="ion-text-center custom-margin">
                  <IonText color="favorite" class="custom-login-text">
                    <b>Or Connect With</b>
                  </IonText>
                </IonCol>
                <IonCol class="custom-line" />
              </IonRow>
            </IonCol>
          </IonRow>
          <br />
          <IonRow>
            <IonCol
              size="10"
              offset="1"
              sizeSm="8"
              offsetSm="2"
              sizeMd="4"
              offsetMd="4"
              class="ion-text-center"
            >
              <IonRow>
                <IonCol size="5" sizeSm="5" class="ion-text-left">
                  <IonButton
                    disabled
                    color="favorite"
                    size="small"
                    shape="round"
                    class="ion-text-capitalize custom-login-btn-2"
                  >
                    <IonIcon name="ios-call" slot="start" class="btn-round" />
                    Phone Number
                  </IonButton>
                </IonCol>
                <IonCol size="1" sizeSm="1" />
                <IonCol size="6" sizeSm="6" class="ion-text-right">
                  <IonButton
                    disabled
                    color="favorite"
                    size="small"
                    shape="round"
                    class="ion-text-capitalize custom-login-btn-2"
                  >
                    <IonIcon
                      name="ios-business"
                      slot="start"
                      class="btn-round"
                    />
                    Org. Email
                  </IonButton>
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </>
  );
}

export default Login;
