import React from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import { IonRow, IonCol, IonButton, IonIcon } from "@ionic/react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton
} from "react-accessible-accordion";

function AppraisalView() {
  return (
    <AuthWrapper
      name="Appraisal: 2019 - Year End"
      bg_name="form-bg-img-2"
      backBtnName="Leave Application"
    >
      <IonRow className="ion-hide-md-down">
        <IonCol size="10" offset="1">
          <IonRow className="ion-padding hide-wrapper">
            <IonCol>
              <IonRow className="appraisal-wrapper">
                <IonCol size="9">
                  <table className="appraisal-view">
                    <tbody>
                      <tr>
                        <td>
                          <b>Name:</b>Opeyemi Glory
                        </td>
                        <td>
                          <b>Designation:</b>Head of Ruby & Shenanigans
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <b>Appraisal Type:</b>Year End
                        </td>
                        <td>
                          <b>Date Appraised:</b>31-06-2016
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <b>Appraisee Section Weight:</b>100%
                        </td>
                        <td>
                          <b>Status:</b> <span>New</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </IonCol>
                <IonCol size="3">
                  <p className="custom-score">
                    <span>- -</span>
                    <br />
                    Score
                  </p>
                </IonCol>
              </IonRow>
              <br /> <br /> <br /> <br />
              <table className="appraisals-table">
                <thead>
                  <tr>
                    <th>Description</th>
                    <th>Weight</th>
                    <th>Appraisee Score</th>
                    <th>Appraiser Score</th>
                    <th>Sectional Weight</th>
                  </tr>
                </thead>
                <br />
                <br />
                <tbody>
                  <tr>
                    <td>Ability to process data</td>
                    <td>10</td>
                    <td>8.5</td>
                    <td>--</td>
                    <td>5%</td>
                  </tr>
                  <br />
                  <tr>
                    <td>Ability to process data</td>
                    <td>10</td>
                    <td>8.5</td>
                    <td>--</td>
                    <td>5%</td>
                  </tr>
                  <br />
                  <tr>
                    <td>Ability to process data</td>
                    <td>10</td>
                    <td>8.5</td>
                    <td>--</td>
                    <td>5%</td>
                  </tr>
                  <br />
                  <tr>
                    <td>Ability to process data</td>
                    <td>10</td>
                    <td>8.5</td>
                    <td>--</td>
                    <td>5%</td>
                  </tr>
                </tbody>
                {/* <br />
                                <tfoot>
                                    <tr>
                                        <th>Description</th>
                                        <th>Weight</th>
                                        <th>Appraisee Score</th>
                                        <th>Appraiser Score</th>
                                        <th>Sectional Weight</th>
                                    </tr>
                                </tfoot> */}
              </table>
              <br /> <br />
              <div className="ion-float-right">
                <IonButton
                  className="custom-btn"
                  size="large"
                  style={{ marginRight: "-1px" }}
                >
                  Submit
                  <span>
                    <IonIcon name="arrow-dropright" />
                  </span>
                </IonButton>
              </div>
            </IonCol>
          </IonRow>
        </IonCol>
      </IonRow>
      <IonRow className="ion-hide-lg-up">
        <IonCol size="12">
          <IonRow>
            <IonCol size="12">
              <div className="ion-float-right appraisal-score">
                <h4>Score: 78.0</h4>
              </div>
            </IonCol>
            <IonCol size="12">
              <Accordion>
                <AccordionItem>
                  <AccordionItemHeading>
                    <AccordionItemButton>
                      Ability to cook beans
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel>
                    <table className="approval-mobile">
                      <tbody>
                        <tr>
                          <td>
                            <b>Appraisee Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <tr>
                          <td>
                            <b>Appraiser Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <br />
                        <tr>
                          <td className="custom-appraisal-text">
                            <span>Weight:</span>10
                          </td>
                          <td className="custom-appraisal-text">
                            <span>Sectional Weight:</span>5%
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem>
                  <AccordionItemHeading>
                    <AccordionItemButton>
                      Ability to cook beans
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel>
                    <table className="approval-mobile">
                      <tbody>
                        <tr>
                          <td>
                            <b>Appraisee Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <tr>
                          <td>
                            <b>Appraiser Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <br />
                        <tr>
                          <td className="custom-appraisal-text">
                            <span>Weight:</span>10
                          </td>
                          <td className="custom-appraisal-text">
                            <span>Sectional Weight:</span>5%
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem>
                  <AccordionItemHeading>
                    <AccordionItemButton>
                      Ability to cook beans
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel>
                    <table className="approval-mobile">
                      <tbody>
                        <tr>
                          <td>
                            <b>Appraisee Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <tr>
                          <td>
                            <b>Appraiser Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <br />
                        <tr>
                          <td className="custom-appraisal-text">
                            <span>Weight:</span>10
                          </td>
                          <td className="custom-appraisal-text">
                            <span>Sectional Weight:</span>5%
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem>
                  <AccordionItemHeading>
                    <AccordionItemButton>
                      Ability to cook beans
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel>
                    <table className="approval-mobile">
                      <tbody>
                        <tr>
                          <td>
                            <b>Appraisee Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <tr>
                          <td>
                            <b>Appraiser Score:</b>
                          </td>
                          <td className="custom-border-bottom-2"></td>
                        </tr>
                        <br />
                        <tr>
                          <td className="custom-appraisal-text">
                            <span>Weight:</span>10
                          </td>
                          <td className="custom-appraisal-text">
                            <span>Sectional Weight:</span>5%
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </AccordionItemPanel>
                </AccordionItem>
              </Accordion>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol size="4" offset="4" className="ion-text-center">
              <IonButton size="large" className="custom-btn">
                Submit
                <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                  <IonIcon name="ios-arrow-dropright" />
                </span>
              </IonButton>
            </IonCol>
          </IonRow>
        </IonCol>
      </IonRow>
    </AuthWrapper>
  );
}

export default AppraisalView;
