import React from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import { IonRow, IonCol, IonButton, IonIcon } from "@ionic/react";

import "../../styles/appraisals.css";
import "react-accessible-accordion/dist/fancy-example.css";

function Appraisals({ history }) {
  return (
    <AuthWrapper
      name="Appraisals"
      bg_name="form-bg-img-2"
      backBtnName="Leave Application"
    >
      <IonRow className="ion-hide-md-down">
        <IonCol size="10" offset="1">
          <IonRow className="ion-padding">
            <IonCol>
              <table className="appraisals-table table-space">
                <thead>
                  <tr>
                    <th>Period</th>
                    <th>Score</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>2019 - Year End</td>
                    <td>78.0</td>
                    <td>New</td>
                    <td>
                      <IonButton
                        className="secondary-color no-raduis"
                        onClick={() => history.push("/appraisal-view")}
                      >
                        View
                      </IonButton>
                    </td>
                  </tr>
                  <tr>
                    <td>2019 - Mid End</td>
                    <td>66.5</td>
                    <td>Pending Review</td>
                    <td>
                      <IonButton
                        className="secondary-color no-raduis"
                        onClick={() => history.push("/appraisal-view")}
                      >
                        View
                      </IonButton>
                    </td>
                  </tr>
                  <tr>
                    <td>2019 - Year Start</td>
                    <td>80.2</td>
                    <td>Completed</td>
                    <td>
                      <IonButton
                        className="secondary-color no-raduis"
                        onClick={() => history.push("/appraisal-view")}
                      >
                        View
                      </IonButton>
                    </td>
                  </tr>
                  <tr>
                    <td>2019 - Presentation</td>
                    <td>95.0</td>
                    <td>Completed</td>
                    <td>
                      <IonButton
                        className="secondary-color no-raduis"
                        onClick={() => history.push("/appraisal-view")}
                      >
                        View
                      </IonButton>
                    </td>
                  </tr>
                </tbody>
              </table>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol size="4" offset="4" className="ion-text-center">
              <IonButton className="history-btn">Load More...</IonButton>
            </IonCol>
          </IonRow>
        </IonCol>
      </IonRow>
      <IonRow className="ion-hide-lg-up">
        <IonCol size="12">
          <IonRow className="custom-appraisal-2">
            <IonCol size="6">
              <h3>2019 - Year Start</h3>
              <h3 className="custom-h3">New</h3>
            </IonCol>
            <IonCol size="6" className="ion-text-center">
              <h2 className="custom-h3">78.0</h2>
              <h3>Score</h3>
            </IonCol>
          </IonRow>
          <br />
          <IonRow className="custom-appraisal-2">
            <IonCol size="6">
              <h3>2019 - Mid Year</h3>
              <h3 className="custom-h3">Pending Approval</h3>
            </IonCol>
            <IonCol size="6" className="ion-text-center">
              <h2 className="custom-h3">66.5</h2>
              <h3>Score</h3>
            </IonCol>
          </IonRow>
          <br />
          <IonRow className="custom-appraisal-2">
            <IonCol size="6">
              <h3>2019 - Year Start</h3>
              <h3 className="custom-h3">Completed</h3>
            </IonCol>
            <IonCol size="6" className="ion-text-center">
              <h2 className="custom-h3">80.0</h2>
              <h3>Score</h3>
            </IonCol>
          </IonRow>
          <br />
          <IonRow className="custom-appraisal-2">
            <IonCol size="6">
              <h3>2019 - Presentation</h3>
              <h3 className="custom-h3">Completed</h3>
            </IonCol>
            <IonCol size="6" className="ion-text-center">
              <h2 className="custom-h3">95.0</h2>
              <h3>Score</h3>
            </IonCol>
          </IonRow>
          <br />
          <IonRow>
            <IonCol size="4" offset="4" className="ion-text-center">
              <IonButton className="history-btn">Load More...</IonButton>
            </IonCol>
          </IonRow>
        </IonCol>
      </IonRow>
    </AuthWrapper>
  );
}

export default Appraisals;
