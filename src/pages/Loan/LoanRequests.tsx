import React, { useEffect, useState } from "react";
import AuthWrapper from "../Layout/AuthWrapper";
import {
  IonRow,
  IonCol,
  IonButton,
  IonIcon,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardContent,
  IonText,
  IonSpinner,
  IonSegment,
  IonSegmentButton,
  IonLabel,
  IonModal,
  IonLoading,
  IonToast,
  IonActionSheet,
  IonList,
  IonItem,
} from "@ionic/react";
import { useAuth } from "../../context/AuthContext";
import { useLoanApplication } from "../../context/LoanApplicationContext";
import { Redirect } from "react-router";
import _ from "lodash";
import { getPlatforms } from "@ionic/core";

import {
  syncLoanApplication,
  syncLoanRequests,
  requestApprovalForLoanApplication,
  cancelApprovalRequestForLoanApplication,
} from "../../utils/apiClient";

function LoanRequests({ history, location }) {
  const { token, logOutUser } = useAuth();

  const [filterState, setFilterState] = useState("All");
  const [filteredLoanRequests, setFilteredLoanRequests] = useState([]);
  const [loanRequestForActions, setLoanRequestForActions] = useState(null);
  const [showLoanRequestModal, setShowLoanRequestModal] = useState(false);
  const [processing, setProcessing] = useState(false);
  const [toastMsg, setToastMsg] = useState("");
  const [mobile, setMobile] = useState(false);
  const [actionButtons, setActionButtons] = useState(null);
  const [showActionSheet, setShowActionSheet] = useState(false);

  const showValues = [
    "loan_amount",
    "number_of_payments",
    "preferred_pmt_method",
    "loan_e_d",
    "description",
    "employee_no",
    "bank_account",
    "employee_category",
  ];

  const {
    loading,
    errors,
    loanApplications,
    fetchLoanApplications,
    syncLoan,
  } = useLoanApplication();

  useEffect(() => {
    fetchLoanApplications(token).then((data) => {
      setFilteredLoanRequests(data);
    });
    let platformType = platform();
    if (
      platformType.includes("mobile") &&
      !platformType.includes("ipad") &&
      !platformType.includes("tablet")
    ) {
      setMobile(true);
    }
  }, []);

  function platform() {
    return getPlatforms(window);
  }

  function applyFilterToLoanRequests(param) {
    setLoanRequestForActions(null);

    setFilterState(param);

    setFilteredLoanRequests(
      loanApplications
        ? loanApplications.filter((loanReq) => {
            if (param == "All") {
              return loanReq;
            }

            return loanReq.status == param;
          })
        : []
    );
  }

  const prepForActions = (index) => {
    setShowActionSheet(true);
    if (loanRequestForActions != null && loanRequestForActions.index == index) {
      setLoanRequestForActions(null);
      return;
    }

    let loanRequest = filteredLoanRequests[index];
    loanRequest["index"] = index;

    setLoanRequestForActions(loanRequest);
    buttonProperty(loanRequest);
  };

  function buttonProperty(loanRequest) {
    let button = [];
    if (loanRequest.status == "Open") {
      button = [
        {
          text: "Edit",
          role: "edit",
          icon: "create",
          cssClass: "ribbon-color-btn",
          handler: () => {
            prepForEdit(loanRequest);
          },
        },
        {
          text: "View More",
          icon: "eye",
          handler: () => {
            setShowLoanRequestModal(true);
          },
        },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            refreshLoanApplication(loanRequest);
          },
        },
        {
          text: "Request Approval",
          role: "request approval",
          icon: "send",
          handler: () => {
            requestApproval(loanRequest);
          },
        },
      ];
    } else if (loanRequest.status == "Pending_Approval") {
      button = [
        {
          text: "View More",
          icon: "eye",
          handler: () => {
            setShowLoanRequestModal(true);
          },
        },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            refreshLoanApplication(loanRequest);
          },
        },
        {
          text: "Cancel Approval Request",
          icon: "close",
          cssClass: "icon-danger",
          handler: () => {
            cancelApprovalRequest(loanRequest);
          },
        },
      ];
    } else {
      button = [
        {
          text: "View More",
          icon: "eye",
          handler: () => {
            setShowLoanRequestModal(true);
          },
        },
        {
          text: "Refresh",
          role: "refresh",
          icon: "refresh",
          handler: () => {
            refreshLoanApplication(loanRequest);
          },
        },
      ];
    }
    setActionButtons(button);
  }

  const prepForEdit = (loanRequest) => {
    if (loanRequest.status != "Open") {
      return;
    }

    history.push(`/loan-request/${loanRequest.id}`);
  };

  const refreshLoanApplication = async (loanRequest) => {
    try {
      setProcessing(true);
      let { message } = await syncLoanApplication(
        { loan_request_id: loanRequest.id },
        token
      );
      let { data } = await fetchLoanApplications(token);
      // setFilteredLoanRequests(data);
      applyFilterToLoanRequests(filterState);
      setProcessing(false);
      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _syncAll = async () => {
    try {
      setProcessing(true);
      let data = await syncLoan(token);
      setFilteredLoanRequests(data);
      // applyFilterToLoanRequests(filterState);
      setProcessing(false);
      // setToastMsg(data);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const requestApproval = async (loanRequest) => {
    try {
      setProcessing(true);
      let { message } = await requestApprovalForLoanApplication(
        { loan_request_id: loanRequest.id },
        token
      );
      let { data } = await fetchLoanApplications(token);
      // setFilteredLoanRequests(data);
      applyFilterToLoanRequests(filterState);
      setProcessing(false);
      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const cancelApprovalRequest = async (loanRequest) => {
    try {
      setProcessing(true);
      let { message } = await cancelApprovalRequestForLoanApplication(
        { loan_request_id: loanRequest.id },
        token
      );
      let { data } = await fetchLoanApplications(token);
      // setFilteredLoanRequests(data);
      applyFilterToLoanRequests(filterState);
      setProcessing(false);
      setToastMsg(message);
    } catch (error) {
      setProcessing(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setToastMsg("Network Error, Check Your Internet Connection");
        return;
      }

      if (!error.response.body) {
        setToastMsg("Something went wrong");
        return;
      }

      setToastMsg(error.response.body.message);
    }
  };

  const _clearLocationStateAndToast = () => {
    if (location.state && location.state.message) {
      location.state.message = "";
    }

    setToastMsg("");
  };

  return (
    <>
      <AuthWrapper
        name="My Loan Requests"
        bg_name="form-bg-img-2"
        backBtnName="Home"
      >
        {toastMsg || (location.state && location.state.message) ? (
          <IonToast
            isOpen={true}
            message={toastMsg || location.state.message}
            duration={3000}
            onDidDismiss={_clearLocationStateAndToast}
          />
        ) : null}

        {/* <IonLoading
          isOpen={loading || processing}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        <IonModal
          isOpen={showLoanRequestModal}
          className="ModalScroll"
          onDidDismiss={() => setShowLoanRequestModal(false)}
        >
          <IonList>
            {loanRequestForActions ? (
              _.map(loanRequestForActions, (val, key) => (
                <React.Fragment key={key}>
                  {showValues.includes(key) ? (
                    <IonItem key={key}>
                      <IonLabel>
                        {key}: <span style={{ color: "#999" }}> {val}</span>
                      </IonLabel>
                    </IonItem>
                  ) : null}
                </React.Fragment>
              ))
            ) : (
              <div>No loan card was selected</div>
            )}
          </IonList>
          <div style={{ width: "100%", textAlign: "center" }}>
            <IonButton
              color="danger"
              className="ribbon-color-btn"
              onClick={() => setShowLoanRequestModal(false)}
            >
              Close
            </IonButton>
          </div>
        </IonModal>

        <IonRow>
          <IonCol
            sizeLg="10"
            offsetLg="1"
            sizeMd="10"
            offsetMd="1"
            sizeSm="10"
            offsetSm="1"
            sizeXs="12"
          >
            <IonRow className="ion-padding custom-sticky">
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <IonButton
                  className="create-btn"
                  onClick={() => history.push("/loan-request")}
                >
                  <IonIcon slot="start" name="add" />
                  New Loan Request
                </IonButton>
                <IonButton
                  title="Sync All"
                  className="create-btn ribbon-color-btn"
                  // color="tertiary"
                  onClick={_syncAll}
                >
                  <IonIcon slot="icon-only" name="sync" />
                </IonButton>
                {loanRequestForActions ? (
                  <>
                    {mobile ? (
                      <>
                        <IonActionSheet
                          isOpen={showActionSheet}
                          onDidDismiss={() => {
                            setShowActionSheet(false);
                          }}
                          buttons={[...actionButtons]}
                        ></IonActionSheet>
                      </>
                    ) : (
                      <>
                        {loanRequestForActions.status == "Open" ? (
                          <IonButton
                            title="Edit"
                            className="ribbon-color-btn"
                            // color="medium"
                            // style={{ backGroundColor: "#5D8CDA !important" }}
                            onClick={() => prepForEdit(loanRequestForActions)}
                          >
                            <IonIcon slot="icon-only" name="create" />
                          </IonButton>
                        ) : null}

                        <IonButton
                          title="View More"
                          className="ribbon-color-btn"
                          onClick={() => setShowLoanRequestModal(true)}
                        >
                          <IonIcon slot="icon-only" name="eye" />
                        </IonButton>

                        <IonButton
                          title="Refresh"
                          className="ribbon-color-btn"
                          onClick={() =>
                            refreshLoanApplication(loanRequestForActions)
                          }
                        >
                          <IonIcon slot="icon-only" name="refresh" />
                        </IonButton>

                        {loanRequestForActions.status == "Open" ? (
                          <IonButton
                            title="Request Approval"
                            className="ribbon-color-btn"
                            onClick={() =>
                              requestApproval(loanRequestForActions)
                            }
                          >
                            <IonIcon slot="icon-only" name="send" />
                          </IonButton>
                        ) : null}

                        {loanRequestForActions.status == "Pending_Approval" ? (
                          <IonButton
                            title="Cancel Approval Request"
                            color="danger"
                            className="create-btn"
                            onClick={() =>
                              cancelApprovalRequest(loanRequestForActions)
                            }
                          >
                            <IonIcon slot="icon-only" name="close" />
                          </IonButton>
                        ) : null}
                      </>
                    )}
                  </>
                ) : null}
              </IonCol>
              <IonCol sizeLg="6" sizeMd="6" sizeSm="6" sizeXs="12">
                <div
                  style={{ maxWidth: "100%" }}
                  className="ion-float-right content-center"
                >
                  <IonSegment
                    onIonChange={(e) =>
                      applyFilterToLoanRequests(e.detail.value)
                    }
                    value={filterState}
                  >
                    <IonSegmentButton
                      style={{ marginRight: "3px" }}
                      value="All"
                      className="custom-history-btn custom-history-padding border-right-shape"
                    >
                      <IonLabel style={{ color: "#fff" }}>All</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Open"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Open</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Pending_Approval"
                      className="custom-history-btn custom-history-padding-2"
                    >
                      <IonLabel style={{ color: "#fff" }}>Pending</IonLabel>
                    </IonSegmentButton>
                    <IonSegmentButton
                      value="Approved"
                      className="custom-history-btn border-right-shape-3"
                    >
                      <IonLabel style={{ color: "#fff" }}>Approved</IonLabel>
                    </IonSegmentButton>
                  </IonSegment>
                </div>
              </IonCol>
            </IonRow>

            {processing || loading ? (
              <IonRow className="custom-sticky">
                <IonCol
                  sizeMd="8"
                  sizeSm="12"
                  sizeXs="12"
                  style={{ margin: "auto", textAlign: "center" }}
                >
                  <h6 color="blue">Please Wait...</h6>
                </IonCol>
              </IonRow>
            ) : null}

            <IonRow className="ion-padding card-history-main">
              {filteredLoanRequests ? (
                filteredLoanRequests.map((loanReq, i) => (
                  <IonCol key={i} sizeLg="4" sizeMd="4" sizeSm="4" sizeXs="12">
                    <IonCard
                      style={
                        loanRequestForActions &&
                        loanRequestForActions.index == i
                          ? { border: "4px solid #2c4d85", cursor: "pointer" }
                          : { cursor: "pointer" }
                      }
                      className="card-history"
                      onClick={() => prepForActions(i)}
                    >
                      <IonCardHeader className="card-history-header">
                        <IonCardTitle>{loanReq.description}</IonCardTitle>
                      </IonCardHeader>
                      <IonCardContent className="ion-text-center">
                        <h5>{loanReq.status}</h5>
                        <h3>{loanReq.loan_id}</h3>
                        <div>
                          <span>
                            <IonText>
                              <IonIcon title="Loan Amount" name="cash" />
                              <IonText>{loanReq.loan_amount}</IonText>
                            </IonText>
                          </span>
                        </div>
                      </IonCardContent>
                    </IonCard>
                  </IonCol>
                ))
              ) : errors ? (
                <IonCol size="4" offset="4" className="ion-text-center">
                  {errors.map((error, i) => (
                    <div key={i}>{error}</div>
                  ))}
                </IonCol>
              ) : null}
            </IonRow>
          </IonCol>
        </IonRow>
      </AuthWrapper>
    </>
  );
}

export default LoanRequests;
