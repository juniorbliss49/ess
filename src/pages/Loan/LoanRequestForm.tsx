import React, { useState, useEffect } from "react";
import {
  IonRow,
  IonCol,
  IonButton,
  IonIcon,
  IonItem,
  IonLabel,
  IonInput,
  IonSelectOption,
  IonSelect,
  IonSpinner,
  IonLoading,
  IonToast,
} from "@ionic/react";
import AuthWrapper from "../Layout/AuthWrapper";
import { useLoanApplication } from "../../context/LoanApplicationContext";
import { useAuth } from "../../context/AuthContext";
import { createLoanApplication } from "../../utils/apiClient";
import { Redirect } from "react-router";
import Select from "react-select";

function LoanRequestForm({ history }) {
  const {
    errors,
    loading,
    prepLoanApplicationFields,
    payrollEdList,
    preferredPmtMethod,
    department,
    project,
    loanApplicationFields,
    loanEdlist,
    globaldimension1code,
    globaldimension2code,
    loanEdlistSelectOptions,
    globalDimensionCodeSelectOptions,
    preferredPmtMethodSelectOptions,
    globalDimension2CodeSelectOptions,
  } = useLoanApplication();
  const { token, logOutUser } = useAuth();

  const [submitting, setSubmitting] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [requestWasCreated, setRequestWasCreated] = useState(false);
  const [toastMsg, setToastMsg] = useState("");

  const [payrollList, setPayrolList] = useState();
  const [loanAmount, setloanAmount] = useState();
  const [numberofPayment, setnumberofPayment] = useState();
  const [description, setdescription] = useState();
  const [paymentMethod, setPaymentMethod] = useState("");
  const [bankCode, setbankCode] = useState("");
  const [departmentCode, setdepartmentCode] = useState("");
  const [projectCode, setProject] = useState("");
  const [loanEd, setloanEd] = useState("");
  const [openyn, setOpenyn] = useState("");
  const [guarantor, setGuarantor] = useState("");
  const [employeeNo, setEmployeeNo] = useState("");
  const [gradeLevel, setgradeLevel] = useState("");
  const [bankAccount, setbankAccount] = useState("");
  const [cancelledBy, setcancelledBy] = useState("");
  const [suspendedBy, setsuspendedBy] = useState("");
  const [employeeName, setemployeeName] = useState("");
  const [repaidAmount, setrepaidAmount] = useState("");
  const [suspendedyn, setSuspendedyn] = useState("");
  const [interestAmount, setInterestAmount] = useState("");
  const [remainingAmount, setRemainingAmount] = useState("");
  const [bankAccountName, setbankAccountName] = useState("");
  const [employeeCategory, setEmployeeCategory] = useState("");
  const [monthlyRepayment, setmonthlyRepayment] = useState("");
  // const [numberofPayments, setnumberofPayments] = useState("");
  const [preferredBankCode, setpreferredBankCode] = useState("");
  const [interestDateFormula, setInterestDateFormula] = useState("");
  const [interestRatePercent, setinterestRatePercent] = useState("");
  const [interestRepaidAmount, setInterestRepaidAmount] = useState("");
  const [interestStartingDate, setInterestStartingDate] = useState("");
  const [suspensionEndingDate, setSuspensionEndingDate] = useState("");
  const [deductionStartingDate, setdeductionStartingDate] = useState("");
  const [globaldimension_1code, setglobaldimension1code] = useState("");
  const [globaldimension_2code, setglobaldimension2code] = useState("");
  const [interestRemainingAmount, setinterestRemainingAmount] = useState("");

  useEffect(() => {
    prepLoanApplicationFields(token);
  }, []);

  const submitForm = async () => {
    // setLoading(true)
    let formData = {
      loan_e_d: loanEd,
      loan_amount: loanAmount,
      number_of_payments: numberofPayment,
      description: description,
      preferred_pmt_method: paymentMethod,
      preferred_bank_code: preferredBankCode,
      global_dimension_1_code: globaldimension_1code,
      global_dimension_2_code: globaldimension_2code,
    };

    setSubmitting(true);
    try {
      let loanReq = await createLoanApplication(formData, token);

      if (loanReq.error) {
        throw Error(loanReq.message);
      }

      // setRequestWasCreated(true);
      setTimeout(() => {
        setToastMsg(loanReq.message);
        setSubmitting(false);
        setRequestWasCreated(true);
      }, 2000);
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setFormErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      const {
        response: { body },
      } = error;

      body.message && setFormErrors([body.message]);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  if (requestWasCreated) {
    return <Redirect to="/loan-requests" />;
  }

  const handleloanAmount = (e) => {
    setloanAmount(e.target.value);
  };

  const handleNumberofPayment = (e) => {
    setnumberofPayment(e.target.value);
  };

  const handleDescription = (e) => {
    setdescription(e.target.value);
  };

  const handlePaymentMethod = (e) => {
    setPaymentMethod(e.value);
  };

  const handleloanEd = (e) => {
    setloanEd(e.value);
  };

  const handleOpenyn = (e) => {
    setOpenyn(e.target.value);
  };

  const handleGuarantor = (e) => {
    setGuarantor(e.target.value);
  };

  const handleEmployeeNo = (e) => {
    setEmployeeNo(e.target.value);
  };

  const handleGradeLevel = (e) => {
    setgradeLevel(e.target.value);
  };

  const handlebankAccount = (e) => {
    setbankAccount(e.target.value);
  };

  const handleCancelledBy = (e) => {
    setcancelledBy(e.target.value);
  };

  const handleSuspendedBy = (e) => {
    setsuspendedBy(e.target.value);
  };

  const handleEmployeeName = (e) => {
    setemployeeName(e.target.value);
  };

  const handleRepaidAmount = (e) => {
    setrepaidAmount(e.target.value);
  };

  const handleSuspendedyn = (e) => {
    setSuspendedyn(e.target.value);
  };

  const handleInterestAmount = (e) => {
    setInterestAmount(e.target.value);
  };

  const handleRemainingAmount = (e) => {
    setRemainingAmount(e.target.value);
  };

  const handleBankAccountName = (e) => {
    setbankAccountName(e.target.value);
  };

  const handleEmployeeCategory = (e) => {
    setEmployeeCategory(e.target.value);
  };

  const handleMonthlyRepayment = (e) => {
    setmonthlyRepayment(e.target.value);
  };

  const handlePreferredBankCode = (e) => {
    setpreferredBankCode(e.target.value);
  };

  const handleInterestDateFormula = (e) => {
    setInterestDateFormula(e.target.value);
  };

  const handleInterestRatePercent = (e) => {
    setinterestRatePercent(e.target.value);
  };

  const handleInterestRepaidAmount = (e) => {
    setInterestRepaidAmount(e.target.value);
  };

  const handleInterestStartingDate = (e) => {
    setInterestStartingDate(e.target.value);
  };

  const handleSuspensionEndingDate = (e) => {
    setSuspensionEndingDate(e.target.value);
  };

  const handleDeductionStartingDate = (e) => {
    setdeductionStartingDate(e.target.value);
  };

  const handleGlobalDimension1Code = (e) => {
    setglobaldimension1code(e.value);
  };

  const handleGlobalDimension2Code = (e) => {
    setglobaldimension2code(e.value);
  };

  const handleInterestRemainingAmount = (e) => {
    setinterestRemainingAmount(e.target.value);
  };

  const closeError = () => {
    setFormErrors([]);
  };

  return (
    <>
      <AuthWrapper
        name="Loan Request"
        bg_name="form-bg-img"
        backBtnName="My Loan Requests"
      >
        {toastMsg ? (
          <IonToast
            isOpen={true}
            message={toastMsg}
            duration={3000}
            onDidDismiss={() => setToastMsg("")}
          />
        ) : null}
        {/* <IonLoading
          isOpen={submitting}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {formErrors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {formErrors.map((error, i) => (
                    <li key={i}>{error}</li>
                  ))}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        {loading ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : (
          <IonRow className="ion-justify-content-center">
            {loanApplicationFields ? (
              <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
                <IonRow className="ion-padding form-bg form-border">
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.loan_amount.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonLabel className="form-input">
                        {loanApplicationFields.fields.card.loan_amount.mapping}
                      </IonLabel>
                      <IonItem>
                        <IonInput
                          type="number"
                          onInput={handleloanAmount}
                          value={loanAmount}
                          required
                        ></IonInput>
                      </IonItem>
                      <br />
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.number_of_payments.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <IonLabel className="form-input">
                        {
                          loanApplicationFields.fields.card.number_of_payments
                            .mapping
                        }
                      </IonLabel>
                      <IonItem>
                        <IonInput
                          type="number"
                          onInput={handleNumberofPayment}
                          value={numberofPayment}
                          required
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}

                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.preferred_pmt_method.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          loanApplicationFields.fields.card.preferred_pmt_method
                            .mapping
                        }
                      </IonLabel>

                      {preferredPmtMethod ? (
                        <Select
                          onChange={handlePaymentMethod}
                          options={preferredPmtMethodSelectOptions}
                        />
                      ) : null}
                      {/* <IonSelect
                        className="request_select_placeholder"
                        placeholder="Payment Method"
                        okText="Okay"
                        cancelText="Dismiss"
                        onIonChange={handlePaymentMethod}
                      >
                        {preferredPmtMethod
                          ? Object.keys(preferredPmtMethod.lookup).map(
                              (data, id) => {
                                return (
                                  <IonSelectOption key={id} value={data}>
                                    {preferredPmtMethod.lookup[data]}
                                  </IonSelectOption>
                                );
                              }
                            )
                          : null}
                      </IonSelect>
                    </IonItem> */}
                    </IonCol>
                  ) : null}
                </IonRow>
                <br />
                <br />
                <br />
                <IonRow className="ion-padding form-bg form-img">
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.loan_e_d.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {loanApplicationFields.fields.card.loan_e_d.mapping}
                        </p>

                        {/* <IonItem className="form-border-bottom">
                        <IonSelect
                          className="request_select_placeholder"
                          placeholder="Select Loan ED"
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={handleloanEd}
                        > */}
                        {loanEdlist ? (
                          <Select
                            onChange={handleloanEd}
                            options={loanEdlistSelectOptions}
                          />
                        ) : null}
                        {/* </IonSelect>
                      </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}

                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.open_y_n.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {loanApplicationFields.fields.card.open_y_n.mapping}
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleOpenyn}
                            value={openyn}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.guarantor.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {loanApplicationFields.fields.card.guarantor.mapping}
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleGuarantor}
                            value={guarantor}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.description.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.description
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleDescription}
                            value={description}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.employee_no.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.employee_no
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleEmployeeNo}
                            value={employeeNo}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.grade_level.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.grade_level
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleGradeLevel}
                            value={gradeLevel}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.bank_account.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.bank_account
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handlebankAccount}
                            value={bankAccount}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.cancelled_by.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.cancelled_by
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleCancelledBy}
                            value={cancelledBy}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.suspended_by.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.suspended_by
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleSuspendedBy}
                            value={suspendedBy}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.employee_name.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.employee_name
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleEmployeeName}
                            value={employeeName}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.repaid_amount.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.repaid_amount
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleRepaidAmount}
                            value={repaidAmount}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.suspended_y_n.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.suspended_y_n
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleSuspendedyn}
                            value={suspendedyn}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_amount.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.interest_amount
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleInterestAmount}
                            value={interestAmount}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.remaining_amount.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.remaining_amount
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleRemainingAmount}
                            value={remainingAmount}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.bank_account_name.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.bank_account_name
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleBankAccountName}
                            value={bankAccountName}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.employee_category.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.employee_category
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleEmployeeCategory}
                            value={employeeCategory}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.monthly_repayment.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.monthly_repayment
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleMonthlyRepayment}
                            value={monthlyRepayment}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}

                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.preferred_bank_code.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .preferred_bank_code.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handlePreferredBankCode}
                            value={preferredBankCode}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_date_formula.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_date_formula.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleInterestDateFormula}
                            value={interestDateFormula}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_rate_percent.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_rate_percent.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleInterestRatePercent}
                            value={interestRatePercent}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_repaid_amount.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_repaid_amount.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleInterestRepaidAmount}
                            value={interestRepaidAmount}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_starting_date.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_starting_date.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleInterestStartingDate}
                            value={interestStartingDate}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.suspension_ending_date.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .suspension_ending_date.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleSuspensionEndingDate}
                            value={suspensionEndingDate}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.deduction_starting_date.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .deduction_starting_date.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleDeductionStartingDate}
                            value={deductionStartingDate}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.global_dimension_1_code.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .global_dimension_1_code.mapping
                          }
                        </p>
                        {/* <IonItem className="form-border-bottom"> */}
                        {globaldimension1code ? (
                          <Select
                            onChange={handleGlobalDimension1Code}
                            options={globalDimensionCodeSelectOptions}
                          />
                        ) : null}
                        {/* <IonSelect
                          okText="Okay"
                          className="request_select_placeholder"
                          placeholder="Global Dimension 1 Code"
                          cancelText="Dismiss"
                          onIonChange={handleGlobalDimension1Code}
                        >
                          {globaldimension1code
                            ? globaldimension1code.lookup.map((data, id) => {
                                return (
                                  <IonSelectOption key={id} value={data.code}>
                                    {data.name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect> */}
                        {/* </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.global_dimension_2_code.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .global_dimension_2_code.mapping
                          }
                        </p>

                        {globaldimension2code ? (
                          <Select
                            onChange={handleGlobalDimension2Code}
                            options={globalDimension2CodeSelectOptions}
                          />
                        ) : null}
                        {/* <IonItem className="form-border-bottom">
                        <IonSelect
                          className="request_select_placeholder"
                          placeholder="Global Dimension 2 Code"
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={handleGlobalDimension2Code}
                        >
                          {globaldimension2code
                            ? globaldimension2code.lookup.map((data, id) => {
                                return (
                                  <IonSelectOption key={id} value={data.code}>
                                    {data.name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect>
                      </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_remaining_amount.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_remaining_amount.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onInput={handleInterestRemainingAmount}
                            value={interestRemainingAmount}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}

                  <IonCol sizeXs="12" sizeMd="12">
                    <br />
                    <br />
                    <div className="ion-float-right">
                      <IonButton
                        className="custom-btn"
                        size="large"
                        onClick={submitForm}
                        disabled={submitting}
                      >
                        Submit
                        <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                          {submitting ? (
                            <IonSpinner
                              name="crescent"
                              color="light"
                              slot="end"
                            />
                          ) : (
                            <IonIcon name="arrow-dropright" />
                          )}
                        </span>
                      </IonButton>
                    </div>
                  </IonCol>
                </IonRow>
              </IonCol>
            ) : errors ? (
              <IonCol size="4" offset="4" className="ion-text-center">
                {errors.map((error) => (
                  <div key={error}>{error}</div>
                ))}
              </IonCol>
            ) : null}
          </IonRow>
        )}
      </AuthWrapper>
    </>
  );
}

export default LoanRequestForm;
