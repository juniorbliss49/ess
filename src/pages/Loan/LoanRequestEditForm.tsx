import React, { useState, useEffect } from "react";
import {
  IonRow,
  IonCol,
  IonButton,
  IonIcon,
  IonItem,
  IonLabel,
  IonInput,
  IonSelectOption,
  IonSelect,
  IonLoading,
  IonSpinner,
} from "@ionic/react";
import AuthWrapper from "../Layout/AuthWrapper";
import { useLoanApplication } from "../../context/LoanApplicationContext";
import { useAuth } from "../../context/AuthContext";
import { updateLoanApplication } from "../../utils/apiClient";
import { Redirect } from "react-router";
import Select from "react-select";

function LoanRequestEditForm({ history, match }) {
  const {
    errors,
    loading,
    prepLoanApplicationFieldsForUpdate,
    preferredPmtMethod,
    loanApplicationFields,
    loanEdlist,
    globaldimension1code,
    globaldimension2code,
    loanEdlistSelectOptions,
    globalDimensionCodeSelectOptions,
    preferredPmtMethodSelectOptions,
    globalDimension2CodeSelectOptions,
    preferredmethodOption,
    loanEdOption,
    globaldimension1codeOption,
    globaldimension2codeOption,
  } = useLoanApplication();

  const { token, logOutUser } = useAuth();

  const [submitting, setSubmitting] = useState(false);
  const [formErrors, setFormErrors] = useState([]);
  const [requestWasUpdated, setRequestWasUpdated] = useState(false);
  const [loanRequest, setLoanRequest] = useState(null);

  useEffect(() => {
    prepLoanApplicationFieldsForUpdate(match.params.reqId, token).then(
      (data) => {
        setLoanRequest(data.loan_application);
      }
    );
  }, []);

  const handleFormUpdate = (field, value) => {
    setLoanRequest({ ...loanRequest, [field]: value });
  };
  const handleFormUpdateDropDown = (field, value) => {
    setLoanRequest({ ...loanRequest, [field]: value });
  };

  const submitForm = async () => {
    const { id, ...formData } = loanRequest;

    try {
      setSubmitting(true);

      let { error, message } = await updateLoanApplication(id, formData, token);

      if (error) {
        throw Error(message);
      }

      setSubmitting(false);

      // setRequestWasUpdated(true);
      history.replace("/loan-requests", { message });
    } catch (error) {
      setSubmitting(false);

      if (error && error.status == 401) {
        logOutUser();
        return;
      }

      if (!error.response) {
        setFormErrors(["Network Error, Check Your Internet Connection"]);
        return;
      }

      const {
        response: { body },
      } = error;

      body.message && setFormErrors([body.message]);

      body.errors && setFormErrors(Object.values(body.errors).flat());
    }
  };

  if (requestWasUpdated) {
    return <Redirect to="/loan-request-history" />;
  }

  const closeError = () => {
    setFormErrors([]);
  };

  return (
    <>
      <AuthWrapper
        name="Loan Request"
        bg_name="form-bg-img"
        backBtnName="My Loan Requests"
      >
        {/* <IonLoading
          isOpen={loading || submitting}
          onDidDismiss={() => null}
          message="Please wait..."
        /> */}

        {formErrors.length ? (
          <IonRow>
            <IonCol
              sizeMd="10"
              offsetMd="1"
              sizeSm="12"
              style={{ backgroundColor: "#d05b5b" }}
            >
              <IonRow>
                <IonCol size="12">
                  <IonIcon
                    name="close"
                    className="ion-float-right closeBtn"
                    onClick={closeError}
                  />
                </IonCol>
              </IonRow>
              <IonRow>
                <ul style={{ listStyleType: "none", color: "#fff" }}>
                  {formErrors.map((error, i) => (
                    <li key={i}>{error}</li>
                  ))}
                </ul>
              </IonRow>
            </IonCol>
            <br /> <br />
          </IonRow>
        ) : null}

        {loading ? (
          <IonCol
            sizeMd="8"
            sizeSm="12"
            sizeXs="12"
            style={{ margin: "auto", textAlign: "center" }}
          >
            <h4>Please Wait...</h4>
          </IonCol>
        ) : (
          <IonRow className="ion-justify-content-center">
            {loanApplicationFields ? (
              <IonCol sizeMd="10" sizeXs="11" sizeSm="10">
                <IonRow className="ion-padding form-bg form-border">
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.loan_amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {loanApplicationFields.fields.card.loan_amount.mapping}
                      </IonLabel>
                      <IonItem>
                        <IonInput
                          type="number"
                          onIonChange={(e) =>
                            handleFormUpdate("loan_amount", e.detail.value)
                          }
                          value={loanRequest ? loanRequest.loan_amount : null}
                          required
                        ></IonInput>
                      </IonItem>
                      <br />
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.number_of_payments.enabled.pages.create ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          loanApplicationFields.fields.card.number_of_payments
                            .mapping
                        }
                      </IonLabel>
                      <IonItem>
                        <IonInput
                          type="number"
                          onIonChange={(e) =>
                            handleFormUpdate(
                              "number_of_payments",
                              e.detail.value
                            )
                          }
                          value={
                            loanRequest ? loanRequest.number_of_payments : null
                          }
                          required
                        ></IonInput>
                      </IonItem>
                    </IonCol>
                  ) : null}

                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.preferred_pmt_method.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      {/* <IonItem> */}
                      <IonLabel className="form-input">
                        {
                          loanApplicationFields.fields.card.preferred_pmt_method
                            .mapping
                        }
                      </IonLabel>
                      {preferredPmtMethod && loanRequest ? (
                        <Select
                          onChange={(e) =>
                            handleFormUpdateDropDown(
                              "preferred_pmt_method",
                              e.value
                            )
                          }
                          options={preferredPmtMethodSelectOptions}
                          value={preferredPmtMethodSelectOptions.filter(
                            (item) =>
                              item.value == loanRequest.preferred_pmt_method
                          )}
                        />
                      ) : null}
                      {/* <IonSelect
                        className="request_select_placeholder"
                        placeholder="Payment Method"
                        okText="Okay"
                        cancelText="Dismiss"
                        onIonChange={e =>
                          handleFormUpdate(
                            "preferred_pmt_method",
                            e.detail.value
                          )
                        }
                        value={
                          loanRequest ? loanRequest.preferred_pmt_method : null
                        }
                      >
                        {preferredPmtMethod
                          ? Object.keys(preferredPmtMethod.lookup).map(
                              (data, id) => {
                                return (
                                  <IonSelectOption key={id} value={data}>
                                    {preferredPmtMethod.lookup[data]}
                                  </IonSelectOption>
                                );
                              }
                            )
                          : null}
                      </IonSelect> */}
                      {/* </IonItem> */}
                    </IonCol>
                  ) : null}
                </IonRow>
                <br />
                <br />
                <br />
                <IonRow className="ion-padding form-bg form-img">
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.loan_e_d.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {loanApplicationFields.fields.card.loan_e_d.mapping}
                        </p>
                        {/* <IonItem className="form-border-bottom"> */}
                        {preferredPmtMethod && loanRequest ? (
                          <Select
                            onChange={(e) =>
                              handleFormUpdateDropDown("loan_e_d", e.value)
                            }
                            options={loanEdlistSelectOptions}
                            value={loanEdlistSelectOptions.filter(
                              (item) => item.value == loanRequest.loan_e_d
                            )}
                          />
                        ) : null}
                        {/* <IonSelect
                          className="request_select_placeholder"
                          placeholder="Select Loan ED"
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={e =>
                            handleFormUpdate("loan_e_d", e.detail.value)
                          }
                          value={loanRequest ? loanRequest.loan_e_d : null}
                        >
                          {loanEdlist
                            ? loanEdlist.lookup.map((data, id) => {
                                return (
                                  <IonSelectOption
                                    key={id}
                                    value={data.ed_code}
                                  >
                                    {data.description}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect> */}
                        {/* </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}

                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.open_y_n.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {loanApplicationFields.fields.card.open_y_n.mapping}
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("open_y_n", e.detail.value)
                            }
                            value={loanRequest ? loanRequest.open_y_n : null}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.guarantor.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {loanApplicationFields.fields.card.guarantor.mapping}
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("guarantor", e.detail.value)
                            }
                            value={loanRequest ? loanRequest.guarantor : null}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.description.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.description
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("description", e.detail.value)
                            }
                            value={loanRequest ? loanRequest.description : null}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.employee_no.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.employee_no
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            onIonChange={(e) =>
                              handleFormUpdate("employee_no", e.detail.value)
                            }
                            value={loanRequest ? loanRequest.employee_no : null}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.grade_level.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.grade_level
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("grade_level", e.detail.value)
                            }
                            value={loanRequest ? loanRequest.grade_level : null}
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.bank_account.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.bank_account
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            onIonChange={(e) =>
                              handleFormUpdate("bank_account", e.detail.value)
                            }
                            value={
                              loanRequest ? loanRequest.bank_account : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.cancelled_by.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.cancelled_by
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "number_of_payments",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest ? loanRequest.cancelled_by : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.suspended_by.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.suspended_by
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("suspended_by", e.detail.value)
                            }
                            value={
                              loanRequest ? loanRequest.suspended_by : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.employee_name.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.employee_name
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            onIonChange={(e) =>
                              handleFormUpdate("employee_name", e.detail.value)
                            }
                            value={
                              loanRequest ? loanRequest.employee_name : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.repaid_amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.repaid_amount
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("repaid_amount", e.detail.value)
                            }
                            value={
                              loanRequest ? loanRequest.repaid_amount : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.suspended_y_n.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.suspended_y_n
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate("suspended_y_n", e.detail.value)
                            }
                            value={
                              loanRequest ? loanRequest.suspended_y_n : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.interest_amount
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "interest_amount",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest ? loanRequest.interest_amount : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.remaining_amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.remaining_amount
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "remaining_amount",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest ? loanRequest.remaining_amount : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.bank_account_name.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.bank_account_name
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "bank_account_name",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest ? loanRequest.bank_account_name : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.employee_category.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.employee_category
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "employee_category",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest ? loanRequest.employee_category : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.monthly_repayment.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card.monthly_repayment
                              .mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "monthly_repayment",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest ? loanRequest.monthly_repayment : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}

                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.preferred_bank_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .preferred_bank_code.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            disabled
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "preferred_bank_code",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.preferred_bank_code
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_date_formula.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_date_formula.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "interest_date_formula",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.interest_date_formula
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_rate_percent.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_rate_percent.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "interest_rate_percent",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.interest_rate_percent
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_repaid_amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_repaid_amount.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "interest_repaid_amount",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.interest_repaid_amount
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_starting_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_starting_date.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "interest_starting_date",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.interest_starting_date
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.suspension_ending_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .suspension_ending_date.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "suspension_ending_date",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.suspension_ending_date
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.deduction_starting_date.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .deduction_starting_date.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "deduction_starting_date",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.deduction_starting_date
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.global_dimension_1_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .global_dimension_1_code.mapping
                          }
                        </p>
                        {preferredPmtMethod && loanRequest ? (
                          <Select
                            onChange={(e) =>
                              handleFormUpdateDropDown(
                                "global_dimension_1_code",
                                e.value
                              )
                            }
                            options={globalDimensionCodeSelectOptions}
                            value={globalDimensionCodeSelectOptions.filter(
                              (item) =>
                                item.value ==
                                loanRequest.global_dimension_1_code
                            )}
                          />
                        ) : null}
                        {/* <IonItem className="form-border-bottom">
                        <IonSelect
                          okText="Okay"
                          className="request_select_placeholder"
                          placeholder="Global Dimension 1 Code"
                          cancelText="Dismiss"
                          onIonChange={e =>
                            handleFormUpdate(
                              "global_dimension_1_code",
                              e.detail.value
                            )
                          }
                          value={
                            loanRequest
                              ? loanRequest.global_dimension_1_code
                              : null
                          }
                        >
                          {globaldimension1code
                            ? globaldimension1code.lookup.map((data, id) => {
                                return (
                                  <IonSelectOption key={id} value={data.code}>
                                    {data.name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect>
                      </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.global_dimension_2_code.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .global_dimension_2_code.mapping
                          }
                        </p>
                        {preferredPmtMethod && loanRequest ? (
                          <Select
                            onChange={(e) =>
                              handleFormUpdateDropDown(
                                "global_dimension_2_code",
                                e.value
                              )
                            }
                            options={globalDimension2CodeSelectOptions}
                            value={globalDimension2CodeSelectOptions.filter(
                              (item) =>
                                item.value ==
                                loanRequest.global_dimension_2_code
                            )}
                          />
                        ) : null}
                        {/* <IonItem className="form-border-bottom">
                        <IonSelect
                          className="request_select_placeholder"
                          placeholder="Global Dimension 2 Code"
                          okText="Okay"
                          cancelText="Dismiss"
                          onIonChange={e =>
                            handleFormUpdate(
                              "global_dimension_2_code",
                              e.detail.value
                            )
                          }
                          value={
                            loanRequest
                              ? loanRequest.global_dimension_2_code
                              : null
                          }
                        >
                          {globaldimension2code
                            ? globaldimension2code.lookup.map((data, id) => {
                                return (
                                  <IonSelectOption key={id} value={data.code}>
                                    {data.name}
                                  </IonSelectOption>
                                );
                              })
                            : null}
                        </IonSelect>
                      </IonItem> */}
                      </div>
                    </IonCol>
                  ) : null}
                  {!loanApplicationFields ? null : loanApplicationFields.fields
                      .card.interest_remaining_amount.enabled.pages.update ? (
                    <IonCol sizeXs="12" sizeMd="4">
                      <div>
                        <p className="form-p">
                          {
                            loanApplicationFields.fields.card
                              .interest_remaining_amount.mapping
                          }
                        </p>
                        <IonItem className="form-border-bottom">
                          <IonInput
                            onIonChange={(e) =>
                              handleFormUpdate(
                                "interest_remaining_amount",
                                e.detail.value
                              )
                            }
                            value={
                              loanRequest
                                ? loanRequest.interest_remaining_amount
                                : null
                            }
                          ></IonInput>
                        </IonItem>
                      </div>
                    </IonCol>
                  ) : null}

                  <IonCol sizeXs="12" sizeMd="12">
                    <br />
                    <br />
                    <div className="ion-float-right">
                      <IonButton
                        className="custom-btn"
                        size="large"
                        onClick={submitForm}
                        disabled={submitting}
                      >
                        Update
                        <span style={{ marginTop: "-3px", marginLeft: "3px" }}>
                          {submitting ? (
                            <IonSpinner
                              name="crescent"
                              color="light"
                              slot="end"
                            />
                          ) : (
                            <IonIcon name="arrow-dropright" />
                          )}
                        </span>
                      </IonButton>
                    </div>
                  </IonCol>
                </IonRow>
              </IonCol>
            ) : errors ? (
              <IonCol size="4" offset="4" className="ion-text-center">
                {errors.map((error, i) => (
                  <div key={i}>{error}</div>
                ))}
              </IonCol>
            ) : null}
          </IonRow>
        )}
      </AuthWrapper>
    </>
  );
}

export default LoanRequestEditForm;
