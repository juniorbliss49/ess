import React, { Component } from "react";
import {
  IonRow,
  IonCol,
  IonImg,
  IonIcon
} from "@ionic/react";
import "../../styles/coming-soon.css";
import AuthWrapper from "./AuthWrapper";

const ComingSoon = props => {
  return (
    <>

        <AuthWrapper name="Coming Soon" bg_name="comingsoon-bg" backBtnName="">
          <IonRow className="img-row">
            <IonCol size="12">
              <IonRow>
                <IonCol sizeXs="10" offsetXs="1">
                  <IonImg
                    src="assets/images/logo.png"
                    style={{ width: "100px", margin: "0 auto" }}
                  ></IonImg>
                </IonCol>
              </IonRow>
              <br />
              <IonRow>
                <IonCol
                  sizeSm="4"
                  offsetSm="4"
                  sizeXs="10"
                  offsetXs="1"
                  sizeLg="4"
                  offsetLg="4"
                >
                  <IonImg
                    src="assets/images/coming_soon_illustration.png"
                    style={{ width: "300px", margin: "0 auto" }}
                  />
                </IonCol>
              </IonRow>
              <br />
              <IonRow>
                <IonCol sizeXs="10" offsetXs="1" sizeLg="6" offsetLg="3" sizeMd="4" offsetMd="4">
                  <div className="fielset">
                    <p className="fieldset-data">COMING SOON</p>
                  </div>
                </IonCol>
              </IonRow>
              <br />
              <IonRow>
                <IonCol
                  sizeXs="10"
                  offsetXs="1"
                  style={{ textAlign: "center" }}
                >
                  <IonIcon
                    onClick={() => props.history.goBack()}
                    name="arrow-back"
                    class="icon-style"
                    style={{
                      margin: "0 auto",
                      color: "#7785a7",
                      background: "#fff",
                      borderRadius: "50px",
                      padding: "10px"
                    }}
                  />
                </IonCol>
              </IonRow>
            </IonCol>
          </IonRow>
        </AuthWrapper>
    </>
  );
};

export default ComingSoon;
