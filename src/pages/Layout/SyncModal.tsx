import React from "react";
import { IonContent, IonRow, IonCol, IonIcon, IonButton } from "@ionic/react";
import { useUtility, } from "../../context/UtilityContext";

function SyncModal(props) {
    const { syncmodal, changeSyncModal } = useUtility();

    return(
        <>
            <IonContent>
                <IonRow>
                    <IonCol size="12" className="ion-text-center">
                        <IonIcon
                        name="sync"
                        style={{fontSize:"100px"}}
                        />
                        <h4>Are you sure you want to sync {props.name}?</h4>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol size="6" offset="3" className="ion-text-center">
                        <IonButton color="medium" onClick={() => changeSyncModal(false)}>Cancel</IonButton>
                        <IonButton color="danger" >Sync</IonButton>
                    </IonCol>
                </IonRow>
            </IonContent>
        </>
    )
}

export default SyncModal;