import React from "react";
import { IonRow, IonCol, IonImg } from "@ionic/react";
import { withRouter } from "react-router-dom";
import "../../styles/side-sub-menu-icon.css";

function SideSubMenuIcon({ history }) {
  const Menus = [
    {
      link: "/",
      name: "Home",
      img: "assets/images/home.svg",
    },
    {
      link: "/profile",
      name: "Profile",
      img: "assets/images/profile.svg",
    },
    {
      link: "/payslip",
      name: "Payslip",
      img: "assets/images/surface1.svg",
    },
    {
      link: "/leave-management",
      name: "Leave Management",
      img: "assets/images/calendar_10.svg",
    },
    {
      link: "/loan-requests",
      name: "Loan Request",
      img: "assets/images/loan.svg",
    },
    {
      link: "/payment-request-history",
      name: "Payment Request",
      img: "assets/images/receipt_2.svg",
    },
    {
      link: "/query-management",
      name: "Query Management",
      img: "assets/images/query_1.svg",
    },
  ];

  return (
    <>
      <IonRow className="desktopmenu-2 sidesubmenu-list-icon ion-hide-md-down">
        {/* <div > */}
        {/* </div> */}
        <IonCol size="12">
          <div className="sidebar-logo-div">
            <IonImg
              className="sidebar-logo-icon"
              src="assets/images/logo.png"
            />
          </div>
          <br />
          <br />
          {Menus.map((menu, i) => {
            return (
              <div
                key={i}
                className="sidesubmenu-text-icon"
                onClick={() => history.push(menu.link)}
              >
                <IonImg src={menu.img} class="sub-menu-img" />
              </div>
            );
          })}
        </IonCol>
      </IonRow>
    </>
  );
}

export default withRouter(SideSubMenuIcon);
