import React from "react";
import { IonRow, IonCol, IonImg } from "@ionic/react";
import { withRouter, Link, NavLink } from "react-router-dom";
import "../../styles/side-sub-menu.css";
import { useTenant } from "../../context/TenantContext";

function SideSubMenu({ history }) {
  const Menus = [
    {
      link: "/",
      name: "Home",
      img: "assets/images/home.svg",
    },
    {
      link: "/profile",
      name: "Profile",
      img: "assets/images/profile.svg",
    },
    {
      link: "/payslip",
      name: "Payslip",
      img: "assets/images/surface1.svg",
    },
    {
      link: "/leave-management",
      name: "Leave Management",
      img: "assets/images/calendar_10.svg",
    },
    {
      link: "/loan-requests",
      name: "Loan Request",
      img: "assets/images/loan.svg",
    },
    {
      link: "/payment-request-history",
      name: "Payment Request",
      img: "assets/images/receipt_2.svg",
    },
    {
      link: "/query-management",
      name: "Query Management",
      img: "assets/images/query_1.svg",
    },
  ];

  const { tenant } = useTenant();

  return (
    <>
      <IonRow className="desktopmenu-2 sidesubmenu-list ion-hide-md-down ">
        {/* <div > */}
        {/* </div> */}
        <div className="fixed-nav sidebar-menu-bg">
          <div className="sidebar-logo-div">
            <IonImg
              className="sidebar-logo"
              src={
                tenant.companyLogo
                  ? tenant.companyLogo
                  : "assets/images/logo.png"
              }
            />
          </div>
          <br />
          <br />
          {Menus.map((menu, key) => {
            return (
              <NavLink
                key={key}
                activeClassName="sidemenu-isactive"
                exact={true}
                to={menu.link}
                className="sidesubmenu-link"
              >
                <div className="sidesubmenu-text">
                  <IonImg src={menu.img} class="sub-menu-img" />
                  {menu.name}
                </div>
              </NavLink>
            );
          })}
        </div>
      </IonRow>
    </>
  );
}

export default withRouter(SideSubMenu);
