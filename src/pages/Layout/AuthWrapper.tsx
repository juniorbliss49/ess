import React from "react";
import { IonContent, IonGrid, IonRow, IonCol } from "@ionic/react";
import MenuHeader from "../../components/MenuHeader";
import SidebarMenu from "./SidebarMenu";
import { useAuth } from "../../context/AuthContext";
import { useTenant } from "../../context/TenantContext";
import Notification from "./Notification";
import SideSubMenu from "./SideSubMenu";
import SideSubMenuIcon from "./SideSubMenuIcon";
import "../../styles/auth-wrapper.css";

function AuthWrapper(props) {
  const { toggle, subMenuToggle, sideSubMenuToggle } = useAuth();
  const { tenant } = useTenant();
  return (
    <>
      <MenuHeader
        appPage={{ name: props.name }}
        backBtn={{ name: props.backBtnName }}
      />
      <IonContent class={props.bg_name}>
        {/* <IonGrid> */}
        <br />
        <br />
        {toggle ? <SidebarMenu /> : ""}
        {subMenuToggle ? <Notification /> : ""}

        {sideSubMenuToggle ? (
          <IonRow>
            <IonCol
              sizeLg="2"
              sizeXl="2"
              className="custom-column desktopmenu-2"
            >
              {/* <IonRow style={{height: "100%"}}>
                                            <IonCol sizeLg="9" className="desktopmenu-2" style={{height: "100%"}}> */}
              <SideSubMenu />

              {/* </IonCol>
                                        </IonRow> */}
            </IonCol>
            <IonCol
              sizeXl="10"
              sizeLg="10"
              sizeMd="12"
              sizeSm="12"
              sizeXs="12"
              className="no-padding"
            >
              <IonRow className="custom-comp-row">
                <IonCol size="4" push="8">
                  <div className="ion-float-right custom-comp-name">
                    <p>{tenant.company.company_name}</p>
                  </div>
                </IonCol>
              </IonRow>

              {props.children}
            </IonCol>
          </IonRow>
        ) : (
          <IonRow>
            <IonCol sizeLg="1" sizeXl="1">
              <SideSubMenuIcon />
            </IonCol>
            <IonCol
              sizeXl="11"
              sizeLg="11"
              sizeMd="12"
              sizeSm="12"
              sizeXs="12"
              className="no-padding"
            >
              {props.children}
            </IonCol>
          </IonRow>
        )}

        {/* </IonGrid> */}
      </IonContent>
    </>
  );
}

export default AuthWrapper;
