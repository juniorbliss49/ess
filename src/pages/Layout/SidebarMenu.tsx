import React from "react";
import {
  IonContent,
  IonRow,
  IonGrid,
  IonCol,
  IonIcon,
  IonButton,
} from "@ionic/react";
import "../../styles/side-bar-menu.css";

import { useAuth } from "../../context/AuthContext";

function SidebarMenu() {
  const { logOutUser, changeMenu, toggle } = useAuth();
  return (
    <>
      {/* <IonContent >
                <IonGrid> */}
      <div className="sidebar">
        <IonRow>
          <IonCol>
            <IonButton
              color="light"
              className="icon-btn icon-margin icon-menu-color"
              onClick={changeMenu}
            >
              <IonIcon
                slot="icon-only"
                title="Menu"
                style={{ cursor: "pointer" }}
                name="apps"
                class="icon-style icon-hover icon-menu-color icon-size"
              />
            </IonButton>
          </IonCol>
        </IonRow>
        <br />
        <IonRow className="sidebar-padding">
          <IonCol size="12">
            <h3>Apps</h3>
          </IonCol>
          <IonCol className="menu-title no-padding" size="6">
            <h6>ESS</h6>
          </IonCol>
          {/* <IonCol className="menu-title no-padding" size="6">
            <h6>OUTLET PORTAL</h6>
          </IonCol>
          <IonCol className="menu-title no-padding" size="6">
            <h6>STAFFHUB</h6>
          </IonCol>
          <IonCol className="menu-title no-padding" size="6">
            <h6>VMS</h6>
          </IonCol> */}
        </IonRow>
      </div>

      {/* </IonGrid>
            </IonContent> */}
    </>
  );
}

export default SidebarMenu;
