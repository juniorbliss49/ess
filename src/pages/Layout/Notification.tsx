import React from "react";
import { IonRow, IonCol, IonImg, IonIcon } from "@ionic/react";
import "../../styles/notification.css";
import { useAuth, } from "../../context/AuthContext";

function Notification() {
  const { changeSubMenu, changeToggleSubMenu, subMenuContent } = useAuth();
    return(
        <>
            <div className="notification">
                <IonRow className="ion-padding">
                    <IonCol className="6">{subMenuContent}</IonCol>
                    <IonCol className="6 ion-text-right">
                    <IonIcon
                      // slot="end"
                      onClick={()=>changeToggleSubMenu()}
                      title="Log Out"
                      style={{ cursor: "pointer", color:"#000" }}
                      name="close"
                      class="icon-style  icon-size"
                      
                    />
                    </IonCol>
                </IonRow>
                <IonRow className="top_nav">
                    <IonCol size="12" className="ion-text-center">
                        <IonImg src="assets/images/notifications.svg" className="img-notify" />
                        <h6>You don't have any {subMenuContent}</h6>
                        <p>Please check back later</p>
                    </IonCol>
                    {/* <IonCol size="12">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque qui facilis, corporis praesentium nesciunt libero! Delectus necessitatibus, 
                        repellat eligendi at repellendus quidem corrupti! Amet dolor rem, mollitia aliquam nisi expedita?    
                    </IonCol>
                    <IonCol size="12">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque qui facilis, corporis praesentium nesciunt libero! Delectus necessitatibus, 
                        repellat eligendi at repellendus quidem corrupti! Amet dolor rem, mollitia aliquam nisi expedita? 
                    </IonCol>
                    <IonCol size="12">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque qui facilis, corporis praesentium nesciunt libero! Delectus necessitatibus, 
                        repellat eligendi at repellendus quidem corrupti! Amet dolor rem, mollitia aliquam nisi expedita? 
                    </IonCol> */}
                </IonRow>
            </div>
        </>
    )
}

export default Notification;