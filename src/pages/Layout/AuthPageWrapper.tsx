import React from "react";
import { 
        IonHeader, 
        IonToolbar, 
        IonButtons, 
        IonMenuButton, 
        IonTitle, 
        IonIcon,
        IonContent,
        IonRow,
        IonCol,
        
    } from "@ionic/react";
import { useAuth, } from "../../context/AuthContext"
import SideSubMenu from "./SideSubMenu";
import SideSubMenuIcon from "./SideSubMenuIcon";

function AuthPageWrapper(props) {
    const { logOutUser, sideSubMenuToggle, subMenuToggle } = useAuth();
    return(
        <>
            <div className="ion-hide-lg-up">
                <IonHeader>
                    <IonToolbar class="custom-menu ion-hide-lg-up" color="favorite ">
                        <IonButtons slot="start">
                        <IonMenuButton />
                        </IonButtons>
                        <IonTitle>{props.name}</IonTitle>
                        <IonButtons slot="end">
                        <IonIcon
                            title="Log Out"
                            style={{ cursor: "pointer" }}
                            name="ios-log-out"
                            class="icon-style ion-padding"
                            onClick={logOutUser}
                        />
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
            </div>
            <IonContent class={props.bg_name}>
                <br/>
                <br/>
                    {
                        sideSubMenuToggle?
                        <IonRow>
                            <IonCol sizeLg="2" sizeXl="2">
                                <SideSubMenu />
                            </IonCol>
                            <IonCol sizeXl="10" sizeLg="10" sizeMd="12" sizeSm="12" sizeXs="12">
                            {props.children}
                            </IonCol>
                        </IonRow>

                        :
                        <IonRow>
                            <IonCol sizeLg="1" sizeXl="1">
                                <SideSubMenuIcon />
                            </IonCol>
                            <IonCol sizeXl="11" sizeLg="11" sizeMd="12" sizeSm="12" sizeXs="12">
                            {props.children}
                            </IonCol>
                        </IonRow>
                        }
            </IonContent>
        </>
    )
}

export default AuthPageWrapper;